Operator Resources
==================

Below you can find a graphical overview over all resources the operators provision and their dependencies.

CinderDeployment
------------------

.. graphviz:: cinder-operator-cinderdeployment.dot

GlanceDeployment
------------------

.. graphviz:: glance-operator-glancedeployment.dot

KeystoneDeployment
------------------

.. graphviz:: keystone-operator-keystonedeployment.dot

NeutronDeployment
------------------

.. graphviz:: neutron-operator-neutrondeployment.dot

NovaDeployment
--------------

.. graphviz:: nova-operator-novadeployment.dot