MemcachedService
================

The MemcachedService is used by operators to get multiple memcacheds for their service.
This resource is normally not used by the users directly but we provide here an example for better understanding.

.. literalinclude:: memcachedservice.yaml

Making memcached available from outside the cluster
---------------------------------------------------

There can be reasons why you want to make memcached available also from outside the cluster.
One of the most common ones is if you have a single, global keystone-cluster and multiple, region-based nova/neutron/etc. clusters
(thereby basically implementing the openstack region concept).

In this case the region-based clusters need to access the keystone memcached over k8s cluster boundries.
In order to do this you need to create individual services for each member of the memcached cluster.
These services must then be available from wherever you want to access memcached from (e.g. by used a Loadbalancer type).

Such three services could look as follows for the first instance of the cluster from above.

.. literalinclude:: memcachedservice_external.yaml
