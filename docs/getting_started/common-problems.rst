.. _common-problems:

Common Problems
===============

How to find the kubernetes object causing a dependency
--------------------------------------------------------------

When it says that an object is waiting for an other object, it might be difficult to understand which one it is refering to.

If in the status of resource ``X`` it says ``Waiting for Y``, you can find which resource ``Y`` is meant by performing the following steps:

1. Find the python class name implementing resource ``Y``:

    .. code-block:: bash

        grep -Rni 'KIND: "X"' yaook/op -A 1000 | grep "Y = "

#. Now find the class definition of that class ``CLASS_NAME``:

    .. code-block:: bash

        grep -R "class CLASS_NAME" .

#. Examine the python code of this class definition. It will mention a kubernetes resource and this is the kind of kubernetes resource ``X`` is waiting for.

Network interface not found
---------------------------

If the initcontainer ``neutron-ovs-bridge-setup`` in the ``neutron-l2`` statefulset pod fails with a ``Cannot find device "eth1"`` error (or similar) in its log, you probably are listing a device in the l2 agent's bridge configuration, which does not exist on the the target system. If this is your intention, you can ignore the error. If not, go ahead and change it in ``docs/examples/neutron.yaml`` and re-apply the specification:

.. code-block:: bash

    kubectl -n $YAOOK_OP_NAMESPACE apply -f docs/examples/neutron.yaml

Unable to establish connection to neutron
-----------------------------------------

If you get an error similar to ``Unable to establish connection to https://neutron.yaook.cloud:32443/v2.0/agents``, your ``glance``, ``cinder``, ``gnocchi`` ceph client secrets are probably not present in the ``$YAOOOK_OP_NAMESPACE`` namespace. Did you forget to :ref:`copy them<copy_cephclient_secrets>`?


.. note::

    The ceph client secrets might disappear without error at creation time, for example if there is a problem with the ``metadata.ownerReferences`` path.

Services or agents are not scheduled
------------------------------------

The way to schedule (network) agents and (compute) services on nodes depends
on many facts.

First have a look at :ref:`scheduling<concepts.scheduling>`

The nodes that should run the agents needing:
- :ref:`SchedulingKeys<concepts.scheduling>`
- l2 label set by l2 operator :ref:`concepts.scheduling.neutron-l2-agent`
- for bgp: bgp section at neutron deployment
