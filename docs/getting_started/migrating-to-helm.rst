Migrating to use Helm Charts
############################

If you have previously deployed Yaook by applying plain YAML files, a few
manual steps are necessary to migrate to a helm-based setup.

1. ``git checkout a17eec34ca9cb9b210db58e88109b6d38022ae54`` to go back to the last state where all CRDs and operators were built as plain YAML files.
2. Run ``make yamls roles``.
3. Run ``make k8s_clean_roles k8s_clean_operators`` to uninstall the operators and their roles. Alternatively, you can ``kubectl delete -f`` all generated YAML files from ``deploy/`` **except** the CRDs.

    .. note::

        After uninstalling the operators, your resources will not be updated anymore until you completed the remaining steps of this guide. No data- or control-plane disruptions on the OpenStack level are expected.

    .. warning::

        Ensure that all your resources are converged correctly and that no compute nodes are currently running eviction jobs, as those will be cancelled forcefully.

4. Return to your previous checkout using ``git checkout devel``.
5. Run:

    .. note::

        If you want to install your CRD helm chart in a different namespace than the default namespace, you have to replace ``default`` in the command line below.

    .. code-block:: bash

        kubectl get crd -o jsonpath='{ range .items[*] }{ .metadata.name }{ "\n" }{ end }' | grep yaook.cloud | xargs -I{} -- kubectl annotate crd {} meta.helm.sh/release-name=yaook-crds meta.helm.sh/release-namespace=default
        kubectl get crd -o jsonpath='{ range .items[*] }{ .metadata.name }{ "\n" }{ end }' | grep yaook.cloud | xargs -I{} -- kubectl label crd {} app.kubernetes.io/managed-by=Helm

    To allow Helm to take ownership of the CRDs.

6. Install the CRD Helm chart:

    .. note::

        If you want to install your CRD helm chart in a different namespace than the default namespace, you have to replace ``default`` in the command line below.

    .. code-block:: bash

        python3 -m yaook.helm_builder -vvv crds
        helm upgrade --install -n default yaook-crds yaook/helm_builder/Charts/crds

7. Now your cluster is ready to re-install the operators you want to install via Helm. Follow the instructions from the ``dev_setup.rst`` for information on that.
