##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
ARG DESTDIR=/build
ARG ovs_version


FROM golang:1.22@sha256:969349b8121a56d51c74f4c273ab974c15b3a8ae246a5cffc1df7d28b66cf978 AS cue-builder
WORKDIR /build
RUN GOPATH=/build go install cuelang.org/go/cmd/cue@v0.4.3

FROM debian:12.5-slim@sha256:804194b909ef23fb995d9412c9378fb3505fe2427b70f3cc425339e48a828fca AS python
RUN \
	apt-get update && \
	apt-get install python3-minimal python3-pip wget --no-install-recommends -y && \
	apt-get clean -y

FROM python:3.11 AS cue-and-ovs-builder

ARG DESTDIR
ARG ovs_version

RUN \
	apt-get update && \
	apt-get install python3-dev gcc make --no-install-recommends -y
WORKDIR /build
COPY requirements-build.txt /build/
RUN pip3 install -r requirements-build.txt

RUN set -eux ; \
    git clone -b ${ovs_version} https://github.com/openvswitch/ovs.git /ovs --depth 1; \
    cd /ovs; \
    ./boot.sh; \
    ./configure \
        --localstatedir=/var \
        --prefix=/usr \
        --sysconfdir=/etc \
        # We need shared linking for the ovs._json python lib
        # With that lib OVS parses json in C instead of python
        --enable-shared; \
    gmake; \
    make install DESTDIR=${DESTDIR}; \
    cp -r /ovs/python ${DESTDIR}

# cuelang is needed for buildcue.py (make cue-templates)
COPY --from=cue-builder /build/bin/cue /bin/cue
COPY buildcue.py GNUmakefile /build/
RUN make cue-templates

FROM python:3.11
ARG userid=2020
ARG DESTDIR

COPY --from=cue-and-ovs-builder ${DESTDIR}/ /
RUN \
	groupadd -g $userid yaook-operator && \
	useradd -u $userid -g $userid -d /tmp/yaook-operator -m yaook-operator
RUN \
	apt-get update && \
	apt-get install iputils-ping patch --no-install-recommends -y && \
	apt-get clean -y; \
    # A more recent version of the OVS python library is required
    # This is not intended to work nor tested to work with an Neutron OVS backend!
    cd /python/; \
    python3 setup.py build; \
    python3 setup.py install; \
    cd /; rm -rf /python;

COPY yaook /app/yaook
COPY setup.py MANIFEST.in /app/
RUN set -eux; cd /tmp ;\
	wget -O helm.tar.gz https://get.helm.sh/helm-v3.5.0-linux-amd64.tar.gz ;\
	echo '3fff0354d5fba4c73ebd5db59a59db72f8a5bbe1117a0b355b0c2983e98db95b helm.tar.gz' > helm.tar.gz.sha256sum ;\
	sha256sum -c helm.tar.gz.sha256sum ;\
	tar -xf helm.tar.gz ;\
	mv linux-amd64/helm /bin/helm ;\
	rm -f helm.tar.gz helm.tar.gz.sha256sum
COPY --from=cue-builder /build/bin/cue /bin/cue
COPY --from=cue-and-ovs-builder /build/yaook/op /app/yaook/op
RUN cd /app && pip install . && rm -rf -- /root/.cache
COPY patches/apiclient.patch /apiclient.patch
RUN cd /usr/local/lib/python3.11/site-packages/kubernetes_asyncio/; patch -p 1 < /apiclient.patch

ENTRYPOINT ["python3", "-m", "yaook.op"]
