#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from datetime import datetime, timedelta
import time
import typing
import environ

import openstack
import kubernetes_asyncio.client
import logging
from yaook.op.daemon import PatchingApiClient
from . import NovaComputeNode
import yaook.statemachine as sm

from yaook.statemachine import context, interfaces


@environ.config(prefix="EVICT_MANAGER")
class EvictManagerConfig:
    namespace = environ.var()
    interval = environ.var(converter=int)
    max_per_hour = environ.var(converter=int)


def _connect(
        connection_info: typing.Mapping) -> openstack.connection.Connection:
    client = openstack.connect(**connection_info)
    client.compute.default_microversion = "2.60"
    return client


class EvictionManager():
    def __init__(self, connection_info: typing.Mapping,
                 k8s_client: kubernetes_asyncio.client.ApiClient):
        self.openstack_client = _connect(connection_info)
        self.k8s_client = k8s_client
        self.previous_down: typing.Set = set()
        config = environ.to_config(EvictManagerConfig)
        self.namespace = config.namespace
        self.interval = config.interval
        self.max_per_hour = config.max_per_hour
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.node_down_log: typing.List[typing.Tuple[datetime, str]] = []

    def get_newly_down_computenodes(self) -> typing.List[str]:
        currently_down = {hypervisor.name
                          for hypervisor
                          in self.openstack_client.compute.hypervisors(
                              state="down")}
        newly_down = currently_down - self.previous_down
        self.previous_down = currently_down
        return list(newly_down)

    async def check_if_managed_by_yaook(self, hostname: str) -> bool:
        computeNodeInterface = interfaces.nova_compute_node_interface(
            self.k8s_client)
        computenodes = await computeNodeInterface.list_(
            namespace=self.namespace,
            label_selector={context.LABEL_INSTANCE: hostname})
        if not len(computenodes) == 1:
            self.logger.info(
                "There is no NovaComputeNode with the name "
                f"{hostname} resource existing in the cluster. "
                "Skipping the node as it is not managed by YAOOK.")
            return False
        else:
            if not computenodes[0].get("status", {}).get("state") == 'Enabled':
                self.logger.info(
                    f"The computenode {hostname} resource exists but it will "
                    "be ignored, as it is not Enabled.")
                return False
            else:
                return True

    async def check_if_evict_job_exists(self, hostname: str) -> bool:
        jobInterface = interfaces.job_interface(self.k8s_client)
        jobs = await jobInterface.list_(
            namespace=self.namespace,
            label_selector={
                context.LABEL_COMPONENT: "api_state",
                context.LABEL_PARENT_NAME: hostname,
                context.LABEL_PARENT_PLURAL: "novacomputenodes"
            }
        )
        return len(jobs) > 0

    async def spawn_eviction_job(self, computenode: str) -> None:
        cr_obj = NovaComputeNode(logger=self.logger)
        intf = cr_obj.get_resource_interface(self.k8s_client)
        obj = await intf.read(self.namespace, computenode)
        ctx = sm.Context(
            parent=obj,
            parent_intf=cr_obj.get_resource_interface(self.k8s_client),
            namespace=self.namespace,
            api_client=PatchingApiClient(self.k8s_client.configuration),
            instance=None,
            instance_data=None,
            logger=self.logger,
            field_manager=f"operator.yaook.cloud:{cr_obj.PLURAL}." +
            f"{cr_obj.API_GROUP}",
        )
        deps = {
                "job_service_account": cr_obj.job_service_account,
                "user_password": cr_obj.user_password,
                "keystone_operator_api": cr_obj.keystone_operator_api,
                "keystone_internal_api": cr_obj.keystone_internal_api,
                }
        await intf.delete(
            namespace=self.namespace,
            name=computenode
        )
        # need to patch context, otherwise no evict job will be spawned
        ctx.parent["status"]["eviction"] = {
            "reason": "Deleting"
        }
        await cr_obj.api_state._background_job.reconcile(
            ctx, force=True, dependencies=deps
            )

    async def process_node(self, computenode):
        if await self.check_if_evict_job_exists(computenode):
            self.logger.info(
                "Evict Job already exists for computenode "
                f"{computenode}, no need to create it.")
            return
        self.logger.info(
            f"Will trigger eviction of computenode {computenode}")
        await self.spawn_eviction_job(computenode)

    def update_node_down_log(self, newly_down):
        self.node_down_log = [
            log_entry
            for log_entry in self.node_down_log
            if log_entry[0] > datetime.now() - timedelta(hours=1)
        ]
        self.node_down_log.extend([(datetime.now(), node)
                                  for node in newly_down])

    async def eviction_manager_iteration_loop(self):
        newly_down = [node
                      for node in self.get_newly_down_computenodes()
                      if await self.check_if_managed_by_yaook(node)
                      ]
        self.logger.info(
            f"Currently there are {len(self.previous_down)} unavailable "
            f"compute nodes. And {len(newly_down)} nodes are newly down."
        )
        self.update_node_down_log(newly_down)
        if len(self.node_down_log) > self.max_per_hour:
            self.logger.warning(
                f"In the last hour {len(self.node_down_log)} nodes went down. "
                f"This is more than the allowed {self.max_per_hour} nodes "
                "down per hour. This indicates a general communication "
                "problem and not a problem with a single hypervisor. "
                "No evictions will be triggered.")
            return
        for computenode in newly_down:
            await self.process_node(computenode)

    async def iterate(self):
        self.logger.info("Starting Eviction Manager")
        self.previous_down = set(self.get_newly_down_computenodes())
        while True:
            await self.eviction_manager_iteration_loop()
            self.logger.info(f"Next check in {self.interval}s.")
            time.sleep(self.interval)
