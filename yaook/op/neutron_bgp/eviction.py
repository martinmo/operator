#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from __future__ import annotations

import dataclasses
import typing

import openstack

import yaook.statemachine.commoneviction as eviction
import yaook.statemachine.resources.openstack as os_resource


@dataclasses.dataclass(frozen=True)
class EvictionStatus():
    disabled_agents: int
    unhandleable_agent: bool

    def as_json_object(self) -> typing.Mapping[str, typing.Any]:
        return {
            "disabled_agents": self.disabled_agents,
            "unhandleable_agent": self.unhandleable_agent,
        }


class Evictor(eviction.Evictor[EvictionStatus]):
    def __init__(
            self,
            **kwargs: typing.Any,
            ):
        super().__init__(**kwargs)

    def _connect(self) -> openstack.connection.Connection:
        client = openstack.connect(**self._connection_info)
        return client

    def _os_iterate(self) -> EvictionStatus:
        with self._connect() as client:
            is_disabled: bool
            is_down: bool
            try:
                agent = os_resource.get_network_agent(
                    client.network,
                    host=self._node_name,
                    binary="neutron-bgp-dragent",
                )
            except LookupError:
                self._logger.error(
                    "bgp agent %s has vanished ... we can't do anything, as "
                    "we can't get the list of networks on this agent.",
                    self._node_name,
                )
                # This is a very unpleasant situation.
                # We set unhandleable_agent to True.
                return EvictionStatus(
                    disabled_agents=0,
                    unhandleable_agent=True,
                )
            else:
                # We only need to know if the agent is disabled, to not
                # disable it again.
                is_disabled = not agent.is_admin_state_up
                is_down = not agent.is_alive
                self._logger.info(
                    "bgp agent %s: is_down=%s, is_disabled=%s",
                    agent.id, is_down, is_disabled,
                )

            if not is_disabled:
                os_resource.disable_network_agent(
                    client.network,
                    agent,
                )

            # Since the disabling of the bgp agent covers the whole eviction,
            # it is sufficient to just return the disabled_agent.
            return EvictionStatus(
                disabled_agents=1,
                unhandleable_agent=False,
            )

    async def is_migration_done(
            self,
            status: EvictionStatus,
            ) -> bool:
        # There is no good way to monitor how the migration is going since we
        # trigger the migration by just disabling the bgp agent.
        # So if the agent is not unhandleable we procceed.
        return not status.unhandleable_agent

    def start_eviction_log(
            self,
            poll_interval: int,
            ) -> str:
        return (f"initiating eviction of node {self._node_name} with reason "
                f"{self._reason}.\npoll interval is {poll_interval}s")
