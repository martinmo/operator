##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ "cinder-volume-{}".format(instance) }}
spec:
  replicas: {{ crd_spec.backends[instance].volume.replicas }}
  strategy:
    type: Recreate
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
      annotations:
        config-timestamp: {{ dependencies['volume_config'].last_update_timestamp() }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: {{ crd_spec.backends[instance].volume.scheduleRuleWhenUnsatisfiable }}
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "cinder-volume"
          image:  {{ versioned_dependencies['cinder_docker_image'] }}
          imagePullPolicy: IfNotPresent
          args: ["cinder-volume"]
          securityContext:
            {% if "netapp" in crd_spec.backends[instance] %}capabilities:
              add:
                - "SYS_ADMIN"
                - "DAC_OVERRIDE"
                - "DAC_READ_SEARCH"
                - "FOWNER"
                - "NET_ADMIN"
                - "CHOWN"
            {% endif %}
          volumeMounts:
            - name: cinder-config-volume
              mountPath: /etc/cinder/
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
            - name: ca-certs
              mountPath: /etc/ssl/certs
            - name: varlibcinder
              mountPath: /var/lib/cinder
            {% if "rbd" in crd_spec.backends[instance] %}
            - name: cinder-ceph-conf
              mountPath: /etc/ceph/
            {% endif %}
            {% if copyoffload_configmap != None %}
            - name: copyoffload
              mountPath: /na_copyoffload/na_copyoffload_64
              subPath: na_copyoffload_64
            {% endif %}
            {% if "conversionVolume" in crd_spec%}
            - name: varlibcinderconversion
              mountPath: /var/lib/cinder/conversion
            {% endif %}
          env:
            - name: REQUESTS_CA_BUNDLE
              value: /etc/pki/tls/certs/ca-bundle.crt
{% if crd_spec.ids | default(False) and crd_spec.ids.uid | default(False) and crd_spec.ids.gid | default(False) %}
            - name: CINDER_UID
              value: {{ crd_spec.ids.uid | string }}
            - name: CINDER_GID
              value: {{ crd_spec.ids.gid | string }}
{% endif %}
          resources: {{ crd_spec | resources('backends.{}.volume.cinder-volume'.format(instance)) }}
      volumes:
        - name: cinder-config-volume
          secret:
            secretName: {{ dependencies['volume_config'].resource_name() }}
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: varlibcinder
          emptyDir: {}
        {% if copyoffload_configmap != None %}
        - name: copyoffload
          configMap:
            name: {{ copyoffload_configmap }}
            defaultMode: 0755
        {% endif %}
        {% if "rbd" in crd_spec.backends[instance] %}
        - name: cinder-ceph-conf
          projected:
            sources:
              - secret:
                  name: {{ dependencies['volume_config'].resource_name() }}
                  items:
                    - key: ceph.conf
                      path: ceph.conf
              {% if "rbd" in crd_spec.backends[instance] %}
              - secret:
                  name: {{ crd_spec.backends[instance].rbd.keyringReference }}
                  items:
                    - key: {{ crd_spec.backends[instance].rbd.keyringUsername }}
                      path: {{ crd_spec.backends[instance].rbd.keyringUsername }}
              {% endif %}
        {% endif %}
        {% if "conversionVolume" in crd_spec %}
        - name: varlibcinderconversion
          emptyDir: {{ crd_spec.conversionVolume.emptyDir}}
        {% endif %}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
