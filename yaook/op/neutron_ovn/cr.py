#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import environ
import typing

import yaook.common
import yaook.common.config
import yaook.op.common
import yaook.op.scheduling_keys
import yaook.statemachine as sm
from yaook.statemachine.resources.base import Resource
import yaook.statemachine.resources.openstack as resource
from . import resources


def _is_compute_node(ctx: sm.Context) -> bool:
    return ctx.parent_spec["deployedOnComputeNode"]


# Condition to check if the neutron-ovn-metadata-agent is inside the
# ovn-controller or not. After the migration phase/deprecation phase of 3
# months we can remove this -> remove this if you see this after january 18th
async def _compute_ovn_controller_has_no_metadata_agent(
        wrapped_state: Resource,
        ctx: sm.Context) -> bool:

    if not ctx.parent_spec["deployedOnComputeNode"]:
        return False

    try:
        for d in wrapped_state._dependencies:
            if isinstance(d, resources.OVNControllerStatefulSet):
                instance = await d._get_current(ctx)
                continue
    except sm.ResourceNotPresent:
        return False

    for container in instance.spec.template.spec.containers:
        if container.name == "neutron-ovn-metadata-agent":
            return False
    return True


class BridgeMapping(typing.NamedTuple):
    bridge_name: str
    uplink_device: str
    openstack_physnet: str


class OVNConfigState(sm.CueSecret):
    def _extract_bridge_mappings(
            self,
            mappings: typing.List[typing.Dict],
    ) -> typing.List[BridgeMapping]:
        return [
            BridgeMapping(
                bridge_name=mapping["bridgeName"],
                uplink_device=mapping["uplinkDevice"],
                openstack_physnet=mapping["openstackPhysicalNetwork"],
            )
            for mapping in mappings
        ]

    async def _render_cue_config(
            self,
            ctx: sm.Context,
    ) -> sm.ResourceBody:
        config = await super()._render_cue_config(ctx)
        bridge_mappings = self._extract_bridge_mappings(
            ctx.parent_spec.get("bridgeConfig", [])
        )
        bridge_config = []
        for mapping in bridge_mappings:
            bridge_config.append(
                f'{mapping.bridge_name};{mapping.uplink_device};'
                f'{mapping.openstack_physnet}'
            )
        config["bridge_mappings"] = '\n'.join(bridge_config) + '\n'
        return config


class OvnMetadataSpecSequenceLayer(sm.CueLayer):
    async def get_layer(self, ctx: sm.Context) -> sm.cue.CueInput:
        southbound_servers = ','.join(
                            ctx.parent_spec.get("southboundServers", []))
        return {
            "neutron_ovn_metadata_agent":
                yaook.common.config.OSLO_CONFIG.declare(
                    ctx.parent_spec.get("neutronMetadataAgentConfig", []) +
                    [{
                        "ovn": {
                            "ovn_sb_connection": southbound_servers
                        }
                    }]
                ),
        }


@environ.config(prefix="YAOOK_NEUTRON_OVN_AGENT_OP")
class OpOVNAgentConfig:
    interface = environ.var("internal")
    job_image = environ.var()


class NeutronOVNAgent(sm.ReleaseAwareCustomResource):
    API_GROUP = "network.yaook.cloud"
    API_GROUP_VERSION = "v1"
    PLURAL = "neutronovnagents"
    KIND = "NeutronOVNAgent"
    ADDITIONAL_PERMISSIONS = (
        # The jobs management is hidden behind the OVNStateResource and
        # thus cannot be auto-discovered by the CLI tool.
        (False, "batch", "jobs", {"create", "list", "watch", "get", "delete",
                                  "patch", "update", "deletecollection"}),
        (False, "network.yaook.cloud", "neutronovnagents/status",
         {"get", "patch", "replace", "update"}),
        # Required to read the node state for judging the next steps in
        # OVNStateResource
        (True, "", "nodes", {"get", "patch", "watch"}),
    )
    RELEASES = [
        "yoga",
        "zed",
        "2023.1",
    ]

    VALID_UPGRADE_TARGETS: typing.List[str] = [
        "zed",
        "2023.1",
    ]

    ovn_controller_docker_image = sm.ConfigurableVersionedDockerImage(
        "ovn",
        sm.OVNVersionSelector(),
    )

    neutron_ovn_agent_docker_image = yaook.op.common.image_dependencies(
        "neutron-ovn-agent-{release}",
        RELEASES,
    )

    openvswitch_docker_image = sm.ConfigurableVersionedDockerImage(
        "openvswitch",
        sm.SoftwareBuildVersionSelector(),
    )

    ovsdb_service = sm.TemplatedService(
        template="ovn-service.yaml",
        params={
            "generate_name": "neutron-ovn-ovsdb-server",
            "peer_component": "ovsdb_server",
        },
    )

    keystone = sm.KeystoneReference()
    keystone_operator_api = yaook.op.common.keystone_api_config_reference(
        keystone,
    )

    keystone_admin_secret = sm.ForeignResourceDependency(
        resource_interface_factory=sm.secret_interface,
        foreign_resource=keystone,
        foreign_component=yaook.op.common.KEYSTONE_ADMIN_CREDENTIALS_COMPONENT,
        broadcast_listener=True,
    )

    nova = sm.NovaReference()
    nova_metadata_proxy_secret = sm.ForeignResourceDependency(
        resource_interface_factory=sm.secret_interface,
        foreign_resource=nova,
        foreign_component=yaook.op.common.NOVA_METADATA_SECRET_COMPONENT,
        broadcast_listener=True,
    )

    user = sm.Orphan(resource_interface_factory=sm.keystoneuser_interface)

    ovsdb_pod_cleanup = resources.PodCleanupHandler(
        pod_component="ovsdb_server"
    )

    ovsdb_server = resource.TemplatedRecreatingStatefulSet(
        template="ovsdb-server-statefulset.yaml",
        add_dependencies=[
            ovsdb_pod_cleanup,
            ovsdb_service,
        ],
        versioned_dependencies=[
            openvswitch_docker_image,
        ],
        params={
            "tolerations": list(map(
                sm.resources.base.make_toleration,
                yaook.op.scheduling_keys.OVN_SCHEDULING_KEYS,
            )),
        },
    )

    ovsdb_server_pdb = sm.DisallowedPodDisruptionBudget(
        metadata=("ovn-ovsdb-server-pdb-", True),
        replicated=ovsdb_server,
    )

    new_ovs_vswitchd_service = sm.TemplatedService(
        template="ovs-vswitchd-service.yaml",
        params={
            "ovs_vswitchd_component": "ovs_vswitchd",
            "generate_name": "neutron-ovs-vswitchd",
        }
    )

    ovs_vswitchd_certificate_secret = sm.Orphan(
        resource_interface_factory=sm.secret_interface
    )

    ovs_vswitchd_certificate = sm.Orphan(
        resource_interface_factory=sm.certificates_interface
    )

    ca_certs = sm.Orphan(resource_interface_factory=sm.secret_interface)

    ovsdb_service_monitor = sm.Orphan(
        resource_interface_factory=sm.servicemonitor_interface,
    )

    ovs_vswitchd = resource.TemplatedRecreatingStatefulSet(
        template="ovs-vswitchd-statefulset.yaml",
        add_dependencies=[
            new_ovs_vswitchd_service,
            ovsdb_server,
        ],
        versioned_dependencies=[
            openvswitch_docker_image,
        ],
        params={
            "tolerations": list(map(
                sm.resources.base.make_toleration,
                yaook.op.scheduling_keys.OVN_SCHEDULING_KEYS,
            )),
        },
    )

    ovs_vswitchd_pdb = sm.DisallowedPodDisruptionBudget(
        metadata=("ovn-statefulset-vswitchd-pdb-", True),
        replicated=ovs_vswitchd,
    )

    ovn_config = OVNConfigState(
        metadata=("neutron-ovn-config-", True),
        copy_on_write=True,
    )

    ovn_metadata_config = sm.CueSecret(
        metadata=("neutron-ovn-metadata-", True),
        copy_on_write=True,
        add_cue_layers=[
            OvnMetadataSpecSequenceLayer(),
            sm.MetadataSecretLayer(
                metadata_key="neutron_ovn_metadata_agent",
                proxy_secret=nova_metadata_proxy_secret,
            ),
        ]
    )

    ovn_service = sm.TemplatedService(
        template="ovn-service.yaml",
        params={
            "generate_name": "neutron-ovn-controller",
            "peer_component": "ovn_controller",
        },
    )

    certificate_secret = sm.EmptyTlsSecret(
        metadata=("ovn-controller-certificate-", True),
    )

    certificate = sm.TemplatedCertificate(
        template="ovn-controller-certificate.yaml",
        add_dependencies=[
            certificate_secret,
            ovn_service,
            ],
    )

    ready_certificate_secret = sm.ReadyCertificateSecretReference(
        certificate_reference=certificate,
    )

    ovn_controller = resources.OVNControllerStatefulSet(
        template='ovn-controller-statefulset.yaml',
        add_dependencies=[
            ovn_service,
            ready_certificate_secret,
            ovn_config,
            ovn_metadata_config,
            ovsdb_server,
        ],
        versioned_dependencies=[
            ovn_controller_docker_image,
            neutron_ovn_agent_docker_image,
        ],
        params={
            "tolerations": list(map(
                sm.resources.base.make_toleration,
                yaook.op.scheduling_keys.OVN_SCHEDULING_KEYS,
            )),
        },
    )

    ovn_controller_pdb = sm.DisallowedPodDisruptionBudget(
        metadata=("neutron-ovn-agent-pdb-", True),
        replicated=ovn_controller,
    )

    # We only need the service as a StatefulSet requires a it
    neutron_ovn_metadata_service = sm.Optional(
        condition=_is_compute_node,
        wrapped_state=sm.TemplatedService(
            template="ovn-service.yaml",
            params={
                "generate_name": "neutron-ovn-metadata-agent",
                "peer_component": "neutron_ovn_metadata_agent",
            },
        )
    )
    # The WrappedStateAwareOptional can be transformed to an Optional with
    # the condition=_is_compute_node after the deprecation period of 3 months
    # -> remove this if you see this after january 18th
    neutron_ovn_metadata_agent = sm.WrappedStateAwareOptional(
        condition=_compute_ovn_controller_has_no_metadata_agent,
        wrapped_state=resource.TemplatedStatefulSet(
            template='neutron-ovn-metadata-agent-statefulset.yaml',
            add_dependencies=[
                ovn_controller,
                neutron_ovn_metadata_service,
                ovn_metadata_config,
                ready_certificate_secret,
            ],
            versioned_dependencies=[
                neutron_ovn_agent_docker_image,
            ],
            params={
                "tolerations": list(map(
                    sm.resources.base.make_toleration,
                    yaook.op.scheduling_keys.OVN_SCHEDULING_KEYS,
                )),
            },
        ),
    )

    job_service_account = sm.TemplatedServiceAccount(
        component="job_service_account",
        template="serviceaccount.yaml",
        params={
            "suffix": "job",
        },
        add_dependencies=[ovn_service],
    )

    job_role = sm.TemplatedRole(
        component="job_role",
        template="job-role.yaml",
        params={
            "password_secret_name":
            yaook.op.common.NEUTRON_OVN_OPERATOR_USER_SECRET,
        },
    )

    job_role_binding = sm.TemplatedRoleBinding(
        template="rolebinding.yaml",
        copy_on_write=True,
        params={
            "role": job_role.component,
            "service_account": job_service_account.component,
            "name_prefix": "ovn-job-",
        },
        add_dependencies=[job_role, job_service_account],
    )

    api_state = resources.OVNStateResource(
        finalizer="network.yaook.cloud/neutron-ovn-agent",
        endpoint_config=keystone_operator_api,
        job_endpoint_config=keystone_operator_api,
        credentials_secret=keystone_admin_secret,
        scheduling_keys=[
            yaook.op.scheduling_keys.SchedulingKey.OPERATOR_ANY.value,
            yaook.op.scheduling_keys.SchedulingKey.OPERATOR_NEUTRON.value,
        ],
        eviction_job_template="eviction-job.yaml",
        add_dependencies=[
            job_service_account,
            ready_certificate_secret,
            ],
    )

    def __init__(self, **kwargs):
        super().__init__(assemble_sm=True, **kwargs)
        config = environ.to_config(OpOVNAgentConfig)
        self.keystone_operator_api._foreign_component = {
            "internal": yaook.op.common.KEYSTONE_INTERNAL_API_COMPONENT,
            "public": yaook.op.common.KEYSTONE_PUBLIC_API_COMPONENT,
        }[config.interface]
        self.api_state._params["job_image"] = config.job_image


sm.register(NeutronOVNAgent)
