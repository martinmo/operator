##
## Copyright (c) 2024 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set northbound_servers = [] %}
{% for server in crd_spec.northboundServers %}
{%   set _ = northbound_servers.append(server) %}
{% endfor %}

{% set southbound_servers = [] %}
{% for server in crd_spec.southboundServers %}
{%   set _ = southbound_servers.append(server) %}
{% endfor %}

apiVersion: batch/v1
kind: Job
metadata:
  generateName: {{ "ovn-evict-%s-" | format(vars.node_name) }}
spec:
  parallelism: 1
  activeDeadlineSeconds: 86400
  template:
    metadata:
      labels: {{ labels }}
    spec:
      restartPolicy: OnFailure
      serviceAccountName: {{ dependencies['job_service_account'].resource_name() }}
      containers:
      - name: evict
        image: {{ params.job_image }}
        imagePullPolicy: Always
        args:
        - neutron_ovn
        - command
        - evict
        envFrom:
        - configMapRef:
            name: {{ params.endpoint_config }}
        - secretRef:
            name: {{ params.credentials_secret }}
        env:
        - name: YAOOK_NEUTRON_OVN_AGENT_EVICT_NODE_NAME
          value: {{ vars.node_name }}
        - name: YAOOK_NEUTRON_OVN_AGENT_EVICT_NB_DB
          value: {{ northbound_servers | join(',') }}
        - name: YAOOK_NEUTRON_OVN_AGENT_EVICT_SB_DB
          value: {{ southbound_servers | join(',') }}
        - name: YAOOK_NEUTRON_OVN_AGENT_EVICT_NODE_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: YAOOK_OP_VERBOSITY
          value: "3"
        - name: YAOOK_OP_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: YAOOK_NEUTRON_OVN_AGENT_EVICT_REASON
          value: {{ vars.reason }}
        - name: REQUESTS_CA_BUNDLE
          value: /etc/ssl/certs/ca-bundle.crt
        volumeMounts:
          - name: ca-certs
            mountPath: /etc/ssl/certs
          - name: tls-secret
            mountPath: /etc/ssl/private
        resources: {{ crd_spec | resources('l3-evict-job') }}
      volumes:
        - name: ca-certs
          configMap:
            name: {{ crd_spec.caConfigMapName }}
        - name: tls-secret
          secret:
            secretName: {{ dependencies['ready_certificate_secret'].resource_name() }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
