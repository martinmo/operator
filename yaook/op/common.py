#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
:mod:`~yaook.op.common` -- Common utilities shared among operators
##################################################################

.. autofunction:: extract_labeled_configs

.. autofunction:: transpose_config

.. autofunction:: get_node_labels_from_instance

"""

from __future__ import annotations

import dataclasses
import enum
import typing

import jinja2

import kubernetes_asyncio.client

import yaook.statemachine as sm
from yaook.statemachine import version_utils
from yaook.statemachine import api_utils
import yaook.op.scheduling_keys as scheduling_keys


LABEL_NETWORK_L2_PROVIDER = "network.yaook.cloud/provides-l2"

# Labels are not passwords so we need to exclude these :)
LABEL_NOVA_METADATA_SECRET_NAME = \
    "nova.yaook.cloud/metadata-secret-name"  # nosemgrep
NOVA_METADATA_SECRET_COMPONENT = "metadata_proxy_shared_secret"  # nosemgrep

NOVA_COMPUTE_OPERATOR_USER_SECRET = \
    "nova-compute-operator-user-password"  # nosemgrep
NOVA_COMPUTE_OPERATOR_USER_NAME = "nova-compute-operator"

NEUTRON_DHCP_OPERATOR_USER_SECRET = \
    "neutron-dhcp-operator-user-password"  # nosemgrep

NEUTRON_L3_OPERATOR_USER_SECRET = \
    "neutron-l3-operator-user-password"  # nosemgrep

NEUTRON_BGP_OPERATOR_USER_SECRET = \
    "neutron-bgp-operator-user-password"  # nosemgrep

NEUTRON_OVN_OPERATOR_USER_SECRET = \
    "neutron-ovn-operator-user-password"  # nosemgre

MYSQL_DATABASE_SERVICE_COMPONENT = "public_service"
OVSDB_DATABASE_SERVICE_COMPONENT = "services_per_replica"
OVN_ACCESS_SERVICE_COMPONENT = "access_service"
AMQP_SERVER_SERVICE_COMPONENT = "public_service"
MEMCACHED_STATEFUL_COMPONENT = "memcached"
POLICY_CONFIGMAP_COPMONENT = "policy"
SERVICE_ACCOUNT_COPMONENT = "service_account"
KEYSTONE_INTERNAL_API_COMPONENT = "internal_config"
KEYSTONE_PUBLIC_API_COMPONENT = "public_config"
KEYSTONE_USER_CREDENTIALS_COMPONENT = "credentials"
NOVA_COMPUTE_PUBLIC_KEYS_COMPONENT = "public_keys"
KEYSTONE_CA_CERTIFICATES_COMPONENT = "ca_certs"
KEYSTONE_ADMIN_CREDENTIALS_COMPONENT = "admin_credentials"


class InvalidOVSDBSchema(Exception):
    """
    This exception indicates, that a non-anticipated OVSDBSchema has been
    encountered.
    """
    pass


@dataclasses.dataclass(frozen=True)
class OVSDBSchemaParams:
    """
    This data class holds parameters specific to the schema of an OVS database.
    """
    port: int
    stanza: str
    access_service: str
    db_name: str
    db_global: str


class OVSDBSchema(enum.Enum):
    """
    This enumeration defines the schemas of OVS databases used throughout
    the operator code.
    """
    northbound = OVSDBSchemaParams(
        port=6641, stanza='nb',
        access_service="northbound_ovsdb_access_service",
        db_name="OVN_Northbound",
        db_global="NB_Global"
    )
    southbound = OVSDBSchemaParams(
        port=6642, stanza='sb',
        access_service="southbound_ovsdb_access_service",
        db_name="OVN_Southbound",
        db_global="SB_Global"
    )

    @classmethod
    def schema_params_by_label(cls, label: str) -> OVSDBSchemaParams:
        return cls.schema_by_label(label).value

    @staticmethod
    def schema_by_label(label: str) -> OVSDBSchema:
        if label == OVSDBSchema.northbound.name:
            return OVSDBSchema.northbound
        if label == OVSDBSchema.southbound.name:
            return OVSDBSchema.southbound
        raise InvalidOVSDBSchema(f'label "{label}" unknown')


def generate_standard_tolerations(scheduling_keys):
    return [
        {
            "key": scheduling_key.value,
            "operator": "Exists",
        }
        for scheduling_key in scheduling_keys
    ]


def generate_standard_node_affinity(scheduling_keys):
    return {
        "requiredDuringSchedulingIgnoredDuringExecution": {
            "nodeSelectorTerms": [
                {
                    "matchExpressions": [
                        {
                            "key": scheduling_key.value,
                            "operator": "Exists",
                            "values": [],
                        },
                    ],
                }
                for scheduling_key in scheduling_keys
            ],
        },
    }


def inject_scheduling_globals(jinja_env: jinja2.Environment) -> None:
    """
    Inject scheduling-reltaed globals into a given Jinja2 environment.

    The provided globals are:

    - ``to_tolerations`` (filter): :func:`generate_standard_tolerations`
    - ``to_node_affinity`` (filter): :func:`generate_standard_node_affinity`
    - ``SchedulingKey`` (value): :class:`SchedulingKey`
    """
    jinja_env.filters["to_tolerations"] = generate_standard_tolerations
    jinja_env.filters["to_node_affinity"] = generate_standard_node_affinity
    jinja_env.globals["SchedulingKey"] = scheduling_keys.SchedulingKey


def extract_labeled_configs(
        configset: typing.List[typing.Mapping],
        object_labels: typing.Mapping[str, str],
        ) -> typing.List[typing.Mapping]:
    result = []
    for cfg in configset:
        if api_utils.matches_node_selector(
                cfg["nodeSelectors"], object_labels):
            result.append(cfg)
    return result


def transpose_config(
        configsets: typing.Sequence[typing.Mapping],
        keys: typing.Sequence[str],
        ) -> typing.MutableMapping[str, typing.List[typing.Any]]:
    result: typing.MutableMapping[str, typing.List[typing.Any]] = {
        key: [] for key in keys
    }
    for cfgs in configsets:
        for key in keys:
            try:  # nosemgrep it is planned to ignore configs that miss keys
                cfg = cfgs[key]
            except KeyError:
                continue
            result[key].append(cfg)
    return result


async def get_node_labels_from_instance(
        ctx: sm.Context) -> typing.Mapping[str, str]:
    """
    Obtain the labels of the node referenced by the `instance` of the context.

    :param ctx: The context to use.
    :type ctx: :class:`.statemachine.Context`
    :return: The label dictionary of the node.

    This looks up the node with the name matching the
    :attr:`~.Context.instance` attribute of the context, using the API client
    of the context.
    """
    core_client = kubernetes_asyncio.client.CoreV1Api(ctx.api_client)
    return (await core_client.read_node(ctx.instance)).metadata.labels


async def get_ovn_db_servers(
        ctx: sm.Context,
        db_service_ref: typing.Mapping[
            typing.Union[str, None],
            kubernetes_asyncio.client.V1ObjectReference],
        port: int) -> typing.List[str]:
    """
    Return the complete URL for the service of the database provided

    :param ctx: The context of the current operation.
    :param db_service_ref: Reference to the service for database
    :param port: Port of the database
    """
    api_client = kubernetes_asyncio.client.CoreV1Api(ctx.api_client)
    scheme = "ssl"
    servers_list = []
    for db_pod_service in db_service_ref.values():
        service_object = await api_client.read_namespaced_service(
            db_pod_service.name,
            db_pod_service.namespace,
        )
        servers_list.append(
            f"{scheme}:{service_object.spec.cluster_ip}:{port}"
        )

    return sorted(servers_list)


def keystone_api_config_reference(
        keystone: sm.KubernetesReference[typing.Mapping],
        component: str = KEYSTONE_INTERNAL_API_COMPONENT,
        ) -> sm.KubernetesReference[kubernetes_asyncio.client.V1ConfigMap]:
    return sm.ForeignResourceDependency(
        resource_interface_factory=sm.config_map_interface,
        foreign_resource=keystone,
        foreign_component=component,
        broadcast_listener=True,
    )


def keystone_user_credentials_reference(
        keystone_user: sm.KubernetesReference[typing.Mapping],
        ) -> sm.KubernetesReference[kubernetes_asyncio.client.V1Secret]:
    return sm.ForeignResourceDependency(
        resource_interface_factory=sm.secret_interface,
        foreign_resource=keystone_user,
        foreign_component=KEYSTONE_USER_CREDENTIALS_COMPONENT,
    )


def image_dependencies(
    image_selector_template: str,
    releases: typing.Sequence[str],
    targetfn: typing.Callable[
        [sm.context.Context], typing.Any
    ] = lambda ctx: version_utils.get_target_release(ctx),
) -> sm.ReleaseAwareVersionedDependency:
    """
    Create a list of image dependencies

    :param image_selector_template: The image selector, templated with the
        release name (see below).
    :param releases: A list of openstack release names.
    :returns: A list of image dependencies for each of the release names.

    The template must include the sequence "{release}" where the release name
    is to be substituted, e.g. "keystone/keystone-{release}".
    """
    return sm.ReleaseAwareVersionedDependency({
        release: sm.ConfigurableVersionedDockerImage(
                image_selector_template.format(release=release),
                sm.YaookSemVerSelector(),
            )
        for release in releases
    }, targetfn=targetfn)


def publish_endpoint(ctx: sm.Context) -> bool:
    return ctx.parent_spec.get('api', {}).get('publishEndpoint', True)


def create_ingress(ctx: sm.Context) -> bool:
    """
    Create the k8s ingress object. The default value is true.
    We need to update the crd if we change the default here.
    """
    return ctx.parent_spec.get('api', {}).get('ingress', {})\
        .get('createIngress', True)
