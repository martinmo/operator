##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
apiVersion: batch/v1
kind: Job
metadata:
  generateName: {{ "l3-evict-%s-" | format(vars.node_name) }}
spec:
  parallelism: 1
  activeDeadlineSeconds: 86400
  template:
    metadata:
      labels: {{ labels }}
    spec:
      restartPolicy: OnFailure
      serviceAccountName: {{ dependencies['job_service_account'].resource_name() }}
      containers:
      - name: evict
        image: {{ params.job_image }}
        imagePullPolicy: IfNotPresent
        securityContext:
          runAsUser: 2020
          runAsGroup: 2020
        args:
        - neutron_l3
        - command
        - evict
        envFrom:
        - configMapRef:
            name: {{ params.endpoint_config }}
        - secretRef:
            name: {{ params.credentials_secret }}
        env:
        - name: YAOOK_NEUTRON_L3_AGENT_EVICT_NODE_NAME
          value: {{ vars.node_name }}
        - name: YAOOK_NEUTRON_L3_AGENT_EVICT_NODE_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: YAOOK_OP_VERBOSITY
          value: "3"
        - name: YAOOK_OP_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: YAOOK_NEUTRON_L3_AGENT_EVICT_REASON
          value: {{ vars.reason }}
{% if crd_spec.evictor | default(False) %}
        - name: YAOOK_NEUTRON_L3_AGENT_EVICT_ALLOW_FALLBACK
          value: {{ crd_spec.evictor.allowFallback | string }}
        - name: YAOOK_NEUTRON_L3_AGENT_EVICT_POLL_INTERVAL
          value: {{ crd_spec.evictor.pollInterval | string }}
        - name: YAOOK_NEUTRON_L3_AGENT_EVICT_MAX_PARALLEL_MIGRATIONS
          value: {{ crd_spec.evictor.maxParallelMigrations | string }}
        - name: YAOOK_NEUTRON_L3_AGENT_EVICT_RESPECT_AZS
          value: {{ crd_spec.evictor.respectAvailabilityZones | string }}
        - name: YAOOK_NEUTRON_L3_AGENT_EVICT_VERIFY_SECONDS
          value: {{ crd_spec.evictor.verifySeconds | string }}
{% endif %}
        - name: REQUESTS_CA_BUNDLE
          value: /etc/ssl/certs/ca-bundle.crt
        volumeMounts:
          - name: ca-certs
            mountPath: /etc/ssl/certs
        resources: {{ crd_spec | resources('l3-evict-job') }}
      volumes:
        - name: ca-certs
          configMap:
            name: {{ crd_spec.caConfigMapName }}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}