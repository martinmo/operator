##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
{% set pod_labels = labels %}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "horizon"
  labels:
    app: "horizon"
spec:
  replicas:  {{ crd_spec.replicas }}
  selector:
    matchLabels: {{ pod_labels }}
  template:
    metadata:
      labels: {{ pod_labels }}
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      shareProcessNamespace: true
      topologySpreadConstraints:
        - maxSkew: 1
          topologyKey: kubernetes.io/hostname
          whenUnsatisfiable: {{ crd_spec.scheduleRuleWhenUnsatisfiable }}
          labelSelector:
            matchLabels: {{ pod_labels }}
      containers:
        - name: "horizon"
          image:  {{ versioned_dependencies['horizon_docker_image'] }}
          imagePullPolicy: IfNotPresent
          env:
          - name: YAOOK_HORIZON_KEYSTONE_URL
            valueFrom:
              configMapKeyRef:
                name: {{ dependencies['keystone_internal_api'].resource_name() }}
                key: OS_AUTH_URL
          - name: YAOOK_HORIZON_INTERFACE
            valueFrom:
              configMapKeyRef:
                name: {{ dependencies['keystone_internal_api'].resource_name() }}
                key: OS_INTERFACE
          - name: YAOOK_HORIZON_SECRET_KEY
            valueFrom:
              secretKeyRef:
                name: {{ dependencies['django_secret'].resource_name() }}
                key: password
          - name: REQUESTS_CA_BUNDLE
            value: /etc/pki/tls/certs/ca-bundle.crt
          volumeMounts:
            - name: ca-certs
              mountPath: /etc/pki/tls/certs
          lifecycle:
            preStop:
              exec:
                command:
                - /bin/sleep
                - "5"
          livenessProbe:
            exec:
              command:
                - curl
                - --fail
                - localhost:8080
          readinessProbe:
            exec:
              command:
                - curl
                - --fail
                - localhost:8080
          resources: {{ crd_spec | resources('horizon') }}
        - name: "ssl-terminator-external"
          image: {{ versioned_dependencies['ssl_terminator_image'] }}
          imagePullPolicy: IfNotPresent
          env:
            - name: SERVICE_PORT
              value: "8443"
            - name: LOCAL_PORT
              value: "8080"
            - name: METRICS_PORT
              value: "9091"
            - name: REQUESTS_CA_BUNDLE
              value: /etc/ssl/certs/ca-certificates.crt
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
            - name: ca-certs
              mountPath: /etc/ssl/certs/ca-certificates.crt
              subPath: ca-bundle.crt
          livenessProbe:
            httpGet:
              path: /.yaook.cloud/ssl-terminator-healthcheck
              port: 8443
              scheme: HTTPS
          readinessProbe:
            httpGet:
              path: /
              port: 8443
              scheme: HTTPS
          resources: {{ crd_spec | resources('ssl-terminator-external') }}
        - name: "service-reload-external"
          image: {{ versioned_dependencies['service_reload_image'] }}
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: ssl-terminator-external-config
              mountPath: /config
            - name: tls-secret-external
              mountPath: /data
          env:
            - name: YAOOK_SERVICE_RELOAD_MODULE
              value: traefik
          args:
            - /data/
          resources: {{ crd_spec | resources('service-reload-external') }}
      volumes:
        - name: ca-certs
          configMap:
            name: {{ dependencies['ca_certs'].resource_name() }}
        - name: tls-secret-external
          secret:
            secretName: {{ dependencies['external_certificate_secret'].resource_name() }}
        - name: ssl-terminator-external-config
          emptyDir: {}
{% if crd_spec.imagePullSecrets | default(False) %}
      imagePullSecrets: {{ crd_spec.imagePullSecrets }}
{% endif %}
