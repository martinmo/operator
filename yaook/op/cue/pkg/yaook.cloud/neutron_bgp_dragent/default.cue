package neutron_bgp_dragent

import (
	"yaook.cloud/neutron_bgp_dragent_template"
	"yaook.cloud/neutron_agent"
)

neutron_bgp_dragent_conf_spec: neutron_bgp_dragent_template.neutron_bgp_dragent_template_conf_spec
neutron_bgp_dragent_conf_spec: neutron_agent.neutron_agent_conf_spec
neutron_bgp_dragent_conf_spec: {
	bgp: {
		"bgp_speaker_driver": string | *"neutron_dynamic_routing.services.bgp.agent.driver.os_ken.driver.OsKenBgpDriver"
		"bgp_router_id"?:     string
	}
}
