#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""
Yaook Configured Daemon Set resources
#####################################

.. currentmodule:: yaook.statemachine.resources

.. autoclass:: ConfiguredDaemonSet

.. autoclass:: TemplatedConfiguredDaemonSet

"""
import copy
import typing

import jinja2

import kubernetes_asyncio.client as kclient
from opentelemetry import trace

from .. import api_utils, context, exceptions, interfaces, templating
from .base import (
    ResourceBody,
    # TODO(resource-refactor): make it non-private
    _scheduling_keys_to_selectors,
)
from .k8s import BodyTemplateMixin, SingleObject
from yaook.statemachine.tracing import start_as_current_span_async


tracer = trace.get_tracer(__name__)


class ConfiguredDaemonSet(SingleObject[typing.Mapping]):
    """
    :param scheduling_keys: Scheduling keys to use.
    :type scheduling_keys: collection of :class:`str`

    When configuring the ConfiguredDaemonSet resource, the ``targetNodes`` will
    be fored to match the nodes matching any of the `scheduling_keys` given.

    A node matches a scheduling key if it has a label with that key; the value
    of that label is ignored.

    In addition, tolerations for all the scheduling keys (for any taint effect)
    are added to the pods.
    """

    def __init__(self, *,
                 scheduling_keys: typing.Collection[str],
                 **kwargs: typing.Any):
        super().__init__(**kwargs)
        self._scheduling_keys = scheduling_keys

    def _create_resource_interface(
            self,
            api_client: kclient.ApiClient
            ) -> interfaces.ResourceInterface[typing.Mapping]:
        return interfaces.custom_resource_interface(
            "apps.yaook.cloud", "v1", "configureddaemonsets",
            api_client,
        )

    def _needs_update(self,
                      current: typing.Mapping,
                      new: typing.Mapping) -> bool:
        result = api_utils.deep_has_changes(current["spec"], new["spec"])
        if result:
            return result

        v2_metadata = copy.deepcopy(new.get("metadata", {}))
        v2_metadata.get("annotations", {}).pop(
            context.ANNOTATION_LAST_UPDATE,
            None,
        )

        return api_utils.deep_has_changes(current.get("metadata", {}),
                                          v2_metadata)

    async def _get_relevant_nodes(self,
                                  ctx: context.Context
                                  ) -> typing.Sequence[kclient.V1Node]:
        return await interfaces.get_selected_nodes_union(
            ctx.api_client,
            _scheduling_keys_to_selectors(
                self._scheduling_keys,
            ),
        )

    async def adopt_object(
            self,
            ctx: context.Context,
            body: ResourceBody) -> None:
        await super().adopt_object(ctx, body)
        cds_spec = body.setdefault("spec", {})
        nodes = await self._get_relevant_nodes(ctx)
        cds_spec["targetNodes"] = [
            node.metadata.name
            for node in nodes
        ]
        pod_spec = cds_spec.setdefault("template", {}).setdefault("spec", {})
        pod_spec.setdefault("tolerations", []).extend([
            {
                "key": key,
                "operator": "Exists",
            }
            for key in self._scheduling_keys
        ])

    @start_as_current_span_async(tracer, "get_used_resources")
    async def get_used_resources(
            self,
            ctx: context.Context,
            ) -> typing.Collection[api_utils.ResourceReference]:
        try:
            current = await self._get_current(ctx, True)
        except exceptions.ResourceNotPresent:
            return []

        cds_spec = current.get("spec", {})
        result = set(api_utils.extract_cds_references(
            cds_spec,
            ctx.namespace,
        ))

        api_selector = api_utils.build_selector(
            {context.LABEL_CDS_CONTROLLER_UID: current["metadata"]["uid"]}
        )

        pods = interfaces.pod_interface(ctx.api_client)
        for pod in await pods.list_(
                ctx.namespace,
                label_selector=api_selector):
            result.update(api_utils.extract_pod_references(
                pod.spec,
                ctx.namespace,
            ))

        return result


class TemplatedConfiguredDaemonSet(BodyTemplateMixin,
                                   ConfiguredDaemonSet):
    """
    Manage a jinja2-templated ConfiguredDaemonSet.

    This class adds some filters to the Jinja2 environment for easier
    generation of CDS specs:

    - :func:`~.to_config_map_volumes`
    - :func:`~.to_secret_volumes`

    .. seealso::

        :class:`~.BodyTemplateMixin`:
            for arguments related to templating.

        :class:`~.ConfiguredDaemonSetState`:
            for arguments specific to ConfiguredDaemonSets, such as the
            `scheduling_keys`.
    """

    def _add_filters(self, env: jinja2.Environment) -> None:
        super()._add_filters(env)
        env.filters['to_config_map_volumes'] = templating.to_config_map_volumes
        env.filters['to_secret_volumes'] = templating.to_secret_volumes
