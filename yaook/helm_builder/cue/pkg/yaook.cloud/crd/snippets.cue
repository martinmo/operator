// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package crd

apiendpoint: {
	replicated
	publishEndpoint
	topologySpreadConstraints
	type: "object"
	required: ["ingress", ...]
	properties: {
		ingress: #ingress
		internal: {
			type:        "object"
			description: "Optional override for the internal API endpoint. Normally, the internal API endpoint uses the Kubernetes service. By configuring an Ingress here, that Ingress is used instead."
			required: ["ingress"]
			properties: {
				ingress: #ingress
			}
		}
	}
}

#ingress: {
	type:        "object"
	description: "Ingress configuration"
	required: ["fqdn", "port"]
	properties: {
		fqdn: {
			type:        "string"
			description: "The outer fully-qualified domain name of the Ingress"
		}
		port: {
			type:        "integer"
			description: "Port number under which the Ingress is available. This is required to be set to generate the full URL e.g. for KeystoneEndpoints"
		}
		ingressClassName: {
			type:        "string"
			default:     "nginx"
			description: "Class of the Ingress to use"
		}
		externalCertificateSecretRef: {
			#ref
			#description: "Reference to a Kubernetes TLS Secret containing the TLS certificate and keys to use for this endpoint. If unset, a certificate will be generated using the Issuer configured via issuerRef at the top level."
		}
		createIngress: {
			type:        "boolean"
			description: "Create the k8s ingress object. The default value is true"
		}
	}
}

replicated: {
	type: "object"
	properties: {
		replicas: {
			type:        "integer"
			default:     *3 | int
			description: "Number of replicas for the service"
		}
	}
}

timeoutClient: {
	type: "object"
	properties: {
		timeoutClient: {
			type:        "integer"
			default:     *300 | int
			description: *"Defines the maximum time a client can be inactive when connected to the server. Hint should be ~10% more than connection_recycle_time." | string
		}
	}
}

topologySpreadConstraints: {
	type: "object"
	properties: {
		scheduleRuleWhenUnsatisfiable: {
			type: "string"
			enum: [
				"ScheduleAnyway",
				"DoNotSchedule",
			]
			default:     "ScheduleAnyway"
			description: "whenUnsatisfiable indicates how to deal with a Pod if it doesn't satisfy the spread constraint"
		}
	}
}

publishEndpoint: {
	type: "object"
	properties: {
		publishEndpoint: {
			type:        "boolean"
			default:     *true | bool
			description: "Publish it to Keystone endpoints as well"
		}
	}
}

pernodeconfig: {
	type: "object"
	required: ["configTemplates"]
	properties: configTemplates: {
		type:        "array"
		description: "Label-selected configuration templates. This is **a list of objects**, each describing a piece of configuration."
		items: {
			type:        "object"
			description: "A single configuration template snippet. This is applied to nodes based on the nodeSelectors.\n\nConfiguration options are merged intelligently, generally using cuelang semantics. That means that conflicting values will always cause an InvalidConfiguration error during rollout."
			required: ["nodeSelectors"]
			properties: {
				nodeSelectors: {
					type:        "array"
					description: "List of node selectors, used to select the nodes to which the configuration applies. Each node selector selects a group of nodes; the groups are OR'ed together."
					items: {
						type: "object"
						required: ["matchLabels"]
						description: "A single node selector."
						properties: matchLabels: {
							type:        "object"
							description: "Label keys and values all of which nodes must have to match the node selector."
							additionalProperties: type: "string"
						}
					}
				}
			}
		}
	}
}

#anyconfig: {
	#description: string | *""
	type:         "object"
	description:  "\(#description)The keys of this object are the configuration file sections, the values are objects whose keys correspond to config keys within that section."
	additionalProperties: {
		type:                                   "object"
		"x-kubernetes-preserve-unknown-fields": true
	}
}

#anyobj: {
	#description:                           string | *""
	type:                                   "object"
	description:                            #description
	"x-kubernetes-preserve-unknown-fields": true
}

#configsecret: {
	type:        "array"
	description: "List of secrets to inject into the service configuration."
	items: {
		type:        "object"
		description: "A single secret injection configuration. This causes the operator to read the referenced Secret, extract the individual data entries and put them into the corresponding configuration paths, according to the `items`."
		required: ["secretName", "items"]
		properties: {
			secretName: {
				type:        "string"
				description: "Name of the Kubernetes Secret to read"
			}
			items: {
				type:        "array"
				description: "Assignment of Secret keys to configuration keys"
				items: {
					type: "object"
					required: ["key", "path"]
					properties: {
						key: {
							type:        "string"
							description: "Key name in the Secret"
						}
						path: {
							type:        "string"
							description: "Path inside the configuration to put the value in. Path must be in the form of ``/section/key``. For example, to put a value into the `debug` key in the `DEFAULT` section, you would use `/DEFAULT/debug`."
							pattern:     "/.*"
						}
					}
				}
			}
		}
	}
}

quantilepattern: "^([+-]?[0-9.]+)([eEinumkKMGTP]*[-+]?[0-9]*)$"
storageconfig: {
	type: "object"
	properties: {
		storageSize: {
			type:        "string"
			description: "Size to request for the PVC. Changing the size after the initial rollout requires manual intervention."
			pattern:     quantilepattern
			default:     *"8Gi" | string
		}
		storageClassName: {
			type:        "string"
			description: "Storage class name. If unset, the cluster-wide default storage class will be used. If that storage class is not set or does not work, the deployment will be broken. Changing storage classes after the initial rollout requires manual intervention."
		}
	}
}

#pvcaccessmode: {
	type:        "string"
	description: "PVC access mode. See upstream Kubernetes documentation."
	default:     "ReadWriteOnce"
	enum: ["ReadWriteOnce", "ReadWriteMany"]
}

#mysqlconfig: {
	type:        "object"
	description: "Additional MySQL configuration. This is more detailed than typical configuration snippets because of the special `optimizer_switch` config option."
	properties: {
		mysqld: {
			type:        "object"
			description: "Configuration options for the mysqld section."
			properties: {
				optimizer_switch: {
					type:        "object"
					description: "Boolean flags for individual optimizer switches. See MariaDB upstream configuration for details."
					additionalProperties: {
						type: "boolean"
					}
				}
			}

			"x-kubernetes-preserve-unknown-fields": true
		}
		galera: {
			type:                                   "object"
			description:                            "Configuration options for the galera section."
			"x-kubernetes-preserve-unknown-fields": true
		}
		"client-server": {
			type:                                   "object"
			description:                            "Configuration options for the client-server section."
			"x-kubernetes-preserve-unknown-fields": true
		}
		sst: {
			type:                                   "object"
			description:                            "Configuration options for the sst section."
			"x-kubernetes-preserve-unknown-fields": true
		}
	}
}

#servicemonitor: {
	type:        "object"
	description: "Configure the ServiceMonitor objects created by the operator."
	properties: {
		additionalLabels: {
			type:        "object"
			description: "Additional labels to set on the ServiceMonitor metadata."
			additionalProperties: type: "string"
		}
	}
}

#databasesnip: {
	storageconfig
	replicated
	timeoutClient
	topologySpreadConstraints
	type:        "object"
	description: "Configure the database deployment"
	required: [
		"proxy",
		"backup",
	]
	properties: {
		tolerateNodeDown: #tolerateNodeDown
		proxy:            replicated
		proxy:            topologySpreadConstraints
		proxy: description: "Configure the proxy deployment, which brokers traffic to the database"
		proxy: properties: {
			replicas: default: 2
			resources: {
				type:        "object"
				description: "Configure resource requests/limits for containers related to the database proxy."
				properties: {
					"create-ca-bundle": {
						#containerresources
						#containername: "create-ca-bundle"
					}
					"haproxy": {
						#containerresources
						#containername: "haproxy"
					}
					"service-reload": {
						#containerresources
						#containername: "service-reload"
					}
				}
			}
		}
		backup: backupspec
		backup: description: "Configure automated database backups"
		backup: properties: {
			mysqldump: {
				type:        "boolean"
				description: "Execute a plain mysqldump in addition to the standard backup."
				default:     false
			}
		}
		mysqlConfig: #mysqlconfig
		resources: {
			type: "object"
			properties: {
				"mariadb-galera": {
					#containerresources
					#containername: "mariadb-galera"
				}
				"backup-creator": {
					#containerresources
					#containername: "backup-creator"
				}
				"backup-shifter": {
					#containerresources
					#containername: "backup-shifter"
				}
				"mysqld-exporter": {
					#containerresources
					#containername: "mysqld-exporter"
				}
			}
		}
	}
}

#memcachedsnip: {
	replicated
	topologySpreadConstraints
	type:        "object"
	description: "Memcached deployment configuration"
	properties: {
		memory: {
			type:        "integer"
			description: "Maximum memory used for the cache"
			default:     512
		}
		connections: {
			type:        "integer"
			description: "Maximum number of parallel connections"
			default:     1024
		}
		resources: {
			type: "object"
			properties: memcached: {
				#containerresources
				#containername: "memcached"
			}
		}
	}
}

#messagequeuesnip: {
	#description: string | *""
	storageconfig
	replicated
	topologySpreadConstraints
	type:        "object"
	description: "Configure the RabbitMQ instance\(#description)."
	properties: {
		tolerateNodeDown: #tolerateNodeDown
		resources: {
			type: "object"
			properties: rabbitmq: {
				#containerresources
				#containername: "RabbitMQ"
			}
		}
	}
}

#ref: {
	#description: string | *""
	type:         "object"
	required: ["name"]
	description: #description
	properties: {
		name: {
			type: "string"
		}
	}
}

#keystoneref: {
	#ref
	#description: "Keystone deployment to link this service to"
	properties: kind: {
		type:        "string"
		description: "Specify the kind of Keystone deployment to reference"
		enum: ["KeystoneDeployment", "ExternalKeystoneDeployment"]
		default: "KeystoneDeployment"
	}
}

#secretkeyref: {
	#ref
	properties: key: {
		type:        "string"
		description: "Key within the Secret to extract"
		default:     "password"
	}
}

#regionspec: {
	type:        "object"
	description: "Configure the Region to use for this OpenStack service."
	properties: {
		name: {
			type:        "string"
			default:     "RegionOne"
			description: "Name of the OpenStack region to connect with and to set up the own endpoints in."
		}
		parent: {
			type: "string"
		}
	}
}

#issuerspec: {
	type:        "object"
	description: "Reference an issuer for the certificates used internally by YAOOK (and, by default, also for external services unless overridden)."
	properties: {
		name: {
			type:        "string"
			default:     "ca-issuer"
			description: "Name of the cert-manager Issuer object"
		}
	}
}

eviction: {
	type:        "object"
	description: "Compute node eviction configuration"
	properties: {
		ironicNodeShutdown: {
			description: "Secret reference to Credentials for Ironic, containing: OS_AUTH_URL, OS_USERNAME, OS_PASSWORD, OS_PROJECT_NAME, OS_REGION_NAME, OS_INTERFACE"
			type:        "object"
			required: ["credentialsSecretRef"]
			properties: {
				credentialsSecretRef: #ref
			}
		}
		volumeLockDurationSeconds: {
			description: "wait for releasing the volume lock during the eviction"
			type:        "integer"
			default:     0
		}
		manager: {
			type: "object"
			default: {}
			properties: {
				"enabled": {
					type:        "boolean"
					default:     *false | bool
					description: "Defines if the Eviction Manager will be deployed."
				}
				"interval": {
					type:        "integer"
					default:     15
					description: "The interval at which the operator polls the Compute Node status in seconds."
				}
				"max_per_hour": {
					type:        "integer"
					default:     5
					description: "The number of computenodes that are allowed to change to down in the last hour, before the Eviction Manager stops creating eviction jobs."
				}
			}
		}
	}
}

#cronschedulesnip: {
	#description: string | *""
	type:         "string"
	pattern:      "((\\d+|([\\d*]+(\\/|-)\\d+)|\\*) ?){5}"
	description:  "\(#description)The schedule is given in standard cron notation."
	default:      "0 0 * * *"
}

backupspec: {
	type:        "object"
	description: string | *"Backup configuration"
	required: [
		"schedule",
	]
	properties: {
		schedule: {
			#cronschedulesnip
			#description: "Schedule in which to create backups. "
		}
		targets: {
			type:        "object"
			description: "Configure zero or more targets to save the backups to. If no targets are configured, backups will only be stored within the pod and are gone when the pod is deleted."
			anyOf: [
				{required: ["s3"]},
			]
			properties: {
				s3: {
					type:        "object"
					description: "Send backups into an S3 bucket."
					required: [
						"endpoint",
						"bucket",
						"credentialRef",
					]
					properties: {
						endpoint: {
							type:        "string"
							description: "URL to the S3 service"
						}
						bucket: {
							type:        "string"
							description: "Name of the bucket to store data into"
						}
						filePrefix: {
							type:        "string"
							description: "Prefix to add to the file name before storing it in the bucket"
						}
						credentialRef: {
							#ref
							#description: "Reference a Secret for the S3 credentials"
						}
						addressingStyle: {
							type:    "string"
							default: "virtual"
						}
					}
				}
			}
		}
	}
}

ovnBgpConfig: {
	type: "object"
	properties: {
		addressScopes: {
			type: "array"
			items: {
				type: "string"
			}
			description: "List of address scope IDs for the subnet, in case you are using `ovn_stretched_l2_bgp_driver`"
		}
		bridgeName: {
			type:        "string"
			description: "Name of the provider bridge to which the BGP interface should be added."
		}
		debug: {
			type:        "boolean"
			default:     *false | bool
			description: "Enable debug logging."
		}
		driver: {
			type: "string"
			enum: [
				"ovn_bgp_driver",
				"ovn_stretched_l2_bgp_driver",
				"ovn_evpn_driver",
			]
			default:     "ovn_stretched_l2_bgp_driver"
			description: "Name of the `ovn-bgp-agent` driver that can be used. Please check `ovn-bgp-agent` docs for further details."
		}
		localAS: {
			type:        "integer"
			minimum:     1
			maximum:     4294967295
			description: "The AS number to be used on the BGP agent side."
		}
		peers: {
			type: "object"
			additionalProperties: {
				type: "object"
				required: ["AS", "IP"]
				properties: {
					AS: {
						type:        "integer"
						minimum:     1
						maximum:     4294967295
						description: "The AS number to be peered with."
					}
					IP: {
						type:        "string"
						regex:       "^((^\\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\\s*$)|(^\\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)(\\.(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)){3}))|:)))(%.+)?\\s*$))$" // Shamelessly stolen from https://www.regextester.com/104038
						description: "List of peer IPs that have the same AS number and can be added to the same peer group."
					}
				}
			}
		}
		resources: {
			type: "object"
			properties: {
				"ovn-bgp-agent": #containerresources
				"frr-bgpd":      #containerresources
				"frr-zebra":     #containerresources
			}
		}
		syncInterval: {
			type:        "integer"
			default:     *120 | int
			description: "The interval time(seconds) when it should resync with southbound database."
		}

	}
}

#imagepullsecretsnip: {
	type:        "array"
	description: "References to image pull secrets which should be included in all Pods spawned directly or indirectly by this resource."
	items:       #ref
}

// To make normalization easier, only integer values are allowed.
containerreqlimits: {
	type: "object"
	properties: {
		cpu: {
			type:    "string"
			pattern: "^[1-9][0-9]*m?$"
		}
		memory: {
			type:    "string"
			pattern: "^[1-9][0-9]*(E|P|T|G|M|k|Ei|Pi|Ti|Gi|Mi|Ki)?$"
		}
	}
}

#containerresources: {
	#containername: string | *""
	type:           "object"
	description:    "Define resource requests/limits for the \(#containername) container"
	properties: {
		limits: containerreqlimits
		limits: description: "Define resource limits for the container"
		limits: properties: cpu: description:    "CPU time limit for the container"
		limits: properties: memory: description: "Memory limit for the container"
		requests: containerreqlimits
		requests: description: "Define resource requests for the container"
		requests: properties: cpu: description:    "CPU time request for the container"
		requests: properties: memory: description: "Memory request for the container"
	}
}

#tolerateNodeDown: {
	type:        "boolean"
	default:     false
	description: "If set, Kubernetes will not evict the pod after 300s in case of a NodeDown or Node unreachable."
}
