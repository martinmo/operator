// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#region

crd.#openstackcrd
{
	#group:    "network.yaook.cloud"
	#kind:     "NeutronL3Agent"
	#plural:   "neutronl3agents"
	#singular: "neutronl3agent"
	#shortnames: ["l3agent", "l3agents"]
	#releases: ["train"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"caConfigMapName",
			"messageQueue",
			"neutronL3AgentConfig",
			"novaRef",
			"targetRelease",
		]
		properties: {
			startupLimitMinutes: {
				type:    "integer"
				default: 60
			}
			keystoneRef: crd.#keystoneref
			novaRef:     crd.#ref
			caConfigMapName: type: "string"
			neutronConfig: {
				type:  "array"
				items: crd.#anyconfig
			}
			neutronL3AgentConfig: {
				type:  "array"
				items: crd.#anyconfig
			}
			neutronMetadataAgentConfig: {
				type:  "array"
				items: crd.#anyconfig
			}
			messageQueue: {
				type: "object"
				required: ["amqpServerRef"]
				properties: {
					amqpServerRef: crd.#ref
				}
			}
			state: {
				type:    "string"
				default: "Enabled"
				enum: [
					"Enabled",
					"Disabled",
					"DisabledAndCleared",
				]
			}
			resources: {
				type: "object"
				properties: {
					"neutron-l3-agent":       crd.#containerresources
					"neutron-metadata-agent": crd.#containerresources
					"l3-evict-job":           crd.#containerresources
				}
			}
			evictor: {
				type:        "object"
				description: "Configuration for the L3 eviction job"
				properties: {
					allowFallback: {
						type:        "boolean"
						default:     true
						description: "This setting allows the evict job to disable the agent if not all routers could be migrated because no agent was available for them. This violates the redundancy and is unsafe"
					}
					pollInterval: {
						type:        "integer"
						default:     5
						description: "Defines in seconds how long to wait between iterates until the next poll of the API."
					}
					maxParallelMigrations: {
						type:        "integer"
						default:     15
						description: "Defines how many routers may be evacuated in parallel per iteration"
					}
					respectAvailabilityZones: {
						type:        "boolean"
						default:     false
						description: "If enabled, excludes all agents where the router is already scheduled except the availibility zone of the agent being evicted."
					}
					verifySeconds: {
						type:        "integer"
						default:     0
						description: "If the value is greater than 0, allows the user to specify how long the API should be queried until the router is active, if this did not work in the defined time period an error is logged"
					}
				}
			}
		}
	}
	#schema: properties: status: properties: {
		conditions: items: properties: type: enum: [
			"Converged",
			"GarbageCollected",
			"Evicted",
			"Enabled",
			"BoundToNode",
			"RequiresRecreation",
		]
		eviction: {
			type:     "object"
			nullable: true
			required: ["reason"]
			properties: reason: type: "string"
		}
		state: {
			type:    "string"
			default: "Creating"
			enum: [
				"Creating",
				"Enabled",
				"Disabled",
				"Evicting",
				"DisabledAndCleared",
			]
		}
	}
	#additionalprintercolumns: [
		{
			name:        "Phase"
			type:        "string"
			description: "Current status of the Resource"
			jsonPath:    ".status.phase"
		},
		{
			name:        "Reason"
			type:        "string"
			description: "The reason for the current status"
			jsonPath:    ".status.conditions[?(@.type==\"Converged\")].reason"
		},
		{
			name:        "State"
			type:        "string"
			description: "The state of the service"
			jsonPath:    ".status.state"
		},
		{
			name:        "Evicting"
			type:        "string"
			description: "Eviction status"
			jsonPath:    ".status.eviction.mode"
		},
		{
			name:        "Enabled"
			type:        "string"
			description: "Enabled status"
			jsonPath:    ".status.conditions[?(@.type==\"Enabled\")].status"
		},
		{
			name:        "Requires Recreation"
			type:        "string"
			description: "Requires Recreation status"
			jsonPath:    ".status.conditions[?(@.type==\"RequiresRecreation\")].status"
		},
	]
}
