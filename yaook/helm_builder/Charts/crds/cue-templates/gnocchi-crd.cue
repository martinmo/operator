// Copyright (c) 2021 The Yaook Authors.
//
// This file is part of Yaook.
// See https://yaook.cloud for further info.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"yaook.cloud/crd"
)

crd.#openstackcrd
crd.#database
crd.#memcached
crd.#policy
crd.#region
crd.#issuer
{
	#kind:     "GnocchiDeployment"
	#plural:   "gnocchideployments"
	#singular: "gnocchideployment"
	#shortnames: ["gnocchid", "gnocchids"]
	#releases: ["train"]
	#schema: properties: spec: {
		required: [
			"keystoneRef",
			"api",
			"metricd",
			"gnocchiConfig",
			"backends",
			"database",
			"memcached",
			"region",
			"targetRelease",
			"issuerRef",
		]
		properties: {
			keystoneRef: crd.#keystoneref

			api:            crd.apiendpoint
			serviceMonitor: crd.#servicemonitor
			metricd:        crd.replicated
			metricd:        crd.topologySpreadConstraints

			database: {
				properties: {
					timeoutClient: {
						type:        "integer"
						default:     3600
						description: "There seems to be no corresponding option in the gnocchi configuration, so we raise the timeout here in order to avoid too much overhead from killed connections."
					}
				}
			}

			gnocchiConfig: crd.#anyconfig
			backends: {
				type: "object"
				oneOf: [
					{required: ["ceph"]},
					{required: ["s3"]},
				]
				properties: {
					ceph: {
						type: "object"
						required: ["keyringReference", "keyringUsername"]
						properties: {
							keyringReference: type: "string"
							keyringUsername: type:  "string"
							cephConfig: crd.#anyconfig
						}
					}
					s3: {
						type: "object"
						required: ["endpoint", "credentialRef", "bucketPrefix"]
						properties: {
							endpoint: type: "string"
							credentialRef: crd.#ref
							bucketPrefix: type: "string"
						}
					}
				}
			}

			api: properties: resources: {
				type: "object"
				properties: {
					"gnocchi-api":             crd.#containerresources
					"ssl-terminator":          crd.#containerresources
					"ssl-terminator-external": crd.#containerresources
					"ssl-terminator-internal": crd.#containerresources
					"service-reload":          crd.#containerresources
					"service-reload-external": crd.#containerresources
					"service-reload-internal": crd.#containerresources
				}
			}
			metricd: properties: resources: {
				type: "object"
				properties: "gnocchi-metricd": crd.#containerresources
			}
			jobResources: {
				type: "object"
				properties: "gnocchi-upgrade-job": crd.#containerresources
			}
		}
	}
}
