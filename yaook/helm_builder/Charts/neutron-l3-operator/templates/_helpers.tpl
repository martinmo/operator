{{- define "yaook.operator.name" -}}
neutron_l3
{{- end -}}

{{- define "yaook.operator.extraEnv" -}}
- name: YAOOK_NEUTRON_L3_AGENT_OP_JOB_IMAGE
  value: {{ include "yaook.operator-lib.image" . }}
{{- end -}}
