#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -euo pipefail
IFS=$'\n'

function retry {
  local max=3;
  local delay=5;
  for i in $(seq $max); do
    "$@" && return 0;
    if [[ $i -lt $max ]]; then
      echo "Command failed. Attempt $i/$max:";
      sleep $delay;
    fi
  done
  echo "The command has failed after $i attempts.";
  return 1;
}

nodes="$(retry kubectl get node -o 'jsonpath={ range .items[*] }{ .metadata.name }{ "\n" }{ end }')"
for node in $nodes; do
    # strip all yaook labels
    if yaook_label_strippers="$(echo $(retry kubectl get node "$node" -o json) | jq -r '(.metadata.labels | keys | map(. + "-"))[]' | grep yaook.cloud)"; then
        retry kubectl label node "$node" $yaook_label_strippers
    fi
done

# label all non-masters
non_master_nodes="$(retry kubectl get node -l '!node-role.kubernetes.io/master' -o 'jsonpath={ range .items[*] }{ .metadata.name }{ "\n" }{ end }')"
all_node_labels="any.yaook.cloud/api=true
operator.yaook.cloud/any=true
compute.yaook.cloud/nova-any-service=true
block-storage.yaook.cloud/cinder-any-service=true
key-manager.yaook.cloud/barbican-any-service=true
infra.yaook.cloud/any=true
ceilometer.yaook.cloud/ceilometer-any-service=true
gnocchi.yaook.cloud/metricd=true
heat.yaook.cloud/engine=true
network.yaook.cloud/neutron-northd=true
"
compute_node_labels="
compute.yaook.cloud/hypervisor=true"
network_node_labels="
network.yaook.cloud/neutron-l3-agent=true
network.yaook.cloud/neutron-dhcp-agent=true
network.yaook.cloud/neutron-bgp-dragent=true"
network_nodes="$(head -n3 <<<"$non_master_nodes")"
compute_nodes="$(tail -n3 <<<"$non_master_nodes")"
for node in $non_master_nodes; do
    retry kubectl label node "$node" $all_node_labels
done
for node in $compute_nodes; do
    retry kubectl label node "$node" $compute_node_labels
done
for node in $network_nodes; do
    retry kubectl label node "$node" $network_node_labels
    # for bgp the nodes need an annotation containing the IP, bgp interface should get.
    # as we don't really connect the nodes via bgp, the IP can stay the same.
    retry kubectl annotate node "$node" "bgp-interface-ip.network.yaook.cloud/test-bgp=10.1.2.3/24"
done
