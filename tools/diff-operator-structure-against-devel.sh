#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -euo pipefail
operator="$1"
shift
oldfile="$operator.$$.json"
current_branch=$(git rev-parse --abbrev-ref HEAD)
if [ "$current_branch" = 'HEAD' ]; then
    restore_to='HEAD@{1}'
else
    restore_to="$current_branch"
fi
compare_branch='origin/devel'

function restore() {
    if [ ! -z "$restore_to" ]; then
        git checkout "$restore_to"
    fi
    rm -f "$oldfile"
}

git checkout "$compare_branch"
trap restore EXIT INT TERM

export YAOOK_NOVA_COMPUTE_OP_JOB_IMAGE=dummy
export YAOOK_NEUTRON_DHCP_AGENT_OP_JOB_IMAGE=dummy
export YAOOK_NEUTRON_L3_AGENT_OP_JOB_IMAGE=dummy
export YAOOK_NEUTRON_BGP_DRAGENT_OP_JOB_IMAGE=dummy
export YAOOK_NEUTRON_OVN_AGENT_OP_JOB_IMAGE=dummy

python3 -m yaook.op "$operator" dump-structure > "$oldfile"
git checkout "$restore_to"
restore_to=''

python3 -m yaook.op -vvv "$operator" diff-structure "$oldfile"
