##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

# ---
# apiVersion: v1
# kind: ConfigMap
# metadata:
#   name: devpubkey
# data:
#   pubkey: <place your SSH public key here>
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: devpod-sa
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: devbinding
  labels:
    app.kubernetes.io/name: devpod
    app.kubernetes.io/version: "20210422.1042"
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: infra-operator
subjects:
- kind: ServiceAccount
  name: devpod-sa
  namespace: yaook
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: devbinding
  labels:
    app.kubernetes.io/name: devpod
    app.kubernetes.io/version: "20210422.1042"
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: infra-operator
subjects:
- kind: ServiceAccount
  name: devpod-sa
  namespace: yaook
---
apiVersion: v1
kind: Service
metadata:
  name: devpod
  labels:
    app.kubernetes.io/name: devpod
    app.kubernetes.io/version: "20210422.1042"
spec:
  type: NodePort
  ports:
  - port: 22
    protocol: TCP
  selector:
    app.kubernetes.io/name: devpod
    app.kubernetes.io/version: "20210422.1042"
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: devpod
  labels:
    app.kubernetes.io/name: devpod
    app.kubernetes.io/version: "20210422.1042"
spec:
  replicas: 1
  serviceName: devpod
  selector:
    matchLabels:
      app.kubernetes.io/name: devpod
      app.kubernetes.io/version: "20210422.1042"
  template:
    metadata:
      labels:
        app.kubernetes.io/name: devpod
        app.kubernetes.io/version: "20210422.1042"
    spec:
      containers:
      - name: dev
        image: debian:bullseye
        imagePullPolicy: IfNotPresent
        command:
        - bash
        - "-xec"
        - |
          set -euo pipefail
          apt-get update
          DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y --no-install-recommends
          DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends tini openssh-server rsync direnv python3-venv virtualenv libmariadb-dev build-essential less vim wget python3-dev
          mkdir -p /run/sshd
          echo 'ServerAliveCount 3' >> /etc/ssh/ssh_config.d/99-ping.conf
          echo 'ServerAliveInterval 30' >> /etc/ssh/ssh_config.d/99-ping.conf
          echo 'eval "$(direnv hook bash)"' >> /root/.bashrc
          printf 'export KUBERNETES_SERVICE_HOST=%q\n' "$KUBERNETES_SERVICE_HOST" >> /root/.bashrc
          printf 'export KUBERNETES_SERVICE_PORT=%q\n' "$KUBERNETES_SERVICE_PORT" >> /root/.bashrc
          pushd /tmp
          wget -O helm.tar.gz https://get.helm.sh/helm-v3.5.0-linux-amd64.tar.gz
          echo '3fff0354d5fba4c73ebd5db59a59db72f8a5bbe1117a0b355b0c2983e98db95b helm.tar.gz' > helm.tar.gz.sha256sum
          sha256sum -c helm.tar.gz.sha256sum
          tar -xf helm.tar.gz
          mv linux-amd64/helm /bin/helm
          rm -f helm.tar.gz helm.tar.gz.sha256sum
          popd
          mkdir -p /tmp/cue
          pushd /tmp/cue
          wget "https://github.com/cue-lang/cue/releases/download/v0.4.3/cue_v0.4.3_linux_amd64.tar.gz" -O cue.tgz
          tar -xf cue.tgz
          cp cue /bin/cue
          popd
          exec tini -- /usr/sbin/sshd -D
        volumeMounts:
        - name: devpod-data
          mountPath: /data
        - name: publickey
          mountPath: /root/.ssh/authorized_keys
          subPath: pubkey
        - name: sshd
          mountPath: /etc/ssh
      serviceAccount: devpod-sa
      volumes:
      - name: publickey
        configMap:
          name: devpubkey
          defaultMode: 0600
      - name: sshd
        emptyDir: {}
  volumeClaimTemplates:
  - metadata:
      name: devpod-data
      labels:
        app.kubernetes.io/name: devpod
        app.kubernetes.io/version: "20210422.1042"
    spec:
      accessModes:
      - ReadWriteOnce
      resources:
        requests:
          storage: 8Gi
      volumeMode: Filesystem
