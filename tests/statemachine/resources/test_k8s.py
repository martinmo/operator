import contextlib
import dataclasses
import itertools
import unittest
import uuid
from unittest.mock import sentinel

import kubernetes_asyncio.client as kclient

import yaook.statemachine.api_utils as api_utils
import yaook.statemachine.context as context
import yaook.statemachine.exceptions as exceptions
import yaook.statemachine.interfaces as interfaces
import yaook.statemachine.versioneddependencies as versioneddependencies

import yaook.statemachine.resources.k8s as k8s
import yaook.statemachine.resources.orchestration as orchestration

from .utils import (
    KubernetesObjectStateMock,
    ResourceInterfaceMock,
    TemplateTestMixin,
    SingleObjectMock,
)


class TestDependencyTemplateWrapper(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.resource_interface = ResourceInterfaceMock()
        self.resource = unittest.mock.Mock(k8s.KubernetesResource)
        self.resource.get = unittest.mock.AsyncMock()
        self.resource.get_with_instanced = unittest.mock.AsyncMock()
        self.resource.get_all = unittest.mock.AsyncMock()
        self.resource.get_resource_interface = unittest.mock.Mock([])
        self.resource.get_resource_interface.return_value = \
            self.resource_interface
        self.ctx = unittest.mock.Mock(["namespace", "with_instance",
                                       "instance", "without_instance"])
        self.object = unittest.mock.Mock
        self.ctx.without_instance.return_value.instance = None
        self.dtw = k8s.DependencyTemplateWrapper(
            self.ctx,
            self.resource,
        )

    async def test_instances_returns_get_all(self):
        self.resource.get_all.return_value = unittest.mock.sentinel.all_

        self.assertEqual(
            await self.dtw.instances(),
            unittest.mock.sentinel.all_,
        )

        self.resource.get_all.assert_called_once_with(self.ctx)

    async def test_resource_name_uses_get_to_find_name(self):
        ref = unittest.mock.Mock(["name", "namespace"])
        self.resource.get_with_instanced.return_value = ref, False

        self.assertEqual(
            await self.dtw.resource_name(),
            ref.name,
        )

        self.resource.get_with_instanced.assert_called_with(self.ctx)

    async def test_get_instance_not_relevant(self):
        self.resource.get_with_instanced.return_value = \
            unittest.mock.MagicMock(), False
        result = []
        self.dtw._ctx.instance = "instance1"
        result.append(await self.dtw._get())
        self.dtw._ctx.instance = "instance2"
        result.append(await self.dtw._get())
        self.assertEqual(result[0], result[1])
        self.resource.get_with_instanced.assert_called_once()

    async def test_get_instance_is_none(self):
        self.resource.get_with_instanced.return_value = \
            unittest.mock.MagicMock(), False
        result = []
        self.dtw._ctx.instance = None
        result.append(await self.dtw._get())
        result.append(await self.dtw._get())
        self.assertEqual(result[0], result[1])
        self.assertEqual(self.resource.get_with_instanced.call_count, 1)

    async def test_get_not_instance_is_relevant(self):
        self.resource.get_with_instanced.return_value = \
            unittest.mock.MagicMock(), True
        result = []
        self.dtw._ctx.instance = "instance1"
        result.append(await self.dtw._get())
        self.dtw._ctx.instance = "instance2"
        result.append(await self.dtw._get())
        self.assertEqual(result[0], result[1])
        self.assertEqual(self.resource.get_with_instanced.call_count, 2)

    async def test_last_update_timestamp_retrieves_timestamp_from_metadata(
            self):
        resource_ref_mock = unittest.mock.Mock(["name"])
        self.resource.get_with_instanced.return_value = (
            resource_ref_mock, False)
        with contextlib.ExitStack() as stack:
            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {
                "annotations": {
                    context.ANNOTATION_LAST_UPDATE:
                        unittest.mock.sentinel.last_update
                }
            }

            result = await self.dtw.last_update_timestamp()

        self.resource.get_with_instanced.assert_called_once_with(self.ctx)
        self.resource.get_resource_interface.assert_called_once_with(self.ctx)
        self.resource_interface.read.assert_called_once_with(
            self.ctx.namespace,
            resource_ref_mock.name,
        )
        extract_metadata.assert_called_once_with(
            self.resource_interface.read.return_value,
        )

        self.assertEqual(
            result,
            unittest.mock.sentinel.last_update,
        )

    async def test_instance_name_map_extracts_names_from_references(self):
        def make_ref(namespace, name):
            return kclient.V1ObjectReference(
                namespace=namespace,
                name=name,
            )

        self.resource.get_all.return_value = {
            "instance-0": make_ref("foo", "name-0"),
            "instance-1": make_ref("bar", "name-1"),
            "instance-2": make_ref("baz", "name-2"),
        }

        result = await self.dtw.instance_name_map()

        self.resource.get_all.assert_called_once_with(self.ctx)

        self.assertEqual(
            result,
            {
                "instance-0": "name-0",
                "instance-1": "name-1",
                "instance-2": "name-2",
            }
        )

    def test_labels_calls_labels(self):
        self.resource.labels = unittest.mock.Mock()
        self.resource.labels.return_value = unittest.mock.sentinel.labels

        result = self.dtw.labels()

        self.assertEqual(
            result,
            unittest.mock.sentinel.labels,
        )

        self.resource.labels.assert_called_once_with(self.ctx)


class TestKubernetesResource(unittest.IsolatedAsyncioTestCase):
    class KubernetesResourceTest(k8s.KubernetesResource):
        def _create_resource_interface(self, api_client):
            pass

        async def reconcile(self, ctx, dependencies):
            pass

        async def get(self, ctx):
            pass

        async def get_with_instanced(self, ctx):
            pass

        async def get_all(self, ctx):
            pass

        async def cleanup_orphans(self, ctx):
            pass

        async def delete(self, ctx):
            pass

    def setUp(self):
        self.st = self.KubernetesResourceTest(component="test-component")

    def test_labels_adds_component_to_base_labels(self):
        base_labels = {
            "foo": "bar",
        }

        ctx = unittest.mock.Mock()
        ctx.base_label_match.return_value = base_labels

        result = self.st.labels(ctx)
        self.assertEqual(
            result,
            {
                "foo": "bar",
                context.LABEL_COMPONENT: "test-component",
            }
        )

    async def test_adopt_object_applies_labels(self):
        obj = {
            "metadata": {},
        }
        ctx = unittest.mock.Mock()

        labels = {
            "foo": "bar",
            "baz": "fnord",
        }

        with unittest.mock.patch.object(self.st, "labels") as labels_mock:
            labels_mock.return_value = labels
            await self.st.adopt_object(ctx, obj)

        self.assertEqual(
            obj["metadata"]["labels"],
            labels,
        )

    async def test_adopt_object_overwrites_conflicting_labels(self):
        obj = {
            "metadata": {
                "labels": {
                    "foo": "notbar",
                },
            },
        }
        ctx = unittest.mock.Mock()

        labels = {
            "foo": "bar",
            "baz": "fnord",
        }

        with unittest.mock.patch.object(self.st, "labels") as labels_mock:
            labels_mock.return_value = labels
            await self.st.adopt_object(ctx, obj)

        self.assertEqual(
            obj["metadata"]["labels"],
            labels,
        )

    async def test_adopt_object_keeps_unrelated_labels(self):
        obj = {
            "metadata": {
                "labels": {
                    "existing": "stays",
                },
            },
        }
        ctx = unittest.mock.Mock()

        labels = {
            "foo": "bar",
            "baz": "fnord",
        }

        with unittest.mock.patch.object(self.st, "labels") as labels_mock:
            labels_mock.return_value = labels
            await self.st.adopt_object(ctx, obj)

        self.assertIn(
            "existing",
            obj["metadata"]["labels"].keys(),
        )

        self.assertIn(
            "stays",
            obj["metadata"]["labels"].values(),
        )

    async def test_adopt_object_adds_owner_reference(self):
        obj = {}
        ctx = unittest.mock.Mock()

        with unittest.mock.patch.object(self.st, "labels") as labels_mock:
            labels_mock.return_value = {}
            await self.st.adopt_object(ctx, obj)

        self.assertSequenceEqual(
            obj["metadata"]["ownerReferences"],
            [
                {
                    "apiVersion": ctx.parent_api_version,
                    "name": ctx.parent_name,
                    "kind": ctx.parent_kind,
                    "uid": ctx.parent_uid,
                    "blockOwnerDeletion": True,
                    "controller": False,
                }
            ]
        )

    async def test_adopt_object_adds_creator_labels(self):
        obj = {}
        ctx = unittest.mock.Mock()
        ctx.parent = {
            "metadata": {
                "labels": {
                    context.LABEL_PARENT_NAME:  sentinel.parent_name,
                    context.LABEL_PARENT_PLURAL:  sentinel.parent_plural,
                    context.LABEL_PARENT_VERSION:  sentinel.parent_version,
                    context.LABEL_PARENT_GROUP:  sentinel.parent_group,
                }
            }
        }

        with unittest.mock.patch.object(self.st, "labels") as labels_mock:
            labels_mock.return_value = {}
            await self.st.adopt_object(ctx, obj)

        self.assertEqual(
            obj["metadata"]["labels"][context.LABEL_CREATOR_NAME],
            sentinel.parent_name)
        self.assertEqual(
            obj["metadata"]["labels"][context.LABEL_CREATOR_PLURAL],
            sentinel.parent_plural)
        self.assertEqual(
            obj["metadata"]["labels"][context.LABEL_CREATOR_VERSION],
            sentinel.parent_version)
        self.assertEqual(
            obj["metadata"]["labels"][context.LABEL_CREATOR_GROUP],
            sentinel.parent_group)


    async def test_adopt_object_keeps_unrelated_owner_references_in_place(self):  # NOQA
        existing_owner_ref = {
            "apiVersion": "test.yaook.cloud/v1",
            "kind": "NotThisObject",
            "name": "unrelated-object",
            "uid": str(uuid.uuid4()),
            "blockOwnerDeletion": True,
            "controller": False,
        }

        obj = {
            "metadata": {
                "ownerReferences": [dict(existing_owner_ref)],
            }
        }
        ctx = unittest.mock.Mock()

        with unittest.mock.patch.object(self.st, "labels") as labels_mock:
            labels_mock.return_value = {}
            await self.st.adopt_object(ctx, obj)

        self.assertSequenceEqual(
            obj["metadata"]["ownerReferences"],
            [
                existing_owner_ref,
                {
                    "apiVersion": ctx.parent_api_version,
                    "name": ctx.parent_name,
                    "kind": ctx.parent_kind,
                    "uid": ctx.parent_uid,
                    "blockOwnerDeletion": True,
                    "controller": False,
                }
            ]
        )

    async def test_adopt_object_does_not_duplicate_matching_owner_ref(self):
        existing_owner_ref = {
            "apiVersion": "test.yaook.cloud/v1",
            "kind": "NotThisObject",
            "name": "unrelated-object",
            "uid": str(uuid.uuid4()),
            "blockOwnerDeletion": True,
            "controller": False,
        }

        ctx = unittest.mock.Mock()
        matching_owner_ref = {
            "apiVersion": ctx.parent_api_version,
            "kind": ctx.parent_kind,
            "name": unittest.mock.sentinel.some_name,
            "uid": unittest.mock.sentinel.some_uuid,
            "blockOwnerDeletion": unittest.mock.sentinel.blockOwnerDeletion,
            "controller": unittest.mock.sentinel.controller,
        }

        obj = {
            "metadata": {
                "ownerReferences": [dict(existing_owner_ref),
                                    dict(matching_owner_ref)],
            }
        }

        with unittest.mock.patch.object(self.st, "labels") as labels_mock:
            labels_mock.return_value = {}
            await self.st.adopt_object(ctx, obj)

        self.assertSequenceEqual(
            obj["metadata"]["ownerReferences"],
            [
                existing_owner_ref,
                {
                    "apiVersion": ctx.parent_api_version,
                    "name": ctx.parent_name,
                    "kind": ctx.parent_kind,
                    "uid": ctx.parent_uid,
                    "blockOwnerDeletion": True,
                    "controller": False,
                }
            ]
        )


class TestKubernetesObject(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.kos = KubernetesObjectStateMock()

    async def test_cleanup_orphans_uses_list_and_filters_by_protect(self):
        ctx = unittest.mock.Mock(["namespace", "api_client", "logger"])
        labels = {
            "foo": unittest.mock.sentinel.foo,
            "bar": unittest.mock.sentinel.bar,
        }
        selector = api_utils.LabelSelector(
            match_labels=labels,
            match_expressions=[api_utils.LabelExpression(
                key=context.LABEL_ORPHANED,
                operator=api_utils.SelectorOperator.EXISTS,
                values=None,
            )],
        ).as_api_selector()

        def generate_metadata():
            yield {
                "namespace": unittest.mock.sentinel.ns1,
                "name": unittest.mock.sentinel.name1,
            }
            yield {
                "namespace": unittest.mock.sentinel.ns2,
                "name": unittest.mock.sentinel.name2,
            }
            yield {
                "namespace": unittest.mock.sentinel.ns3,
                "name": unittest.mock.sentinel.name3,
            }
            yield {
                "namespace": unittest.mock.sentinel.ns4,
                "name": unittest.mock.sentinel.name4,
            }

        self.kos.resource_interface_mock.api_version = "test.yaook.cloud/v1"
        self.kos.resource_interface_mock.plural = "mocked"

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.kos, "labels",
            ))
            labels_mock.return_value = dict(labels)

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = generate_metadata()

            self.kos.resource_interface_mock.list_.return_value = [
                unittest.mock.sentinel.res1,
                unittest.mock.sentinel.res2,
                unittest.mock.sentinel.res3,
                unittest.mock.sentinel.res4,
            ]

            await self.kos.cleanup_orphans(
                ctx,
                {
                    ("test.yaook.cloud/v1", "mocked"): {
                        (unittest.mock.sentinel.ns2,
                         unittest.mock.sentinel.name2),
                        (unittest.mock.sentinel.ns3,
                         unittest.mock.sentinel.name3),
                    },
                }
            )

        labels_mock.assert_called_once_with(ctx)

        self.kos.resource_interface_mock.list_.\
            assert_awaited_once_with(
                ctx.namespace,
                label_selector=selector,
            )

        self.assertCountEqual(
            extract_metadata.mock_calls,
            [
                unittest.mock.call(unittest.mock.sentinel.res1),
                unittest.mock.call(unittest.mock.sentinel.res2),
                unittest.mock.call(unittest.mock.sentinel.res3),
                unittest.mock.call(unittest.mock.sentinel.res4),
            ],
        )

        self.assertCountEqual(
            self.kos.resource_interface_mock.delete.await_args_list,
            [
                unittest.mock.call(unittest.mock.sentinel.ns1,
                                   unittest.mock.sentinel.name1),
                unittest.mock.call(unittest.mock.sentinel.ns4,
                                   unittest.mock.sentinel.name4),
            ],
        )

    async def test_cleanup_orphans_uses_protect_of_matching_key(self):
        ctx = unittest.mock.Mock(["namespace", "api_client", "logger"])
        labels = {
            "foo": unittest.mock.sentinel.foo,
            "bar": unittest.mock.sentinel.bar,
        }
        selector = api_utils.LabelSelector(
            match_labels=labels,
            match_expressions=[api_utils.LabelExpression(
                key=context.LABEL_ORPHANED,
                operator=api_utils.SelectorOperator.EXISTS,
                values=None,
            )],
        ).as_api_selector()

        def generate_metadata():
            yield {
                "namespace": unittest.mock.sentinel.ns1,
                "name": unittest.mock.sentinel.name1,
            }
            yield {
                "namespace": unittest.mock.sentinel.ns2,
                "name": unittest.mock.sentinel.name2,
            }
            yield {
                "namespace": unittest.mock.sentinel.ns3,
                "name": unittest.mock.sentinel.name3,
            }
            yield {
                "namespace": unittest.mock.sentinel.ns4,
                "name": unittest.mock.sentinel.name4,
            }

        self.kos.resource_interface_mock.api_version = "test.yaook.cloud/v1"
        self.kos.resource_interface_mock.plural = "mocked"

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.kos, "labels",
            ))
            labels_mock.return_value = dict(labels)

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = generate_metadata()

            self.kos.resource_interface_mock.list_.return_value = [
                unittest.mock.sentinel.res1,
                unittest.mock.sentinel.res2,
                unittest.mock.sentinel.res3,
                unittest.mock.sentinel.res4,
            ]

            await self.kos.cleanup_orphans(
                ctx,
                {
                    ("test.yaook.cloud/v1", "other"): {
                        (unittest.mock.sentinel.ns1,
                         unittest.mock.sentinel.name1),
                        (unittest.mock.sentinel.ns3,
                         unittest.mock.sentinel.name3),
                    },
                    ("test.yaook.cloud/v1", "mocked"): {
                        (unittest.mock.sentinel.ns2,
                         unittest.mock.sentinel.name2),
                        (unittest.mock.sentinel.ns3,
                         unittest.mock.sentinel.name3),
                    },
                }
            )

        labels_mock.assert_called_once_with(ctx)

        self.kos.resource_interface_mock.list_.\
            assert_awaited_once_with(
                ctx.namespace,
                label_selector=selector,
            )

        self.assertCountEqual(
            extract_metadata.mock_calls,
            [
                unittest.mock.call(unittest.mock.sentinel.res1),
                unittest.mock.call(unittest.mock.sentinel.res2),
                unittest.mock.call(unittest.mock.sentinel.res3),
                unittest.mock.call(unittest.mock.sentinel.res4),
            ],
        )

        self.assertCountEqual(
            self.kos.resource_interface_mock.delete.await_args_list,
            [
                unittest.mock.call(unittest.mock.sentinel.ns1,
                                   unittest.mock.sentinel.name1),
                unittest.mock.call(unittest.mock.sentinel.ns4,
                                   unittest.mock.sentinel.name4),
            ],
        )

    async def test_cleanup_orphans_uses_delete_collection_if_none_protected(self):  # NOQA
        ctx = unittest.mock.Mock(["namespace", "api_client", "logger"])
        labels = {
            "foo": unittest.mock.sentinel.foo,
            "bar": unittest.mock.sentinel.bar,
        }
        selector = api_utils.LabelSelector(
            match_labels=labels,
            match_expressions=[api_utils.LabelExpression(
                key=context.LABEL_ORPHANED,
                operator=api_utils.SelectorOperator.EXISTS,
                values=None,
            )],
        ).as_api_selector()

        self.kos.resource_interface_mock.api_version = "test.yaook.cloud/v1"
        self.kos.resource_interface_mock.plural = "mocked"

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.kos, "labels",
            ))
            labels_mock.return_value = dict(labels)

            await self.kos.cleanup_orphans(
                ctx,
                {
                    ("test.yaook.cloud/v1", "other"): {
                        (unittest.mock.sentinel.ns1,
                         unittest.mock.sentinel.name1),
                        (unittest.mock.sentinel.ns3,
                         unittest.mock.sentinel.name3),
                    },
                    ("test.yaook.cloud/v1", "mocked"): set(),
                }
            )

        labels_mock.assert_called_once_with(ctx)

        self.kos.resource_interface_mock.list_.assert_not_called()
        self.kos.resource_interface_mock.delete_collection\
            .assert_awaited_once_with(
                ctx.namespace,
                label_selector=selector,
            )

    async def test_cleanup_orphans_can_handle_missing_key_in_protect(self):
        ctx = unittest.mock.Mock(["namespace", "api_client", "logger"])
        labels = {
            "foo": unittest.mock.sentinel.foo,
            "bar": unittest.mock.sentinel.bar,
        }
        selector = api_utils.LabelSelector(
            match_labels=labels,
            match_expressions=[api_utils.LabelExpression(
                key=context.LABEL_ORPHANED,
                operator=api_utils.SelectorOperator.EXISTS,
                values=None,
            )],
        ).as_api_selector()

        self.kos.resource_interface_mock.api_version = "test.yaook.cloud/v1"
        self.kos.resource_interface_mock.plural = "mocked"

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.kos, "labels",
            ))
            labels_mock.return_value = dict(labels)

            await self.kos.cleanup_orphans(
                ctx,
                {
                    ("test.yaook.cloud/v1", "other"): {
                        (unittest.mock.sentinel.ns1,
                         unittest.mock.sentinel.name1),
                        (unittest.mock.sentinel.ns3,
                         unittest.mock.sentinel.name3),
                    },
                }
            )

        labels_mock.assert_called_once_with(ctx)

        self.kos.resource_interface_mock.list_.assert_not_called()
        self.kos.resource_interface_mock.delete_collection\
            .assert_awaited_once_with(
                ctx.namespace,
                label_selector=selector,
            )


class TestSingleObject(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.ctx = unittest.mock.Mock([
            "api_client",
            "namespace",
            "parent_api_version",
            "parent_kind",
            "parent_uid",
            "parent_name",
            "logger",
            "field_manager",
        ])
        self.ctx.base_label_match = unittest.mock.Mock([])
        self.ctx.base_label_match.return_value = {}
        self.sos = SingleObjectMock(
            component="test-component",
        )
        self.ri = self.sos.get_resource_interface(unittest.mock.Mock())

    def _default_extract_metadata_mock(self):
        def wrapper(obj):
            print("side_effect")
            return {
                "_object": obj,
            }

        result = unittest.mock.Mock([])
        result.side_effect = wrapper
        return result

    def test___init___sets_default(self):
        self.assertTrue(self.sos._ignore_deleted_resources)

    async def test__get_current_finds_by_labels_and_returns_object(self):
        ctx = unittest.mock.Mock(["api_client", "namespace"])
        self.ri.list_.return_value = [unittest.mock.sentinel.object_]

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.sos, "labels",
            ))
            labels_mock.return_value = unittest.mock.sentinel.labels

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
                new=self._default_extract_metadata_mock(),
            ))
            print(extract_metadata("foo"))

            result = await self.sos._get_current(ctx)

        labels_mock.assert_called_once_with(ctx)
        self.ri.list_.assert_called_once_with(
            ctx.namespace,
            label_selector=unittest.mock.sentinel.labels,
        )

        self.assertEqual(
            result,
            unittest.mock.sentinel.object_,
        )

    async def test__get_current_raises_on_no_result(self):
        ctx = unittest.mock.Mock(["api_client", "namespace",
                                  "parent_kind",
                                  "parent_api_version",
                                  "parent_name",
                                  "instance"])
        self.ri.list_.return_value = interfaces.ListWrapper(
            [],
            resource_version=None,
        )

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.sos, "labels",
            ))
            labels_mock.return_value = unittest.mock.sentinel.labels

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
                new=self._default_extract_metadata_mock(),
            ))

            with self.assertRaises(exceptions.ResourceNotPresent):
                await self.sos._get_current(ctx)

        labels_mock.assert_called_once_with(ctx)
        self.ri.list_.assert_called_once_with(
            ctx.namespace,
            label_selector=unittest.mock.sentinel.labels,
        )

    async def test__get_current_includes_resource_version_on_no_result(self):
        ctx = unittest.mock.Mock(["api_client", "namespace",
                                  "parent_kind",
                                  "parent_api_version",
                                  "parent_name",
                                  "instance"])
        self.ri.list_.return_value = interfaces.ListWrapper(
            [],
            resource_version=unittest.mock.sentinel.resource_version,
        )

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.sos, "labels",
            ))
            labels_mock.return_value = unittest.mock.sentinel.labels

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
                new=self._default_extract_metadata_mock(),
            ))

            with self.assertRaises(exceptions.ResourceNotPresent) as info:
                await self.sos._get_current(ctx)

        labels_mock.assert_called_once_with(ctx)
        self.ri.list_.assert_called_once_with(
            ctx.namespace,
            label_selector=unittest.mock.sentinel.labels,
        )

        self.assertEqual(
            info.exception.resource_version,
            unittest.mock.sentinel.resource_version
        )

    async def test__get_current_raises_on_multiple_results(self):
        ctx = unittest.mock.Mock(["api_client", "namespace",
                                  "parent_kind",
                                  "parent_api_version",
                                  "parent_name",
                                  "instance"])
        self.ri.list_.return_value = [
            unittest.mock.sentinel.object1,
            unittest.mock.sentinel.object2,
        ]

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.sos, "labels",
            ))
            labels_mock.return_value = unittest.mock.sentinel.labels

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
                new=self._default_extract_metadata_mock(),
            ))

            with self.assertRaises(exceptions.AmbiguousRequest):
                await self.sos._get_current(ctx)

        labels_mock.assert_called_once_with(ctx)
        self.ri.list_.assert_called_once_with(
            ctx.namespace,
            label_selector=unittest.mock.sentinel.labels,
        )

    async def test__get_current_ignores_objects_with_deletionTimestamp(self):
        ctx = unittest.mock.Mock(["api_client", "namespace"])
        self.ri.list_.return_value = [
            (unittest.mock.sentinel.object1, True),
            (unittest.mock.sentinel.object3, False),
            (unittest.mock.sentinel.object2, True),
        ]

        def extract_metadata_proc(obj):
            opaque, deleted = obj
            return {
                "deletionTimestamp":
                    unittest.mock.sentinel.deleted if deleted else None,
                "_object": opaque,
            }

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.sos, "labels",
            ))
            labels_mock.return_value = unittest.mock.sentinel.labels

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = extract_metadata_proc

            result = await self.sos._get_current(ctx)

        labels_mock.assert_called_once_with(ctx)
        self.ri.list_.assert_called_once_with(
            ctx.namespace,
            label_selector=unittest.mock.sentinel.labels,
        )

        self.assertEqual(result, (unittest.mock.sentinel.object3, False))

    async def test__get_current_takes_objects_with_deletionTimestamp_if_requested(self):  # noqa: E501
        ctx = unittest.mock.Mock(["api_client", "namespace"])
        self.sos._ignore_deleted_resources = False
        self.ri.list_.return_value = [
            (unittest.mock.sentinel.object1),
        ]

        def extract_metadata_proc(obj):
            return {
                "deletionTimestamp": unittest.mock.sentinel.deleted,
                "_object": obj,
            }

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.sos, "labels",
            ))
            labels_mock.return_value = unittest.mock.sentinel.labels

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = extract_metadata_proc

            result = await self.sos._get_current(ctx)

        labels_mock.assert_called_once_with(ctx)
        self.ri.list_.assert_called_once_with(
            ctx.namespace,
            label_selector=unittest.mock.sentinel.labels,
        )

        self.assertEqual(result, unittest.mock.sentinel.object1)

    async def test__get_current_raises_if_all_objects_are_deleted(self):
        ctx = unittest.mock.Mock(["api_client", "namespace",
                                  "parent_kind",
                                  "parent_api_version",
                                  "parent_name",
                                  "instance"])
        self.ri.list_.return_value = interfaces.ListWrapper(
            [
                (unittest.mock.sentinel.object1, True),
                (unittest.mock.sentinel.object3, True),
                (unittest.mock.sentinel.object2, True),
            ],
            resource_version=unittest.mock.sentinel.resource_version,
        )

        def extract_metadata_proc(obj):
            opaque, deleted = obj
            return {
                "deletionTimestamp":
                    unittest.mock.sentinel.deleted if deleted else None,
                "_object": opaque,
            }

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.sos, "labels",
            ))
            labels_mock.return_value = unittest.mock.sentinel.labels

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = extract_metadata_proc

            with self.assertRaises(exceptions.ResourceNotPresent) as info:
                await self.sos._get_current(ctx)

        labels_mock.assert_called_once_with(ctx)
        self.ri.list_.assert_called_once_with(
            ctx.namespace,
            label_selector=unittest.mock.sentinel.labels,
        )

        self.assertEqual(
            info.exception.resource_version,
            unittest.mock.sentinel.resource_version
        )

    async def test__get_current_does_not_return_orphaned_objects(self):
        ctx = unittest.mock.Mock(["api_client", "namespace"])
        self.ri.list_.return_value = [
            (unittest.mock.sentinel.object1, True),
            (unittest.mock.sentinel.object3, False),
            (unittest.mock.sentinel.object2, True),
        ]

        def extract_metadata_proc(obj):
            opaque, orphaned = obj
            labels = {}
            if orphaned:
                labels[context.LABEL_ORPHANED] = unittest.mock.sentinel.foo
            return {
                "deletionTimestamp": None,
                "labels": labels,
                "_object": opaque,
            }

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.sos, "labels",
            ))
            labels_mock.return_value = unittest.mock.sentinel.labels

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = extract_metadata_proc

            result = await self.sos._get_current(ctx)

        labels_mock.assert_called_once_with(ctx)
        self.ri.list_.assert_called_once_with(
            ctx.namespace,
            label_selector=unittest.mock.sentinel.labels,
        )

        self.assertEqual(result, (unittest.mock.sentinel.object3, False))

    async def test__get_current_raises_if_all_objects_are_deleted_or_orphaned(self):  # NOQA
        ctx = unittest.mock.Mock(["api_client", "namespace",
                                  "parent_kind",
                                  "parent_api_version",
                                  "parent_name",
                                  "instance"])
        self.ri.list_.return_value = interfaces.ListWrapper(
            [
                (unittest.mock.sentinel.object1, True, False),
                (unittest.mock.sentinel.object3, False, True),
                (unittest.mock.sentinel.object2, True, True),
            ],
            resource_version=unittest.mock.sentinel.resource_version,
        )

        def extract_metadata_proc(obj):
            opaque, deleted, orphaned = obj
            labels = {}
            if orphaned:
                labels[context.LABEL_ORPHANED] = unittest.mock.sentinel.foo
            return {
                "deletionTimestamp":
                    unittest.mock.sentinel.deleted if deleted else None,
                "labels": labels,
                "_object": opaque,
            }

        with contextlib.ExitStack() as stack:
            labels_mock = stack.enter_context(unittest.mock.patch.object(
                self.sos, "labels",
            ))
            labels_mock.return_value = unittest.mock.sentinel.labels

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.side_effect = extract_metadata_proc

            with self.assertRaises(exceptions.ResourceNotPresent) as info:
                await self.sos._get_current(ctx)

        labels_mock.assert_called_once_with(ctx)
        self.ri.list_.assert_called_once_with(
            ctx.namespace,
            label_selector=unittest.mock.sentinel.labels,
        )

        self.assertEqual(
            info.exception.resource_version,
            unittest.mock.sentinel.resource_version
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.extract_metadata")
    async def test_get_uses__get_current(self, extract_metadata):
        with unittest.mock.patch.object(
                self.sos,
                "_get_current",
                new=unittest.mock.AsyncMock()) as _get_current:
            _get_current.return_value = unittest.mock.sentinel.object_

            extract_metadata.return_value = {
                "name": unittest.mock.sentinel.object_name,
                "namespace": unittest.mock.sentinel.object_namespace,
            }

            result = await self.sos.get(unittest.mock.sentinel.ctx)

        _get_current.assert_called_once_with(unittest.mock.sentinel.ctx)
        extract_metadata.assert_called_once_with(
            unittest.mock.sentinel.object_
        )

        self.assertEqual(result.name, unittest.mock.sentinel.object_name)
        self.assertEqual(result.namespace,
                         unittest.mock.sentinel.object_namespace)

    @unittest.mock.patch("yaook.statemachine.api_utils.extract_metadata")
    async def test_get_falls_back_to_instanceless_context_if_not_found(
            self,
            extract_metadata):
        ctx = unittest.mock.Mock()
        ctx.without_instance.return_value = sentinel.magic_context

        def side_effector(ctx):
            if ctx == sentinel.magic_context:
                return sentinel.object_
            raise exceptions.ResourceNotPresent(
                sentinel.component,
                ctx,
            )

        with unittest.mock.patch.object(
                self.sos,
                "_get_current",
                new=unittest.mock.AsyncMock()) as _get_current:
            _get_current.side_effect = side_effector

            extract_metadata.return_value = {
                "name": unittest.mock.sentinel.object_name,
                "namespace": unittest.mock.sentinel.object_namespace,
            }

            result = await self.sos.get(ctx)

        self.assertSequenceEqual(
            _get_current.mock_calls,
            [
                unittest.mock.call(ctx),
                unittest.mock.call(sentinel.magic_context),
            ]
        )
        extract_metadata.assert_called_once_with(sentinel.object_)

        self.assertEqual(result.name, sentinel.object_name)
        self.assertEqual(result.namespace, sentinel.object_namespace)

    @unittest.mock.patch("yaook.statemachine.api_utils.extract_metadata")
    async def test_get_reraises_original_exception_with_original_context_if_fallback_get_fails(  # noqa
            self,
            extract_metadata):
        ctx = unittest.mock.Mock()
        ctx.without_instance.return_value = sentinel.magic_context

        inner_exc = ValueError("inner context")
        outer_exc = exceptions.ResourceNotPresent(
            sentinel.component,
            ctx,
        )

        def side_effector(ctx):
            if ctx == sentinel.magic_context:
                raise exceptions.ResourceNotPresent(
                    sentinel.component,
                    unittest.mock.Mock(),
                )
            try:
                raise inner_exc
            except ValueError:
                raise outer_exc

        with unittest.mock.patch.object(
                self.sos,
                "_get_current",
                new=unittest.mock.AsyncMock()) as _get_current:
            _get_current.side_effect = side_effector

            extract_metadata.return_value = {
                "name": unittest.mock.sentinel.object_name,
                "namespace": unittest.mock.sentinel.object_namespace,
            }

            with self.assertRaises(exceptions.ResourceNotPresent) as exc_info:
                await self.sos.get(ctx)

        self.assertEqual(exc_info.exception, outer_exc)
        self.assertEqual(exc_info.exception.__context__, inner_exc)

    @unittest.mock.patch("yaook.statemachine.api_utils.extract_metadata")
    @unittest.mock.patch("dataclasses.replace")
    async def test_get_all_finds_by_labels_and_returns_all(
            self,
            dc_replace,
            extract_metadata):
        ctx = unittest.mock.Mock(["api_client", "namespace"])
        uninstanced_ctx = unittest.mock.Mock(["api_client", "namespace"])
        self.ri.list_.return_value = [
            unittest.mock.sentinel.object1,
            unittest.mock.sentinel.object2,
        ]

        def generate_metadata():
            for i in itertools.count():
                yield {
                    "namespace": "namespace-{}".format(i),
                    "name": "name-{}".format(i),
                    "labels": {
                        context.LABEL_INSTANCE: "instance-{}".format(i),
                    }
                }

        extract_metadata.side_effect = generate_metadata()
        dc_replace.return_value = uninstanced_ctx

        labels = {
            "foo": unittest.mock.sentinel.label1,
            "bar": unittest.mock.sentinel.label2,
        }

        with unittest.mock.patch.object(self.sos, "labels") as labels_mock:
            labels_mock.return_value = labels
            result = await self.sos.get_all(ctx)

        labels_mock.assert_called_once_with(uninstanced_ctx)
        self.ri.list_.assert_called_once_with(
            ctx.namespace,
            label_selector=api_utils.LabelSelector(
                match_labels=labels,
                match_expressions=[
                    api_utils.LabelExpression(
                        key=context.LABEL_ORPHANED,
                        operator=api_utils.SelectorOperator.NOT_EXISTS,
                        values=None,
                    ),
                ]
            ).as_api_selector(),
        )

        self.assertCountEqual(
            extract_metadata.mock_calls,
            [
                unittest.mock.call(unittest.mock.sentinel.object1),
                unittest.mock.call(unittest.mock.sentinel.object2),
            ]
        )

        self.assertEqual(
            {k: (ref.namespace, ref.name) for k, ref in result.items()},
            {
                "instance-0": ("namespace-0", "name-0"),
                "instance-1": ("namespace-1", "name-1"),
            },
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.extract_metadata")
    @unittest.mock.patch("dataclasses.replace")
    async def test_get_all_ignores_deleted_objects(
            self,
            dc_replace,
            extract_metadata):
        ctx = unittest.mock.Mock(["api_client", "namespace"])
        uninstanced_ctx = unittest.mock.Mock(["api_client", "namespace"])
        self.ri.list_.return_value = [
            unittest.mock.sentinel.object1,
            unittest.mock.sentinel.object2,
            unittest.mock.sentinel.object3,
            unittest.mock.sentinel.object4,
        ]

        def generate_metadata():
            for i in itertools.count():
                deleted = i % 2 == 1
                yield {
                    "namespace": "namespace-{}".format(i),
                    "name": "name-{}".format(i),
                    "labels": {
                        context.LABEL_INSTANCE: "instance-{}".format(i // 2),
                    },
                    "deletionTimestamp":
                        unittest.mock.sentinel.deleted if deleted else None
                }

        extract_metadata.side_effect = generate_metadata()
        dc_replace.return_value = uninstanced_ctx

        labels = {
            "foo": unittest.mock.sentinel.label1,
            "bar": unittest.mock.sentinel.label2,
        }

        with unittest.mock.patch.object(self.sos, "labels") as labels_mock:
            labels_mock.return_value = labels
            result = await self.sos.get_all(ctx)

        labels_mock.assert_called_once_with(uninstanced_ctx)
        self.ri.list_.assert_called_once_with(
            ctx.namespace,
            label_selector=api_utils.LabelSelector(
                match_labels=labels,
                match_expressions=[
                    api_utils.LabelExpression(
                        key=context.LABEL_ORPHANED,
                        operator=api_utils.SelectorOperator.NOT_EXISTS,
                        values=None,
                    ),
                ]
            ).as_api_selector(),
        )

        self.assertCountEqual(
            extract_metadata.mock_calls,
            [
                unittest.mock.call(unittest.mock.sentinel.object1),
                unittest.mock.call(unittest.mock.sentinel.object2),
                unittest.mock.call(unittest.mock.sentinel.object3),
                unittest.mock.call(unittest.mock.sentinel.object4),
            ]
        )

        self.assertEqual(
            {k: (ref.namespace, ref.name) for k, ref in result.items()},
            {
                "instance-0": ("namespace-0", "name-0"),
                "instance-1": ("namespace-2", "name-2"),
            },
        )

    @unittest.mock.patch(
        "yaook.statemachine.api_utils.generate_update_timestamp")
    async def test__add_annotations_adds_update_timestamp(
            self,
            generate_update_timestamp):
        generate_update_timestamp.return_value = unittest.mock.sentinel.ts

        obj = {}
        self.sos._add_annotations(obj)
        self.assertEqual(
            obj["metadata"]["annotations"],
            {
                context.ANNOTATION_LAST_UPDATE: unittest.mock.sentinel.ts,
            }
        )

    @unittest.mock.patch(
        "yaook.statemachine.api_utils.generate_update_timestamp")
    async def test__add_annotations_leaves_existing_annotations_in_place(
            self,
            generate_update_timestamp):
        generate_update_timestamp.return_value = unittest.mock.sentinel.ts

        obj = {
            "metadata": {
                "annotations": {
                    "existing": "stays",
                }
            }
        }
        self.sos._add_annotations(obj)
        self.assertEqual(
            obj["metadata"]["annotations"],
            {
                "existing": "stays",
                context.ANNOTATION_LAST_UPDATE: unittest.mock.sentinel.ts,
            }
        )

    @unittest.mock.patch(
        "yaook.statemachine.api_utils.generate_update_timestamp")
    async def test__add_annotations_overwrites_timestamp(
            self,
            generate_update_timestamp):
        generate_update_timestamp.return_value = unittest.mock.sentinel.ts

        obj = {
            "metadata": {
                "annotations": {
                    "existing": "stays",
                    context.ANNOTATION_LAST_UPDATE: "legacy",
                }
            }
        }
        self.sos._add_annotations(obj)
        self.assertEqual(
            obj["metadata"]["annotations"],
            {
                "existing": "stays",
                context.ANNOTATION_LAST_UPDATE: unittest.mock.sentinel.ts,
            }
        )

    async def test_adopt_object_adds_annotations(self):
        with contextlib.ExitStack() as stack:
            super_mock = stack.enter_context(unittest.mock.patch.object(
                k8s.KubernetesResource, "adopt_object",
            ))

            _add_annotations = stack.enter_context(unittest.mock.patch.object(
                self.sos, "_add_annotations",
            ))

            await self.sos.adopt_object(
                unittest.mock.sentinel.ctx,
                unittest.mock.sentinel.obj,
            )

        _add_annotations.assert_called_once_with(
            unittest.mock.sentinel.obj,
        )
        super_mock.assert_called_once_with(
            unittest.mock.sentinel.ctx,
            unittest.mock.sentinel.obj,
        )

    async def test_reconcile_creates_object_if_nonexistent(self):
        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.sos, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            _get_current.side_effect = exceptions.ResourceNotPresent(
                unittest.mock.sentinel.component,
                unittest.mock.Mock(),
                resource_version=unittest.mock.sentinel.resource_version,
            )

            adopt_object = stack.enter_context(unittest.mock.patch.object(
                self.sos, "adopt_object",
            ))

            self.sos.make_body_mock.side_effect = None
            self.sos.make_body_mock.return_value = unittest.mock.sentinel.body

            await self.sos.reconcile(
                self.ctx,
                dependencies=unittest.mock.sentinel.deps,
            )

        self.sos.make_body_mock.assert_called_once_with(
            self.ctx,
            unittest.mock.sentinel.deps,
        )
        adopt_object.assert_called_once_with(
            self.ctx,
            unittest.mock.sentinel.body,
        )

        self.ri.create.assert_awaited_once_with(
            self.ctx.namespace,
            unittest.mock.sentinel.body,
            field_manager=self.ctx.field_manager,
        )

        self.ri.patch.assert_not_called()

    async def test_reconcile_does_nothing_if_update_not_needed(self):
        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.sos, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            _get_current.return_value = unittest.mock.sentinel.existing

            adopt_object = stack.enter_context(unittest.mock.patch.object(
                self.sos, "adopt_object",
            ))

            self.sos.make_body_mock.side_effect = None
            self.sos.make_body_mock.return_value = unittest.mock.sentinel.body

            self.sos.needs_update_mock.side_effect = None
            self.sos.needs_update_mock.return_value = False

            await self.sos.reconcile(
                self.ctx,
                dependencies=unittest.mock.sentinel.deps,
            )

        self.sos.make_body_mock.assert_called_once_with(
            self.ctx,
            unittest.mock.sentinel.deps,
        )
        adopt_object.assert_called_once_with(
            self.ctx,
            unittest.mock.sentinel.body,
        )
        self.sos.needs_update_mock.assert_called_once_with(
            unittest.mock.sentinel.existing,
            unittest.mock.sentinel.body,
        )

        self.ri.patch.assert_not_called()
        self.ri.create.assert_not_called()

    async def test_reconcile_patches_existing_object_if_update_needed(self):
        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.sos, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            _get_current.return_value = unittest.mock.sentinel.existing

            adopt_object = stack.enter_context(unittest.mock.patch.object(
                self.sos, "adopt_object",
            ))

            _update_resource = stack.enter_context(unittest.mock.patch.object(
                self.sos, "_update_resource",
                wraps=self.sos._update_resource,
            ))

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {
                "name": unittest.mock.sentinel.object_name,
            }

            self.sos.make_body_mock.side_effect = None
            self.sos.make_body_mock.return_value = {
                "key": "value",
            }

            self.sos.needs_update_mock.side_effect = None
            self.sos.needs_update_mock.return_value = True

            await self.sos.reconcile(
                self.ctx,
                dependencies=unittest.mock.sentinel.deps,
            )

        self.sos.make_body_mock.assert_called_once_with(
            self.ctx,
            unittest.mock.sentinel.deps,
        )
        adopt_object.assert_called_once_with(
            self.ctx,
            {
                "key": "value",
            },
        )
        self.sos.needs_update_mock.assert_called_once_with(
            unittest.mock.sentinel.existing,
            {
                "key": "value",
            },
        )

        _update_resource.assert_called_once_with(
            self.ctx,
            unittest.mock.sentinel.existing,
            {"key": "value"},
        )

        extract_metadata.assert_called_once_with(
            unittest.mock.sentinel.existing,
        )

        self.ri.patch.assert_called_once_with(
            self.ctx.namespace,
            unittest.mock.sentinel.object_name,
            b'{"key": "value"}',
            field_manager=self.ctx.field_manager,
            force=True,
        )
        self.ri.create.assert_not_called()

    async def test_reconcile_recreates_existing_object_if_update_needed_and_copy_on_write(self):  # NOQA
        self.sos.copy_on_write = True

        with contextlib.ExitStack() as stack:
            _get_current = stack.enter_context(unittest.mock.patch.object(
                self.sos, "_get_current",
                new=unittest.mock.AsyncMock()
            ))
            _get_current.return_value = unittest.mock.sentinel.existing

            _orphan = stack.enter_context(unittest.mock.patch.object(
                self.sos, "_orphan",
            ))

            adopt_object = stack.enter_context(unittest.mock.patch.object(
                self.sos, "adopt_object",
            ))

            _update_resource = stack.enter_context(unittest.mock.patch.object(
                self.sos, "_update_resource",
                wraps=self.sos._update_resource,
            ))

            extract_metadata = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.api_utils.extract_metadata",
            ))
            extract_metadata.return_value = {
                "name": unittest.mock.sentinel.object_name,
            }

            self.sos.make_body_mock.side_effect = None
            self.sos.make_body_mock.return_value = unittest.mock.sentinel.body

            self.sos.needs_update_mock.side_effect = None
            self.sos.needs_update_mock.return_value = True

            await self.sos.reconcile(
                self.ctx,
                dependencies=unittest.mock.sentinel.deps,
            )

        self.sos.make_body_mock.assert_called_once_with(
            self.ctx,
            unittest.mock.sentinel.deps,
        )
        adopt_object.assert_called_once_with(
            self.ctx,
            unittest.mock.sentinel.body,
        )
        self.sos.needs_update_mock.assert_called_once_with(
            unittest.mock.sentinel.existing,
            unittest.mock.sentinel.body,
        )

        _update_resource.assert_called_once_with(
            self.ctx,
            unittest.mock.sentinel.existing,
            unittest.mock.sentinel.body,
        )

        extract_metadata.assert_called_once_with(
            unittest.mock.sentinel.existing,
        )

        self.ri.patch.assert_not_called()
        _orphan.assert_awaited_once_with(
            self.ctx,
            self.ctx.namespace,
            unittest.mock.sentinel.object_name,
        )

        self.ri.create.assert_awaited_once_with(
            self.ctx.namespace,
            unittest.mock.sentinel.body,
            field_manager=self.ctx.field_manager,
        )

        self.ri.patch.assert_not_called()

    async def test__orphan_adds_orphaned_label(self):
        await self.sos._orphan(
            self.ctx,
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
        )

        self.ri.patch.assert_awaited_once_with(
            unittest.mock.sentinel.namespace,
            unittest.mock.sentinel.name,
            [
                {"op": "add",
                 "path": "/metadata/labels/state.yaook.cloud~1orphaned",
                 "value": ""},
            ],
        )

    async def test_delete_calls_orphan_on_current(self):
        with contextlib.ExitStack() as stack:
            get = stack.enter_context(unittest.mock.patch.object(
                self.sos, "get",
                new=unittest.mock.AsyncMock(),
            ))
            get.return_value = kclient.V1ObjectReference(
                namespace=sentinel.namespace,
                name=sentinel.name,
            )

            _orphan = stack.enter_context(unittest.mock.patch.object(
                self.sos, "_orphan",
                new=unittest.mock.AsyncMock(),
            ))

            await self.sos.delete(sentinel.ctx, dependencies=sentinel.deps)

        get.assert_awaited_once_with(sentinel.ctx)
        _orphan.assert_awaited_once_with(
            sentinel.ctx,
            sentinel.namespace, sentinel.name,
        )

    async def test_delete_ignores_resource_not_found(self):
        ctx = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            get = stack.enter_context(unittest.mock.patch.object(
                self.sos, "get",
                new=unittest.mock.AsyncMock(),
            ))
            get.side_effect = exceptions.ResourceNotPresent(
                sentinel.component,
                ctx,
            )

            _orphan = stack.enter_context(unittest.mock.patch.object(
                self.sos, "_orphan",
                new=unittest.mock.AsyncMock(),
            ))

            await self.sos.delete(ctx, dependencies=sentinel.deps)

        get.assert_awaited_once_with(ctx)
        _orphan.assert_not_called()


class TestDefaultTemplateParamsMixin(unittest.IsolatedAsyncioTestCase):

    def setUp(self):
        self.patches = [
            unittest.mock.patch(
                "yaook.statemachine.version_utils.get_target_release",
                new=unittest.mock.Mock(return_value="yoga")
            ),
        ]
        self.active_patches = []
        self._patch()

    def tearDown(self):
        for p in self.active_patches:
            p.stop()

    def _patch(self):
        for p in self.patches:
            p.start()
            self.active_patches.append(p)

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test__get_template_parameters(self, get_cluster_domain):
        get_cluster_domain.return_value = "cluster-domain"
        ctx = unittest.mock.Mock(["namespace", "parent_spec", "instance"])
        ctx.namespace = sentinel.namespace
        ctx.parent_spec = sentinel.parent_spec
        ctx.instance = None
        ctx.instance_data = None
        dependencies = {}
        out = await k8s.DefaultTemplateParamsMixin().\
            _get_template_parameters(ctx, dependencies)

        self.assertEqual(
            out["namespace"],
            sentinel.namespace,
        )

        self.assertEqual(
            out["crd_spec"],
            sentinel.parent_spec,
        )

        self.assertNotIn(
            "instance",
            out["vars"],
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test__get_template_parameters_includes_kubernetes_references(
            self,
            get_cluster_domain,
    ):
        get_cluster_domain.return_value = "cluster-domain"
        dep = unittest.mock.Mock(k8s.KubernetesReference)

        ctx = unittest.mock.Mock()
        dependencies = {
            "something": dep,
        }
        out = await k8s.DefaultTemplateParamsMixin().\
            _get_template_parameters(ctx, dependencies)

        self.assertIsInstance(
            out["dependencies"]["something"],
            k8s.DependencyTemplateWrapper,
        )
        self.assertEqual(
            out["dependencies"]["something"]._object,
            dep,
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test__get_template_parameters_unwraps_optional_if_possible(
            self,
            get_cluster_domain,
    ):
        get_cluster_domain.return_value = "cluster-domain"
        inner = unittest.mock.Mock(k8s.KubernetesReference)
        opt = unittest.mock.Mock(orchestration.Optional)
        opt.get_inner.return_value = inner

        ctx = unittest.mock.Mock()
        dependencies = {
            "something": opt,
        }
        out = await k8s.DefaultTemplateParamsMixin().\
            _get_template_parameters(ctx, dependencies)

        opt.get_inner.assert_called_once_with(ctx)

        self.assertIsInstance(
            out["dependencies"]["something"],
            k8s.DependencyTemplateWrapper,
        )
        self.assertEqual(
            out["dependencies"]["something"]._object,
            inner,
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test__get_template_parameters_skips_optional_if_disabled(
            self,
            get_cluster_domain,
    ):
        get_cluster_domain.return_value = "cluster-domain"
        opt = unittest.mock.Mock(orchestration.Optional)
        opt.get_inner.return_value = None

        ctx = unittest.mock.Mock()
        dependencies = {
            "something": opt,
        }
        out = await k8s.DefaultTemplateParamsMixin().\
            _get_template_parameters(ctx, dependencies)

        opt.get_inner.assert_called_once_with(ctx)

        self.assertNotIn(
            "something",
            out["dependencies"],
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test__get_template_parameters_injects_instance(
            self, get_cluster_domain):
        get_cluster_domain.return_value = "cluster-domain"
        ctx = unittest.mock.Mock(["namespace", "parent_spec", "instance"])
        ctx.namespace = sentinel.namespace
        ctx.parent_spec = sentinel.parent_spec
        ctx.instance = sentinel.instance
        ctx.instance_data = None
        dependencies = {}
        out = await k8s.DefaultTemplateParamsMixin().\
            _get_template_parameters(ctx, dependencies)

        self.assertEqual(
            out["namespace"],
            sentinel.namespace,
        )

        self.assertEqual(
            out["crd_spec"],
            sentinel.parent_spec,
        )

        self.assertEqual(
            out["instance"],
            sentinel.instance,
        )


class TestBodyTemplateMixin(TemplateTestMixin,
                            unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()
        self.value = {
           "key1": "value with \"complex\" escaping 'requirements'",
           "key2": 23,
        }
        self.tm = k8s.BodyTemplateMixin(
            template="ctx",
            templatedir=str(self._template_path),
            params={
                "foo": self.value,
            }
        )

        self.patches = [
            unittest.mock.patch(
                "yaook.statemachine.version_utils.get_target_release",
                new=unittest.mock.Mock(return_value="yoga")
            ),
        ]
        self.active_patches = []
        self._patch()

    def tearDown(self):
        for p in self.active_patches:
            p.stop()

    def _patch(self):
        for p in self.patches:
            p.start()
            self.active_patches.append(p)

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test__make_body(self, get_cluster_domain):
        get_cluster_domain.return_value = "test.cluster.domain"
        ctx = unittest.mock.Mock(["namespace", "parent_spec", "instance",
                                  "instance_data"])
        ctx.namespace = "the-namespace"
        ctx.parent_spec = {"replicas": 42}

        data = await self.tm._make_body(ctx, dependencies={})

        self.assertEqual(
            data,
            {
                "test": {
                    "namespace": "the-namespace",
                    "parent": 42,
                    "param": self.value,
                }
            }
        )

    async def test__make_body_uses__get_template_parameters(self):
        with contextlib.ExitStack() as stack:
            _get_template_parameters = stack.enter_context(
                unittest.mock.patch.object(
                    self.tm,
                    "_get_template_parameters",
                    new=unittest.mock.AsyncMock(),
                )
            )
            _get_template_parameters.return_value = \
                unittest.mock.sentinel.params

            _render_template = stack.enter_context(
                unittest.mock.patch.object(
                    self.tm,
                    "_render_template",
                    new=unittest.mock.AsyncMock(),
                )
            )
            _render_template.return_value = \
                unittest.mock.sentinel.body

            result = await self.tm._make_body(
                unittest.mock.sentinel.ctx,
                dependencies=unittest.mock.sentinel.dependencies,
            )

        _get_template_parameters.assert_called_once_with(
            unittest.mock.sentinel.ctx,
            unittest.mock.sentinel.dependencies,
        )
        _render_template.assert_called_once_with(
            "ctx",
            unittest.mock.sentinel.params,
        )
        self.assertEqual(result, unittest.mock.sentinel.body)

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test__get_template_paramaters_wraps_dependencies(
            self, get_cluster_domain):
        get_cluster_domain.return_value = "test.cluster.domain"
        ctx = unittest.mock.Mock()
        state_object = unittest.mock.Mock(k8s.KubernetesResource)

        with contextlib.ExitStack() as stack:
            DependencyTemplateWrapper = stack.enter_context(
                unittest.mock.patch(
                    "yaook.statemachine.resources.k8s."
                    "DependencyTemplateWrapper"
                )
            )

            deps = {
                "key": state_object,
            }

            result = await self.tm._get_template_parameters(ctx, deps)

        DependencyTemplateWrapper.assert_called_once_with(
            ctx, state_object,
        )

        self.assertEqual(
            result["dependencies"],
            {
                "key": DependencyTemplateWrapper(),
            }
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test__get_template_paramaters_passes_versioneddependencies(
            self, get_cluster_domain):
        get_cluster_domain.return_value = "test.cluster.domain"
        ctx = unittest.mock.Mock()

        versioneddep = versioneddependencies.VersionedDockerImage(
            "imageurl",
            versioneddependencies.SemVerSelector([]),
        )
        versioneddep.name = "testimage"
        versioneddep.pinned_version = "1.2.3"
        tm = k8s.BodyTemplateMixin(
            template="ctx",
            templatedir=str(self._template_path),
            versioned_dependencies=[versioneddep],
        )

        result = await tm._get_template_parameters(ctx, {})

        self.assertEqual(
            result["versioned_dependencies"],
            {
                "testimage": "imageurl:1.2.3",
            }
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test__get_template_paramaters_extracts_crd_and_namespace(
            self, get_cluster_domain):
        get_cluster_domain.return_value = "test.cluster.domain"
        ctx = unittest.mock.Mock()
        result = await self.tm._get_template_parameters(ctx, {})

        self.assertEqual(
            result["crd_spec"],
            ctx.parent_spec,
        )

        self.assertEqual(
            result["namespace"],
            ctx.namespace,
        )

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test__get_template_paramaters_passes_labels_if_method_exists(
            self, get_cluster_domain):
        get_cluster_domain.return_value = "test.cluster.domain"
        ctx = unittest.mock.Mock()

        self.tm.labels = labels = unittest.mock.Mock()
        labels.return_value = unittest.mock.sentinel.labels

        result = await self.tm._get_template_parameters(ctx, {})

        self.assertEqual(
            result["labels"],
            unittest.mock.sentinel.labels,
        )

        labels.assert_called_once_with(ctx)

    @unittest.mock.patch("yaook.statemachine.api_utils.get_cluster_domain")
    async def test__get_template_paramaters_passes_cluster_domain(
            self, get_cluster_domain):
        ctx = unittest.mock.Mock()
        get_cluster_domain.return_value = unittest.mock.sentinel.dns_domain

        result = await self.tm._get_template_parameters(ctx, {})

        self.assertEqual(
            result["cluster_domain"],
            unittest.mock.sentinel.dns_domain,
        )


class TestOrphan(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.rif = unittest.mock.Mock([])
        self.ri = unittest.mock.Mock(interfaces.ResourceInterfaceWithStatus)
        self.ri.plural = sentinel.plural
        self.rif.return_value = self.ri
        self.os = k8s.Orphan(
            resource_interface_factory=self.rif,
            component=sentinel.component,
        )
        self.ctx = context.Context(
            api_client=None,
            field_manager=None,
            instance=None,
            instance_data=None,
            logger=None,
            namespace="NAMESPACE",
            parent={
                "apiVersion": "test.yaook.cloud/v1",
                "metadata": {
                    "name": "myname"
                }
            },
            parent_intf=self.ri,
        )

    async def test_get_raises(self):
        with self.assertRaises(NotImplementedError):
            await self.os.get(self.ctx)

    async def test_get_all_raises(self):
        with self.assertRaises(NotImplementedError):
            await self.os.get_all(self.ctx)

    async def test_delete_deletes_existing_resources(self):
        await self.os.delete(self.ctx, dependencies=sentinel.deps)
        self.ri.delete_collection.assert_called_once_with(
            'NAMESPACE',
            label_selector="state.yaook.cloud/parent-plural=sentinel.plural,"
                           "state.yaook.cloud/parent-group=test.yaook.cloud,"
                           "state.yaook.cloud/parent-version=v1,"
                           "state.yaook.cloud/parent-name=myname,"
                           "state.yaook.cloud/component=sentinel.component")

    async def test_delete_deletes_existing_resources_ignores_instance(self):
        self.ctx = dataclasses.replace(self.ctx, instance=5)
        await self.os.delete(self.ctx, dependencies=sentinel.deps)
        self.ri.delete_collection.assert_called_once_with(
            'NAMESPACE',
            label_selector="state.yaook.cloud/parent-plural=sentinel.plural,"
                           "state.yaook.cloud/parent-group=test.yaook.cloud,"
                           "state.yaook.cloud/parent-version=v1,"
                           "state.yaook.cloud/parent-name=myname,"
                           "state.yaook.cloud/component=sentinel.component")

    async def test_reconcile_calls_delete(self):
        with contextlib.ExitStack() as stack:
            delete = stack.enter_context(unittest.mock.patch.object(
                self.os, "delete",
                new=unittest.mock.AsyncMock(),
            ))

            await self.os.reconcile(sentinel.ctx, dependencies=sentinel.deps)

        delete.assert_awaited_once_with(
            sentinel.ctx,
            dependencies=sentinel.deps,
        )
