import unittest

import ddt

import kubernetes_asyncio.client as kclient

import yaook.statemachine.resources.k8s_authz as k8s_authz

from .utils import ResourceTestMixin


@ddt.ddt()
class TestRoleBinding(unittest.TestCase):
    class RoleBindingTest(ResourceTestMixin, k8s_authz.RoleBinding):
        pass

    @unittest.mock.patch(
        "yaook.statemachine.api_utils.k8s_obj_to_yaml_data")
    @unittest.mock.patch(
        "yaook.statemachine.api_utils.deep_has_changes")
    @ddt.data(
        # roleRef, subjects, expected result
        (True, True, True),
        (False, True, True),
        (True, False, True),
        (False, False, False))
    def test_needs_update_compares_spec(self, data, deep_has_changes,
                                        k8s_obj_to_yaml_data):
        lhs = unittest.mock.Mock()
        rb = self.RoleBindingTest()
        k8s_obj_to_yaml_data.return_value = {
            "roleRef": unittest.mock.sentinel.roleRef,
            "subjects": unittest.mock.sentinel.subjects,
        }
        deep_has_changes.side_effect = (data[0], data[1])

        result = rb._needs_update(
            lhs,
            {
                "roleRef": unittest.mock.sentinel.roleRefRhs,
                "subjects": unittest.mock.sentinel.subjectsRhs,
            })

        k8s_obj_to_yaml_data.assert_called_with(
            lhs
        )

        deep_has_changes.assert_any_call(
            unittest.mock.sentinel.roleRef,
            unittest.mock.sentinel.roleRefRhs
        )

        if not data[0]:
            deep_has_changes.assert_any_call(
                unittest.mock.sentinel.subjects,
                unittest.mock.sentinel.subjectsRhs
            )

        self.assertEqual(result, data[2])

    def test__needs_update_returns_true_on_label_change(self):
        rb = self.RoleBindingTest()

        lhs = kclient.V1RoleBinding(
            role_ref=kclient.V1RoleRef(
                api_group="api",
                kind="kind",
                name="name"
            ),
            metadata=kclient.V1ObjectMeta(
                labels={
                    "foo": "foo",
                }
            )
        )
        rhs = {
            "spec": {},
            "metadata": {
                "labels": {
                    "foo": "foo",
                    "bar": "bar",
                },
            },
        }

        self.assertTrue(rb._needs_update(lhs, rhs))
