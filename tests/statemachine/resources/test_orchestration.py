import contextlib
import json
import unittest
import unittest.mock
from unittest.mock import sentinel

import ddt

import kubernetes_asyncio.client as kclient

import yaook.statemachine.context as context
import yaook.statemachine.resources.k8s as k8s
import yaook.statemachine.resources.orchestration as orchestration


class TestL2Lock(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        super().setUp()

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.lock = orchestration.L2Lock()
        self.ctx = unittest.mock.Mock([])
        self.ctx.api_client = unittest.mock.Mock([])
        self.ctx.field_manager = unittest.mock.Mock([])
        self.ctx.parent_kind = "test"
        self.ctx.parent_name = "node1"
        self.node = unittest.mock.Mock(['V1Node'])
        self.node.metadata = unittest.mock.Mock([])
        self.node.metadata.annotations = {}
        self.node.metadata.labels = {}
        self.node.metadata.resource_version = 42

    async def test_reconcile_sets_l2_migration_lock(self):
        with contextlib.ExitStack() as stack:
            read_node = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api.read_node",
                new=unittest.mock.AsyncMock([])
            ))
            self.node.metadata.labels = {
                context.LABEL_L2_REQUIRE_MIGRATION: 'False'
            }
            read_node.return_value = self.node
            patch_node = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api.patch_node",
                new=unittest.mock.AsyncMock([])
            ))
            await self.lock.reconcile(self.ctx, dependencies={})

        patch = [{
            "op": "add",
            # In the openstackresource we are using a jsonpatch and since we
            # don't want this here, the slash gets converted to a '~1
            "path": "/metadata/annotations/l2-lock.maintenance.yaook.cloud~1test",  # noqa: E501
            "value": "",
        }, {
            'op': 'add', 'path': '/metadata/resourceVersion', 'value':
                self.node.metadata.resource_version
        }]
        patch_node.assert_called_once_with(
            self.ctx.parent_name,
            patch,
            field_manager=self.ctx.field_manager
        )

    async def test_reconcile_does_nothing_if_lock_exists(self):
        with contextlib.ExitStack() as stack:
            read_node = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api.read_node",
                new=unittest.mock.AsyncMock([])
            ))
            self.node.metadata.annotations = {
                "l2-lock.maintenance.yaook.cloud/test": ''
            }
            read_node.return_value = self.node
            patch_node = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api.patch_node",
                new=unittest.mock.AsyncMock([])
            ))
            await self.lock.reconcile(self.ctx, dependencies={})

        patch_node.assert_not_called()

    async def test_reconcile_raises_if_l2_annotation_unset(self):
        self.ctx.parent = {}
        self.ctx.parent['metadata'] = {}

        with self.assertRaises(Exception):
            await self.lock.reconcile(self.ctx, dependencies={})

    async def test_reconcile_raises_if_l2_annotation_true(self):
        self.ctx.parent = {}
        self.ctx.parent['metadata'] = {}

        with contextlib.ExitStack() as stack:
            read_node = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api.read_node",
                new=unittest.mock.AsyncMock([])
            ))
            self.node.metadata.labels = {
                context.LABEL_L2_REQUIRE_MIGRATION: 'True'
            }
            read_node.return_value = self.node
            with self.assertRaises(Exception):
                await self.lock.reconcile(self.ctx, dependencies={})

    async def test_reconcile_not_raises_if_l2_annotation_true_and_deleted_time_set(self):  # noqa: E501
        self.ctx.parent = {}
        self.ctx.parent['metadata'] = {}
        self.ctx.parent['metadata']['deletionTimestamp'] = 'sometime'

        with contextlib.ExitStack() as stack:
            read_node = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api.read_node",
                new=unittest.mock.AsyncMock([])
            ))
            self.node.metadata.labels = {
                context.LABEL_L2_REQUIRE_MIGRATION: 'True'
            }
            read_node.return_value = self.node
            with self.assertRaises(Exception):
                await self.lock.reconcile(self.ctx, dependencies={})


@ddt.ddt
class TestOptional(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.ctx = unittest.mock.Mock([
            "api_client",
            "namespace",
            "parent_api_version",
            "parent_kind",
            "parent_uid",
            "parent_name",
            "logger",
            "field_manager",
        ])
        self.wrapped = unittest.mock.Mock(k8s.SingleObject)
        self.wrapped._dependencies = ()

    def _get_optional_state(self, status: bool):
        return orchestration.Optional(
            condition=lambda x: status,
            wrapped_state=self.wrapped
        )

    def test_passes___set_name__(self):
        os = self._get_optional_state(False)
        os.__set_name__(type(self), unittest.mock.sentinel.name)
        self.wrapped.__set_name__.assert_called_once_with(
            type(self), unittest.mock.sentinel.name)

    def test_get_inner_returns_None_if_disabled(self):
        os = self._get_optional_state(False)
        self.assertIsNone(os.get_inner(self.ctx))

    def test_get_inner_returns_wrapped_resource_if_enabled(self):
        os = self._get_optional_state(True)
        self.assertIs(os.get_inner(self.ctx), self.wrapped)

    @ddt.data(True, False)
    def test_passes_get_listeners(self, data):
        os = self._get_optional_state(data)
        os.get_listeners()
        self.wrapped.get_listeners.assert_called_once_with()

    async def test_passes_reconcile_if_enabled(self):
        os = self._get_optional_state(True)
        await os.reconcile(self.ctx,
                           dependencies={})
        self.wrapped.reconcile.assert_called_once_with(
            self.ctx, dependencies={})

    async def test_reconcile_deletes_if_disabled(self):
        os = self._get_optional_state(False)
        ret = await os.reconcile(self.ctx,
                                 dependencies=sentinel.deps)
        self.wrapped.delete.assert_awaited_once_with(
            self.ctx,
            dependencies=sentinel.deps,
        )
        self.assertFalse(ret)

    @ddt.data(True, False)
    async def test_passes_delete(self, enabled):
        os = self._get_optional_state(enabled)
        await os.delete(self.ctx, dependencies=sentinel.deps)
        self.wrapped.delete.assert_awaited_once_with(
            self.ctx,
            dependencies=sentinel.deps,
        )

    async def test_passes_is_ready(self):
        os = self._get_optional_state(True)
        await os.is_ready(self.ctx)
        self.wrapped.is_ready.assert_called_once_with(self.ctx)

    async def test_is_ready_not_active(self):
        os = self._get_optional_state(False)
        ret = await os.is_ready(self.ctx)
        self.assertTrue(ret)
        self.wrapped.is_ready.assert_not_called()

    @ddt.data(True, False)
    async def test_passes_cleanup_orphans(self, data):
        os = self._get_optional_state(data)
        await os.cleanup_orphans(self.ctx, {})
        self.wrapped.cleanup_orphans.assert_called_once_with(
            self.ctx, {}
        )


@ddt.ddt
class TestWrappedStateAwareOptional(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.ctx = unittest.mock.Mock([
            "api_client",
            "namespace",
            "parent_api_version",
            "parent_kind",
            "parent_uid",
            "parent_name",
            "logger",
            "field_manager",
        ])
        self.wrapped = unittest.mock.Mock(k8s.SingleObject)
        self.wrapped._dependencies = ()

    def _get_optional_state(self, status: bool):
        async def _mock_condition(x, y) -> bool:
            return status
        return orchestration.WrappedStateAwareOptional(
            condition=_mock_condition,
            wrapped_state=self.wrapped
        )

    def test_passes___set_name__(self):
        os = self._get_optional_state(False)
        os.__set_name__(type(self), unittest.mock.sentinel.name)
        self.wrapped.__set_name__.assert_called_once_with(
            type(self), unittest.mock.sentinel.name)

    async def test_get_inner_returns_None_if_disabled(self):
        os = self._get_optional_state(False)
        self.assertIsNone(await os.get_inner(self.ctx))

    async def test_get_inner_returns_wrapped_resource_if_enabled(self):
        os = self._get_optional_state(True)
        self.assertIs(await os.get_inner(self.ctx), self.wrapped)

    @ddt.data(True, False)
    def test_passes_get_listeners(self, data):
        os = self._get_optional_state(data)
        os.get_listeners()
        self.wrapped.get_listeners.assert_called_once_with()

    async def test_passes_reconcile_if_enabled(self):
        os = self._get_optional_state(True)
        await os.reconcile(self.ctx,
                           dependencies={})
        self.wrapped.reconcile.assert_called_once_with(
            self.ctx, dependencies={})

    async def test_reconcile_deletes_if_disabled(self):
        os = self._get_optional_state(False)
        ret = await os.reconcile(self.ctx,
                                 dependencies=sentinel.deps)
        self.wrapped.delete.assert_awaited_once_with(
            self.ctx,
            dependencies=sentinel.deps,
        )
        self.assertFalse(ret)

    @ddt.data(True, False)
    async def test_passes_delete(self, enabled):
        os = self._get_optional_state(enabled)
        await os.delete(self.ctx, dependencies=sentinel.deps)
        self.wrapped.delete.assert_awaited_once_with(
            self.ctx,
            dependencies=sentinel.deps,
        )

    async def test_passes_is_ready(self):
        os = self._get_optional_state(True)
        await os.is_ready(self.ctx)
        self.wrapped.is_ready.assert_called_once_with(self.ctx)

    async def test_is_ready_not_active(self):
        os = self._get_optional_state(False)
        ret = await os.is_ready(self.ctx)
        self.assertTrue(ret)
        self.wrapped.is_ready.assert_not_called()

    @ddt.data(True, False)
    async def test_passes_cleanup_orphans(self, data):
        os = self._get_optional_state(data)
        await os.cleanup_orphans(self.ctx, {})
        self.wrapped.cleanup_orphans.assert_called_once_with(
            self.ctx, {}
        )


@ddt.ddt
class TestOptionalKubernetesReference(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.ctx = unittest.mock.Mock([
            "api_client",
            "namespace",
            "parent_api_version",
            "parent_kind",
            "parent_uid",
            "parent_name",
            "logger",
            "field_manager",
        ])
        self.wrapped = unittest.mock.Mock(k8s.SingleObject)
        self.wrapped._dependencies = ()

    def _get_optional_state(self, status: bool):
        return orchestration.OptionalKubernetesReference(
            condition=lambda x: status,
            wrapped_state=self.wrapped
        )

    def _make_listener_mock(self):
        listener = unittest.mock.Mock([
            "api_group",
            "version",
            "plural",
            "broadcast",
            "component",
            "broadcast_filter",
        ])
        listener.__class__ = context.KubernetesListener
        listener.listener = lambda ctx, event: sentinel.callback
        return listener

    @ddt.data(True, False)
    async def test_passes_get_resource_interface(self, data):
        os = self._get_optional_state(data)
        os.get_resource_interface(self.ctx)
        self.wrapped.get_resource_interface.assert_called_once_with(self.ctx)

    @ddt.data(True, False)
    async def test_passes_get_all(self, data):
        self.wrapped.get_all.return_value = sentinel.k8s_objs
        os = self._get_optional_state(data)
        objs = await os.get_all(self.ctx)
        self.wrapped.get_all.assert_called_once_with(self.ctx)
        self.assertEquals(objs, sentinel.k8s_objs)

    @ddt.data(True, False)
    async def test_passes_get(self, data):
        self.wrapped.get_with_instanced.return_value = (sentinel.k8s_obj,
                                                        sentinel.k8s_obj)
        os = self._get_optional_state(data)
        obj = await os.get(self.ctx)
        self.wrapped.get_with_instanced.assert_called_once_with(self.ctx)
        self.assertEquals(obj, sentinel.k8s_obj)

    @ddt.data(True, False)
    def test_wraps_listeners(self, data):
        listeners = [
            self._make_listener_mock(),
            self._make_listener_mock(),
            self._make_listener_mock()
        ]
        self.wrapped.get_listeners.return_value = listeners
        os = self._get_optional_state(data)
        returned_listeners = os.get_listeners()
        self.wrapped.get_listeners.assert_called()

        for lis in returned_listeners:
            if data:
                # condition is True, original listener is called
                self.assertEqual(
                    lis.listener(sentinel.ctx, sentinel.event),
                    sentinel.callback
                )
            else:
                # condition is False, original listener is intercepted
                ret = lis.listener(sentinel.ctx, sentinel.event)
                self.assertNotEqual(ret, sentinel.callback)
                self.assertEqual(ret, False)


@ddt.ddt()
class TestNonUpgrade(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.wrapped = unittest.mock.Mock(k8s.SingleObject)
        self.wrapped._dependencies = ()
        self.nus = orchestration.Optional(
            condition=orchestration.optional_non_upgrade(),
            wrapped_state=self.wrapped
        )

    def test_is_optional_state(self):
        self.assertIsInstance(self.nus, orchestration.Optional)

    @unittest.mock.patch(
        "yaook.statemachine.version_utils.is_upgrading"
    )
    @ddt.data(True, False)
    def test_condition_not_upgrading(self, data, is_upgrading):
        is_upgrading.return_value = data
        self.assertEqual(
            not data,
            self.nus.condition(unittest.mock.sentinel.ctx)
        )


@ddt.ddt()
class TestOnlyUpgrade(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.wrapped = unittest.mock.Mock(k8s.SingleObject)
        self.wrapped._dependencies = ()
        self.nus = orchestration.Optional(
            condition=orchestration.optional_only_upgrade(),
            wrapped_state=self.wrapped
        )

    def test_is_optional_state(self):
        self.assertIsInstance(self.nus, orchestration.Optional)

    @unittest.mock.patch(
        "yaook.statemachine.version_utils.is_upgrading"
    )
    @ddt.data(True, False)
    def test_condition_not_upgrading(self, data, is_upgrading):
        is_upgrading.return_value = data
        self.assertEqual(
            data,
            self.nus.condition(unittest.mock.sentinel.ctx)
        )


@ddt.ddt()
class TestNotInReleases(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.wrapped = unittest.mock.Mock(k8s.SingleObject)
        self.wrapped._dependencies = ()
        self.included_release = "first"
        self.excluded_release = "third"
        self.releases_list = ["first", "second"]
        self.nus = orchestration.Optional(
            condition=orchestration.optional_not_in_releases(
                self.releases_list,
            ),
            wrapped_state=self.wrapped
        )

    def test_is_optional_state(self):
        self.assertIsInstance(self.nus, orchestration.Optional)

    @unittest.mock.patch(
        "yaook.statemachine.version_utils.get_target_release"
    )
    def test_condition_is_included(self, get_target_release):
        get_target_release.return_value = self.included_release
        self.assertEqual(
            False,
            self.nus.condition(unittest.mock.sentinel.ctx)
        )

    @unittest.mock.patch(
        "yaook.statemachine.version_utils.get_target_release"
    )
    def test_condition_is_excluded(self, get_target_release):
        get_target_release.return_value = self.excluded_release
        self.assertEqual(
            True,
            self.nus.condition(unittest.mock.sentinel.ctx)
        )


class TestTriggerRollingRestart(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.controller_ref = unittest.mock.Mock([])
        self.restart_id_func = unittest.mock.Mock()
        self.trr = orchestration.TriggerRollingRestart(
            controller_ref=self.controller_ref,
            restart_id=self.restart_id_func,
        )

    async def test_reconcile_triggers_rolling_restart(self):
        self.controller_ref.rolling_restart = unittest.mock.AsyncMock()
        self.restart_id_func.return_value = sentinel.restart_id

        await self.trr.reconcile(sentinel.ctx, sentinel.deps)

        self.restart_id_func.assert_called_once_with(sentinel.ctx)
        self.controller_ref.rolling_restart.assert_awaited_once_with(
            sentinel.ctx,
            sentinel.restart_id,
        )

    async def test_is_ready_delegates_to_backing_impl(self):
        self.controller_ref.is_ready = unittest.mock.AsyncMock()
        self.controller_ref.is_ready.return_value = sentinel.ready

        result = await self.trr.is_ready(sentinel.ctx)

        self.controller_ref.is_ready.assert_awaited_once_with(sentinel.ctx)

        self.assertEqual(result, sentinel.ready)

    async def test_delete_is_most_likely_noop(self):
        await self.trr.delete(sentinel.ctx, dependencies=sentinel.deps)

    async def test_cleanup_orphans_is_most_likely_noop(self):
        await self.trr.cleanup_orphans(sentinel.ctx, sentinel.protect)


class Testkubernetes_run(unittest.IsolatedAsyncioTestCase):
    async def test_sends_request_via_websocket_and_returns_process_info(self):
        ws_resp = unittest.mock.Mock([])
        ws_resp.receive_bytes = unittest.mock.AsyncMock()
        ws_resp.receive_bytes.return_value = b"\x03" + json.dumps({
            "metadata": {},
            "status": "Success",
        }).encode("utf-8")
        ws_resp.close = unittest.mock.AsyncMock()

        with contextlib.ExitStack() as stack:
            CoreV1Api = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
            ))
            CoreV1Api.return_value.connect_get_namespaced_pod_exec = \
                unittest.mock.AsyncMock(return_value=ws_resp)

            result = await orchestration.kubernetes_run(
                sentinel.ws_client,
                namespace=sentinel.namespace,
                pod=sentinel.pod,
                container=sentinel.container,
                command=sentinel.command,
            )

        CoreV1Api.assert_called_once_with(sentinel.ws_client)
        CoreV1Api.return_value.connect_get_namespaced_pod_exec\
            .assert_called_once_with(
                sentinel.pod,
                sentinel.namespace,
                command=sentinel.command,
                container=sentinel.container,
                tty=False,
                stdin=False,
                stdout=True,
                stderr=True,
                _preload_content=False,
            )

        ws_resp.receive_bytes.assert_awaited_once_with()
        ws_resp.close.assert_awaited_once_with()

        self.assertEqual(
            result.args,
            sentinel.command,
        )

        self.assertEqual(
            result.returncode,
            0,
        )

        self.assertEqual(
            result.stdout,
            b"",
        )

        self.assertEqual(
            result.stderr,
            b"",
        )

    async def test_returns_nonzero_exit_code_via_result(self):
        ws_resp = unittest.mock.Mock([])
        ws_resp.receive_bytes = unittest.mock.AsyncMock()
        ws_resp.receive_bytes.return_value = b"\x03" + json.dumps({
            "metadata": {},
            "status": "Failure",
            "reason": "NonZeroExitCode",
            "details": {
                "causes": [
                    {"reason": "ExitCode", "message": "2342"},
                ],
            },
        }).encode("utf-8")
        ws_resp.close = unittest.mock.AsyncMock()

        with contextlib.ExitStack() as stack:
            CoreV1Api = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
            ))
            CoreV1Api.return_value.connect_get_namespaced_pod_exec = \
                unittest.mock.AsyncMock(return_value=ws_resp)

            result = await orchestration.kubernetes_run(
                sentinel.ws_client,
                namespace=sentinel.namespace,
                pod=sentinel.pod,
                container=sentinel.container,
                command=sentinel.command,
            )

        CoreV1Api.assert_called_once_with(sentinel.ws_client)
        CoreV1Api.return_value.connect_get_namespaced_pod_exec\
            .assert_called_once_with(
                sentinel.pod,
                sentinel.namespace,
                command=sentinel.command,
                container=sentinel.container,
                tty=False,
                stdin=False,
                stdout=True,
                stderr=True,
                _preload_content=False,
            )

        ws_resp.receive_bytes.assert_awaited_once_with()
        ws_resp.close.assert_awaited_once_with()

        self.assertEqual(
            result.args,
            sentinel.command,
        )

        self.assertEqual(
            result.returncode,
            2342,
        )

        self.assertEqual(
            result.stdout,
            b"",
        )

        self.assertEqual(
            result.stderr,
            b"",
        )

    async def test_returns_stdout_and_stderr_via_result(self):
        def messages():
            # somehow, this always happens
            yield b"\x01"
            yield b"\x01some stdout binary garbage \xf0"
            yield b"\x02some stderr binary garbage \xf1"
            yield b'\x03{"status": "Success"}'

        ws_resp = unittest.mock.Mock([])
        ws_resp.receive_bytes = unittest.mock.AsyncMock()
        ws_resp.receive_bytes.side_effect = messages()
        ws_resp.close = unittest.mock.AsyncMock()

        with contextlib.ExitStack() as stack:
            CoreV1Api = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.client.CoreV1Api",
            ))
            CoreV1Api.return_value.connect_get_namespaced_pod_exec = \
                unittest.mock.AsyncMock(return_value=ws_resp)

            result = await orchestration.kubernetes_run(
                sentinel.ws_client,
                namespace=sentinel.namespace,
                pod=sentinel.pod,
                container=sentinel.container,
                command=sentinel.command,
            )

        CoreV1Api.assert_called_once_with(sentinel.ws_client)
        CoreV1Api.return_value.connect_get_namespaced_pod_exec\
            .assert_called_once_with(
                sentinel.pod,
                sentinel.namespace,
                command=sentinel.command,
                container=sentinel.container,
                tty=False,
                stdin=False,
                stdout=True,
                stderr=True,
                _preload_content=False,
            )

        ws_resp.close.assert_awaited_once_with()

        self.assertEqual(
            result.args,
            sentinel.command,
        )

        self.assertEqual(
            result.returncode,
            0,
        )

        self.assertEqual(
            result.stdout,
            b"some stdout binary garbage \xf0",
        )

        self.assertEqual(
            result.stderr,
            b"some stderr binary garbage \xf1",
        )


class Testpkill_container(unittest.IsolatedAsyncioTestCase):
    async def test_executes_pkill_and_raises_errors(self):
        api_client = unittest.mock.Mock([])
        api_client.configuration = sentinel.config

        proc = unittest.mock.Mock([])
        proc.check_returncode = unittest.mock.Mock()

        ws_client = unittest.mock.Mock()

        ws_client_handle = unittest.mock.MagicMock()
        ws_client_handle.__aenter__ = unittest.mock.AsyncMock()
        ws_client_handle.__aenter__.return_value = ws_client

        with contextlib.ExitStack() as stack:
            kubernetes_run = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.orchestration.kubernetes_run",
                new=unittest.mock.AsyncMock(),
            ))
            kubernetes_run.return_value = proc

            WsApiClient = stack.enter_context(unittest.mock.patch(
                "kubernetes_asyncio.stream.WsApiClient",
            ))
            WsApiClient.return_value = ws_client_handle

            await orchestration.pkill_container(
                api_client,
                sentinel.namespace,
                sentinel.pod,
                sentinel.container,
                sentinel.process,
                sentinel.signal,
            )

        WsApiClient.assert_called_once_with(sentinel.config)

        kubernetes_run.assert_awaited_once_with(
            ws_client,
            namespace=sentinel.namespace,
            pod=sentinel.pod,
            container=sentinel.container,
            command=["/bin/pkill", "--signal", "sentinel.signal",
                     "-f", sentinel.process],
        )

        proc.check_returncode.assert_called_once_with()


class TestSendSignal(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.container = "foocont"
        self.ref = unittest.mock.Mock([])
        self.signal = "HUP"
        self.process = "someproc"
        self.ss = orchestration.SendSignal(
            ref=self.ref,
            container=self.container,
            signal=self.signal,
            process_name=self.process,
        )

    @unittest.mock.patch("yaook.statemachine.interfaces.pod_interface")
    async def test__find_pods_uses_labels_from_ref(self, pod_interface):
        ctx = unittest.mock.Mock()

        self.ref.labels = unittest.mock.Mock()
        self.ref.labels.return_value = sentinel.labels

        pod_interface.return_value.list_ = unittest.mock.AsyncMock()
        pod_interface.return_value.list_.return_value = [
            kclient.V1Pod(
                metadata=kclient.V1ObjectMeta(
                    name="pod1",
                    namespace="ns1",
                ),
            ),
            kclient.V1Pod(
                metadata=kclient.V1ObjectMeta(
                    name="pod2",
                    namespace="ns2",
                ),
            ),
        ]

        result = await self.ss._find_pods(ctx)

        pod_interface.assert_called_once_with(ctx.api_client)
        self.ref.labels.assert_called_once_with(ctx)
        pod_interface().list_.assert_awaited_once_with(
            ctx.namespace,
            label_selector=sentinel.labels,
        )

        self.assertCountEqual(
            [
                ("ns1", "pod1"),
                ("ns2", "pod2"),
            ],
            [
                (ref.namespace, ref.name)
                for ref in result
            ],
        )

    async def test_reconcile_signals_all_pods(self):
        ctx = unittest.mock.Mock()

        with contextlib.ExitStack() as stack:
            _find_pods = stack.enter_context(unittest.mock.patch.object(
                self.ss, "_find_pods",
                new=unittest.mock.AsyncMock(),
            ))
            _find_pods.return_value = [
                kclient.V1ObjectReference(namespace="ns1", name="pod1"),
                kclient.V1ObjectReference(namespace="ns2", name="pod2"),
                kclient.V1ObjectReference(namespace="ns3", name="pod3"),
            ]

            pkill_container = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.orchestration.pkill_container",
                new=unittest.mock.AsyncMock(),
            ))

            await self.ss.reconcile(ctx, sentinel.deps)

        _find_pods.assert_awaited_once_with(ctx)

        self.assertCountEqual(
            pkill_container.await_args_list,
            [
                unittest.mock.call(
                    ctx.api_client,
                    "ns1", "pod1",
                    self.container,
                    self.process,
                    self.signal,
                ),
                unittest.mock.call(
                    ctx.api_client,
                    "ns2", "pod2",
                    self.container,
                    self.process,
                    self.signal,
                ),
                unittest.mock.call(
                    ctx.api_client,
                    "ns3", "pod3",
                    self.container,
                    self.process,
                    self.signal,
                ),
            ],
        )

    async def test_delete_is_most_likely_noop(self):
        await self.ss.delete(sentinel.ctx, dependencies=sentinel.deps)

    async def test_cleanup_orphans_is_most_likely_noop(self):
        await self.ss.cleanup_orphans(sentinel.ctx, sentinel.protect)


class TestBarrier(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.b = orchestration.Barrier()

    async def test_reconcile_is_most_likely_noop(self):
        await self.b.reconcile(sentinel.ctx, dependencies=sentinel.deps)

    async def test_delete_is_most_likely_noop(self):
        await self.b.delete(sentinel.ctx, dependencies=sentinel.deps)

    async def test_cleanup_orphans_is_most_likely_noop(self):
        await self.b.cleanup_orphans(sentinel.ctx, sentinel.protect)


class TestScale(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self.controller_ref = unittest.mock.Mock()
        self.controller_ref.deployment_has_zero_replicas =\
            unittest.mock.AsyncMock()
        self.controller_ref.scale = unittest.mock.AsyncMock()
        self.wrapped = unittest.mock.Mock(k8s.SingleObject)
        self.wrapped._dependencies = ()
        self.wrapped.is_ready = unittest.mock.AsyncMock()
        self.ts = orchestration.Upgrade(
            controller_ref=self.controller_ref,
            wrapped=self.wrapped,
        )
        self.ctx = unittest.mock.Mock(["logger"])

    async def test_deployment_has_non_zero_replicas_and_job_not_ready(self):
        self.controller_ref.deployment_has_zero_replicas.return_value = False
        self.wrapped.is_ready.return_value = False

        await self.ts.reconcile(self.ctx, sentinel.deps)

        self.controller_ref.scale.assert_awaited_with(
            self.ctx,
            0
        )
        self.wrapped.is_ready.assert_awaited_with(
            self.ctx,
        )

    async def test_deployment_has_zero_replicas_and_job_not_ready(self):
        self.controller_ref.deployment_has_zero_replicas.return_value = True
        self.wrapped.is_ready.return_value = False

        await self.ts.reconcile(self.ctx, sentinel.deps)

        self.controller_ref.scale.assert_awaited_with(
            self.ctx,
            0
        )

        self.wrapped.reconcile.assert_awaited_with(
            self.ctx,
            dependencies=sentinel.deps
        )

        self.wrapped.is_ready.assert_awaited_with(
            self.ctx,
        )

    async def test_deployment_has_zero_replicas_and_job_ready(self):
        self.controller_ref.deployment_has_zero_replicas.return_value = True
        self.wrapped.is_ready.return_value = True

        await self.ts.reconcile(self.ctx, sentinel.deps)

        self.controller_ref.scale.assert_not_awaited()
        self.wrapped.reconcile.assert_not_awaited()

    async def test_deployment_has_non_zero_replicas_and_job_ready(self):
        self.controller_ref.deployment_has_zero_replicas.return_value = False
        self.wrapped.is_ready.return_value = True

        await self.ts.reconcile(self.ctx, sentinel.deps)

        self.controller_ref.scale.assert_not_awaited()
        self.wrapped.reconcile.assert_not_awaited()
