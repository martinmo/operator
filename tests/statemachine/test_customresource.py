#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import contextlib
import dataclasses
import kubernetes_asyncio.client as kclient
import logging
import unittest
import unittest.mock
import uuid

import ddt

import yaook.statemachine.api_utils as api_utils
import yaook.statemachine.context as context
import yaook.statemachine.customresource as customresource
import yaook.statemachine.exceptions as exceptions
import yaook.statemachine.resources as resources
import yaook.statemachine.versioneddependencies as versioneddependencies
import yaook.statemachine.interfaces as interfaces
import yaook.statemachine.version_utils as version_utils


class TestCustomResource(unittest.IsolatedAsyncioTestCase):
    class CustomResourceTest(customresource.CustomResource):
        API_GROUP = "test.yaook.cloud"
        API_GROUP_VERSION = "v1"
        PLURAL = "testresources"
        KIND = "TestResource"

    def setUp(self):
        self.logger = logging.getLogger(__name__ + type(self).__qualname__)
        self.sm = unittest.mock.Mock(["ensure"])
        self.sm.ensure = unittest.mock.AsyncMock()
        self.sm.ensure.return_value = {}

        self.ctx = unittest.mock.Mock(["logger",
                                       "api_client",
                                       "namespace",
                                       "parent_name",
                                       "parent"])

        with contextlib.ExitStack() as stack:
            StateMachine = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.statemachine.StateMachine",
            ))
            StateMachine.return_value = self.sm

            self.cr = self.CustomResourceTest(logger=self.logger)

    def _state_mock(self, base_class=resources.KubernetesResource):
        s = unittest.mock.Mock(base_class)
        s.component = str(uuid.uuid4())
        s.get_used_resources = unittest.mock.AsyncMock()
        s.cleanup_orphans = unittest.mock.AsyncMock()
        return s

    @unittest.mock.patch(
        "yaook.statemachine.customresource.CustomResource._cleanup")
    @unittest.mock.patch(
        "yaook.statemachine.customresource.update_status")
    @unittest.mock.patch(
        "yaook.statemachine.customresource.CustomResource._validate_config")
    async def test_reconcile_validates_before_ensure(self, _validate_config,
                                                     update_status, _cleanup):
        order = []
        _validate_config.side_effect = lambda _: order.append(
            "_validate_config")
        self.sm.ensure.side_effect = lambda _: order.append("ensure")

        await self.cr.reconcile(self.ctx, 0)

        self.assertEqual(
            ["_validate_config", "ensure"],
            order
        )

    async def test__get_used_resources_collects_all_states_resources_into_api_groups(self):  # NOQA
        s1 = self._state_mock()
        s1.get_used_resources.return_value = [
            api_utils.ResourceReference(
                api_version="v1",
                plural="foos",
                namespace="ns1",
                name="name1",
            ),
            api_utils.ResourceReference(
                api_version="test/v1",
                plural="bars",
                namespace="ns1",
                name="name1",
            ),
            api_utils.ResourceReference(
                api_version="v1",
                plural="foos",
                namespace="ns2",
                name="name2",
            ),
        ]
        s2 = self._state_mock()
        s2.get_used_resources.return_value = [
            api_utils.ResourceReference(
                api_version="v1",
                plural="foos",
                namespace="ns3",
                name="name3",
            ),
            api_utils.ResourceReference(
                api_version="test/v1",
                plural="bars",
                namespace="ns2",
                name="name2",
            ),
        ]

        self.sm.states = [s1, s2]

        result = await self.cr._get_used_resources(unittest.mock.sentinel.ctx)

        self.assertDictEqual(
            result,
            {
                ("v1", "foos"): {
                    ("ns1", "name1"),
                    ("ns2", "name2"),
                    ("ns3", "name3"),
                },
                ("test/v1", "bars"): {
                    ("ns1", "name1"),
                    ("ns2", "name2"),
                }
            }
        )

        s1.get_used_resources.assert_awaited_once_with(
            unittest.mock.sentinel.ctx,
        )
        s2.get_used_resources.assert_awaited_once_with(
            unittest.mock.sentinel.ctx,
        )

    async def test_cleanup_calls_cow_cleanup_for_all_states_with_protection(self):  # NOQA
        s1 = self._state_mock()
        s2 = self._state_mock()

        self.sm.states = [s1, s2]

        with contextlib.ExitStack() as stack:
            _cleanup_orphaned_components = stack.enter_context(
                unittest.mock.patch.object(
                    self.cr, "_cleanup_orphaned_components",
                ),
            )

            _get_used_resources = stack.enter_context(
                unittest.mock.patch.object(
                    self.cr, "_get_used_resources",
                )
            )
            _get_used_resources.return_value = {
                "protected": [unittest.mock.sentinel.items],
            }

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.customresource."
                "_build_resource_usage_report",
            ))

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.customresource.update_status",
            ))

            await self.cr._cleanup(self.ctx)

        _get_used_resources.assert_awaited_once_with(self.ctx)

        _cleanup_orphaned_components.assert_not_called()

        s1.cleanup_orphans.assert_awaited_once_with(
            self.ctx,
            protect={
                "protected": [unittest.mock.sentinel.items],
            },
        )
        s2.cleanup_orphans.assert_awaited_once_with(
            self.ctx,
            protect={
                "protected": [unittest.mock.sentinel.items],
            },
        )

    async def test_cleanup_call_cleanup_on_k8s_and_optional_states_only(self):
        s1 = self._state_mock()
        s2 = self._state_mock(resources.Resource)
        s3 = self._state_mock(resources.Optional)

        self.sm.states = [s1, s2, s3]

        with contextlib.ExitStack() as stack:
            _cleanup_orphaned_components = stack.enter_context(
                unittest.mock.patch.object(
                    self.cr, "_cleanup_orphaned_components",
                ),
            )

            _get_used_resources = stack.enter_context(
                unittest.mock.patch.object(
                    self.cr, "_get_used_resources",
                )
            )
            _get_used_resources.return_value = {
                "protected": [unittest.mock.sentinel.items],
            }

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.customresource."
                "_build_resource_usage_report",
            ))

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.customresource.update_status",
            ))

            await self.cr._cleanup(self.ctx)

        _get_used_resources.assert_awaited_once_with(self.ctx)

        _cleanup_orphaned_components.assert_not_called()

        s1.cleanup_orphans.assert_awaited_once_with(
            self.ctx,
            protect={
                "protected": [unittest.mock.sentinel.items],
            },
        )
        s2.cleanup_orphans.assert_not_called()
        s3.cleanup_orphans.assert_awaited_once_with(
            self.ctx,
            protect={
                "protected": [unittest.mock.sentinel.items],
            },
        )

    async def test_smoke_cleanup_runs_after_reconcile(self):
        effects = []

        async def fake_ensure(ctx):
            effects.append("e1")
            await asyncio.sleep(0.1)
            effects.append("e2")
            await asyncio.sleep(0.2)
            effects.append("e3")

        async def fake_get_used_resources(ctx):
            effects.append("i1")
            await asyncio.sleep(0.2)
            effects.append("i2")
            await asyncio.sleep(0.1)
            effects.append("i3")
            return {}

        self.sm.states = []
        with contextlib.ExitStack() as stack:
            _get_used_resources = stack.enter_context(
                unittest.mock.patch.object(
                    self.cr, "_get_used_resources",
                )
            )
            _get_used_resources.side_effect = fake_get_used_resources

            self.sm.ensure = unittest.mock.AsyncMock()
            self.sm.ensure.side_effect = fake_ensure

            stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.customresource.update_status",
            ))

            await asyncio.gather(
                self.cr.reconcile(self.ctx, 0),
            )

        self.assertEquals(
            tuple(effects), ("e1", "e2", "e3", "i1", "i2", "i3")
        )

    @unittest.mock.patch("yaook.statemachine.customresource.update_status")
    async def test_reconcile_sets_configuration_invalid(self, update_status):
        self.sm.ensure.side_effect = exceptions.ConfigurationInvalid("test")

        await self.cr.reconcile(self.ctx, 0)

        update_status.assert_any_call(
                    unittest.mock.ANY,
                    self.ctx.namespace, self.ctx.parent_name,
                    phase=context.Phase.UPDATING,
                    generation=0,
                    conditions=[customresource.ConditionUpdate(
                        type_=customresource.CommonConditionTypes.
                        CONVERGED.value,
                        reason="InProgress",
                        message="",
                        status=False,
                    )],
        )

        update_status.assert_called_with(
                    unittest.mock.ANY,
                    self.ctx.namespace, self.ctx.parent_name,
                    phase=context.Phase.INVALID_CONFIGURATION,
                    generation=0,
                    conditions=[customresource.ConditionUpdate(
                        type_=customresource.CommonConditionTypes.
                        CONVERGED.value,
                        reason="ConfigurationInvalid",
                        message="test",
                        status=False,
                    )],
        )

    @unittest.mock.patch(
        "yaook.statemachine.customresource.CustomResource._cleanup")
    @unittest.mock.patch("yaook.statemachine.customresource.update_status")
    async def test_reconcile_sets_updated_on_success(self, update_status,
                                                     _cleanup):
        self.sm.ensure.return_value = []

        await self.cr.reconcile(self.ctx, 0)

        update_status.assert_any_call(
                    unittest.mock.ANY,
                    self.ctx.namespace, self.ctx.parent_name,
                    phase=context.Phase.UPDATING,
                    generation=0,
                    conditions=[customresource.ConditionUpdate(
                        type_=customresource.CommonConditionTypes.
                        CONVERGED.value,
                        reason="InProgress",
                        message="",
                        status=False,
                    )],
        )

        update_status.assert_called_with(
                    unittest.mock.ANY,
                    self.ctx.namespace, self.ctx.parent_name,
                    phase=context.Phase.UPDATED,
                    generation=0,
                    updated_generation=0,
                    conditions=[customresource.ConditionUpdate(
                        type_=customresource.CommonConditionTypes.
                        CONVERGED.value,
                        reason="Success",
                        message="",
                        status=True,
                    )],
                    additional_patch={},
        )

    @unittest.mock.patch(
        "yaook.statemachine.customresource.CustomResource._cleanup")
    @unittest.mock.patch("yaook.statemachine.customresource.update_status")
    async def test_reconcile_does_no_cleanup_after_config_invalid(
            self, update_status, _cleanup):
        self.sm.ensure.side_effect = exceptions.ConfigurationInvalid("test")

        await self.cr.reconcile(self.ctx, 0)
        self.cr._cleanup.assert_not_called()

    @unittest.mock.patch(
        "yaook.statemachine.customresource.CustomResource._cleanup")
    @unittest.mock.patch("yaook.statemachine.customresource.update_status")
    async def test_reconcile_does_no_cleanup_after_kclient_404(
            self, update_status, _cleanup):
        update_status.side_effect = [1, kclient.ApiException(status=404)]
        self.ctx.parent = {"metadata": {"deletionTimestamp": "1"}}

        await self.cr.reconcile(self.ctx, 0)
        self.cr._cleanup.assert_not_called()


class DummyResource(resources.Resource):
    async def reconcile(self, ctx, dependencies):
        pass

    async def delete(self, ctx):
        pass

    async def cleanup_orphans(self, ctx, protect):
        pass


class TestStateMachineAssembly(unittest.IsolatedAsyncioTestCase):

    class StateMachineAssemblyTest(customresource.CustomResource):
        API_GROUP = "test.yaook.cloud"
        API_GROUP_VERSION = "v1"
        PLURAL = "assemblytests"
        KIND = "AssemblyTest"

        res_a = DummyResource()
        res_b = DummyResource(add_dependencies=[res_a])
        res_c = DummyResource()

        vdep = versioneddependencies.VersionedDockerImage(
            "imageurl",
            versioneddependencies.SemVerSelector([]),
        )

        def __init__(self, **kwargs):
            super().__init__(assemble_sm=True, **kwargs)

    def setUp(self):
        self.logger = logging.getLogger(__name__ + type(self).__qualname__)

    def test_state_machine_assembly(self):
        cr = self.StateMachineAssemblyTest(logger=self.logger)

        self.assertCountEqual(
            cr.sm._dependencies.keys(),
            [cr.res_a, cr.res_b, cr.res_c],
        )

        self.assertEqual(
            cr.sm._dependencies[cr.res_a],
            set([]),
        )

        self.assertEqual(
            cr.sm._dependencies[cr.res_b],
            set([cr.res_a]),
        )

        self.assertEqual(
            cr.sm._dependencies[cr.res_c],
            set([]),
        )


class TestStateMachineVersiondDependency(unittest.IsolatedAsyncioTestCase):

    class StateMachineVdepTest(customresource.CustomResource):
        API_GROUP = "test.yaook.cloud"
        API_GROUP_VERSION = "v1"
        PLURAL = "versioneddependencytests"
        KIND = "VersionedDependencyTest"

        res_a = DummyResource()

        vdep_a = versioneddependencies.VersionedDockerImage(
            "imageurl_a",
            versioneddependencies.SemVerSelector([]),
        )
        vdep_b = versioneddependencies.VersionedDockerImage(
            "imageurl_b",
            versioneddependencies.SemVerSelector([]),
        )

        def __init__(self, **kwargs):
            super().__init__(assemble_sm=True, **kwargs)

    def setUp(self):
        self.logger = logging.getLogger(__name__ + type(self).__qualname__)

    def test_get_versioned_dependency(self):
        cr = self.StateMachineVdepTest(logger=self.logger)
        depdenencies = cr.get_versioned_dependency()
        self.assertEqual(depdenencies, [cr.vdep_a, cr.vdep_b])


@ddt.ddt
class TestSingleReleaseAwareCustomResource(unittest.IsolatedAsyncioTestCase):
    class CustomResourceTest(customresource.ReleaseAwareCustomResource):
        API_GROUP = "test.yaook.cloud"
        API_GROUP_VERSION = "v1"
        PLURAL = "testresources"
        KIND = "TestResource"
        RELEASES = ["rel-a", "rel-b", "rel-c", "rel-d"]
        VALID_UPGRADE_TARGETS = ["rel-b", "rel-c"]
        UPGRADE_STRATEGY = customresource.UpgradeStrategy.SINGLE_RELEASE

    def setUp(self):
        self.logger = logging.getLogger(__name__ + type(self).__qualname__)
        self.sm = unittest.mock.Mock(["ensure"])
        self.sm.ensure = unittest.mock.AsyncMock()
        self.sm.ensure.return_value = {}

        self.ri = unittest.mock.Mock(interfaces.ResourceInterfaceWithStatus)
        self.ri.plural = unittest.mock.sentinel.plural

        self.ctx = context.Context(
            api_client=None,
            field_manager=None,
            instance=None,
            instance_data=None,
            logger=self.logger,
            namespace="NAMESPACE",
            parent={
                "metadata": {
                    "name": "myname"
                },
            },
            parent_intf=self.ri,
        )

        with contextlib.ExitStack() as stack:
            StateMachine = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.statemachine.StateMachine",
            ))
            StateMachine.return_value = self.sm

            self.cr = self.CustomResourceTest(logger=self.logger)

    def _set_versions(
        self, current, target, next_=None, additional_patch=None
    ) -> context.Context:
        parent = dict(self.ctx.parent)
        parent.update(
            {
                "spec": {"targetRelease": target},
                "status": {
                    "installedRelease": current,
                    "nextRelease": next_,
                },
            }
        )
        if additional_patch is not None:
            parent.update(additional_patch)
        return dataclasses.replace(self.ctx, parent=parent)

    async def test___validate_config_new_deploy(self):
        ctx = self._set_versions(None, "rel-b")
        await self.cr._validate_config(ctx)

    async def test___validate_config_new_deploy_to_non_upgradable_version(
            self):
        ctx = self._set_versions(None, "rel-d")
        await self.cr._validate_config(ctx)

    async def test___validate_config_new_deploy_raises_wrong_ver(self):
        ctx = self._set_versions(None, "rel-x")
        with self.assertRaises(exceptions.ConfigurationInvalid):
            await self.cr._validate_config(ctx)

    async def test___validate_config_upgrade(self):
        ctx = self._set_versions("rel-a", "rel-b")
        await self.cr._validate_config(ctx)

    async def test___validate_config_upgrade_raises_downgrade(self):
        ctx = self._set_versions("rel-b", "rel-a")
        with self.assertRaises(exceptions.ConfigurationInvalid):
            await self.cr._validate_config(ctx)

    async def test___validate_config_upgrade_raises_jump(self):
        ctx = self._set_versions("rel-a", "rel-c")
        with self.assertRaisesRegex(
                exceptions.ConfigurationInvalid,
                "upgrading one Release at a time"):
            await self.cr._validate_config(ctx)

    async def test___validate_config_upgrade_raises_non_upgradable(self):
        ctx = self._set_versions("rel-c", "rel-d")
        with self.assertRaisesRegex(
                exceptions.ConfigurationInvalid,
                "upgrade from rel-c to rel-d not implemented"):
            await self.cr._validate_config(ctx)

    @unittest.mock.patch(
        "yaook.statemachine.interfaces."
        "ResourceInterfaceWithStatus.read_status")
    @unittest.mock.patch(
        "yaook.statemachine.customresource.CustomResource._cleanup")
    @unittest.mock.patch("yaook.statemachine.customresource.update_status")
    async def test_reconcile_sets_next_release(
        self,
        update_status,
        _cleanup,
        read_status
    ):
        ctx = self._set_versions("rel-a", "rel-b")

        update_status.return_value = ctx.parent

        read_status.return_value = {
            "status": {
                "phase": "Updated",
            }
        }

        await self.cr.reconcile(ctx, 0)

        update_status.assert_any_call(
            unittest.mock.ANY,
            ctx.namespace, ctx.parent_name,
            phase=context.Phase.UPDATED,
            updated_generation=0,
            generation=0,
            conditions=[
                customresource.ConditionUpdate(
                    type_=customresource.CommonConditionTypes.CONVERGED.value,
                    reason="Success",
                    message="",
                    status=True,)
            ],
            additional_patch={
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-b",
            }
        )

    async def test_get_next_version(self):
        ctx = self._set_versions("rel-a", "rel-c")

        ver = self.cr.get_next_version(ctx)
        self.assertEqual(ver, "rel-b")

    @unittest.mock.patch("yaook.statemachine.customresource.update_status")
    async def test_discover_initial_installed_release_with_status(
        self,
        update_status
    ):
        ctx = self._set_versions(None, "rel-b", additional_patch={
            "status": {
                "exsting": "resource",
            },
        })

        await self.cr.discover_initial_installed_release(ctx)
        update_status.assert_any_call(
            unittest.mock.ANY,
            ctx.namespace, ctx.parent_name,
            phase=None,
            generation=None,
            updated_generation=None,
            conditions=[],
            additional_patch={
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-b",
            }
        )

    @unittest.mock.patch("yaook.statemachine.customresource.update_status")
    async def test_discover_initial_installed_release_new_deployment(
        self,
        update_status
    ):
        ctx = self._set_versions(None, "rel-b")
        await self.cr.discover_initial_installed_release(ctx)
        update_status.assert_any_call(
            unittest.mock.ANY,
            ctx.namespace, ctx.parent_name,
            phase=None,
            generation=None,
            updated_generation=None,
            conditions=[],
            additional_patch={
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-b",
            }
        )

    @unittest.mock.patch(
        "yaook.statemachine.customresource.ReleaseAwareCustomResource."
        "get_unversioned_installed_release")
    @unittest.mock.patch("yaook.statemachine.customresource.update_status")
    async def test_overriden_discover_initial_installed_release(
        self,
        update_status,
        get_unversioned_installed_release
    ):
        ctx = self._set_versions(
            None,
            "rel-c",
            additional_patch={
                "status": {
                    "exsting": "resource"
                }
            })

        await self.cr.discover_initial_installed_release(ctx)
        update_status.assert_any_call(
            unittest.mock.ANY,
            ctx.namespace, ctx.parent_name,
            phase=None,
            generation=None,
            updated_generation=None,
            conditions=[],
            additional_patch={
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-c",
            }
        )
        get_unversioned_installed_release.assert_not_called()

    async def test__replace_context_status(self):
        new_ctx = self.cr._replace_context_status(
            self.ctx,
            {
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-d",
                version_utils.KEY_STATUS_NEXT_RELEASE: "rel-d",
            }
        )

        self.assertEqual(
            new_ctx.parent["status"],
            {
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-d",
                version_utils.KEY_STATUS_NEXT_RELEASE: "rel-d",
            }
        )

    async def test__get_succesful_update_patch_multiple(self):
        ctx = self._set_versions("rel-a", "rel-b")

        self.assertEqual(
            self.cr._get_successful_update_patch(ctx),
            {
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-b",
            }
        )

    @ddt.data(
        ("Updating", True),
        ("Created", False),
        ("WaitingForDependency", False),
        ("BackingOff", False),
        ("InvalidConfiguration", False),
        ("Failed", False),
        ("Completed", False),
        ("Updated", True),
    )
    @ddt.unpack
    async def test__needs_release_update(
            self, phase, expected):
        ctx = self._set_versions("rel-a", "rel-b", additional_patch={
            "status": {
                "phase": phase,
            },
        })
        intf = unittest.mock.Mock(interfaces.ResourceInterfaceWithStatus)
        intf.read_status.return_value = ctx.parent

        self.assertEqual(
            await self.cr._needs_release_update(ctx, intf),
            expected
        )

    @ddt.data(
        ("rel-a", "rel-b", 1),
        ("rel-b", "rel-c", 1),
    )
    async def test__get_successful_updated_generation(self, data):
        installed, target, expected = data
        ctx = self._set_versions(
            current=installed,
            target=target,
        )
        self.assertEqual(
            self.cr._get_successful_updated_generation(ctx, 1),
            expected
        )


@ddt.ddt
class TestMultipleReleaseAwareCustomResource(unittest.IsolatedAsyncioTestCase):
    class CustomResourceTest(customresource.ReleaseAwareCustomResource):
        API_GROUP = "test.yaook.cloud"
        API_GROUP_VERSION = "v1"
        PLURAL = "testresources"
        KIND = "TestResource"
        RELEASES = ["rel-a", "rel-b", "rel-c", "rel-d", "rel-e"]
        VALID_UPGRADE_TARGETS = ["rel-b", "rel-c", "rel-e"]
        UPGRADE_STRATEGY = customresource.UpgradeStrategy.MULTIPLE_RELEASE

    def setUp(self):
        self.logger = logging.getLogger(__name__ + type(self).__qualname__)
        self.sm = unittest.mock.Mock(["ensure"])
        self.sm.ensure = unittest.mock.AsyncMock()
        self.sm.ensure.return_value = {}

        self.ri = unittest.mock.Mock(interfaces.ResourceInterfaceWithStatus)
        self.ri.plural = unittest.mock.sentinel.plural

        self.ctx = context.Context(
            api_client=None,
            field_manager=None,
            instance=None,
            instance_data=None,
            logger=self.logger,
            namespace="NAMESPACE",
            parent={
                "metadata": {
                    "name": "myname"
                },
            },
            parent_intf=self.ri,
        )
        with contextlib.ExitStack() as stack:
            StateMachine = stack.enter_context(unittest.mock.patch(
                "yaook.statemachine.statemachine.StateMachine",
            ))
            StateMachine.return_value = self.sm

            self.cr = self.CustomResourceTest(logger=self.logger)

    def _set_versions(
        self, current, target, next_=None, additional_patch=None
    ) -> context.Context:
        parent = dict(self.ctx.parent)
        if target is not None:
            parent.update({
                "spec": {"targetRelease": target},
            })

        if current is not None or next_ is not None:
            parent.update({
                "status": {
                    "installedRelease": current,
                    "nextRelease": next_,
                },
            })
        if additional_patch is not None:
            parent.update(additional_patch)
        return dataclasses.replace(self.ctx, parent=parent)

    async def test___validate_config_new_deploy(self):
        ctx = self._set_versions(None, "rel-b")
        await self.cr._validate_config(ctx)

    async def test___validate_config_new_deploy_to_non_upgradable_version(
            self):
        ctx = self._set_versions(None, "rel-d")
        await self.cr._validate_config(ctx)

    async def test___validate_config_new_deploy_raises_wrong_ver(self):
        ctx = self._set_versions(None, "rel-x")
        with self.assertRaises(exceptions.ConfigurationInvalid):
            await self.cr._validate_config(ctx)

    async def test___validate_config_upgrade_one_version(self):
        ctx = self._set_versions("rel-a", "rel-b")
        await self.cr._validate_config(ctx)

    async def test___validate_config_upgrade_multiple_versions(self):
        ctx = self._set_versions("rel-a", "rel-c")
        await self.cr._validate_config(ctx)

    async def test___validate_config_upgrade_raises_downgrade(self):
        ctx = self._set_versions("rel-b", "rel-a")
        with self.assertRaises(exceptions.ConfigurationInvalid):
            await self.cr._validate_config(ctx)

    async def test___validate_config_wrong_upgrade_path_raises(self):
        ctx = self._set_versions("rel-a", "rel-e")
        with self.assertRaisesRegex(
                exceptions.ConfigurationInvalid,
                "is needed to reach the target release"):
            await self.cr._validate_config(ctx)

    async def test___validate_config_upgrade_raises_non_upgradable(self):
        ctx = self._set_versions("rel-c", "rel-d")
        with self.assertRaisesRegex(
                exceptions.ConfigurationInvalid,
                "upgrade from rel-c to rel-d not implemented"):
            await self.cr._validate_config(ctx)

    @unittest.mock.patch(
        "yaook.statemachine.interfaces."
        "ResourceInterfaceWithStatus.read_status")
    @unittest.mock.patch(
        "yaook.statemachine.customresource.CustomResource._cleanup")
    @unittest.mock.patch("yaook.statemachine.customresource.update_status")
    async def test_reconcile_sets_next_release(
        self,
        update_status,
        _cleanup,
        read_status
    ):
        ctx = self._set_versions("rel-a", "rel-c")

        mocked_ctx = self._set_versions("rel-a", "rel-c", next_="rel-b")

        update_status.return_value = mocked_ctx.parent

        read_status.return_value = {
            "status": {
                "phase": "Updated",
            }
        }

        with self.assertRaises(exceptions.TriggerReconcile):
            await self.cr.reconcile(ctx, 0)

        update_status.assert_any_call(
            unittest.mock.ANY,
            ctx.namespace, ctx.parent_name,
            phase=context.Phase.UPDATING,
            generation=0,
            updated_generation=None,
            conditions=[
                customresource.ConditionUpdate(
                    type_=customresource.CommonConditionTypes.CONVERGED.value,
                    reason="Updating",
                    message="",
                    status=True,)
            ],
            additional_patch={
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-b",
                version_utils.KEY_STATUS_NEXT_RELEASE: "rel-c",
            }
        )

        update_status.assert_any_call(
            unittest.mock.ANY,
            ctx.namespace, ctx.parent_name,
            phase=None,
            generation=None,
            updated_generation=None,
            conditions=[],
            additional_patch={
                version_utils.KEY_STATUS_NEXT_RELEASE: "rel-b",
            }
        )

    async def test_get_next_version(self):
        ctx = self._set_versions("rel-a", "rel-c")

        ver = self.cr.get_next_version(ctx)
        self.assertEqual(ver, "rel-b")

    @unittest.mock.patch("yaook.statemachine.customresource.update_status")
    async def test_discover_initial_installed_release_with_status(
        self,
        update_status
    ):
        ctx = self._set_versions(None, "rel-c", additional_patch={
            "status": {
                "exsting": "resource",
            },
        })

        await self.cr.discover_initial_installed_release(ctx)
        update_status.assert_any_call(
            unittest.mock.ANY,
            ctx.namespace, ctx.parent_name,
            phase=None,
            generation=None,
            updated_generation=None,
            conditions=[],
            additional_patch={
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-a",
                version_utils.KEY_STATUS_NEXT_RELEASE: "rel-b",
            }
        )

    @unittest.mock.patch("yaook.statemachine.customresource.update_status")
    async def test_discover_initial_installed_release_new_deployment(
        self,
        update_status
    ):
        ctx = self._set_versions(None, "rel-c")
        await self.cr.discover_initial_installed_release(ctx)
        update_status.assert_any_call(
            unittest.mock.ANY,
            ctx.namespace, ctx.parent_name,
            phase=None,
            generation=None,
            updated_generation=None,
            conditions=[],
            additional_patch={
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-c",
                version_utils.KEY_STATUS_NEXT_RELEASE: "rel-c",
            }
        )

    @unittest.mock.patch(
        "yaook.statemachine.customresource.ReleaseAwareCustomResource."
        "get_unversioned_installed_release")
    @unittest.mock.patch("yaook.statemachine.customresource.update_status")
    async def test_overriden_discover_initial_installed_release(
        self,
        update_status,
        get_unversioned_installed_release
    ):
        get_unversioned_installed_release.return_value = "rel-b"
        ctx = self._set_versions(None, "rel-c", additional_patch={
            "status": {
                "exsting": "resource",
            },
        })

        await self.cr.discover_initial_installed_release(ctx)
        update_status.assert_any_call(
            unittest.mock.ANY,
            ctx.namespace, ctx.parent_name,
            phase=None,
            generation=None,
            updated_generation=None,
            conditions=[],
            additional_patch={
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-b",
                version_utils.KEY_STATUS_NEXT_RELEASE: "rel-c",
            }
        )
        get_unversioned_installed_release.assert_called()

    async def test__replace_context_status(self):
        new_ctx = self.cr._replace_context_status(
            self.ctx,
            {
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-d",
                version_utils.KEY_STATUS_NEXT_RELEASE: "rel-d",
            }
        )

        self.assertEqual(
            new_ctx.parent["status"],
            {
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-d",
                version_utils.KEY_STATUS_NEXT_RELEASE: "rel-d",
            }
        )

    async def test__get_succesful_update_patch_multiple(self):
        ctx = self._set_versions("rel-a", "rel-c", next_="rel-b")

        self.assertEqual(
            self.cr._get_successful_update_patch(ctx),
            {
                version_utils.KEY_STATUS_INSTALLED_RELEASE: "rel-b",
                version_utils.KEY_STATUS_NEXT_RELEASE: "rel-c",
            }
        )

    @ddt.data(
        ("Updating", True),
        ("Created", False),
        ("WaitingForDependency", False),
        ("BackingOff", False),
        ("InvalidConfiguration", False),
        ("Failed", False),
        ("Completed", False),
        ("Updated", True),
    )
    @ddt.unpack
    async def test__needs_release_update(
            self, phase, expected):
        ctx = self._set_versions("rel-a", "rel-c", additional_patch={
            "status": {
                "phase": phase,
            },
        })
        intf = unittest.mock.Mock(interfaces.ResourceInterfaceWithStatus)
        intf.read_status.return_value = ctx.parent

        self.assertEqual(
            await self.cr._needs_release_update(ctx, intf),
            expected
        )

    @ddt.data(
        ("rel-a", "rel-b", "rel-c", None),
        ("rel-b", "rel-c", "rel-c", 1),
        ("rel-a", "rel-b", "rel-d", None),
    )
    async def test__get_successful_updated_generation(self, data):
        installed, next_, target, expected = data
        ctx = self._set_versions(
            current=installed,
            next_=next_,
            target=target,
        )
        self.assertEqual(
            self.cr._get_successful_updated_generation(ctx, 1),
            expected
        )


class FinalDummyResource(resources.Resource):
    ret_value = True

    async def reconcile(self, ctx, dependencies):
        pass

    async def is_successful(self, ctx):
        return self.ret_value

    async def delete(self, ctx):
        pass

    async def cleanup_orphans(self, ctx, protect):
        pass


@ddt.ddt()
class TestOneshotCustomResource(unittest.IsolatedAsyncioTestCase):

    class TestOneshotCR(customresource.OneshotCustomResource):
        API_GROUP = "test.yaook.cloud"
        API_GROUP_VERSION = "v1"
        PLURAL = "testresources"
        KIND = "TestResource"

        res_a = FinalDummyResource()

        def __init__(self, test_return_value=True, **kwargs):
            super().__init__(assemble_sm=True, **kwargs)
            self.FINAL_STATE = self.res_a
            self.res_a.ret_value = test_return_value

    def setUp(self):
        self.logger = logging.getLogger(__name__ + type(self).__qualname__)

        self.ctx = unittest.mock.Mock(["logger",
                                       "api_client",
                                       "namespace",
                                       "parent_name"])
        self.cr = self.TestOneshotCR(logger=self.logger)

    @unittest.mock.patch(
        "yaook.statemachine.interfaces."
        "ResourceInterfaceWithStatus.read_status")
    @unittest.mock.patch(
        "yaook.statemachine.customresource.CustomResource.reconcile"
    )
    async def test_reconciles_if_not_final(self, reconcile, read_status):
        read_status.return_value = {}
        await self.cr.reconcile(self.ctx, 0)
        reconcile.assert_called_once_with(self.ctx, 0)

    @unittest.mock.patch(
        "yaook.statemachine.interfaces."
        "ResourceInterfaceWithStatus.read_status")
    @unittest.mock.patch(
        "yaook.statemachine.customresource.CustomResource.reconcile"
    )
    @ddt.data(context.Phase.COMPLETED, context.Phase.FAILED)
    async def test_does_not_reconcile_if_final(self, data, reconcile,
                                               read_status):
        read_status.return_value = {
            "status": {
                "phase": data.value
            }
        }
        await self.cr.reconcile(self.ctx, 0)
        reconcile.assert_not_called()

    @ddt.data(
        (True, context.Phase.COMPLETED, "Success"),
        (False, context.Phase.FAILED, "Failed")
    )
    async def test__get_successful_update_status(self, data):
        result, phase, condition_text = data
        cr = self.TestOneshotCR(logger=self.logger, test_return_value=result)
        out_phase, out_cond = await cr._get_successful_update_status(self.ctx)
        self.assertEqual(phase, out_phase)
        self.assertEqual(condition_text, out_cond.reason)
