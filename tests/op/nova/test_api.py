#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64
import os
import pprint
import unittest.mock
import uuid
import ddt

import yaook.op.common as op_common
import yaook.op.scheduling_keys as scheduling_keys
import yaook.op.nova as nova
import yaook.op.nova.resources as n_resources
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces

from tests import testutils
from unittest.mock import sentinel


NAMESPACE = "test-namespace"
NAME = "nova"
CONFIG_FILENAME = "nova.conf"
CONFIG_PATH = "/etc/nova"
POLICY_FILENAME = "policy.yaml"
POLICY_RULE_KEY = "os_compute_api:os-admin-password"
POLICY_RULE_VALUE = "rule:admin_or_owner"
NOVA_POLICY = {"policy": {POLICY_RULE_KEY: POLICY_RULE_VALUE}}
CONFIG_SECRET_NAME = "config-secret"
CONFIG_SECRET_KEY = "mysecretkey"
CONFIG_SECRET_VALUE = "mysecretvalue"


def _generate_ssh_keypair():
    key_id = str(uuid.uuid4())
    private_key = f"{key_id}-private"
    public_key = f"{key_id}-public"
    return private_key, public_key


def _get_nova_deployment_yaml(
        keystone_name,
        compute_extra=None,
        compute_ceph=None):
    return {
        "metadata": {
            "name": NAME,
            "namespace": NAMESPACE,
        },
        "spec": {
            "keystoneRef": {
                "name": keystone_name,
                "kind": "KeystoneDeployment",
            },
            "region": {
                "name": "regionname",
                "parent": "parentregionname",
            },
            "api": {
                "ingress": {
                    "fqdn": "nova-ingress",
                    "port": 8080,
                    "ingressClassName": "nginx",
                },
                "replicas": 2,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "resources": testutils.generate_resources_dict(
                    "api.nova-api",
                    "api.ssl-terminator",
                    "api.ssl-terminator-external",
                    "api.ssl-terminator-external",
                    "api.service-reload",
                    "api.service-reload-external",
                ),
            },
            "database": {
                "api": {
                    "replicas": 2,
                    "timeoutClient": 300,
                    "storageSize": "8Gi",
                    "storageClassName": "api-class",
                    "proxy": {
                        "replicas": 1,
                        "resources": testutils.generate_db_proxy_resources(
                            "database.api.proxy"),
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                    "resources": testutils.generate_db_resources(
                        "database.api"),
                },
                "cell0": {
                    "replicas": 20,
                    "timeoutClient": 300,
                    "storageSize": "8Gi",
                    "storageClassName": "cell0-class",
                    "proxy": {
                        "replicas": 10,
                        "resources": testutils.generate_db_proxy_resources(
                            "database.cell0.proxy"),
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                    "resources": testutils.generate_db_resources(
                        "database.cell0"),
                },
                "cell1": {
                    "replicas": 21,
                    "timeoutClient": 300,
                    "storageSize": "8Gi",
                    "storageClassName": "cell1-class",
                    "proxy": {
                        "replicas": 11,
                        "resources": testutils.generate_db_proxy_resources(
                            "database.cell1.proxy"),
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                    "resources": testutils.generate_db_resources(
                        "database.cell1"),
                },
                "placement": {
                    "replicas": 22,
                    "timeoutClient": 300,
                    "storageSize": "9Gi",
                    "storageClassName": "placement-class",
                    "proxy": {
                        "replicas": 12,
                        "resources": testutils.generate_db_proxy_resources(
                            "database.placement.proxy"),
                    },
                    "backup": {
                        "schedule": "0 * * * *"
                    },
                    "resources": testutils.generate_db_resources(
                        "database.placement"),
                },
            },
            "databaseCleanup": {
                "schedule": "0 0 * * *",
                "deletionTimeRange": 13,
            },
            "placementCleanup": {
                "schedule": "0 0 1 * *",
            },
            "messageQueue": {
                "cell1": {
                    "replicas": 11,
                    "storageSize": "2Gi",
                    "storageClassName": "cell1-mq-class",
                    "resources": testutils.generate_amqp_resources(
                        "messageQueue.cell1"),
                },
            },
            "memcached": {
                "replicas": 2,
                "memory": "512",
                "connections": "2000",
                "resources": testutils.generate_memcached_resources(),
            },
            "issuerRef": {
                "name": "issuername"
            },
            "consoleauth": {
                "replicas": 1,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "resources": testutils.generate_resources_dict(
                    "consoleauth.nova-consoleauth",
                ),
            },
            "vnc": {
                "ingress": {
                    "fqdn": "vnc-ingress",
                    "port": 8080,
                    "ingressClassName": "nginx",
                },
                "replicas": 1,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "resources": testutils.generate_resources_dict(
                    "vnc.nova-novncproxy",
                    "vnc.ssl-terminator-external",
                    "vnc.service-reload-external",
                ),
            },
            "placement": {
                "ingress": {
                    "fqdn": "placement-ingress",
                    "port": 8080,
                    "ingressClassName": "nginx",
                },
                "replicas": 4,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "region": {
                    "name": "regionname",
                    "parent": "parentregionname",
                },
                "resources": testutils.generate_resources_dict(
                    "placement.placement",
                    "placement.ssl-terminator",
                    "placement.ssl-terminator-external",
                    "placement.service-reload",
                    "placement.service-reload-external",
                ),
            },
            "metadata": {
                "replicas": 5,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "resources": testutils.generate_resources_dict(
                    "metadata.nova-metadata",
                    "metadata.ssl-terminator",
                    "metadata.service-reload",
                ),
            },
            "conductor": {
                "replicas": 6,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "resources": testutils.generate_resources_dict(
                    "conductor.nova-conductor",
                ),
            },
            "scheduler": {
                "replicas": 7,
                "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                "resources": testutils.generate_resources_dict(
                    "scheduler.nova-scheduler",
                ),
            },
            "targetRelease": "yoga",
            "eviction": {},
            "compute": {
                "configTemplates": [
                    {
                        "nodeSelectors": [
                            {"matchLabels": {}},  # all nodes
                        ],
                        "novaComputeConfig": {}
                    },
                    {
                        "nodeSelectors": [
                            {"matchLabels": compute_extra},
                        ],
                        "novaComputeConfig": {
                            "libvirt": {
                                "virt_type": "qemu",
                            },
                        },
                    },
                    {
                        "nodeSelectors": [
                            {"matchLabels": {"provider_aggregate": "custom"}},
                        ],
                        "computeProviderConfig": {
                            "providers": {
                                "traits": {
                                    "additional": ["CUSTOM_TRAIT"]
                                },
                            },
                        },
                    },
                    {
                        "nodeSelectors": [
                            {"matchLabels": {"hostaggregate_test": "dummy"}},
                        ],
                        "hostAggregates": [
                            "aggregate-az-and-property",
                            "aggregate-dual-property",
                        ]
                    },
                    {
                        "nodeSelectors": [
                            {"matchLabels": {"multizone_test": "dummy"}},
                        ],
                        "hostAggregates": [
                            "aggregate-zone1",
                            "aggregate-zone2",
                        ]
                    },
                    {
                        "nodeSelectors": [
                            {"matchLabels": {"idonotexist": "dummy"}},
                        ],
                        "hostAggregates": [
                            "aggregateusedvanish",
                        ]
                    },
                    {
                        "nodeSelectors": [
                            {"matchLabels": compute_ceph}
                        ],
                        "volumeBackends": {
                            "ceph": {
                                "enabled": True,
                                "keyringSecretName": "keyring-ref",
                                "user": "keyring-user",
                                "uuid": "some-libvirt-uuid",
                                "cephConfig": {}
                            }
                        }
                    }
                ],
                "resources": testutils.generate_resources_dict(
                    "compute.keygen",
                    "compute.chown-nova",
                    "compute.nova-compute",
                    "compute.nova-compute-ssh",
                    "compute.libvirtd",
                    "compute.compute-evict-job",
                ),
            },
            "novaConfig": {},
            "novaSecrets": [
                {
                    "secretName": CONFIG_SECRET_NAME,
                    "items": [{
                        "key": CONFIG_SECRET_KEY,
                        "path": "/DEFAULT/mytestsecret",
                    }],
                },
            ],
            "jobResources": testutils.generate_resources_dict(
                "job.nova-db-cleanup-cronjob",
                "job.nova-placement-cleanup-cronjob",
                "job.nova-api-db-sync-job",
                "job.nova-create-cell1-job",
                "job.nova-db-sync-job",
                "job.nova-map-cell0-job",
                "job.placement-db-sync-job",
            ),
            "evictPollMigrationSpeedLocalDisk": 30,

        },
    }


def _get_nova_deployment_yaml_train(
        keystone_name,
        compute_extra=None,
        compute_ceph=None):
    deployment = _get_nova_deployment_yaml(
        keystone_name, compute_extra, compute_ceph)

    deployment["spec"]["targetRelease"] = "train"
    deployment["spec"]["database"]["placement"] = {
        "replicas": 21,
        "timeoutClient": 300,
        "storageSize": "8Gi",
        "storageClassName": "placement-class",
        "proxy": {
            "replicas": 11,
            "resources": testutils.generate_db_proxy_resources(
                "database.placement.proxy"),
        },
        "backup": {
            "schedule": "0 * * * *"
        },
        "memcached": {
            "replicas": 2,
            "memory": "512",
            "connections": "2000",
            "resources": testutils.generate_memcached_resources(),
        },
        "resources": testutils.generate_db_resources(
            "database.placement"),
    }

    return deployment


@ddt.ddt
class TestNovaDeploymentsBase(testutils.ReleaseAwareCustomResourceTestCase):
    @classmethod
    def setUpClass(cls) -> None:
        if cls is TestNovaDeploymentsBase:
            raise unittest.SkipTest("not relevant")
        super().setUpClass()

    def setUp(self):
        super().setUp()
        # improve test speed!
        self.__silent_patches = [
            unittest.mock.patch(
                "yaook.common.ssh.generate_ssh_keypair",
                new=_generate_ssh_keypair,
            ),
        ]
        for patch in self.__silent_patches:
            patch.start()
        self._env_backup = os.environ.pop(
            "YAOOK_EVICT_MANAGER_IMAGE", None
        )
        os.environ["YAOOK_EVICT_MANAGER_IMAGE"] = "dummy-image"

    def tearDown(self):
        for patch in self.__silent_patches:
            patch.stop()
        if self._env_backup is not None:
            os.environ["YAOOK_EVICT_MANAGER_IMAGE"] = self._env_backup
        else:
            del os.environ["YAOOK_EVICT_MANAGER_IMAGE"]
        super().tearDown()

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.labels_compute = {
            scheduling_keys.SchedulingKey.COMPUTE_HYPERVISOR.value:
                str(uuid.uuid4()),
            context.LABEL_L2_REQUIRE_MIGRATION: "False",
        }
        self.labels_hostaggregate = dict(self.labels_compute)
        self.labels_hostaggregate["hostaggregate_test"] = "dummy"
        self.labels_compute_extra = dict(self.labels_compute)
        self.labels_compute_extra["extra-compute"] = str(uuid.uuid4())
        self.labels_provider_config = dict(self.labels_compute)
        self.labels_provider_config["provider_aggregate"] = "custom"
        self.labels_compute_ceph = dict(self.labels_compute)
        self.labels_compute_ceph["extra-ceph"] = str(uuid.uuid4())
        self.default_node_setup = {
            "node1": {
                "unrelated": "key",
            },
            "node2": self.labels_compute,
            "node3": self.labels_compute,
            "node4": self.labels_compute_extra,
            "node5": self.labels_hostaggregate,
            "node7": self.labels_provider_config,
        }

        self.annotations = {
            context.LABEL_L2_REQUIRE_MIGRATION: False,
        }
        self.ceph_node_setup = dict(self.default_node_setup)
        self.ceph_node_setup["node3"] = self.labels_compute_ceph
        self.default_compute_nodes = [
            "node2", "node3", "node4", "node5", "node7",
        ]
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, CONFIG_SECRET_NAME,
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": CONFIG_SECRET_NAME,
                    },
                    "data": sm.api_utils.encode_secret_data({
                        CONFIG_SECRET_KEY: CONFIG_SECRET_VALUE,
                    }),
                },
            )
        self.client_mock.put_object(
            "compute.yaook.cloud", "v1", "novahostaggregates",
            NAMESPACE, "aggregate-az-and-property",
            {
                "apiVersion": "compute.yaook.cloud/v1",
                "kind": "NovaHostAggregate",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "aggregate-az-and-property",
                },
                "spec": {
                    "novaRef": {"name": NAME}
                }
            },
        )
        self.client_mock.put_object(
            "compute.yaook.cloud", "v1", "novahostaggregates",
            NAMESPACE, "aggregate-dual-property",
            {
                "apiVersion": "compute.yaook.cloud/v1",
                "kind": "NovaHostAggregate",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "aggregate-dual-property",
                },
                "spec": {
                    "novaRef": {"name": NAME}
                }
            },
        )

    async def test_nova_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "compute_api_keystone_user",
            },
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_nova_keystone_endpoint_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "compute_keystone_endpoint",
            },
        )

        self.assertEqual(endpoint["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(endpoint["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_nova_keystone_endpoint_is_created(self):
        deployment_yaml = _get_nova_deployment_yaml(self._keystone_name)
        self._configure_cr(nova.Nova, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "compute_keystone_endpoint"
            },
        )

        self.assertEqual(len(endpoints), 1)

    async def test_nova_keystone_endpoint_is_not_created(self):
        deployment_yaml = _get_nova_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["api"]["publishEndpoint"] = False
        self._configure_cr(nova.Nova, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "compute_keystone_endpoint"
            },
        )

        self.assertEqual(len(endpoints), 0)

    async def test_nova_keystone_endpoint_matches_region(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "compute_keystone_endpoint"
            },
        )

        self.assertEqual(endpoint["spec"]["region"]["name"],
                         "regionname")
        self.assertEqual(endpoint["spec"]["region"]["parent"],
                         "parentregionname")

    async def test_placement_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_api_keystone_user",
            },
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_placement_keystone_endpoint_matches_keystone_reference(self):  # noqa:E501
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_keystone_endpoint",
            },
        )

        self.assertEqual(endpoint["spec"]["keystoneRef"]["name"],
                         self._keystone_name)

    async def test_placement_keystone_endpoint_is_created(self):
        deployment_yaml = _get_nova_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["placement"]["publishEndpoint"] = True
        self._configure_cr(nova.Nova, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_keystone_endpoint"
            },
        )

        self.assertEqual(len(endpoints), 1)

    async def test_placement_keystone_endpoint_is_not_created(self):
        deployment_yaml = _get_nova_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["placement"]["publishEndpoint"] = False
        self._configure_cr(nova.Nova, deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        endpoints_int = interfaces.keystoneendpoint_interface(self.api_client)
        endpoints = await endpoints_int.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_keystone_endpoint"
            },
        )

        self.assertEqual(len(endpoints), 0)

    async def test_placement_keystone_endpoint_matches_region(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint, = await endpoints.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_keystone_endpoint"
            },
        )

        self.assertEqual(endpoint["spec"]["region"]["name"],
                         "regionname")
        self.assertEqual(endpoint["spec"]["region"]["parent"],
                         "parentregionname")

    async def test_cell0_job_is_configured_correctly(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell0_db_api_user_password",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell0_db",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "mysqlservices",
                context.LABEL_PARENT_NAME: db["metadata"]["name"],
            },
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell0_db_api_user",
            },
        )
        db_name = db["spec"]["database"]

        jobs = interfaces.job_interface(self.api_client)
        job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "cell0_map_job"},
        )

        envmap = {
            item.name: (item.value or item.value_from)
            for item in job.spec.template.spec.containers[0].env
        }
        self.assertEqual(envmap["DB_NAME"], db_name)
        self.assertEqual(
            envmap["DB_HOST"],
            db_service.metadata.name,
        )
        self.assertEqual(
            envmap["DB_USER"],
            db_user["spec"]["user"],
        )
        self.assertEqual(
            envmap["DB_USER_PASSWORD"].secret_key_ref.name,
            db_user_password_secret.metadata.name,
        )
        self.assertEqual(
            envmap["DB_USER_PASSWORD"].secret_key_ref.key,
            "password",
        )

    async def test_creates_api_database_and_user(self):
        self._make_all_databases_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        dbusers = interfaces.mysqluser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        db, = await dbs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_db",
            }
        )
        api_user, = await dbusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_db_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_db_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                         db["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(db["spec"]["replicas"], 2)
        self.assertEqual(db["spec"]["proxy"]["timeoutClient"], 300)
        self.assertEqual(db["spec"]["proxy"]["replicas"], 1)
        self.assertEqual(db["spec"]["storageClassName"],
                         "api-class")
        self.assertEqual(db["spec"]["storageSize"], "8Gi")

    async def test_creates_cell0_database_and_user(self):
        self._make_all_databases_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        dbusers = interfaces.mysqluser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        db, = await dbs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell0_db",
            }
        )
        api_user, = await dbusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell0_db_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell0_db_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                         db["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(db["spec"]["replicas"], 20)
        self.assertEqual(db["spec"]["proxy"]["timeoutClient"], 300)
        self.assertEqual(db["spec"]["proxy"]["replicas"], 10)
        self.assertEqual(db["spec"]["storageClassName"],
                         "cell0-class")
        self.assertEqual(db["spec"]["storageSize"], "8Gi")

    async def test_creates_cell1_database_and_user(self):
        self._make_all_databases_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        dbs = interfaces.mysqlservice_interface(self.api_client)
        dbusers = interfaces.mysqluser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        db, = await dbs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell1_db",
            }
        )
        api_user, = await dbusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell1_db_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell1_db_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serviceRef"]["name"],
                         db["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(db["spec"]["replicas"], 21)
        self.assertEqual(db["spec"]["proxy"]["timeoutClient"], 300)
        self.assertEqual(db["spec"]["proxy"]["replicas"], 11)
        self.assertEqual(db["spec"]["storageClassName"],
                         "cell1-class")
        self.assertEqual(db["spec"]["storageSize"], "8Gi")

    async def test_creates_config_with_api_database_uri(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_db_api_user_password",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_db",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "mysqlservices",
                context.LABEL_PARENT_NAME: db["metadata"]["name"],
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_db_api_user",
            },
        )
        db_name = db["spec"]["database"]
        nova_conf = config.data[CONFIG_FILENAME]
        cfg = testutils._parse_config(nova_conf, decode=True)

        self.assertEqual(
            cfg.get("api_database", "connection"),
            f"mysql+pymysql://{db_user['spec']['user']}:{db_user_password}@"
            f"{db_service.metadata.name}.{db_service.metadata.namespace}:3306/"
            f"{db_name}?charset=utf8&ssl_ca=/etc/ssl/certs/ca-bundle.crt",
        )

    async def test_creates_config_with_vnc_section(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        nova_conf = config.data[CONFIG_FILENAME]
        cfg = testutils._parse_config(nova_conf, decode=True)

        self.assertEqual(cfg.get('vnc', 'enabled'), 'true')
        self.assertEqual(cfg.get('vnc', 'auth_schemes'), 'vencrypt')
        self.assertEqual(cfg.get('vnc', 'vncserver_listen'), '0.0.0.0')
        self.assertEqual(cfg.get('vnc', 'novncproxy_port'), '8080')
        self.assertEqual(cfg.get('vnc', 'vencrypt_client_key'),
                         '/etc/pki/nova-novncproxy/client-key.pem')
        self.assertEqual(cfg.get('vnc', 'vencrypt_client_cert'),
                         '/etc/pki/nova-novncproxy/client-cert.pem')
        self.assertEqual(cfg.get('vnc', 'vencrypt_ca_certs'),
                         '/etc/pki/nova-novncproxy/ca-cert.pem')

    async def test_api_database_uri_refers_to_mounted_ca_bundle(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )

        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_PLURAL: "novadeployments",
            },
        )

        api, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "nova_api",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            api.spec.template.spec,
            testutils.find_configmap_volume(
                api.spec.template.spec,
                ca_certs.metadata.name,
            ),
            "nova-api",
        )

        nova_conf = config.data["nova.conf"]
        cfg = testutils._parse_config(nova_conf, decode=True)

        self.assertIn(
            f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
            cfg.get("api_database", "connection"),
        )

    async def test_database_frontendIssuer_name(self):
        self._make_all_databases_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        mysqlservices = interfaces.mysqlservice_interface(self.api_client)

        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell1_db",
                context.LABEL_COMPONENT: "cell0_db",
            },
        )

        self.assertEqual(
            "issuername",
            db["spec"]["frontendIssuerRef"]["name"],
        )

    async def test_creates_config_with_cell1_database_uri(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        mysqlusers = interfaces.mysqluser_interface(self.api_client)
        mysqlservices = interfaces.mysqlservice_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        db_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell1_db_api_user_password",
            },
        )
        db, = await mysqlservices.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell1_db",
            },
        )
        db_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "mysqlservices",
                context.LABEL_PARENT_NAME: db["metadata"]["name"],
            },
        )
        db_user_password = await sm.extract_password(
            self.ctx,
            db_user_password_secret.metadata.name,
        )
        db_user, = await mysqlusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell1_db_api_user",
            },
        )
        db_name = db["spec"]["database"]
        nova_conf = config.data[CONFIG_FILENAME]
        cfg = testutils._parse_config(nova_conf, decode=True)

        self.assertEqual(
            cfg.get("database", "connection"),
            f"mysql+pymysql://{db_user['spec']['user']}:{db_user_password}@"
            f"{db_service.metadata.name}.{db_service.metadata.namespace}:3306/"
            f"{db_name}?charset=utf8&ssl_ca=/etc/ssl/certs/ca-bundle.crt",
        )

    async def test_cell1_database_uri_refers_to_mounted_ca_bundle(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )

        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_PLURAL: "novadeployments",
            },
        )

        api, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "nova_api",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            api.spec.template.spec,
            testutils.find_configmap_volume(
                api.spec.template.spec,
                ca_certs.metadata.name,
            ),
            "nova-api",
        )

        nova_conf = config.data["nova.conf"]
        cfg = testutils._parse_config(nova_conf, decode=True)

        self.assertIn(
            f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
            cfg.get("database", "connection"),
        )

    async def test_creates_message_queue_and_user(self):
        self._make_all_mqs_succeed_immediately()

        await self.cr.sm.ensure(self.ctx)

        mqs = interfaces.amqpserver_interface(self.api_client)
        mqusers = interfaces.amqpuser_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        mq, = await mqs.list_(NAMESPACE)
        api_user, = await mqusers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell1_mq_api_user",
            }
        )
        api_user_password, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell1_mq_api_user_password",
            }
        )

        self.assertEqual(api_user["spec"]["serverRef"]["name"],
                         mq["metadata"]["name"])
        self.assertEqual(
            api_user["spec"]["passwordSecretKeyRef"]["name"],
            api_user_password.metadata.name,
        )
        self.assertEqual(mq["spec"]["replicas"], 11)
        self.assertEqual(mq["spec"]["storageSize"], "2Gi")
        self.assertEqual(mq["spec"]["storageClassName"], "cell1-mq-class")

    async def test_creates_config_with_transport_url(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )
        mq_user_password_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "cell1_mq_api_user_password",
            },
        )
        mq_service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_service",
                context.LABEL_PARENT_PLURAL: "amqpservers",
            },
        )
        mq_user_password = await sm.extract_password(
            self.ctx,
            mq_user_password_secret.metadata.name,
        )
        nova_conf = config.data[CONFIG_FILENAME]
        cfg = testutils._parse_config(nova_conf, decode=True)

        self.assertEqual(
            cfg.get("DEFAULT", "transport_url"),
            f"rabbit://api:{mq_user_password}@"
            f"{mq_service.metadata.name}.{mq_service.metadata.namespace}:5671/"
        )
        self.assertTrue(
            cfg.get("oslo_messaging_rabbit", "ssl"),
        )
        self.assertEqual(
            cfg.get("oslo_messaging_rabbit", "ssl_ca_file"),
            "/etc/ssl/certs/ca-bundle.crt",
        )

    async def test_creates_memcached(self):
        await self.cr.sm.ensure(self.ctx)

        memcacheds = interfaces.memcachedservice_interface(self.api_client)
        memcached, = await memcacheds.list_(NAMESPACE)

        self.assertEqual(
            memcached["spec"]["memory"],
            "512"
        )
        self.assertEqual(
            memcached["spec"]["connections"],
            "2000"
        )

    async def test_created_config_matches_keystone_users(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        users = interfaces.keystoneuser_interface(self.api_client)
        novauser, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT:
                            "compute_api_keystone_user"},
        )
        placementuser, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT:
                            "placement_api_keystone_user"},
        )

        nova_credentials_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
                context.LABEL_PARENT_NAME: novauser["metadata"]["name"],
                context.LABEL_PARENT_PLURAL: "keystoneusers",
            },
        )

        placement_credentials_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
                context.LABEL_PARENT_NAME: placementuser["metadata"]["name"],
                context.LABEL_PARENT_PLURAL: "keystoneusers",
            },
        )

        novauser_credentials = sm.api_utils.decode_secret_data(
            nova_credentials_secret.data
        )
        placementuser_credentials = sm.api_utils.decode_secret_data(
            placement_credentials_secret.data
        )
        cfg = testutils._parse_config(
            config.data[CONFIG_FILENAME],
            decode=True
        )

        self.assertEqual(cfg.get("keystone_authtoken", "username"),
                         novauser_credentials["OS_USERNAME"])
        self.assertEqual(cfg.get("keystone_authtoken", "password"),
                         novauser_credentials["OS_PASSWORD"])

        self.assertEqual(cfg.get("neutron", "username"),
                         novauser_credentials["OS_USERNAME"])
        self.assertEqual(cfg.get("neutron", "password"),
                         novauser_credentials["OS_PASSWORD"])

        self.assertEqual(cfg.get("placement", "username"),
                         placementuser_credentials["OS_USERNAME"])
        self.assertEqual(cfg.get("placement", "password"),
                         placementuser_credentials["OS_PASSWORD"])

    async def test_creates_config_with_metadata_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        metadata_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "metadata_proxy_shared_secret"},
        )
        metadata_pw = base64.b64decode(
            metadata_secret.data["password"].encode('ascii')
        ).decode('ascii')

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        cfg = testutils._parse_config(
            config.data[CONFIG_FILENAME],
            decode=True
        )
        cfg_pw = cfg.get("neutron", "metadata_proxy_shared_secret")
        self.assertEqual(metadata_pw, cfg_pw)

    async def test_creates_schedule_cronjob(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        cronjobs = interfaces.cronjob_interface(self.api_client)
        db_cleanup, = await cronjobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_cleanup",
            }
        )
        placement_cleanup, = await cronjobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_cleanup",
            }
        )
        self.assertEqual(db_cleanup.spec.schedule, "0 0 * * *")
        self.assertEqual(
            "13",
            db_cleanup.spec.job_template.spec.template.spec
            .containers[0].env[1].value
        )
        self.assertEqual(placement_cleanup.spec.schedule, "0 0 1 * *")

    async def test_db_cleanup_is_not_concurrent(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        cronjobs = interfaces.cronjob_interface(self.api_client)
        cronjob, = await cronjobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "db_cleanup",
            }
        )

    async def test_placement_cleanup_is_not_concurrent(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        cronjobs = interfaces.cronjob_interface(self.api_client)
        cronjob, = await cronjobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_cleanup",
            }
        )
        self.assertEqual(cronjob.spec.concurrency_policy, "Forbid")

    async def test_creates_certificate_and_halts(self):
        await self.cr.sm.ensure(self.ctx)
        self.api_client.client_side_validation = False
        cert_interface = interfaces.certificates_interface(self.api_client)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 5)

    async def test_nova_api_certificate_contains_service_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "compute_service",
            },
        )

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "nova_api_certificate",
            },
        )

        self.assertIn(
            f"{service.metadata.name}.{NAMESPACE}.svc",
            certificate["spec"]["dnsNames"],
        )

    async def test_nova_api_certificate_contains_ingress_fqdn(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "nova_api_certificate",
            },
        )

        self.assertIn(
            "nova-ingress",
            certificate["spec"]["dnsNames"],
        )

    async def test_nova_metadata_certificate_contains_service_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "metadata_service",
            },
        )

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "nova_metadata_certificate",
            },
        )

        self.assertIn(
            f"{service.metadata.name}.{NAMESPACE}.svc",
            certificate["spec"]["dnsNames"],
        )

    async def test_placement_certificate_contains_service_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_service",
            },
        )

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_certificate",
            },
        )

        self.assertIn(
            f"{service.metadata.name}.{NAMESPACE}.svc",
            certificate["spec"]["dnsNames"],
        )

    async def test_placement_certificate_contains_ingress_fqdn(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_certificate",
            },
        )

        self.assertIn(
            "placement-ingress",
            certificate["spec"]["dnsNames"],
        )

    async def test_placement_certificate_contains_issuer_name(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_certificate",
            },
        )

        self.assertEqual(
            "issuername",
            certificate["spec"]["issuerRef"]["name"],
        )

    async def test_creates_vnc_external_certificate(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)
        services = interfaces.service_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "vnc_service",
            },
        )

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "vnc_external_certificate",
            },
        )

        self.assertIsNotNone(certificate)
        self.assertEqual(certificate["metadata"]["generateName"],
                         "vnc-")
        self.assertEqual(certificate["spec"]["commonName"],
                         "vnc")
        self.assertIn("vnc-external-certificate",
                      certificate["spec"]["secretName"])
        self.assertIn("yaook",
                      certificate["spec"]["subject"]["organizations"])
        self.assertEqual(certificate["spec"]["duration"], "17520h")
        self.assertEqual(certificate["spec"]["renewBefore"], "4320h")
        self.assertIn(f"{service.metadata.name}.{NAMESPACE}.svc",
                      certificate["spec"]["dnsNames"])
        self.assertIn("vnc", certificate["spec"]["dnsNames"])
        self.assertEqual(certificate["spec"]["issuerRef"]["name"],
                         "issuername")

    async def test_creates_vnc_backend_ca_certificate(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        certificates = interfaces.certificates_interface(self.api_client)

        certificate, = await certificates.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "vnc_backend_ca_certificate",
            },
        )

        self.assertIsNotNone(certificate)
        self.assertEqual("nova-vnc-backend-ca",
                         certificate["spec"]["commonName"])
        self.assertIn("vnc-backend-ca", certificate["spec"]["secretName"])
        self.assertIn("yaook",
                      certificate["spec"]["subject"]["organizations"])
        self.assertEqual("43800h", certificate["spec"]["duration"])
        self.assertEqual("4320h", certificate["spec"]["renewBefore"])
        self.assertTrue(certificate["spec"]["isCA"])
        self.assertEqual("selfsigned-issuer",
                         certificate["spec"]["issuerRef"]["name"])

    async def test_creates_vnc_backend_issuer(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        issuers = interfaces.issuer_interface(self.api_client)

        issuer, = await issuers.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "vnc_backend_ca",
            },
        )

        self.assertIsNotNone(issuer)
        self.assertIn("vnc-backend-ca", issuer["spec"]["ca"]["secretName"])

    async def test_creates_api_deployment_with_replica_spec(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_api"},
        )

        self.assertEqual(api_deployment.spec.replicas, 2)

    async def test_creates_cronjobs_and_halts(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        cronjob_interface = interfaces.cronjob_interface(self.api_client)

        all_cronjobs = await cronjob_interface.list_(NAMESPACE)
        self.assertEqual(len(all_cronjobs), 2)
        db_cleanup, = [b for b in all_cronjobs if
                       b.metadata.labels[context.LABEL_COMPONENT] ==
                       "db_cleanup"]
        self.assertEqual(db_cleanup.metadata.labels[context.LABEL_COMPONENT],
                         "db_cleanup")
        placement_cleanup, = [b for b in all_cronjobs if
                              b.metadata.labels[context.LABEL_COMPONENT] ==
                              "placement_cleanup"]
        self.assertEqual(placement_cleanup.metadata.
                         labels[context.LABEL_COMPONENT], "placement_cleanup")

    async def test_creates_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "nova-api")

        self.assertIsNotNone(service)

    async def test_creates_endpoint_with_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "nova-api-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["public"],
            "https://nova-ingress:8080/v2.1",
        )

    async def test_disable_ingress_creation(self):
        deployment_yaml = _get_nova_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["api"]["ingress"]["createIngress"] = False
        self._configure_cr(nova.Nova, deployment_yaml)
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingress_int = interfaces.ingress_interface(self.api_client)
        ingresses = await ingress_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_ingress"},
        )
        self.assertEqual(len(ingresses), 0)

    async def test_enable_ingress_creation(self):
        deployment_yaml = _get_nova_deployment_yaml(self._keystone_name)
        deployment_yaml["spec"]["api"]["ingress"]["createIngress"] = True
        self._configure_cr(nova.Nova, deployment_yaml)
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingress_int = interfaces.ingress_interface(self.api_client)
        ingresses = await ingress_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_ingress"},
        )
        self.assertEqual(len(ingresses), 1)

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "nova")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "nova-ingress",
        )

    async def test_ingress_force_ssl_annotation(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "nova")

        self.assertEqual(
            ingress
            .metadata
            .annotations["nginx.ingress.kubernetes.io/force-ssl-redirect"],
            "true",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "nova")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "nova-api")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )

    async def test_creates_placement_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "placement")

        self.assertIsNotNone(service)

    async def test_creates_placement_endpoint_with_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "placement-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["public"],
            "https://placement-ingress:8080",
        )

    async def test_creates_placement_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "placement")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "placement-ingress",
        )

    async def test_placement_ingress_force_ssl_annotation(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "placement")

        self.assertEqual(
            ingress
            .metadata
            .annotations["nginx.ingress.kubernetes.io/force-ssl-redirect"],
            "true",
        )

    async def test_placement_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "placement")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "placement")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )

    async def test_creates_placement_deployment_verify_ssl_term_port(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        placement_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_api",
            }
        )

        ssl_term_container_spec = \
            placement_deployment.spec.template.spec.containers[1]
        self.assertEqual(len(ssl_term_container_spec.env), 4)
        service_port_env_var = ssl_term_container_spec.env[0]
        self.assertEqual(service_port_env_var.name, "SERVICE_PORT")
        self.assertEqual(service_port_env_var.value, "8778")
        local_port_env_var = ssl_term_container_spec.env[1]
        self.assertEqual(local_port_env_var.name, "LOCAL_PORT")
        self.assertEqual(local_port_env_var.value, "8080")
        metrics_port_env_var = ssl_term_container_spec.env[2]
        self.assertEqual(metrics_port_env_var.name, "METRICS_PORT")
        self.assertEqual(metrics_port_env_var.value, "9090")

    async def test_creates_placement_deployment_verify_ext_ssl_term_port(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        placement_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "placement_api",
            }
        )

        ssl_term_container_spec = \
            placement_deployment.spec.template.spec.containers[2]
        self.assertEqual(len(ssl_term_container_spec.env), 4)
        service_port_env_var = ssl_term_container_spec.env[0]
        self.assertEqual(service_port_env_var.name, "SERVICE_PORT")
        self.assertEqual(service_port_env_var.value, "8779")
        local_port_env_var = ssl_term_container_spec.env[1]
        self.assertEqual(local_port_env_var.name, "LOCAL_PORT")
        self.assertEqual(local_port_env_var.value, "8080")
        metrics_port_env_var = ssl_term_container_spec.env[2]
        self.assertEqual(metrics_port_env_var.name, "METRICS_PORT")
        self.assertEqual(metrics_port_env_var.value, "9091")

    async def test_creates_vnc_deployment_with_replica_spec(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "vnc"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        self.assertEqual(deployment.spec.replicas, 1)
        self.assertEqual(
            deployment.spec.template.spec.volumes[0].secret.secret_name,
            config_secret.metadata.name,
        )

    async def test_creates_vnc_deployment_with_backend_certificate(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "vnc"},
        )
        backend_certificate_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "vnc_backend_certificate_secret"
            },
        )

        self.assertEqual(deployment.spec.replicas, 1)
        self.assertEqual(
            deployment.spec.template.spec.volumes[2].secret.secret_name,
            backend_certificate_secret.metadata.name,
        )

    async def test_creates_vnc_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "vnc")

        self.assertIsNotNone(service)
        self.assertEqual(len(service.spec.ports), 2)
        self.assertEqual(service.spec.ports[0].port, 6080)
        self.assertEqual(service.spec.ports[0].target_port, 6080)
        self.assertEqual(service.spec.ports[1].port, 9091)
        self.assertEqual(service.spec.ports[1].target_port, 9091)

    async def test_creates_vnc_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "vnc")

        self.assertIsNotNone(ingress)
        self.assertEqual(ingress.spec.rules[0].host, "vnc-ingress")
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            "vnc"
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            6080
        )

    async def test_vnc_ingress_force_ssl_annotation(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "vnc")

        self.assertEqual(
            ingress
            .metadata
            .annotations["nginx.ingress.kubernetes.io/force-ssl-redirect"],
            "true",
        )

    async def test_vnc_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "vnc")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "vnc")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "vnc"][0]),
        )

    async def test_creates_vnc_deployment_verify_vnc_envs(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        vnc_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "vnc",
            }
        )

        novncproxy_container_spec = \
            vnc_deployment.spec.template.spec.containers[0]
        ca_bundle = novncproxy_container_spec.env[0]
        self.assertEqual(ca_bundle.name, "REQUESTS_CA_BUNDLE")
        self.assertEqual(ca_bundle.value,
                         "/etc/ssl/certs/ca-bundle.crt")

    async def test_vnc_service_monitor_matches_fqdn(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        servicemonitors = interfaces.servicemonitor_interface(self.api_client)
        vnc_sm, = await servicemonitors.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "vnc_external_ssl_service_monitor",
            }
        )

        self.assertEqual(
            vnc_sm["spec"]["endpoints"][0]["tlsConfig"]["serverName"],
            "vnc-ingress"
        )

    async def test_creates_metadata_deployment_with_replica_spec(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_metadata"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        self.assertEqual(deployment.spec.replicas, 5)
        self.assertEqual(
            deployment.spec.template.spec.volumes[0].secret.secret_name,
            config_secret.metadata.name,
        )

    async def test_creates_conductor_statefulset_with_replica_spec(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        statefulsets = interfaces.stateful_set_interface(self.api_client)

        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_conductor_sfs"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        self.assertEqual(statefulset.spec.replicas, 6)
        self.assertEqual(
            statefulset.spec.template.spec.volumes[0].secret.secret_name,
            config_secret.metadata.name,
        )

    async def test_conductor_service_matches_statefulset_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        statefulsets = interfaces.stateful_set_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_conductor_svc"},
        )
        statefulsets = await statefulsets.list_(
            NAMESPACE,
            label_selector=f"{context.LABEL_COMPONENT}!=memcached",
        )

        service_labels = service.spec.selector

        assert statefulsets, "No statefulsets were found"
        for statefulset in statefulsets:
            matches = (
                statefulset.metadata.labels[context.LABEL_COMPONENT] ==
                "nova_conductor_sfs"
            )

            pod_labels = statefulset.spec.template.metadata.labels

            if matches:
                self.assertTrue(
                    sm.matches_labels(pod_labels, service_labels),
                    f"pods: {pprint.pformat(pod_labels)}\n"
                    f"service: {pprint.pformat(service_labels)}\n",
                )
            else:
                self.assertFalse(
                    sm.matches_labels(pod_labels, service_labels),
                    f"statefulset: {statefulset.metadata.name}\n"
                    f"pods: {pprint.pformat(pod_labels)}\n"
                    f"service: {pprint.pformat(service_labels)}\n",
                )

    async def test_creates_scheduler_statefulset_with_replica_spec(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        statefulsets = interfaces.stateful_set_interface(self.api_client)

        statefulset, = await statefulsets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_scheduler_sfs"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )

        self.assertEqual(statefulset.spec.replicas, 7)
        self.assertEqual(
            statefulset.spec.template.spec.volumes[0].secret.secret_name,
            config_secret.metadata.name,
        )

    async def test_scheduler_service_matches_statefulset_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        statefulsets = interfaces.stateful_set_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_scheduler_svc"},
        )
        statefulsets = await statefulsets.list_(
            NAMESPACE,
            label_selector=f"{context.LABEL_COMPONENT}!=memcached",
        )

        service_labels = service.spec.selector

        assert statefulsets, "No statefulsets were found"
        for statefulset in statefulsets:
            matches = (
                statefulset.metadata.labels[context.LABEL_COMPONENT] ==
                "nova_scheduler_sfs"
            )

            pod_labels = statefulset.spec.template.metadata.labels

            if matches:
                self.assertTrue(
                    sm.matches_labels(pod_labels, service_labels),
                    f"pods: {pprint.pformat(pod_labels)}\n"
                    f"service: {pprint.pformat(service_labels)}\n",
                )
            else:
                self.assertFalse(
                    sm.matches_labels(pod_labels, service_labels),
                    f"statefulset: {statefulset.metadata.name}\n"
                    f"pods: {pprint.pformat(pod_labels)}\n"
                    f"service: {pprint.pformat(service_labels)}\n",
                )

    async def test_creates_compute_nodes(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup, self.annotations)
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        compute_nodes = interfaces.nova_compute_node_interface(self.api_client)

        public_keys_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "public_keys"},
        )

        cnodes = await compute_nodes.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_nodes"},
        )
        compute_node_map = {
            node["metadata"]["name"]: node
            for node in cnodes
        }
        self.assertCountEqual(
            compute_node_map.keys(),
            self.default_compute_nodes,
        )

        for node_name, node_body in compute_node_map.items():
            self.assertEqual(
                node_body["spec"]["publicKeysSecretRef"]["name"],
                public_keys_secret.metadata.name,
            )
            self.assertEqual(
                node_body["spec"]["keystoneRef"]["kind"],
                "KeystoneDeployment",
            )
            self.assertNotIn("ids", node_body["spec"])
            self.assertEqual(
                node_body["spec"]["imageRef"]["nova-compute"],
                'registry.yaook.cloud/yaook/nova-compute-train-ubuntu:0.0.1',
            )

    async def test_sets_cinder_gid_if_requested(self):
        novadeployment = _get_nova_deployment_yaml(
                self._keystone_name,
                self.labels_compute_extra,
                self.labels_compute_ceph,)
        novadeployment["spec"]["ids"] = {"cinderGid": "6789"}
        self._configure_cr(
            nova.Nova,
            novadeployment
        )

        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup, self.annotations)
        await self.cr.sm.ensure(self.ctx)

        compute_nodes = interfaces.nova_compute_node_interface(self.api_client)

        cnodes = await compute_nodes.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_nodes"},
        )
        compute_node_map = {
            node["metadata"]["name"]: node
            for node in cnodes
        }

        for _, node_body in compute_node_map.items():
            self.assertEquals(node_body["spec"]["ids"]["cinderGid"], "6789")

    async def test_collects_public_keys(self):
        self._mock_labelled_nodes(self.default_node_setup)

        data = [
            ("node2", ["hostkey2_1"], ["userkey2_1"]),
            ("node3", ["hostkey3_1"], ["userkey3_1"]),
            ("node4", ["hostkey4_1"], ["userkey4_1"]),
        ]
        for nodename, hostkeys, userkeys in data:
            cfgname = str(uuid.uuid4())
            self.client_mock.put_object(
                "compute.yaook.cloud", "v1", "sshidentities",
                NAMESPACE, cfgname,
                {
                    "apiVersion": "compute.yaook.cloud/v1",
                    "kind": "SSHIdentity",
                    "metadata": {
                        "name": cfgname,
                        "namespace": NAMESPACE,
                        "labels": {
                            context.LABEL_COMPONENT:
                                op_common.NOVA_COMPUTE_PUBLIC_KEYS_COMPONENT,
                            context.LABEL_PARENT_GROUP: "compute.yaook.cloud",
                            context.LABEL_PARENT_PLURAL: "novacomputenodes",
                            context.LABEL_PARENT_NAME: nodename,
                        },
                    },
                    "status": {
                        "keys": {
                            "host": hostkeys,
                            "user": userkeys,
                        },
                    },
                },
            )

        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)

        public_keys_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "public_keys",
                context.LABEL_PARENT_GROUP: "yaook.cloud",
                context.LABEL_PARENT_PLURAL: "novadeployments",
            },
        )
        data = sm.api_utils.decode_secret_data(public_keys_secret.data)
        known_hosts_s = data["known_hosts"]
        known_hosts = known_hosts_s.split("\n")[:-1]

        self.assertCountEqual(
            [
                f"[192.0.2.{i}]:8022,[node{i}]:8022 hostkey{i}_1"
                for i in range(2, 5)
            ],
            known_hosts,
        )

    async def test_configures_nodes_individually(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup, self.annotations)
        await self.cr.sm.ensure(self.ctx)

        cnodes = interfaces.nova_compute_node_interface(self.api_client)

        cnodes = await cnodes.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_nodes"},
        )
        node_configs = {
            node["metadata"]["name"]: node["spec"]["novaConfig"]
            for node in cnodes
        }

        self.assertIn(
            {
                "libvirt": {
                    "virt_type": "qemu",
                },
            },
            node_configs["node4"],
        )

        self.assertNotIn(
            {
                "libvirt": {
                    "virt_type": "qemu",
                },
            },
            node_configs["node2"],
        )

        self.assertNotIn(
            {
                "libvirt": {
                    "virt_type": "qemu",
                },
            },
            node_configs["node3"],
        )

        provider_configs = {
            node["metadata"]["name"]: node["spec"]["computeProviderConfig"]
            for node in cnodes
        }

        self.assertEqual(
            [],
            provider_configs["node2"]
        )

        self.assertEqual(
            [],
            provider_configs["node3"]
        )

        self.assertEqual(
            [],
            provider_configs["node4"]
        )

        self.assertEqual(
            [],
            provider_configs["node5"]
        )

        self.assertIn(
            {
                "providers": {
                    "traits": {
                        "additional": ["CUSTOM_TRAIT"]
                    },
                },
            },
            provider_configs["node7"]
        )

    async def test_nova_compute_aggregates_assignment(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup, self.annotations)
        await self.cr.sm.ensure(self.ctx)

        cnodes = interfaces.nova_compute_node_interface(self.api_client)

        cnodes = await cnodes.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_nodes"},
        )
        node_aggregates = {
            node["metadata"]["name"]: node["spec"].get("hostAggregates", [])
            for node in cnodes
        }

        self.assertEqual(
            set([
                "aggregate-az-and-property",
                "aggregate-dual-property",
            ]),
            set(node_aggregates["node5"]),
        )

        self.assertNotEqual(
            set([
                "aggregate-az-and-property",
                "aggregate-dual-property",
            ]),
            set(node_aggregates["node2"]),
        )

        self.assertNotEqual(
            set([
                "aggregate-az-and-property",
                "aggregate-dual-property",
            ]),
            set(node_aggregates["node3"]),
        )

    async def test_nova_compute_memcacheref_exist(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup, self.annotations)
        await self.cr.sm.ensure(self.ctx)

        cnodes = interfaces.nova_compute_node_interface(self.api_client)

        cnodes = await cnodes.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_nodes"},
        )

        for node in cnodes:
            self.assertIsNotNone(node["spec"].get("memcachedRef"))

    async def test_nova_compute_aggregates_config_invalid_exception(self):
        self._make_all_dependencies_complete_immediately()
        labels_zonetest_hostaggregate = dict(self.labels_compute)
        labels_zonetest_hostaggregate["idonotexist"] = "dummy"

        node_setup = {
            "node6": labels_zonetest_hostaggregate,
        }
        node_setup.update(self.default_node_setup)
        self._mock_labelled_nodes(node_setup, self.annotations)

        self._mock_labelled_nodes(self.default_node_setup, self.annotations)

        with self.assertRaises(
            sm.ConfigurationInvalid,
        ):
            await self.cr.sm.ensure(self.ctx)

    async def test_nova_compute_aggregates_dual_az_invalid_exception(self):
        self._make_all_dependencies_complete_immediately()
        labels_zonetest_hostaggregate = dict(self.labels_compute)
        labels_zonetest_hostaggregate["multizone_test"] = "dummy"

        node_setup = {
            "node6": labels_zonetest_hostaggregate,
        }
        node_setup.update(self.default_node_setup)
        self._mock_labelled_nodes(node_setup, self.annotations)

        self.client_mock.put_object(
            "compute.yaook.cloud", "v1", "novahostaggregates",
            NAMESPACE, "aggregate-zone1",
            {
                "apiVersion": "compute.yaook.cloud/v1",
                "kind": "NovaHostAggregate",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "aggregate-zone1",
                },
                "spec": {
                    "zone": "iamthezone",
                    "novaRef": {"name": NAME}
                }
            },
        )
        self.client_mock.put_object(
            "compute.yaook.cloud", "v1", "novahostaggregates",
            NAMESPACE, "aggregate-zone2",
            {
                "apiVersion": "compute.yaook.cloud/v1",
                "kind": "NovaHostAggregate",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": "aggregate-zone2",
                },
                "spec": {
                    "zone": "anotherone",
                    "novaRef": {"name": NAME}
                }
            },
        )
        with self.assertRaises(
            sm.ConfigurationInvalid,
        ):
            await self.cr.sm.ensure(self.ctx)

    async def test_configures_ceph_individually(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.ceph_node_setup, self.annotations)
        await self.cr.sm.ensure(self.ctx)

        cnodes = interfaces.nova_compute_node_interface(self.api_client)

        cnodes = await cnodes.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_nodes"},
        )
        node_map = {
            node["metadata"]["name"]: node
            for node in cnodes
        }

        self.assertEqual(
            node_map["node3"]["spec"]["cephBackend"],
            {
                "enabled": True,
                "user": "keyring-user",
                "uuid": "some-libvirt-uuid",
                "keyringSecretName": "keyring-ref",
                "cephConfig": {},
            }
        )

    async def test_nova_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "compute_service"},
        )
        deployments = await deployments.list_(
            NAMESPACE,
        )

        service_labels = service.spec.selector

        assert deployments, "No deployments were found"
        for deployment in deployments:
            matches = (
                deployment.metadata.labels[context.LABEL_COMPONENT] ==
                "nova_api"
            )

            pod_labels = deployment.spec.template.metadata.labels

            if matches:
                self.assertTrue(
                    sm.matches_labels(pod_labels, service_labels),
                    f"pods: {pprint.pformat(pod_labels)}\n"
                    f"service: {pprint.pformat(service_labels)}\n",
                )
            else:
                self.assertFalse(
                    sm.matches_labels(pod_labels, service_labels),
                    f"deployment: {deployment.metadata.name}\n"
                    f"pods: {pprint.pformat(pod_labels)}\n"
                    f"service: {pprint.pformat(service_labels)}\n",
                )

    async def test_metadata_service_contains_shared_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        secrets = interfaces.secret_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "metadata_service"},
        )
        secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "metadata_proxy_shared_secret"},
        )
        service_secret_name = service.metadata.annotations[
            op_common.LABEL_NOVA_METADATA_SECRET_NAME]
        secret_name = secret.metadata.name
        self.assertEqual(service_secret_name, secret_name)

    async def test_metadata_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "metadata_service"},
        )
        deployments = await deployments.list_(
            NAMESPACE,
        )

        service_labels = service.spec.selector

        assert deployments, "No deployments were found"
        for deployment in deployments:
            matches = (
                deployment.metadata.labels[context.LABEL_COMPONENT] ==
                "nova_metadata"
            )

            pod_labels = deployment.spec.template.metadata.labels

            if matches:
                self.assertTrue(
                    sm.matches_labels(pod_labels, service_labels),
                    f"pods: {pprint.pformat(pod_labels)}\n"
                    f"service: {pprint.pformat(service_labels)}\n",
                )
            else:
                self.assertFalse(
                    sm.matches_labels(pod_labels, service_labels),
                    f"deployment: {deployment.metadata.name}\n"
                    f"pods: {pprint.pformat(pod_labels)}\n"
                    f"service: {pprint.pformat(service_labels)}\n",
                )

    async def test_placement_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "placement_service"},
        )
        deployments = await deployments.list_(
            NAMESPACE,
        )

        service_labels = service.spec.selector

        assert deployments, "No deployments were found"
        for deployment in deployments:
            matches = (
                deployment.metadata.labels[context.LABEL_COMPONENT] ==
                "placement_api"
            )

            pod_labels = deployment.spec.template.metadata.labels

            if matches:
                self.assertTrue(
                    sm.matches_labels(pod_labels, service_labels),
                    f"pods: {pprint.pformat(pod_labels)}\n"
                    f"service: {pprint.pformat(service_labels)}\n",
                )
            else:
                self.assertFalse(
                    sm.matches_labels(pod_labels, service_labels),
                    f"deployment: {deployment.metadata.name}\n"
                    f"pods: {pprint.pformat(pod_labels)}\n"
                    f"service: {pprint.pformat(service_labels)}\n",
                )

    async def test_vnc_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "vnc_service"
            },
        )
        deployments = await deployments.list_(
            NAMESPACE,
        )

        service_labels = service.spec.selector

        assert deployments, "No deployments were found"
        for deployment in deployments:
            matches = (
                deployment.metadata.labels[context.LABEL_COMPONENT] ==
                "vnc"
            )

            pod_labels = deployment.spec.template.metadata.labels

            if matches:
                self.assertTrue(
                    sm.matches_labels(pod_labels, service_labels),
                    f"pods: {pprint.pformat(pod_labels)}\n"
                    f"service: {pprint.pformat(service_labels)}\n",
                )
            else:
                self.assertFalse(
                    sm.matches_labels(pod_labels, service_labels),
                    f"deployment: {deployment.metadata.name}\n"
                    f"pods: {pprint.pformat(pod_labels)}\n"
                    f"service: {pprint.pformat(service_labels)}\n",
                )

    @ddt.data({}, NOVA_POLICY)
    async def test_creates_config_with_policy_file(self, policy):
        nova_deployment_yaml = _get_nova_deployment_yaml(
            self._keystone_name,
        )
        nova_deployment_yaml["spec"].update(policy)
        self._configure_cr(
            nova.Nova,
            nova_deployment_yaml,
        )

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        expected_policy_file = os.path.join(CONFIG_PATH, "policy.yaml")

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        nova_conf_content = testutils._parse_config(
            config_secret.data[CONFIG_FILENAME],
            decode=True
        )

        observed_policy_file = nova_conf_content.get("DEFAULT", "policy_file")

        self.assertEqual(observed_policy_file, expected_policy_file)

    async def test_creates_config_with_correct_placement_region(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        nova_conf_content = testutils._parse_config(
            config_secret.data[CONFIG_FILENAME],
            decode=True
        )

        placement_region_name = nova_conf_content.get(
            "placement", "os_region_name")

        self.assertEqual(placement_region_name, "regionname")

    @ddt.data({}, NOVA_POLICY)
    async def test_creates_api_deployment_with_cert_volume(self, policy):
        nova_deployment_yaml = _get_nova_deployment_yaml(
            self._keystone_name,
        )
        nova_deployment_yaml["spec"].update(policy)
        self._configure_cr(
            nova.Nova,
            nova_deployment_yaml,
        )

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)
        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
                context.LABEL_PARENT_PLURAL: "novadeployments"
            },
        )

        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_api"},
        )

        self.assertEqual(len(api_deployment.spec.template.spec.volumes), 6)

        expected_volume_name = "ca-certs"

        self.assertEqual(
            len(api_deployment.spec.template.spec.containers[0].volume_mounts),
            2
        )
        cert_volume_mount = \
            api_deployment.spec.template.spec.containers[0].volume_mounts[1]
        self.assertEqual(cert_volume_mount.name, expected_volume_name)
        self.assertEqual(cert_volume_mount.mount_path, "/etc/ssl/certs")

        cert_volume = api_deployment.spec.template.spec.volumes[1]
        self.assertEqual(cert_volume.name, expected_volume_name)
        self.assertEqual(cert_volume.config_map.name, ca_certs.metadata.name)

    @ddt.data({}, NOVA_POLICY)
    async def test_creates_api_deployment_with_projected_volume(self, policy):
        nova_deployment_yaml = _get_nova_deployment_yaml(
            self._keystone_name,
        )
        nova_deployment_yaml["spec"].update(policy)
        self._configure_cr(
            nova.Nova,
            nova_deployment_yaml,
        )

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_api"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        policy_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "policy"},
        )

        self.assertEqual(len(api_deployment.spec.template.spec.volumes), 6)
        observed_projected_volume = \
            api_deployment.spec.template.spec.volumes[0]
        observed_projected_volume_mount = \
            api_deployment.spec.template.spec.containers[0].volume_mounts[0]
        self.assertEqual(
            observed_projected_volume_mount.name,
            observed_projected_volume.name,
        )
        self.assertEqual(
            observed_projected_volume_mount.mount_path,
            CONFIG_PATH,
        )

        observed_projected_volume_sources = \
            observed_projected_volume.projected.sources
        self.assertEqual(len(observed_projected_volume_sources), 2)

        conf_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.secret
        )
        self.assertEqual(
            conf_vol_source.secret.name,
            config_secret.metadata.name,
        )
        self.assertEqual(len(conf_vol_source.secret.items), 1)
        self.assertEqual(
            conf_vol_source.secret.items[0].key,
            CONFIG_FILENAME,
        )
        self.assertEqual(
            conf_vol_source.secret.items[0].path,
            CONFIG_FILENAME,
        )

        policy_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.config_map
        )
        self.assertEqual(
            policy_vol_source.config_map.name,
            policy_config_map.metadata.name,
        )
        self.assertEqual(len(policy_vol_source.config_map.items), 1)
        self.assertEqual(
            policy_vol_source.config_map.items[0].key,
            POLICY_FILENAME,
        )
        self.assertEqual(
            policy_vol_source.config_map.items[0].path,
            POLICY_FILENAME,
        )

    @ddt.data({}, NOVA_POLICY)
    async def test_creates_policy_configmap(self, policy):

        nova_deployment_yaml = _get_nova_deployment_yaml(
            self._keystone_name,
        )
        nova_deployment_yaml["spec"].update(policy)
        self._configure_cr(
            nova.Nova,
            nova_deployment_yaml,
        )

        self._make_all_jobs_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)

        config_maps = interfaces.config_map_interface(self.api_client)
        nova_policy, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "policy"},
        )

        self.assertEqual(nova_policy.kind, "ConfigMap")
        self.assertTrue(
            nova_policy.metadata.name.startswith("nova-policy"))

        self.assertEqual(nova_policy.data, {})
        self.assertEqual(
            nova_policy.metadata.annotations['state.yaook.cloud/policies'],
            str(policy.get("policy", {}))
        )

    async def test_injects_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config"},
        )
        decoded = sm.api_utils.decode_secret_data(secret.data)
        lines = decoded["nova.conf"].splitlines()

        self.assertIn(f"mytestsecret = {CONFIG_SECRET_VALUE}", lines)

    async def test_vnc_service_monitor_external_cert(self):
        nova_deployment_yaml = _get_nova_deployment_yaml(
            self._keystone_name,
        )
        nova_deployment_yaml["spec"]["vnc"]["ingress"][
            "externalCertificateSecretRef"
            ] = {
                "name": "vnc-test-certificate"
            }
        self._configure_cr(
            nova.Nova,
            nova_deployment_yaml,
        )

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        servicemonitors = interfaces.servicemonitor_interface(self.api_client)
        vnc_sm, = await servicemonitors.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "vnc_external_ssl_service_monitor",
            }
        )

        self.assertEqual(vnc_sm["spec"]["endpoints"][0]["tlsConfig"]["ca"]
                         ["secret"]["name"], "vnc-test-certificate")

    async def test_vnc_deployment_external_cert(self):
        nova_deployment_yaml = _get_nova_deployment_yaml(
            self._keystone_name,
        )
        nova_deployment_yaml["spec"]["vnc"]["ingress"][
            "externalCertificateSecretRef"
            ] = {
                "name": "vnc-test-certificate"
            }
        self._configure_cr(
            nova.Nova,
            nova_deployment_yaml,
        )
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(self.api_client)
        vnc_deploy, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "vnc",
            }
        )
        self.assertEqual(
            vnc_deploy._spec._template._spec._volumes[3]._secret.secret_name,
            "vnc-test-certificate"
        )

    async def test_creates_api_deployment_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_api"},
        )

        self.assertEqual(
            testutils.container_resources(deployment, 0),
            testutils.unique_resources("api.nova-api"))
        self.assertEqual(
            testutils.container_resources(deployment, 1),
            testutils.unique_resources("api.ssl-terminator"))
        self.assertEqual(
            testutils.container_resources(deployment, 2),
            testutils.unique_resources("api.ssl-terminator-external"))
        self.assertEqual(
            testutils.container_resources(deployment, 3),
            testutils.unique_resources("api.service-reload"))
        self.assertEqual(
            testutils.container_resources(deployment, 4),
            testutils.unique_resources("api.service-reload-external"))

    async def test_creates_metadata_deployment_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_metadata"},
        )

        self.assertEqual(
            testutils.container_resources(deployment, 0),
            testutils.unique_resources("metadata.nova-metadata"))
        self.assertEqual(
            testutils.container_resources(deployment, 1),
            testutils.unique_resources("metadata.ssl-terminator"))
        self.assertEqual(
            testutils.container_resources(deployment, 2),
            testutils.unique_resources("metadata.service-reload"))

    async def test_creates_placement_deployment_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "placement_api"},
        )

        self.assertEqual(
            testutils.container_resources(deployment, 0),
            testutils.unique_resources("placement.placement"))
        self.assertEqual(
            testutils.container_resources(deployment, 1),
            testutils.unique_resources("placement.ssl-terminator"))
        self.assertEqual(
            testutils.container_resources(deployment, 2),
            testutils.unique_resources("placement.ssl-terminator-external"))
        self.assertEqual(
            testutils.container_resources(deployment, 3),
            testutils.unique_resources("placement.service-reload"))
        self.assertEqual(
            testutils.container_resources(deployment, 4),
            testutils.unique_resources("placement.service-reload-external"))

    async def test_creates_novncproxy_deployment_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "vnc"},
        )

        self.assertEqual(
            testutils.container_resources(deployment, 0),
            testutils.unique_resources("vnc.nova-novncproxy"))
        self.assertEqual(
            testutils.container_resources(deployment, 1),
            testutils.unique_resources("vnc.ssl-terminator-external"))
        self.assertEqual(
            testutils.container_resources(deployment, 2),
            testutils.unique_resources("vnc.service-reload-external"))

    async def test_creates_conductor_statefulset_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        stateful_set_interface = \
            interfaces.stateful_set_interface(self.api_client)
        statefulset, = await stateful_set_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_conductor_sfs"}
        )

        self.assertEqual(
            testutils.container_resources(statefulset, 0),
            testutils.unique_resources("conductor.nova-conductor"))

    async def test_creates_scheduler_statefulset_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        stateful_set_interface = \
            interfaces.stateful_set_interface(self.api_client)
        statefulset, = await stateful_set_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_scheduler_sfs"}
        )

        self.assertEqual(
            testutils.container_resources(statefulset, 0),
            testutils.unique_resources("scheduler.nova-scheduler"))

    async def test_creates_compute_node_cr_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup, self.annotations)
        await self.cr.sm.ensure(self.ctx)

        compute_node_if = interfaces.nova_compute_node_interface(
            self.api_client)
        compute_node_list = await compute_node_if.list_(NAMESPACE)

        crd_spec = self.ctx.parent_spec

        for compute_node in compute_node_list:
            self.assertEqual(
                compute_node["spec"]["resources"],
                crd_spec["compute"]["resources"])

    async def _helper_database_container_resources(self, database_name):
        crd_spec = self.ctx.parent_spec
        expected_db_resources = \
            crd_spec["database"][database_name]["resources"]
        expected_proxy_resources = \
            crd_spec["database"][database_name]["proxy"]["resources"]

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        db_interface = interfaces.mysqlservice_interface(self.api_client)
        db_service, = await db_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: f"{database_name}_db"}
        )

        self.assertEqual(
            db_service["spec"]["resources"],
            expected_db_resources)
        self.assertEqual(
            db_service["spec"]["proxy"]["resources"],
            expected_proxy_resources)

    async def test_api_database_container_resources(self):
        await self._helper_database_container_resources("api")

    async def test_cell0_database_container_resources(self):
        await self._helper_database_container_resources("cell0")

    async def test_cell1_database_container_resources(self):
        await self._helper_database_container_resources("cell1")

    async def _helper_messagequeue_container_resources(self, mq_name):
        crd_spec = self.ctx.parent_spec
        expected_resources = crd_spec["messageQueue"][mq_name]["resources"]

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        msgqueue_interface = interfaces.amqpserver_interface(self.api_client)
        msgqueue_server, = await msgqueue_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: f"{mq_name}_mq"}
        )
        self.assertEqual(
            msgqueue_server["spec"]["resources"],
            expected_resources)

    async def test_cell1_messagequeue_container_resources(self):
        await self._helper_messagequeue_container_resources("cell1")

    async def _helper_job_container_resources(
            self, component, job_name, is_cronjob=False):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        if is_cronjob:
            jobs = interfaces.cronjob_interface(self.api_client)
        else:
            jobs = interfaces.job_interface(self.api_client)
        the_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: component}
        )

        self.assertEqual(
            testutils.container_resources(the_job, 0),
            testutils.unique_resources(f"job.{job_name}")
        )

    async def test_api_db_sync_container_resources(self):
        await self._helper_job_container_resources(
            "api_db_sync_job", "nova-api-db-sync-job")

    async def test_db_sync_container_resources(self):
        await self._helper_job_container_resources(
            "db_sync_job", "nova-db-sync-job")

    async def test_map_cell0_container_resources(self):
        await self._helper_job_container_resources(
            "cell0_map_job", "nova-map-cell0-job")

    async def test_create_cell1_container_resources(self):
        await self._helper_job_container_resources(
            "cell1_create_job", "nova-create-cell1-job")

    async def test_db_cleanup_container_resources(self):
        await self._helper_job_container_resources(
            "db_cleanup", "nova-db-cleanup-cronjob", is_cronjob=True)

    async def test_evict_manager_deployed(self):
        nova_deployment_yaml = _get_nova_deployment_yaml(
            self._keystone_name,
        )
        nova_deployment_yaml["spec"]["eviction"]["manager"] = {
                "enabled": True,
                "interval": 10,
                "max_per_hour": 1
            }
        self._configure_cr(
            nova.Nova,
            nova_deployment_yaml,
        )
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "eviction_manager"},
        )
        self.assertIsNotNone(deployment)

        sa_interface = interfaces.service_account_interface(self.api_client)
        service_account, = await sa_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "eviction_manager_service_account"},
            )
        self.assertIsNotNone(service_account)

        role_binding_interface = interfaces.role_binding_interface(
            self.api_client)
        role_binding, = await role_binding_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "eviction_manager_role_binding"},
            )
        self.assertIsNotNone(role_binding)

        role_binding_keystoneuser = interfaces.keystoneuser_interface(
            self.api_client)
        keystoneuser, = await role_binding_keystoneuser.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "evict_manager_keystone_user"},
            )
        self.assertIsNotNone(keystoneuser)
        self.assertEqual(
            deployment._spec._template._spec._containers[0]._env[4].value,
            '1')
        self.assertEqual(
            deployment._spec._template._spec._containers[0]._env[2].value,
            '10')
        self.assertEqual(
            deployment._spec._template._spec._containers[0]._image,
            'dummy-image')

    async def test_evict_manager_not_deployed(self):
        nova_deployment_yaml = _get_nova_deployment_yaml(
            self._keystone_name,
        )
        nova_deployment_yaml["spec"]["eviction"]["manager"] = {
                "enabled": False,
                "interval": 10
            }
        self._configure_cr(
            nova.Nova,
            nova_deployment_yaml,
        )
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployment_interface = interfaces.deployment_interface(
            self.api_client)
        deployments = await deployment_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "eviction_manager"},
        )
        self.assertEqual(len(deployments), 0)

        sa_interface = interfaces.service_account_interface(
            self.api_client)
        service_accounts = await sa_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                "eviction_manager_service_account"},
        )
        self.assertEqual(len(service_accounts), 0)

        rolebinding_interface = interfaces.role_binding_interface(
            self.api_client)
        role_bindings = await rolebinding_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "eviction_manager_role_binding"},
        )
        self.assertEqual(len(role_bindings), 0)

        keytstoneuser_interface = interfaces.keystoneuser_interface(
            self.api_client)
        keystoneusers = await keytstoneuser_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "eviction_manager_keystone_user"},
        )
        self.assertEqual(len(keystoneusers), 0)

    async def test_placement_cleanup_container_resources(self):
        await self._helper_job_container_resources(
            "placement_cleanup",
            "nova-placement-cleanup-cronjob", is_cronjob=True)


class TestNovaDeploymentsTrain(TestNovaDeploymentsBase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        deployment_yaml = _get_nova_deployment_yaml_train(
            self._keystone_name,
            self.labels_compute_extra,
            self.labels_compute_ceph,
        )
        self._configure_cr(
            nova.Nova,
            deployment_yaml,
        )

    async def test_creates_api_db_sync_jobs_and_halts(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        self._make_all_memcached_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 4)
        components = [
            job.metadata.labels[context.LABEL_COMPONENT] for job in all_jobs]
        self.assertCountEqual(components, [
            "policy_validation",
            "placement_policy_validation",
            "api_db_sync_job",
            "placement_db_sync_job"
            ]
        )

        deployments = interfaces.deployment_interface(self.api_client)
        self.assertFalse(await deployments.list_(NAMESPACE))

    async def test_creates_placement_deployment_with_replica_spec(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)

        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "placement_api"},
        )

        self.assertEqual(deployment.spec.replicas, 4)

    async def test_jobs_use_config_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        placement_config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "placement_config"},
        )

        jobs = interfaces.job_interface(self.api_client)
        for job in await jobs.list_(NAMESPACE):
            if "policy-validator" in job._metadata.generate_name:
                continue
            if job.metadata.name.startswith("nova"):
                self.assertEqual(
                    job.spec.template.spec.volumes[0].secret.secret_name,
                    config_secret.metadata.name,
                    str(job.metadata),
                )
            else:
                self.assertEqual(
                    job.spec.template.spec.volumes[0].secret.secret_name,
                    placement_config_secret.metadata.name,
                    str(job.metadata),
                )

    async def test_creates_vnc_deployment_verify_ext_ssl_term_envs(
            self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)
        vnc_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "vnc",
            }
        )

        ssl_term_container_spec = \
            vnc_deployment.spec.template.spec.containers[1]
        self.assertEqual(len(ssl_term_container_spec.env), 5)
        service_port_env_var = ssl_term_container_spec.env[0]
        self.assertEqual(service_port_env_var.name, "SERVICE_PORT")
        self.assertEqual(service_port_env_var.value, "6080")
        local_port_env_var = ssl_term_container_spec.env[1]
        self.assertEqual(local_port_env_var.name, "LOCAL_PORT")
        self.assertEqual(local_port_env_var.value, "8080")
        metrics_port_env_var = ssl_term_container_spec.env[2]
        self.assertEqual(metrics_port_env_var.name, "METRICS_PORT")
        self.assertEqual(metrics_port_env_var.value, "9091")
        metrics_port_env_var = ssl_term_container_spec.env[3]
        self.assertEqual(metrics_port_env_var.name, "REQUESTS_CA_BUNDLE")
        self.assertEqual(metrics_port_env_var.value,
                         "/etc/ssl/certs/ca-certificates.crt")
        metrics_port_env_var = ssl_term_container_spec.env[4]
        self.assertEqual(metrics_port_env_var.name, "X_FORWARDED_PROTO")
        self.assertEqual(metrics_port_env_var.value,
                         "https")

    async def test_creates_sysinfo_config_map(
            self):

        sysinfo_spec = {
            "sysinfo": {
                "product": "Yaook Cloud",
                "vendor": "Yaook Cloud",
            }
        }

        nova_deployment_yaml = _get_nova_deployment_yaml(
            self._keystone_name,
        )
        nova_deployment_yaml["spec"].update(sysinfo_spec)
        self._configure_cr(
            nova.Nova,
            nova_deployment_yaml,
        )
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)
        sysinfo, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "sysinfo_config",
                context.LABEL_PARENT_PLURAL: "novadeployments"
            },
        )
        self.assertTrue(sysinfo.metadata.name.startswith("nova-sysinfo-"))
        self.assertEqual(
            sysinfo.data["release"],
            '[Nova]\nproduct=Yaook Cloud\nvendor=Yaook Cloud'
        )


@ddt.ddt
class TestNovaHostAggregate(testutils.CustomResourceTestCase):
    run_needs_update_test = False

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._aggregatename = "iamaaggregate"
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._nova_name = self._provide_nova(NAMESPACE)
        self._configure_cr(
            nova.NovaHostAggregate,
            {
                "metadata": {
                    "name": self._aggregatename,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "novaRef": {
                        "name": self._nova_name,
                    },
                    "zone": "southeast-asia-42",
                    "properties": {
                        "cake": "lie",
                        "foo": "bar",
                    }
                },
            }
        )

    def setUp(self):
        self.os_conn_context = unittest.mock.Mock()
        self.os_connection = unittest.mock.Mock()
        self.os_connection.__enter__ = lambda _: self.os_conn_context
        self.os_connection.__exit__ = lambda _a, _b, _c, _d: None
        self.connect_mock = unittest.mock.Mock([])
        self.connect_mock.return_value = self.os_connection

        self.createorupdate_mock = unittest.mock.Mock([])
        self.createorupdate_patch = unittest.mock.patch(
                "yaook.op.nova.resources.create_or_update_hostaggregate",
                new=self.createorupdate_mock
            )

        self._patches = [
            unittest.mock.patch("openstack.connect",
                                new=self.connect_mock),
            self.createorupdate_patch
        ]

        for p in self._patches:
            p.start()

    def tearDown(self):
        for p in reversed(self._patches):
            p.stop()

    async def test__reconcile_hostaggregates(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        self.createorupdate_mock.assert_called_once_with(
            logger=self.cr.logger,
            client=self.os_conn_context,
            aggregate='iamaaggregate',
            spec={
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "novaRef": {
                        "name": self._nova_name,
                    },
                    "zone": "southeast-asia-42",
                    "properties": {
                        "cake": "lie",
                        "foo": "bar",
                    }
                },
        )

    async def test__createorupdate_hostaggregate_function_reconcile(self):
        self.createorupdate_patch.stop()
        self.os_conn_context.compute = unittest.mock.Mock([])
        self.os_conn_context.compute.find_aggregate = unittest.mock.Mock([])
        self.os_conn_context.compute.update_aggregate = unittest.mock.Mock([])
        sentinel.aggregate.id = "not-relevant"
        sentinel.aggregate.name = "iamaaggregate"
        sentinel.aggregate.metadata = {
            "cake": "lie",
            "foo": "bar",
        }
        sentinel.aggregate.availability_zone = ""
        self.os_conn_context.compute.find_aggregate.return_value = \
            sentinel.aggregate

        n_resources.create_or_update_hostaggregate(
            logger=self.cr.logger,
            client=self.os_conn_context,
            aggregate='iamaaggregate',
            spec={
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "novaRef": {
                        "name": self._nova_name,
                    },
                    "zone": sentinel.aggregate.availability_zone,
                    "properties": {
                        "cake": "lie",
                        "foo": "bar",
                    }
                },
        )
        self.os_conn_context.compute.find_aggregate.assert_called_once_with(
            'iamaaggregate'
        )
        self.os_conn_context.compute.update_aggregate.assert_not_called()

    async def test__createorupdate_hostaggregate_function_update_az(self):
        self.createorupdate_patch.stop()
        self.os_conn_context.compute = unittest.mock.Mock([])
        self.os_conn_context.compute.find_aggregate = unittest.mock.Mock([])
        self.os_conn_context.compute.update_aggregate = unittest.mock.Mock([])
        sentinel.aggregate.id = "not-relevant"
        sentinel.aggregate.name = "iamaaggregate"
        sentinel.aggregate.metadata = {
            "cake": "lie",
            "foo": "bar",
        }
        sentinel.aggregate.availability_zone = "different_az"

        sentinel.updated_aggregate.id = sentinel.aggregate.id
        sentinel.updated_aggregate.name = sentinel.aggregate.name
        sentinel.updated_aggregate.metadata = sentinel.aggregate.metadata
        sentinel.updated_aggregate.availability_zone = "anotherone"

        self.os_conn_context.compute.find_aggregate.return_value = \
            sentinel.aggregate

        self.os_conn_context.compute.update_aggregate.return_value = \
            sentinel.updated_aggregate

        n_resources.create_or_update_hostaggregate(
            logger=self.cr.logger,
            client=self.os_conn_context,
            aggregate='iamaaggregate',
            spec={
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "novaRef": {
                        "name": self._nova_name,
                    },
                    "zone": "anotherone",
                    "properties": {
                        "cake": "lie",
                        "foo": "bar",
                    }
                },
        )
        self.os_conn_context.compute.find_aggregate.assert_called_once_with(
            'iamaaggregate'
        )
        self.os_conn_context.compute.update_aggregate.assert_called_once_with(
            'not-relevant', availability_zone='anotherone',
        )

    async def test__createorupdate_hostaggregate_function_update_prop(self):
        self.createorupdate_patch.stop()
        self.os_conn_context.compute = unittest.mock.Mock([])
        self.os_conn_context.compute.find_aggregate = unittest.mock.Mock([])
        self.os_conn_context.compute.set_aggregate_metadata = \
            unittest.mock.Mock([])
        sentinel.aggregate.id = "not-relevant"
        sentinel.aggregate.name = "iamaaggregate"
        sentinel.aggregate.metadata = {
            "cake": "lie",
        }
        sentinel.aggregate.availability_zone = ""

        sentinel.updated_aggregate.id = sentinel.aggregate.id
        sentinel.updated_aggregate.name = sentinel.aggregate.name
        sentinel.updated_aggregate.metadata = {
            "cake": "lie",
            "foo": "bar",
        }
        sentinel.updated_aggregate.availability_zone = \
            sentinel.aggregate.availability_zone

        self.os_conn_context.compute.find_aggregate.return_value = \
            sentinel.aggregate

        self.os_conn_context.compute.set_aggregate_metadata.return_value = \
            sentinel.updated_aggregate

        n_resources.create_or_update_hostaggregate(
            logger=self.cr.logger,
            client=self.os_conn_context,
            aggregate='iamaaggregate',
            spec={
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "novaRef": {
                        "name": self._nova_name,
                    },
                    "zone": sentinel.aggregate.availability_zone,
                    "properties": {
                        "cake": "lie",
                        "foo": "bar",
                    }
                },
        )
        self.os_conn_context.compute.find_aggregate.assert_called_once_with(
            'iamaaggregate'
        )
        self.os_conn_context.compute.set_aggregate_metadata.\
            assert_called_once_with(
                'not-relevant', metadata={'foo': 'bar', 'cake': 'lie'},
            )

    async def test__createorupdate_hostaggregate_function_create_agg(self):
        self.createorupdate_patch.stop()
        self.os_conn_context.compute = unittest.mock.Mock([])
        self.os_conn_context.compute.find_aggregate = unittest.mock.Mock([])
        self.os_conn_context.compute.create_aggregate = \
            unittest.mock.Mock([])
        self.os_conn_context.compute.set_aggregate_metadata = \
            unittest.mock.Mock([])
        sentinel.aggregate.id = "not-relevant"
        sentinel.aggregate.name = "iamaaggregate"
        sentinel.aggregate.metadata = {
            "cake": "lie",
        }
        sentinel.aggregate.availability_zone = ""

        sentinel.updated_aggregate.id = sentinel.aggregate.id
        sentinel.updated_aggregate.name = sentinel.aggregate.name
        sentinel.updated_aggregate.metadata = {
            "cake": "lie",
            "foo": "bar",
        }
        sentinel.updated_aggregate.availability_zone = \
            sentinel.aggregate.availability_zone

        self.os_conn_context.compute.find_aggregate.return_value = \
            None

        self.os_conn_context.compute.create_aggregate.return_value = \
            sentinel.aggregate

        self.os_conn_context.compute.set_aggregate_metadata.return_value = \
            sentinel.updated_aggregate

        n_resources.create_or_update_hostaggregate(
            logger=self.cr.logger,
            client=self.os_conn_context,
            aggregate='iamaaggregate',
            spec={
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "novaRef": {
                        "name": self._nova_name,
                    },
                    "zone": sentinel.aggregate.availability_zone,
                    "properties": {
                        "cake": "lie",
                        "foo": "bar",
                    }
                },
        )
        self.os_conn_context.compute.find_aggregate.assert_called_once_with(
            'iamaaggregate'
        )
        self.os_conn_context.compute.create_aggregate.assert_called_once_with(
            name='iamaaggregate', availability_zone='',
        )

    async def test__reconcile_hostaggregates_correct_region(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        call_args = self.connect_mock.call_args.kwargs
        self.assertEqual(call_args["region_name"], "Test")


@ddt.ddt
class TestNovaDeploymentsTrainWithInternalIngress(TestNovaDeploymentsBase):
    def _get_nova_deployment_yaml_with_ingress(
        self,
        keystone_name,
        labels_compute_extra,
        labels_compute_ceph
    ):
        deployment = _get_nova_deployment_yaml_train(
            keystone_name,
            labels_compute_extra,
            labels_compute_ceph,
        )
        deployment["spec"]["api"]["internal"] = {
            "ingress": {
                "ingressClassName": "nginx",
                "fqdn": "internal-nova-ingress",
                "port": 8081,
            }
        }
        deployment["spec"]["placement"]["internal"] = {
            "ingress": {
                "ingressClassName": "nginx",
                "fqdn": "internal-placement-ingress",
                "port": 8081,
            }
        }
        deployment["spec"]["api"]["resources"] = \
            testutils.generate_resources_dict(
                    "api.nova-api",
                    "api.ssl-terminator",
                    "api.ssl-terminator-external",
                    "api.ssl-terminator-external",
                    "api.ssl-terminator-internal",
                    "api.service-reload",
                    "api.service-reload-external",
                    "api.service-reload-internal",
                )
        deployment["spec"]["placement"]["resources"] = \
            testutils.generate_resources_dict(
                    "placement.placement",
                    "placement.ssl-terminator",
                    "placement.ssl-terminator-external",
                    "placement.ssl-terminator-external",
                    "placement.ssl-terminator-internal",
                    "placement.service-reload",
                    "placement.service-reload-external",
                    "placement.service-reload-internal",
                )
        return deployment

    @classmethod
    def setUpClass(cls) -> None:
        if cls is TestNovaDeploymentsBase:
            raise unittest.SkipTest("not relevant")
        super().setUpClass()

    def setUp(self):
        super().setUp()
        # improve test speed!
        self.__silent_patches = [
            unittest.mock.patch(
                "yaook.common.ssh.generate_ssh_keypair",
                new=_generate_ssh_keypair,
            ),
        ]
        for patch in self.__silent_patches:
            patch.start()

    def tearDown(self):
        for patch in self.__silent_patches:
            patch.stop()
        super().tearDown()

    async def asyncSetUp(self):
        await super().asyncSetUp()
        deployment_yaml = self._get_nova_deployment_yaml_with_ingress(
            self._keystone_name,
            self.labels_compute_extra,
            self.labels_compute_ceph,
        )
        self._configure_cr(
            nova.Nova,
            deployment_yaml,
        )

    async def test_creates_api_db_sync_jobs_and_halts(self):
        self._make_all_databases_ready_immediately()
        self._make_all_mqs_succeed_immediately()
        self._make_all_database_users_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_mq_users_ready_immediately()
        self._make_all_certificates_succeed_immediately()
        self._make_all_memcached_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 4)
        components = [
            job.metadata.labels[context.LABEL_COMPONENT] for job in all_jobs]
        self.assertCountEqual(components, [
            "policy_validation",
            "placement_policy_validation",
            "api_db_sync_job",
            "placement_db_sync_job"]
        )

        deployments = interfaces.deployment_interface(self.api_client)
        self.assertFalse(await deployments.list_(NAMESPACE))

    async def test_creates_placement_deployment_with_replica_spec(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)

        deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "placement_api"},
        )

        self.assertEqual(deployment.spec.replicas, 4)

    async def test_creates_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "nova-api")

        self.assertIsNotNone(service)

    async def test_creates_endpoint_with_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "nova-api-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["internal"],
            "https://internal-nova-ingress:8081/v2.1",
        )

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "nova")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "nova-ingress",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "nova")
        internal_ingress = await ingresses.read(NAMESPACE, "nova-internal")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "nova-api")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].
            backend.service.port.number,
            ([x.port for x in service.spec.ports
                if x.name == "internal-ingress"][0]),
        )

    async def test_creates_placement_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "placement")

        self.assertIsNotNone(service)

    async def test_creates_placement_endpoint_with_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "placement-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["internal"],
            "https://internal-placement-ingress:8081",
        )

    async def test_creates_placement_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "placement-internal")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "internal-placement-ingress",
        )

    async def test_ingress_matches_service_placement(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "placement")
        internal_ingress = await ingresses.read(
            NAMESPACE,
            "placement-internal"
        )

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "placement")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].
            backend.service.port.number,
            ([x.port for x in service.spec.ports
                if x.name == "internal-ingress"][0]),
        )

    async def test_creates_api_deployment_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "nova_api"},
        )

        self.assertEqual(
            testutils.container_resources(deployment, 0),
            testutils.unique_resources("api.nova-api"))
        self.assertEqual(
            testutils.container_resources(deployment, 1),
            testutils.unique_resources("api.ssl-terminator"))
        self.assertEqual(
            testutils.container_resources(deployment, 2),
            testutils.unique_resources("api.ssl-terminator-external"))
        self.assertEqual(
            testutils.container_resources(deployment, 3),
            testutils.unique_resources("api.ssl-terminator-internal"))
        self.assertEqual(
            testutils.container_resources(deployment, 4),
            testutils.unique_resources("api.service-reload"))
        self.assertEqual(
            testutils.container_resources(deployment, 5),
            testutils.unique_resources("api.service-reload-external"))
        self.assertEqual(
            testutils.container_resources(deployment, 6),
            testutils.unique_resources("api.service-reload-internal"))

    async def test_creates_placement_deployment_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployment_interface = interfaces.deployment_interface(self.api_client)
        deployment, = await deployment_interface.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "placement_api"},
        )

        self.assertEqual(
            testutils.container_resources(deployment, 0),
            testutils.unique_resources("placement.placement"))
        self.assertEqual(
            testutils.container_resources(deployment, 1),
            testutils.unique_resources("placement.ssl-terminator"))
        self.assertEqual(
            testutils.container_resources(deployment, 2),
            testutils.unique_resources("placement.ssl-terminator-external"))
        self.assertEqual(
            testutils.container_resources(deployment, 3),
            testutils.unique_resources("placement.ssl-terminator-internal"))
        self.assertEqual(
            testutils.container_resources(deployment, 4),
            testutils.unique_resources("placement.service-reload"))
        self.assertEqual(
            testutils.container_resources(deployment, 5),
            testutils.unique_resources("placement.service-reload-external"))
        self.assertEqual(
            testutils.container_resources(deployment, 6),
            testutils.unique_resources("placement.service-reload-internal"))
