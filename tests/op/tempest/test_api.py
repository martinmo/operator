#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import configparser
import yaook.op.common as op_common
import yaook.op.scheduling_keys as scheduling_keys
import yaook.op.tempest as tempest
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces

from tests import testutils


NAMESPACE = "test-namespace"
NAME = "tempest"


class TestTempestDeployments(testutils.CustomResourceTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "targetRelease": "32.0.0",
                    "target": {
                        "service": "keystone",
                    },
                    "region": "RegionOne",
                    "tempestConfig": {
                        "service_available": {
                            "cinder": False,
                            "glance": False,
                            "horizon": False,
                            "neutron": False,
                            "nova": False,
                            "swift": False,
                            "barbican": False,
                        }
                    },
                    "resources": testutils.generate_resources_dict(
                        "tempest-job"
                    ),
                },
            },
        )

    def test_sets_final_state(self):
        self.assertEqual(
            self.cr.tempest_job,
            self.cr.FINAL_STATE
        )

    async def test_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        user, = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        self.assertEqual(user["spec"]["keystoneRef"]["name"],
                         self._keystone_name)
        self.assertEqual(user["spec"]["keystoneRef"]["kind"],
                         "KeystoneDeployment")

    async def test_tempest_refers_to_mounted_ca_bundle(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)
        jobs = interfaces.job_interface(self.api_client)

        config, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )

        ca_certs, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
            },
        )

        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            job.spec.template.spec,
            testutils.find_configmap_volume(
                job.spec.template.spec,
                ca_certs.metadata.name,
            ),
            "tempest",
        )

        tempest_conf = config.data["tempest.conf"]
        cfg = testutils._parse_config(tempest_conf, decode=True)

        self.assertEqual(
            cert_mountpoint,
            cfg.get("identity", "ca_certificates_file"),
        )

        env = {
            x.name: x.value
            for x in job.spec.template.spec.containers[0].env
        }

        self.assertEqual(
            cert_mountpoint,
            env["REQUESTS_CA_BUNDLE"]
        )

    async def test_uses_scheduling_key_for_tempest_job(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        job_interface = interfaces.job_interface(self.api_client)
        jobs = await job_interface.list_(NAMESPACE)

        for job in jobs:
            self.assertEqual(
                job.spec.template.spec.affinity.node_affinity.
                required_during_scheduling_ignored_during_execution.to_dict(),
                {
                    "node_selector_terms": [
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        scheduling_keys.SchedulingKey.
                                        OPERATOR_TEMPEST.value,
                                    "operator": "Exists",
                                    "values": None,
                                },
                            ],
                            "match_fields": None,
                        },
                        {
                            "match_expressions": [
                                {
                                    "key":
                                        scheduling_keys.SchedulingKey.
                                        OPERATOR_ANY.value,
                                    "operator": "Exists",
                                    "values": None,
                                },
                            ],
                            "match_fields": None,
                        },
                    ],
                },
            )

            self.assertCountEqual(
                job.spec.template.spec.to_dict()["tolerations"],
                [
                    {
                        "key": scheduling_keys.SchedulingKey.
                        OPERATOR_TEMPEST.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                    {
                        "key": scheduling_keys.SchedulingKey.
                        OPERATOR_ANY.value,
                        "operator": "Exists",
                        "effect": None,
                        "toleration_seconds": None,
                        "value": None,
                    },
                ],
            )

    async def test_creates_job_containers_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)
        tempest_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "tempest_job"},
        )

        self.assertEqual(
            testutils.container_resources(tempest_job, 0),
            testutils.unique_resources("tempest-job")
        )

    async def test_serial_flag_in_pod_spec(self):
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "target": {
                        "service": "keystone",
                    },
                    "targetRelease": "32.0.0",
                    "region": "RegionOne",
                    "serial": True,
                },
            },
        )
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)
        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )
        command = job.spec.template.spec.containers[0].command
        self.assertIn("--serial", command)

    async def test_serial_flag_in_pod_spec_negative(self):
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "target": {
                        "service": "keystone",
                    },
                    "targetRelease": "32.0.0",
                    "region": "RegionOne",
                    "serial": False,
                },
            },
        )
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)
        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )
        command = job.spec.template.spec.containers[0].command
        self.assertNotIn("--serial", command)

    async def test_serial_flag_in_pod_spec_default(self):
        """Test default behavior if serial option not set in spec."""
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "target": {
                        "service": "keystone",
                    },
                    "targetRelease": "32.0.0",
                    "region": "RegionOne",
                    # serial not specified
                },
            },
        )
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)
        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )
        command = job.spec.template.spec.containers[0].command
        self.assertNotIn("--serial", command)

    async def test_exclude_list_configured_correctly(self):
        exclude_list_template = [
            "exclude1",
            "exclude2",
            "exclude3"
        ]
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "target": {
                        "service": "keystone",
                    },
                    "targetRelease": "32.0.0",
                    "region": "RegionOne",
                    "exclude": exclude_list_template,
                },
            },
        )
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        configmaps = interfaces.config_map_interface(self.api_client)
        jobs = interfaces.job_interface(self.api_client)

        exclude_list, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "exclude_list",
            },
        )
        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )

        self.assertIn("exclude.list", exclude_list.data)
        self.assertListEqual(exclude_list_template,
                             exclude_list.data["exclude.list"].split('\n'))

        self.assertEqual(exclude_list.kind, 'ConfigMap')

        command = job.spec.template.spec.containers[0].command
        self.assertIn("--blacklist-file /home/tempest/exclude.list",
                      " ".join(command))

        exclude_list_mountpoint = testutils.find_volume_mountpoint(
            job.spec.template.spec,
            testutils.find_configmap_volume(
                job.spec.template.spec,
                exclude_list.metadata.name,
            ),
            "tempest",
        )

        self.assertEqual(
            exclude_list_mountpoint,
            "/home/tempest/exclude.list"
        )

    async def test_config_dynamic_credentials(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        cfg = testutils._parse_config(config.data["tempest.conf"], decode=True)

        self.assertEqual(cfg.get("auth", "use_dynamic_credentials"),
                         "true")
        with self.assertRaises(configparser.NoOptionError):
            cfg.get("auth", "test_accounts_file")

    async def test_not_mount_dynamic_credentials(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)

        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )

        self.assertEquals(len(job.spec.template.spec.volumes), 3)
        self.assertEquals(len(job.spec.template.spec.containers[0]
                              .volume_mounts), 3)

    async def test_config_preprovisioned_users(self):
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "target": {
                        "service": "keystone",
                    },
                    "preprovisioned_users": {
                        "secret": {
                            "name": "test-secret"
                        },
                        "key": "test-secret-key"
                    },
                    "targetRelease": "32.0.0",
                    "region": "RegionOne",
                },
            },
        )
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        cfg = testutils._parse_config(config.data["tempest.conf"], decode=True)

        self.assertEqual(cfg.get("auth", "use_dynamic_credentials"),
                         "false")
        self.assertEqual(cfg.get("auth", "test_accounts_file"),
                         "/home/tempest/accounts.yaml")

    async def test_not_mount_preprovisioned_users(self):
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "target": {
                        "service": "keystone",
                    },
                    "preprovisioned_users": {
                        "secret": {
                            "name": "test-secret"
                        },
                        "key": "test-secret-key"
                    },
                    "targetRelease": "32.0.0",
                    "region": "RegionOne",
                },
            },
        )

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)

        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )
        volume = [volume for volume in job.spec.template.spec.volumes
                  if volume.name == "tempest-preprovisioned-users"][0]

        self.assertEquals(volume.secret.secret_name, "test-secret")
        mount = [mount for mount
                 in job.spec.template.spec.containers[0].volume_mounts
                 if mount.name == "tempest-preprovisioned-users"][0]
        self.assertEquals(mount.mount_path, "/home/tempest/accounts.yaml")
        self.assertEquals(mount.sub_path, "test-secret-key")

    async def test_config_account_cleanup(self):
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "target": {
                        "service": "keystone",
                    },
                    "account_cleanup": True,
                    "targetRelease": "32.0.0",
                    "region": "RegionOne",
                },
            },
        )
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        cfg = testutils._parse_config(config.data["tempest.conf"], decode=True)

        jobs = interfaces.job_interface(self.api_client)

        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )
        configmaps = interfaces.config_map_interface(self.api_client)
        keystone_internal_configmap, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_INTERNAL_API_COMPONENT},
        )
        keystone_user_secretref, = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT:
                    op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT},
        )
        self.assertEqual(cfg.get("auth", "use_dynamic_credentials"),
                         "false")
        self.assertEqual(cfg.get("auth", "test_accounts_file"),
                         "/home/tempest/accounts.yaml")
        self.assertEqual(job.spec.template.spec.containers[0].env[0].name,
                         "ACCOUNT_CLEANUP")
        self.assertEqual(job.spec.template.spec.containers[0].env[0].value,
                         "True")
        self.assertEqual(job.spec.template.spec.containers[0].env_from[0]
                         .config_map_ref.name,
                         keystone_internal_configmap.metadata.name)
        self.assertEqual(job.spec.template.spec.containers[0].env_from[1]
                         .secret_ref.name,
                         keystone_user_secretref.metadata.name)

    async def test_config_account_cleanup_set_false(self):
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "target": {
                        "service": "keystone",
                    },
                    "account_cleanup": False,
                    "targetRelease": "32.0.0",
                    "region": "RegionOne",
                },
            },
        )
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)

        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )
        self.assertEqual(job.spec.template.spec.containers[0].env[0].name,
                         "ACCOUNT_CLEANUP")
        self.assertEqual(job.spec.template.spec.containers[0].env[0].value,
                         "False")

    async def test_config_account_cleanup_and_preprovisioned_users(self):
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "target": {
                        "service": "keystone",
                    },
                    "preprovisioned_users": {
                        "secret": {
                            "name": "test-secret"
                        },
                        "key": "test-secret-key"
                    },
                    "account_cleanup": True,
                    "targetRelease": "32.0.0",
                    "region": "RegionOne",
                },
            },
        )
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        cfg = testutils._parse_config(config.data["tempest.conf"], decode=True)

        jobs = interfaces.job_interface(self.api_client)

        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )
        self.assertEqual(cfg.get("auth", "use_dynamic_credentials"),
                         "false")
        self.assertEqual(cfg.get("auth", "test_accounts_file"),
                         "/home/tempest/accounts.yaml")

        self.assertEquals(job.spec.template.spec.containers[0].env[0].name,
                          "ACCOUNT_CLEANUP")
        self.assertEquals(job.spec.template.spec.containers[0].env[0].value,
                          "True")

    async def test_pushgateway_is_not_set(self):
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "target": {
                        "service": "keystone",
                    },
                    "targetRelease": "32.0.0",
                    "region": "RegionOne",
                },
            },
        )

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)

        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )
        assert not any(d.name == "PUSHGATEWAY"
                       for d in job.spec.template.spec.containers[0].env)
        assert not any(d.name == "PUSHGATEWAY_JOBNAME"
                       for d in job.spec.template.spec.containers[0].env)

    async def test_pushgateway(self):
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "target": {
                        "service": "keystone",
                    },
                    "pushgateway": "example.com",
                    "targetRelease": "32.0.0",
                    "region": "RegionOne",
                },
            },
        )

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)

        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )
        self.assertEquals(job.spec.template.spec.containers[0].env[0].name,
                          "PUSHGATEWAY_URL")
        self.assertEquals(job.spec.template.spec.containers[0].env[0].value,
                          "example.com")
        self.assertEquals(job.spec.template.spec.containers[0].env[1].name,
                          "PUSHGATEWAY_JOBNAME")
        self.assertEquals(job.spec.template.spec.containers[0].env[1].value,
                          "tempest-"+NAME)

    async def test_region(self):
        self._configure_cr(
            tempest.Tempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "target": {
                        "service": "keystone",
                    },
                    "targetRelease": "32.0.0",
                    "region": "RegionOne",
                },
            },
        )

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        jobs = interfaces.job_interface(self.api_client)

        job, = await jobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_job",
            },
        )
        self.assertEqual(job.spec.template.spec.containers[0].env[1].name,
                         "OS_REGION_NAME")
        self.assertEqual(job.spec.template.spec.containers[0].env[1].value,
                         "RegionOne")


class TestCronTempestDeployments(testutils.CustomResourceTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._keystone_name = self._provide_keystone(NAMESPACE)
        self._configure_cr(
            tempest.CronTempest,
            {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "schedule": "*/7 * * * *",
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "targetRelease": "32.0.0",
                    "target": {
                        "service": "keystone",
                    },
                    "region": "RegionOne",
                    "tempestConfig": {
                        "service_available": {
                            "cinder": False,
                            "glance": False,
                            "horizon": False,
                            "neutron": False,
                            "nova": False,
                            "swift": False,
                            "barbican": False,
                        }
                    },
                    "resources": testutils.generate_resources_dict(
                        "tempest-job"
                    ),
                },
            },
        )

    async def test_keystone_user_matches_keystone_reference(self):
        await self.cr.sm.ensure(self.ctx)

        users = interfaces.keystoneuser_interface(self.api_client)
        (user,) = await users.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "keystone_user"},
        )

        self.assertEqual(
            user["spec"]["keystoneRef"]["name"], self._keystone_name
        )
        self.assertEqual(
            user["spec"]["keystoneRef"]["kind"], "KeystoneDeployment"
        )

    async def test_tempest_refers_to_mounted_ca_bundle(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        configmaps = interfaces.config_map_interface(self.api_client)
        cronjobs = interfaces.cronjob_interface(self.api_client)

        (config,) = await secrets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "config",
            },
        )

        (ca_certs,) = await configmaps.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ca_certs",
            },
        )

        (cronjob,) = await cronjobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_cron_job",
            },
        )

        cert_mountpoint = testutils.find_volume_mountpoint(
            cronjob.spec.job_template.spec.template.spec,
            testutils.find_configmap_volume(
                cronjob.spec.job_template.spec.template.spec,
                ca_certs.metadata.name,
            ),
            "tempest",
        )

        tempest_conf = config.data["tempest.conf"]
        cfg = testutils._parse_config(tempest_conf, decode=True)

        self.assertEqual(
            cert_mountpoint,
            cfg.get("identity", "ca_certificates_file"),
        )

        env = {
            x.name: x.value
            for x in cronjob.spec.job_template.spec.template.spec.containers[
                0
            ].env
        }

        self.assertEqual(cert_mountpoint, env["REQUESTS_CA_BUNDLE"])

    async def test_creates_tempest_cronjob(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        cronjobs = interfaces.cronjob_interface(self.api_client)
        (cronjob,) = await cronjobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_cron_job",
            },
        )

        self.assertEqual(cronjob.spec.schedule, "*/7 * * * *")

    async def test_tempest_cron_job_is_not_concurrent(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        cronjobs = interfaces.cronjob_interface(self.api_client)
        (cronjob,) = await cronjobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_cron_job",
            },
        )

        self.assertEqual(cronjob.spec.concurrency_policy, "Forbid")

    async def test_tempest_cron_job_backofflimit(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        cronjobs = interfaces.cronjob_interface(self.api_client)
        (cronjob,) = await cronjobs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "tempest_cron_job",
            },
        )

        self.assertEqual(cronjob.spec.job_template.spec.backoff_limit, 0)
