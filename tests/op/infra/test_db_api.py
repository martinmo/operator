#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import base64

import yaook.op.common as op_common
import yaook.op.infra.db_cr as db_cr
import yaook.statemachine as sm

from ... import testutils


NAME = "mysql-service-xyz"
NAMESPACE = "infra"


def _parse_mysql_config(s: str):
    lines = s.split("\n")
    for i, line in enumerate(lines):
        if not line.strip():
            continue
        if line.strip().startswith("["):
            continue
        if line.strip().startswith("#"):
            continue
        if "=" not in line:
            lines[i] = line + "="

    return testutils._parse_config("\n".join(lines))


class TestMySQLService(testutils.CustomResourceTestCase):
    run_service_reload_test = False  # rolling restarts solve all the things

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._configure_cr(
            db_cr.MySQLService,
            {
                "apiVersion": "infra.yaook.cloud/v1",
                "kind": "MySQLService",
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "database": "funny-db",
                    "proxy": {
                        "replicas": 1,
                        "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                        "timeoutClient": "300",
                        "resources": testutils.generate_resources_dict(
                            "proxy.create-ca-bundle",
                            "proxy.haproxy",
                            "proxy.service-reload"
                        )
                    },
                    "replicas": 3,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "frontendIssuerRef": {
                        "name": "ca-issuer",
                    },
                    "targetRelease": "10.2",
                    "backendCAIssuerRef": {
                        "name": "backend-ca-issuer",
                    },
                    "storageSize": "32Gi",
                    "storageClassName": "foobar",
                    "backup": {
                        "schedule": "0 * * * *",
                        "mysqldump": False,
                    },
                    "resources": testutils.generate_resources_dict(
                        "mariadb-galera",
                        "backup-creator",
                        "backup-shifter",
                        "mysqld-exporter",
                        "ssl-terminator",
                    )
                },
                "status": {
                    "nextRelease": "10.2",
                    "installedRelease": "10.2",
                },
            }
        )
        self.run_podspec_tests = False

    async def test_creates_service_for_unready_endpoints_matching_statefulset(self):  # noqa:E501
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        self._make_all_deployments_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        services = sm.service_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        db_sts, = await statefulsets.list_(NAMESPACE)

        unready_service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "unready_service",
            },
        )

        self.assertTrue(
            unready_service.spec.publish_not_ready_addresses,
        )
        self.assertDictEqual(
            unready_service.spec.selector,
            db_sts.spec.template.metadata.labels,
        )
        self.assertEqual(
            db_sts.spec.service_name,
            unready_service.metadata.name,
        )

    async def test_creates_service_for_ready_endpoints_matching_statefulset(self):  # noqa:E501
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        self._make_all_deployments_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        services = sm.service_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        db_sts, = await statefulsets.list_(NAMESPACE)

        ready_service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "ready_service",
            },
        )

        self.assertFalse(
            ready_service.spec.publish_not_ready_addresses,
        )
        self.assertDictEqual(
            ready_service.spec.selector,
            db_sts.spec.template.metadata.labels,
        )

    async def test_creates_service_for_each_statefulset_pod(self):  # noqa:E501
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        services = sm.service_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        db_sts, = await statefulsets.list_(NAMESPACE)

        pod_services = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "database_pod_services",
            },
        )
        self.assertEqual(3, len(pod_services))

        for i in range(db_sts.spec.replicas):
            pod_name = f"{db_sts.metadata.name}-{i}"
            expected_labels = dict(db_sts.spec.template.metadata.labels)
            expected_labels["statefulset.kubernetes.io/pod-name"] = pod_name
            for service in pod_services:
                if service.spec.selector == expected_labels:
                    break
            else:
                self.fail(f"Could not find service for pod {pod_name}")

    async def test_creates_service_for_haproxy(self):
        self._make_all_deployments_ready_immediately()
        self._make_all_certificates_succeed_immediately()

        await self.cr.sm.ensure(self.ctx)

        services = sm.service_interface(self.api_client)
        deployments = sm.deployment_interface(self.api_client)

        haproxy, = await deployments.list_(NAMESPACE)

        public_service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    op_common.MYSQL_DATABASE_SERVICE_COMPONENT,
            },
        )

        self.assertFalse(
            public_service.spec.publish_not_ready_addresses,
        )
        self.assertDictEqual(
            public_service.spec.selector,
            haproxy.spec.template.metadata.labels,
        )

    async def test_haproxy_connects_to_individual_pod_services(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        services = sm.service_interface(self.api_client)
        configmaps = sm.config_map_interface(self.api_client)

        haproxy_cfg, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "haproxy_config",
            },
        )

        pod_services = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "database_pod_services",
            },
        )
        self.assertEqual(3, len(pod_services))

        for service in pod_services:
            server_name = service.metadata.labels["state.yaook.cloud/instance"]
            service_name = service.metadata.name
            service_namespace = service.metadata.namespace

            self.assertIn(
                f"server {server_name} "
                f"_mysql._tcp.{service_name}.{service_namespace}."
                "svc.test.dns.domain",
                haproxy_cfg.data["haproxy.cfg"],
            )

    async def test_haproxy_timeout_synced_to_stop(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        deployments = sm.deployment_interface(self.api_client)
        configmaps = sm.config_map_interface(self.api_client)

        haproxy_cfg, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "haproxy_config",
            },
        )

        haproxy, = await deployments.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "haproxy",
            },
        )

        timeout_cfg_line = [
            x for x in haproxy_cfg.data["haproxy.cfg"].splitlines()
            if "timeout client" in x
        ][0]
        timeout_cfg = int(timeout_cfg_line.split()[-1][:-1])

        terminationgraceperiod = haproxy.spec.template.spec.\
            termination_grace_period_seconds

        self.assertEqual(
            timeout_cfg,
            terminationgraceperiod
        )

    async def test_creates_haproxy_deployment(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_deployments_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = sm.deployment_interface(self.api_client)

        haproxy, = await deployments.list_(NAMESPACE)

        self.assertEqual(haproxy.spec.replicas, 1)

    async def test_haproxy_service_monitor_references_service_and_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        servicemonitors = sm.servicemonitor_interface(self.api_client)
        services = sm.service_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        sms, = await servicemonitors.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "haproxy_service_monitor"
            },
        )
        public_service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "public_service",
            },
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "haproxy_metrics_certificate_secret",
            }
        )

        servicelabels = sms["spec"]["selector"]["matchLabels"]
        certificatename = (sms["spec"]["endpoints"][0]["tlsConfig"]
                           ["ca"]["secret"]["name"])

        self.assertEqual(
            "prometheus",
            sms["spec"]["endpoints"][0]["port"]
        )

        self.assertEqual(
            servicelabels,
            public_service.metadata.labels
        )

        self.assertEqual(
            certificatename,
            cert_secret.metadata.name
        )

    async def test_creates_haproxy_metrics_certificate(self):
        self._make_all_deployments_ready_immediately()

        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "haproxy_metrics_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "haproxy_metrics_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            "ca-issuer",
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )

    async def test_creates_frontend_certificate(self):
        self._make_all_deployments_ready_immediately()

        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "frontend_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "frontend_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            "ca-issuer",
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )

    async def test_creates_exporter_certificate(self):
        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "exporter_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "exporter_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            "ca-issuer",
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )

    async def test_creates_replication_ca_certificate(self):
        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "replication_ca_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "replication_ca_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            "backend-ca-issuer",
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )
        self.assertTrue(cert["spec"]["isCA"])

    async def test_creates_replication_ca_issuer(self):
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        issuers = sm.issuer_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        issuer, = await issuers.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "replication_ca",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "replication_ca_certificate_secret",
            }
        )

        self.assertEqual(
            issuer["spec"]["ca"]["secretName"],
            cert_secret.metadata.name,
        )

    async def test_creates_replication_certificate_with_own_ca(self):
        self._make_all_issuers_ready_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        issuers = sm.issuer_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        issuer, = await issuers.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "replication_ca",
            }
        )
        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "replication_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "replication_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            issuer["metadata"]["name"],
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )
        self.assertFalse(cert["spec"].get("isCA", False))

    async def test_creates_database_statefulset(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        self._make_all_deployments_ready_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        db, = await statefulsets.list_(NAMESPACE)

        mariadb_container = db.spec.template.spec.containers[0]

        simple_env_map = {
            envdef.name: envdef.value
            for envdef in mariadb_container.env
            if envdef.value is not None
        }

        self.assertEqual(
            mariadb_container.image,
            "bitnami/mariadb-galera:0.0.1",
        )
        self.assertEqual(
            simple_env_map["MARIADB_DATABASE"],
            "funny-db",
        )
        self.assertEqual(
            simple_env_map["MARIADB_GALERA_CLUSTER_NAME"],
            NAME,
        )
        self.assertEqual(
            simple_env_map["MARIADB_ROOT_USER"],
            "yaook-sys-maint",
        )
        self.assertEqual(
            simple_env_map["MARIADB_GALERA_MARIABACKUP_USER"],
            "mariabackup",
        )

        volume_claim_template, = db.spec.volume_claim_templates
        self.assertEqual(
            volume_claim_template.spec.storage_class_name,
            "foobar",
        )
        self.assertEqual(
            volume_claim_template.spec.resources.requests["storage"],
            "32Gi",
        )

    async def test_database_statefulset_exposes_ports_mysql(self):
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        self._make_all_deployments_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        db, = await statefulsets.list_(NAMESPACE)

        mariadb_container = db.spec.template.spec.containers[0]

        port_map = {
            port.name: (port.container_port, port.protocol)
            for port in mariadb_container.ports
        }

        self.assertDictEqual(
            {
                "mysql": (3306, "TCP"),
                "galera": (4567, "TCP"),
                "ist": (4568, "TCP"),
                "sst": (4444, "TCP"),
            },
            port_map,
        )

    async def test_database_statefulset_exposes_ports_exporter(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        db, = await statefulsets.list_(NAMESPACE)

        mysqld_exporter, = [x for x in db.spec.template.spec.containers
                            if x.name == "mysqld-exporter"]

        mysql_backup_exporter, = [x for x in db.spec.template.spec.containers
                                  if x.name == "ssl-terminator"]

        port_map_mysqld = {
            port.name: (port.container_port, port.protocol)
            for port in mysqld_exporter.ports
        }

        port_map_mysql_backup = {
            port.name: (port.container_port, port.protocol)
            for port in mysql_backup_exporter.ports
        }

        self.assertDictEqual(
            {
                "prometheus": (9104, "TCP"),
            },
            port_map_mysqld,
        )

        self.assertDictEqual(
            {
                "backup-metrics": (9101, "TCP"),
            },
            port_map_mysql_backup,
        )

    async def test_database_statefulset_exporter_has_credentials(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = sm.secret_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        db, = await statefulsets.list_(NAMESPACE)

        mysqld_exporter, = [x for x in db.spec.template.spec.containers
                            if x.name == "mysqld-exporter"]

        volume_name, = [x.name for x in mysqld_exporter.volume_mounts
                        if x.name == "exporter-config"]

        secret_name, = [
            x.secret.secret_name for x in db.spec.template.spec.volumes
            if x.name == volume_name]

        secret_data = await secrets.read(NAMESPACE, secret_name)
        config = base64.b64decode(secret_data.data["my.cnf"])

        password_secret, = await secrets.list_(NAMESPACE, label_selector={
                sm.context.LABEL_COMPONENT: "passwords",
            })
        password = base64.b64decode(
            password_secret.data["mariadb-root-password"])

        self.assertIn(password, config)

    async def test_database_statefulset_sets_cert_expiry(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        certificates = sm.certificates_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        replication_cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "replication_certificate",
            },
        )
        frontend_cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "frontend_certificate",
            },
        )
        exporter_cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "exporter_certificate",
            },
        )
        db, = await statefulsets.list_(NAMESPACE)

        annotations = db.spec.template.metadata.annotations

        self.assertEqual(
            replication_cert["status"]["notBefore"],
            annotations["operator.yaook.cloud/replication-cert-not-before"],
        )
        self.assertEqual(
            frontend_cert["status"]["notBefore"],
            annotations["operator.yaook.cloud/frontend-cert-not-before"],
        )
        self.assertEqual(
            exporter_cert["status"]["notBefore"],
            annotations["operator.yaook.cloud/exporter-cert-not-before"],
        )

    async def test_mysql_service_monitor_references_service_and_secret(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        servicemonitors = sm.servicemonitor_interface(self.api_client)
        services = sm.service_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        sms, = await servicemonitors.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "mysql_service_monitor"
            },
        )
        unready_service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "unready_service",
            },
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "exporter_certificate_secret",
            }
        )

        servicelabels = sms["spec"]["selector"]["matchLabels"]
        certificatename = (sms["spec"]["endpoints"][0]["tlsConfig"]
                           ["ca"]["secret"]["name"])

        self.assertEqual(
            "prometheus",
            sms["spec"]["endpoints"][0]["port"]
        )

        self.assertEqual(
            servicelabels,
            unready_service.metadata.labels
        )

        self.assertEqual(
            certificatename,
            cert_secret.metadata.name
        )

    async def test_mysql_backup_service_monitor_references_service_and_secret(self):  # noqa:E501
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        servicemonitors = sm.servicemonitor_interface(self.api_client)
        services = sm.service_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        sms, = await servicemonitors.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "mysql_backup_service_monitor"
            },
        )
        unready_service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "unready_service",
            },
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "exporter_certificate_secret",
            }
        )

        servicelabels = sms["spec"]["selector"]["matchLabels"]
        certificatename = (sms["spec"]["endpoints"][0]["tlsConfig"]
                           ["ca"]["secret"]["name"])

        self.assertEqual(
            "backup-metrics",
            sms["spec"]["endpoints"][0]["port"]
        )

        self.assertEqual(
            servicelabels,
            unready_service.metadata.labels
        )

        self.assertEqual(
            certificatename,
            cert_secret.metadata.name
        )

    async def test_database_statefulset_mounts_certs_and_config_with_correct_cert_references(self):  # noqa:E501
        self._make_all_certificates_succeed_immediately()
        self._make_all_issuers_ready_immediately()
        self._make_all_deployments_ready_immediately()

        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)
        configmaps = sm.config_map_interface(self.api_client)

        frontend_cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "frontend_certificate_secret",
            },
        )
        replication_cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "replication_certificate_secret",
            },
        )
        mariadb_config, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "db_config",
            },
        )
        db, = await statefulsets.list_(NAMESPACE)

        frontend_mapping = testutils.find_projected_secret_mapping(
            db.spec.template.spec,
            frontend_cert_secret.metadata.name,
            "mariadb-galera",
        )

        replication_mapping = testutils.find_projected_secret_mapping(
            db.spec.template.spec,
            replication_cert_secret.metadata.name,
            "mariadb-galera",
        )

        cfg = _parse_mysql_config(mariadb_config.data["my.cnf"])

        self.assertEqual(
            cfg.get("mysqld", "ssl_ca"),
            frontend_mapping["ca.crt"][0],
        )
        self.assertEqual(
            cfg.get("mysqld", "ssl_cert"),
            frontend_mapping["tls.crt"][0],
        )
        self.assertEqual(
            cfg.get("mysqld", "ssl_key"),
            frontend_mapping["tls.key"][0],
        )

        self.assertEqual(
            cfg.get("sst", "tca"),
            replication_mapping["ca.crt"][0],
        )
        self.assertEqual(
            cfg.get("sst", "tcert"),
            replication_mapping["tls.crt"][0],
        )
        self.assertEqual(
            cfg.get("sst", "tkey"),
            replication_mapping["tls.key"][0],
        )

        wsrep_provider_options = cfg.get("galera", "wsrep_provider_options")

        self.assertIn(
            f"socket.ssl_ca={replication_mapping['ca.crt'][0]}",
            wsrep_provider_options,
        )
        self.assertIn(
            f"socket.ssl_cert={replication_mapping['tls.crt'][0]}",
            wsrep_provider_options,
        )
        self.assertIn(
            f"socket.ssl_key={replication_mapping['tls.key'][0]}",
            wsrep_provider_options,
        )

    async def test_database_statefulset_creates_backup_pods(self):  # noqa:E501
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        db, = await statefulsets.list_(NAMESPACE)

        containers = db.spec.template.spec.containers

        self.assertEqual(5, len(containers))

        envs = {
            container.name: {
                envdef.name: envdef.value
                for envdef in container.env or []
            } for container in containers
        }

        self.assertEqual(
            envs["mariadb-galera"]["MARIADB_DATABASE"],
            envs["backup-creator"]["YAOOK_BACKUP_CREATOR_MARIADB_DATABASE"]
        )

        self.assertEqual(
            envs["mariadb-galera"]["MARIADB_ROOT_USER"],
            envs["backup-creator"]["YAOOK_BACKUP_CREATOR_MARIADB_USERNAME"]
        )

        self.assertEqual(
            envs["mariadb-galera"]["MARIADB_ROOT_PASSWORD"],
            envs["backup-creator"]["YAOOK_BACKUP_CREATOR_MARIADB_PASSWORD"]
        )

        self.assertEqual(
            'False',
            envs["backup-creator"]["YAOOK_BACKUP_CREATOR_MARIADB_MYSQLDUMP"]
        )

        self.assertEqual(
            "/backup/new",
            envs["backup-creator"]["YAOOK_BACKUP_CREATOR_OUT_PATH"]
        )

        self.assertEqual(
            "/backup",
            envs["backup-shifter"]["YAOOK_BACKUP_SHIFTER_WORK_DIR"]
        )

        self.assertEqual(
            "dumpinfo",
            envs["backup-shifter"]["YAOOK_BACKUP_SHIFTER_SHIFTERS"]
        )

    async def test_creates_containers_with_resource(self):  # noqa:E501
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)
        deployments = sm.deployment_interface(self.api_client)

        db_sts, = await statefulsets.list_(NAMESPACE)
        proxy_deployment, = await deployments.list_(NAMESPACE)

        self.assertEqual(
            testutils.container_resources(db_sts, 0),
            testutils.unique_resources("mariadb-galera")
        )
        self.assertEqual(
            testutils.container_resources(db_sts, 1),
            testutils.unique_resources("backup-creator")
        )
        self.assertEqual(
            testutils.container_resources(db_sts, 2),
            testutils.unique_resources("backup-shifter")
        )
        self.assertEqual(
            testutils.container_resources(db_sts, 3),
            testutils.unique_resources("mysqld-exporter")
        )
        self.assertEqual(
            testutils.container_resources(db_sts, 4),
            testutils.unique_resources("ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(proxy_deployment, 0, is_init=True),
            testutils.unique_resources("proxy.create-ca-bundle")
        )
        self.assertEqual(
            testutils.container_resources(proxy_deployment, 0),
            testutils.unique_resources("proxy.haproxy")
        )
        self.assertEqual(
            testutils.container_resources(proxy_deployment, 1),
            testutils.unique_resources("proxy.service-reload")
        )

    async def test_database_optimizer_switch_derived_merge_is_off(self):
        self._make_all_dependencies_complete_immediately()

        await self.cr.sm.ensure(self.ctx)

        configmaps = sm.config_map_interface(self.api_client)

        db_cfg, = await configmaps.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "db_config",
            },
        )

        self.assertIn('optimizer_switch=derived_merge=off',
                      db_cfg.data['my.cnf'])


class TestMySQLServiceS3Backup(testutils.CustomResourceTestCase):
    run_service_reload_test = False  # rolling restarts solve all the things

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.s3_cred_secret_name = "backup-s3-password"
        self._configure_cr(
            db_cr.MySQLService,
            {
                "apiVersion": "infra.yaook.cloud/v1",
                "kind": "MySQLService",
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "database": "funny-db",
                    "proxy": {
                        "replicas": 1,
                        "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                        "timeoutClient": "300",
                    },
                    "replicas": 3,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "frontendIssuerRef": {
                        "name": "ca-issuer",
                    },
                    "targetRelease": "10.2",
                    "backendCAIssuerRef": {
                        "name": "backend-ca-issuer",
                    },
                    "storageSize": "32Gi",
                    "storageClassName": "foobar",
                    "backup": {
                        "schedule": "0 * * * *",
                        "mysqldump": True,
                        "targets": {
                            "s3": {
                                "endpoint": "https://cool.s3.endpoint",
                                "bucket": "mybucket",
                                "addressingStyle": "virtual",
                                "credentialRef": {
                                    "name": self.s3_cred_secret_name
                                },
                            },
                        },
                    },
                },
                "status": {
                    "nextRelease": "10.2",
                    "installedRelease": "10.2",
                },
            }
        )
        self.client_mock.put_object(
            "", "v1", "secrets",
            NAMESPACE, self.s3_cred_secret_name,
            {
                "apiVersion": "v1",
                "kind": "Secrets",
                "metadata": {
                    "namespace": NAMESPACE,
                    "name": self.s3_cred_secret_name,
                },
                "data": {
                    "access": "QWNjZXNzS2V5",
                    "secret": "U2VjcmV0S2V5",
                },
            }
        )
        self.run_podspec_tests = False

    async def test_database_statefulset_sets_s3_backup(self):  # noqa:E501
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        db, = await statefulsets.list_(NAMESPACE)

        containers = db.spec.template.spec.containers

        self.assertEqual(5, len(containers))

        envs = {
            container.name: {
                envdef.name: envdef
                for envdef in container.env or []
            } for container in containers
        }

        self.assertEqual(
            'True',
            envs["backup-creator"]
                ["YAOOK_BACKUP_CREATOR_MARIADB_MYSQLDUMP"].value
        )

        self.assertIn(
            "s3_upload",
            envs["backup-shifter"]["YAOOK_BACKUP_SHIFTER_SHIFTERS"].value
        )

        self.assertEqual(
            "https://cool.s3.endpoint",
            envs["backup-shifter"]["YAOOK_BACKUP_SHIFTER_S3_URL"].value
        )

        self.assertEqual(
            "mybucket",
            envs["backup-shifter"]["YAOOK_BACKUP_SHIFTER_S3_BUCKET"].value
        )

        self.assertEqual(
            "virtual",
            envs["backup-shifter"]
                ["YAOOK_BACKUP_SHIFTER_S3_ADDRESSING_STYLE"].value
        )

        self.assertEqual(
            self.s3_cred_secret_name,
            envs["backup-shifter"]["YAOOK_BACKUP_SHIFTER_S3_ACCESS_KEY"]
                .value_from.secret_key_ref.name
        )

        self.assertEqual(
            self.s3_cred_secret_name,
            envs["backup-shifter"]["YAOOK_BACKUP_SHIFTER_S3_SECRET_KEY"]
                .value_from.secret_key_ref.name
        )
