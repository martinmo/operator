#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import yaook.op.infra.memcached_cr as memcached_cr
import yaook.statemachine as sm

from ... import testutils


NAME = "memcached-service-xyz"
NAMESPACE = "infra"


class TestMemcachedService(testutils.CustomResourceTestCase):
    async def asyncSetUp(self):
        await super().asyncSetUp()
        self._configure_cr(
            memcached_cr.MemcachedService,
            {
                "apiVersion": "infra.yaook.cloud/v1",
                "kind": "MemcachedService",
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "replicas": 3,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "imageRef": "example.com/memcached:1.2.3",
                    "memory": 1234,
                    "connections": 5678,
                    "loadbalancer": False,
                    "resources": testutils.generate_resources_dict(
                        "memcached",
                        "memcached-exporter",
                        "ssl-terminator",
                        "service-reload"
                    ),
                    "issuerRef": {
                        "name": "ca-issuer"
                    },
                    "serviceMonitor": {
                      "additionalLabels": {
                        "wrong": "label"
                      }
                    }
                },
            }
        )
        self.run_podspec_tests = False

    async def test_sets_replicas_in_statefulset(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        memcached, = await statefulsets.list_(NAMESPACE)

        self.assertEquals(
            3,
            memcached.spec.replicas
        )

    async def test_services_matches_statefulsets(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = sm.service_interface(self.api_client)
        statefulsets = sm.stateful_set_interface(self.api_client)

        memcacheds = await statefulsets.list_(NAMESPACE)

        services = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "service",
            },
        )

        for memcached in memcacheds:
            service_name = memcached.spec.service_name
            match_svcs = [x for x in services
                          if x.metadata.name == service_name]
            self.assertEqual(1, len(match_svcs))
            svc = match_svcs[0]

            self.assertDictEqual(
                svc.spec.selector,
                memcached.spec.template.metadata.labels,
            )

    async def test_creates_memcached_statefulset(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        memcacheds = await statefulsets.list_(NAMESPACE)

        for memcached in memcacheds:
            container = memcached.spec.template.spec.containers[0]

            simple_env_map = {
                envdef.name: envdef.value
                for envdef in container.env
                if envdef.value is not None
            }

            self.assertEqual(
                container.image,
                "example.com/memcached:1.2.3",
            )
            self.assertEqual(
                simple_env_map["MEMCACHED_CACHE_SIZE"],
                "1234",
            )
            self.assertEqual(
                simple_env_map["MEMCACHED_MAX_CONNECTIONS"],
                "5678",
            )

    async def test_creates_containers_with_resource(self):  # noqa:E501
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        memcached, = await statefulsets.list_(NAMESPACE)

        self.assertEqual(
            testutils.container_resources(memcached, 0),
            testutils.unique_resources("memcached")
        )
        self.assertEqual(
            testutils.container_resources(memcached, 1),
            testutils.unique_resources("memcached-exporter")
        )
        self.assertEqual(
            testutils.container_resources(memcached, 2),
            testutils.unique_resources("ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(memcached, 3),
            testutils.unique_resources("service-reload")
        )

    async def test_memcached_statefulset_exposes_ports(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        memcacheds = await statefulsets.list_(NAMESPACE)

        for memcached in memcacheds:

            container = memcached.spec.template.spec.containers[0]

            port_map = {
                port.name: (port.container_port, port.protocol)
                for port in container.ports
            }

            self.assertDictEqual(
                {
                    "memcached": (11211, "TCP"),

                },
                port_map,
            )

    async def test_memcached_statefulset_exposes_exporter_ports(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        memcacheds = await statefulsets.list_(NAMESPACE)

        for memcached in memcacheds:

            container = memcached.spec.template.spec.containers[2]

            port_map = {
                port.name: (port.container_port, port.protocol)
                for port in container.ports
            }

            self.assertDictEqual(
                {
                    "metrics": (9337, "TCP"),
                },
                port_map,
            )

    async def test_memcached_exporter(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        await self.cr.sm.ensure(self.ctx)

        statefulsets = sm.stateful_set_interface(self.api_client)

        memcacheds = await statefulsets.list_(NAMESPACE)

        for memcached in memcacheds:
            container = memcached.spec.template.spec.containers[1]

            self.assertEqual(container.args[0],
                             '--memcached.address=localhost:11211')

            self.assertIn("memcached-exporter", container.image)

            port_map = {
                port.name: (port.container_port, port.protocol)
                for port in container.ports
            }

            self.assertDictEqual(
                {
                    "prometheus": (9150, "TCP"),
                },
                port_map,
            )

    async def test_creates_exporter_certificate(self):
        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "exporter_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "exporter_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            "ca-issuer",
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )

    async def test_exporter_service_monitor_references_service_and_secret(self):  # noqa:E501
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        servicemonitors = sm.servicemonitor_interface(self.api_client)
        services = sm.service_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        sms, = await servicemonitors.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "memcache_service_monitor"
            },
        )
        service, = await services.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "service",
            },
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "exporter_certificate_secret",
            }
        )

        servicelabels = sms["spec"]["selector"]["matchLabels"]
        certificatename = (sms["spec"]["endpoints"][0]["tlsConfig"]
                           ["ca"]["secret"]["name"])

        self.assertEqual(
            "metrics",
            sms["spec"]["endpoints"][0]["port"]
        )

        self.assertEqual(
            servicelabels,
            service.metadata.labels
        )

        self.assertEqual(
            certificatename,
            cert_secret.metadata.name
        )
