#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import asyncio
import contextlib
import functools
import os
import unittest
import unittest.mock

import ddt

import yaook.op.tasks as tasks


def timelimit(timeout):
    def decorator(f):
        @functools.wraps(f)
        async def wrapped(*args, **kwargs):
            return await asyncio.wait_for(f(*args, **kwargs), timeout)
        return wrapped
    return decorator


@ddt.ddt
class TestTaskQueue_Functional(unittest.IsolatedAsyncioTestCase):
    def setUp(self):
        self._env_backup = os.environ.pop("YAOOK_OP_QUEUE_BACKOFF_BASE", None)
        self._env_backup = os.environ.pop("YAOOK_OP_QUEUE_BACKOFF_START", None)

        os.environ["YAOOK_OP_QUEUE_BACKOFF_BASE"] = "2"
        os.environ["YAOOK_OP_QUEUE_BACKOFF_START"] = "1"

        self.q = tasks.TaskQueue()
        self.jitter_mock = unittest.mock.Mock()
        self.jitter_mock.side_effect = self._unjittered
        self.jitter_patch = unittest.mock.patch(
            "yaook.op.tasks.jitter_backoff",
            new=self.jitter_mock,
        )
        self.jitter_patch.start()

    def tearDown(self):
        self.jitter_patch.stop()

    @staticmethod
    def _unjittered(dt, jitter):
        return dt

    @timelimit(2)
    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_failing_tasks_are_backing_off(
            self,
            monotonic):
        monotonic.return_value = 0

        run_ctr = 0
        run_event = asyncio.Event()

        async def failing_task():
            nonlocal run_ctr
            run_ctr += 1
            run_event.set()
            raise Exception()

        self.q.push(failing_task, ())

        await self.q.run_next_task()
        self.assertEqual(run_ctr, 1)

        t_run = 0
        for expected_ctr, exponent in enumerate(range(0, 6), 2):
            delay = 2**exponent
            t_run += delay
            t_pre_run = t_run - 0.1

            monotonic.return_value = t_pre_run
            with self.assertRaises(asyncio.TimeoutError):
                # this needs to time out because of the 1s exponential back off
                await asyncio.wait_for(self.q.run_next_task(), timeout=0.1)

            t_run += t_run + 0.1
            monotonic.return_value = t_run

            await self.q.run_next_task()
            self.assertEqual(run_ctr, expected_ctr)

    @ddt.data(
        # This covers the case where a generic callable which has no return
        # value is used with the TaskQueue; in that case, we do not want to
        # trigger a reschedule.
        None,

        # This covers using a callable which knows about the TaskQueue and uses
        # the boolean return value as a protocol to indicate whether it wants
        # to be rescheduled.
        False,
    )
    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_no_reschedule_behaviour_for_default_and_explicit_control(
            self,
            retval,
            monotonic):
        monotonic.return_value = 0

        with contextlib.ExitStack() as stack:
            add = stack.enter_context(unittest.mock.patch.object(
                self.q, "_add",
                new=unittest.mock.Mock(wraps=self.q._add),
            ))

            task = unittest.mock.AsyncMock()
            task.return_value = retval

            self.q.push(task, ())

            add.assert_called_once_with(unittest.mock.ANY, unittest.mock.ANY)
            task_item = add.mock_calls[0][1][0]
            self.assertEqual(task_item.func, task)
            self.assertEqual(task_item.data, ())
            add.reset_mock()

            await self.q.run_next_task()
            task.assert_awaited_once_with()

            task.reset_mock()

            # less ugly
            add.assert_not_called()

    @timelimit(2)
    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_request_reschedule_from_task(
            self,
            monotonic):
        monotonic.return_value = 0

        with contextlib.ExitStack() as stack:
            add = stack.enter_context(unittest.mock.patch.object(
                self.q, "_add",
                new=unittest.mock.Mock(wraps=self.q._add),
            ))

            task = unittest.mock.AsyncMock()
            task.return_value = True

            self.q.push(task, ())

            add.assert_called_once_with(unittest.mock.ANY, unittest.mock.ANY)
            task_item = add.mock_calls[0][1][0]
            self.assertEqual(task_item.func, task)
            self.assertEqual(task_item.data, ())
            add.reset_mock()

            await self.q.run_next_task()
            task.assert_awaited_once_with()

            add.assert_called_once_with(unittest.mock.ANY, unittest.mock.ANY)
            task_item = add.mock_calls[0][1][0]
            self.assertEqual(task_item.func, task)
            self.assertEqual(task_item.data, ())

            task.reset_mock()
            task.return_value = False
            monotonic.return_value = 1.1

            await self.q.run_next_task()
            task.assert_awaited_once_with()

    @timelimit(2)
    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_request_reschedule_from_task_always_causes_backoff_of_first_order(  # noqa:E501
            self,
            monotonic):
        monotonic.return_value = 0

        task = unittest.mock.AsyncMock()
        task.return_value = True

        self.q.push(task, ())

        await self.q.run_next_task()
        task.assert_awaited_once_with()

        task.reset_mock()
        task.return_value = True

        monotonic.return_value = 0.8
        with self.assertRaises(asyncio.TimeoutError):
            await asyncio.wait_for(self.q.run_next_task(), timeout=0.1)

        monotonic.return_value = 1.1
        await self.q.run_next_task()
        task.assert_awaited_once_with()

        task.reset_mock()
        task.return_value = False

        monotonic.return_value = 1.9
        with self.assertRaises(asyncio.TimeoutError):
            await asyncio.wait_for(self.q.run_next_task(), timeout=0.1)

        monotonic.return_value = 2.2
        await self.q.run_next_task()
        task.assert_awaited_once_with()

    @timelimit(2)
    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_push_during_execution_causes_reexecution_after_single_backoff(  # noqa:E501
            self,
            monotonic):
        monotonic.return_value = 0

        task_fut = asyncio.Future()
        task_started = asyncio.Event()
        run_ctr = 0

        async def long_running_task():
            nonlocal run_ctr
            run_ctr += 1
            task_started.set()
            return await task_fut

        self.q.push(long_running_task, ())

        run_task = asyncio.create_task(self.q.run_next_task())
        await task_started.wait()
        task_started.clear()

        self.q.push(long_running_task, ())
        task_fut.set_result(None)
        await run_task
        self.assertEqual(run_ctr, 1)

        monotonic.return_value = 0.8
        with self.assertRaises(asyncio.TimeoutError):
            await asyncio.wait_for(self.q.run_next_task(), timeout=0.1)

        monotonic.return_value = 1.1
        await self.q.run_next_task()
        self.assertEqual(run_ctr, 2)

    @timelimit(2)
    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_push_during_backoff_does_not_cause_double_execution(
            self,
            monotonic):
        monotonic.return_value = 0

        # arrange a task to run with control over how it exists
        task = unittest.mock.AsyncMock()
        task.side_effect = Exception

        self.q.push(task, ())

        # run the task once
        await self.q.run_next_task()
        task.assert_awaited_once_with()

        # assert that the task is currently in exponential backoff phase by
        monotonic.return_value = 0.8
        with self.assertRaises(asyncio.TimeoutError):
            await asyncio.wait_for(self.q.run_next_task(), timeout=0.1)

        # push the task a second time ...
        self.q.push(task, ())

        # ... and make the task succeed this time
        task.reset_mock()
        task.side_effect = None
        task.return_value = None

        # advance the clock to trigger re-execution after exponential backoff
        # after error
        monotonic.return_value = 1.1
        await self.q.run_next_task()
        task.assert_awaited_once_with()

        # assert that the task is not executed a second time after the first
        # backoff, even though it was re-pushed by advancing the clock beyond
        # the maximum backoff and retrying execution
        monotonic.return_value = 120
        task.reset_mock()
        with self.assertRaises(asyncio.TimeoutError):
            await asyncio.wait_for(self.q.run_next_task(), timeout=0.1)

        task.assert_not_called()
        task.assert_not_awaited()

    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_new_task_cannot_starve_backlogged_task(
            self,
            monotonic):
        monotonic.return_value = 0

        run_ctr = 0
        run_event = asyncio.Event()
        succeding_ran = False

        async def failing_task():
            nonlocal run_ctr
            run_ctr += 1
            run_event.set()
            raise Exception()

        self.q.push(failing_task, ())

        await self.q.run_next_task()
        await run_event.wait()
        run_event.clear()
        self.assertEqual(run_ctr, 1)

        # this is definitely after the time in point where the failing task
        # should reexecute
        monotonic.return_value = 120

        async def succeeding_task():
            nonlocal succeding_ran
            succeding_ran = True
            run_event.set()
            return False

        self.q.push(succeeding_task, ())

        await self.q.run_next_task()
        await run_event.wait()

        self.assertFalse(succeding_ran)
        self.assertEqual(run_ctr, 2)

    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_new_task_queues_before_backing_off_task(
            self,
            monotonic):
        monotonic.return_value = 0

        run_ctr = 0
        run_event = asyncio.Event()
        succeding_ran = False

        async def failing_task():
            nonlocal run_ctr
            run_ctr += 1
            run_event.set()
            raise Exception()

        self.q.push(failing_task, ())

        await self.q.run_next_task()
        await run_event.wait()
        run_event.clear()
        self.assertEqual(run_ctr, 1)

        # this is before the time the backing off task should run
        monotonic.return_value = 0.1

        async def succeeding_task():
            nonlocal succeding_ran
            succeding_ran = True
            run_event.set()
            return False

        self.q.push(succeeding_task, ())

        await self.q.run_next_task()
        await run_event.wait()

        self.assertTrue(succeding_ran)
        self.assertEqual(run_ctr, 1)

    @unittest.mock.patch("time.monotonic")
    async def test_task_is_re_run_immediately_by_next_worker_on_cancellation(
            self,
            monotonic):
        monotonic.return_value = 0

        # using a future allows us to control precisely when and how the task
        # completes
        task_fut = asyncio.Future()
        task_started = asyncio.Event()

        async def long_task():
            task_started.set()
            await task_fut

        self.q.push(long_task, ())

        task_run = asyncio.create_task(self.q.run_next_task())
        await task_started.wait()
        self.assertFalse(task_run.done())

        task_fut.cancel()
        with self.assertRaises(asyncio.CancelledError):
            await task_run

        task_fut = asyncio.Future()

        task_started.clear()

        task_run = asyncio.create_task(self.q.run_next_task())
        await task_started.wait()
        task_fut.set_result(None)
        self.assertFalse(task_run.done())
        await task_run

    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_runner_wakes_up_when_pushing_a_task(
            self,
            monotonic):
        monotonic.return_value = 0

        runner = asyncio.create_task(self.q.run_next_task())
        await asyncio.sleep(0.1)
        self.assertFalse(runner.done())

        task = unittest.mock.AsyncMock()
        task.return_value = None

        self.q.push(task, ())

        await runner
        task.assert_awaited_once_with()

    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_runner_wakes_up_when_pushing_an_early_task_while_backing_off(  # noqa:E501
            self,
            monotonic):
        monotonic.return_value = 0

        failing_task = unittest.mock.AsyncMock()
        failing_task.side_effect = Exception

        succeeding_task = unittest.mock.AsyncMock()
        succeeding_task.return_value = None

        self.q.push(failing_task, ())
        await self.q.run_next_task()

        failing_task.assert_awaited_once_with()
        failing_task.reset_mock()

        monotonic.return_value = 0.5

        runner = asyncio.create_task(self.q.run_next_task())
        await asyncio.sleep(0.1)
        self.assertFalse(runner.done())
        succeeding_task.assert_not_awaited()
        failing_task.assert_not_called()

        self.q.push(succeeding_task, ())

        await runner

        succeeding_task.assert_awaited_once_with()
        failing_task.assert_not_called()

    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_concurrent_runners_do_not_execute_the_same_tasks(
            self,
            monotonic):
        monotonic.return_value = 0

        runner1 = asyncio.create_task(self.q.run_next_task())
        runner2 = asyncio.create_task(self.q.run_next_task())

        async def slightly_blocking():
            # give the runners a chance to collide
            await asyncio.sleep(0.1)

        task = unittest.mock.AsyncMock()
        task.side_effect = slightly_blocking

        self.q.push(task, ())

        done, pending = await asyncio.wait(
            [runner1, runner2],
            return_when=asyncio.FIRST_COMPLETED,
        )

        task.assert_awaited_once_with()

        for fut in done:
            fut.result()

        for fut in pending:
            fut.cancel()

        self.assertEqual(len(done), 1, (done, pending))
        self.assertEqual(len(pending), 1, (done, pending))

    async def test_concurrent_runners_pick_up_requeues_of_other_runners(self):
        # This test ensures that when a task is re-enqueued by a runner, other
        # runners are woken up to get a chance to pick it up. If this was not
        # the case, the second runner would block indefinitely because it was
        # already blocking when the task got requeued.

        # we have to mess with the backoff start here and run this test against
        # the real clock, otherwise it cannot work
        # we would have to change the clock while we don’t have control
        # (because it is very likely that the control first hands over to the
        # second runner when the task is re-enqueued, which will then read the
        # time to figure out that it needs to sleep until the task is due, at
        # which point we cannot wake it up again)
        self.q._backoff_start = 0.1

        # arrange two concurrently executing queue runners
        runner1 = asyncio.create_task(self.q.run_next_task())
        runner2 = asyncio.create_task(self.q.run_next_task())

        # arrange a task which always returns true (to request a requeue)
        requeing_task = unittest.mock.AsyncMock()
        requeing_task.return_value = True

        # push the task to the queue once
        self.q.push(requeing_task, ())

        # act: wait for the first runner to complete the execution of the task
        done, pending = await asyncio.wait(
            [runner1, runner2],
            return_when=asyncio.FIRST_COMPLETED,
        )

        # assert that the task was executed
        requeing_task.assert_awaited_once_with()
        requeing_task.reset_mock()

        # assert that the second runner picks up the task
        done, pending = await asyncio.wait(
            [runner1, runner2],
            return_when=asyncio.ALL_COMPLETED,
            # 0.1 for the exponential backoff, and a bunch of slack
            timeout=0.25,
        )

        # assert that the task was re-executed by the second runner
        requeing_task.assert_awaited_once_with()

        # assert that both runners have completed without exception
        # (otherwise .result() raises an InvalidStateException or whatever
        # exception the task threw)
        runner1.result()
        runner2.result()

    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_concurrent_runners_pick_up_concurrent_enqueues(
            self,
            monotonic):
        # run this test *a lot* to avoid effects of spurious wakeups making it
        # pass incorrectly
        for i in range(50):
            monotonic.return_value = 0

            runner1 = asyncio.create_task(self.q.run_next_task())
            runner2 = asyncio.create_task(self.q.run_next_task())

            task1 = unittest.mock.AsyncMock()
            task1.return_value = None

            task2 = unittest.mock.AsyncMock()
            task2.return_value = None

            self.q.push(task1, ())
            self.q.push(task2, ())

            done, pending = await asyncio.wait(
                [runner1, runner2],
                return_when=asyncio.ALL_COMPLETED,
                timeout=0.1,
            )

            task1.assert_awaited_once_with()
            task2.assert_awaited_once_with()

            runner1.result()
            runner2.result()

    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_concurrent_runners_pick_up_enqueues_from_other_runners(
            self,
            monotonic):
        # This test protects against a very specific scenario. If implemented
        # incorrectly, the task queue would fail to pick up tasks created by
        # other concurrently running runners because they concurrently clear
        # the wakeup event.
        monotonic.return_value = 0

        runner1 = asyncio.create_task(self.q.run_next_task())
        runner2 = asyncio.create_task(self.q.run_next_task())
        runner3 = asyncio.create_task(self.q.run_next_task())

        task3_started = asyncio.Event()
        task2_started = asyncio.Event()

        async def task3_impl():
            task3_started.set()

        async def task2_impl():
            await task3_started.wait()
            task2_started.set()

        async def task1_impl():
            # pushing two tasks: If the wakeup was not working correctly, one
            # runner may cause the other runner to block even though there are
            # tasks left.
            self.q.push(task2, ())
            self.q.push(task3, ())
            await task2_started.wait()

        task1 = unittest.mock.AsyncMock()
        task1.side_effect = task1_impl

        task2 = unittest.mock.AsyncMock()
        task2.side_effect = task2_impl

        task3 = unittest.mock.AsyncMock()
        task3.side_effect = task3_impl

        self.q.push(task1, ())

        done, pending = await asyncio.wait(
            [runner1, runner2, runner3],
            return_when=asyncio.ALL_COMPLETED,
            timeout=0.2,
        )

        runner1.result()
        runner2.result()
        runner3.result()

        task1.assert_awaited_once_with()
        task2.assert_awaited_once_with()
        task3.assert_awaited_once_with()

    @unittest.mock.patch("yaook.op.tasks.clock")
    async def test_validate_queue_has_no_duplicates_while_running(
            self,
            monotonic):
        # This test validates that a task is guaranteed to be
        # in the queue only once, regardless of where the task
        # is added. The parallelisation for CustomResources in
        # daemon.py is very dependent on this.
        monotonic.return_value = 0

        effects = []
        queue_lengths = []

        runner1 = asyncio.create_task(self.q.run_next_task())
        runner2 = asyncio.create_task(self.q.run_next_task())

        async def task():
            effects.append("e1")
            # Check jobs that can be picked up by a runner
            queue_lengths.append(len(self.q._schedule))
            self.q.push(task, ())

            asyncio.sleep(0.2)
            # Even after re-adding, we must have 0 jobs in the
            # queue that another runner could execute.
            queue_lengths.append(len(self.q._schedule))

            # Give the other runner enough time to take
            # the job he should not be able to take.
            asyncio.sleep(0.5)
            effects.append("e2")

        # Push the task once
        self.q.push(task, ())

        done, pending = await asyncio.wait(
            [runner1, runner2],
            return_when=asyncio.FIRST_COMPLETED,
        )

        self.assertEqual(len(done), 1, (done, pending))
        self.assertEqual(len(pending), 1, (done, pending))

        self.assertEqual(len(self.q._tasks), 1, (len(self.q._tasks)))
        self.assertEqual(len(self.q._schedule), 1, (len(self.q._schedule)))
        self.assertEquals(
            tuple(queue_lengths), (0, 0)
        )
        self.assertEquals(
            tuple(effects), ("e1", "e2")
        )
