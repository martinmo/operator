#
# Copyright (c) 2021 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import pprint
import uuid
import os

import ddt
import unittest

import yaook.op.common as op_common
import yaook.op.scheduling_keys as scheduling_keys
import yaook.op.neutron as neutron
import yaook.statemachine as sm
import yaook.statemachine.context as context
import yaook.statemachine.interfaces as interfaces

from tests import testutils


NAMESPACE = "test-namespace"
NAME = "neutron"
CONFIG_FILE_NAME = "neutron.conf"
CONFIG_PATH = "/etc/neutron"
POLICY_FILE_NAME = "policy.yaml"
POLICY_RULE_KEY = "context_is_admin"
POLICY_RULE_VALUE = "role:admin"
NEUTRON_POLICY = {"policy": {POLICY_RULE_KEY: POLICY_RULE_VALUE}}
CONFIG_SECRET_NAME = "config-secret"
CONFIG_SECRET_KEY = "mysecretkey"
CONFIG_SECRET_VALUE = "mysecretvalue"


class TestNeutronDeploymentCases(unittest.IsolatedAsyncioTestCase):
    @ddt.ddt
    class TestNeutronBaseDeployment(
            testutils.ReleaseAwareCustomResourceTestCase,
            testutils.DatabaseTestMixin,
            testutils.MessageQueueTestMixin,
            testutils.MemcachedTestMixin):
        def _get_neutron_deployment_yaml(self):
            return {
                "metadata": {
                    "name": NAME,
                    "namespace": NAMESPACE,
                },
                "spec": {
                    "keystoneRef": {
                        "name": self._keystone_name,
                        "kind": "KeystoneDeployment",
                    },
                    "region": {
                        "name": "regionname",
                        "parent": "parentregionname",
                    },
                    "issuerRef": {
                        "name": "issuername",
                    },
                    "novaRef": {
                        "name": "nova",
                    },
                    "api": {
                        "ingress": {
                            "fqdn": "neutron-ingress",
                            "port": 8080,
                            "ingressClassName": "nginx",
                        },
                        "replicas": 2,
                        "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                        "resources": testutils.generate_resources_dict(
                            "api.neutron-api",
                            "api.ssl-terminator",
                            "api.ssl-terminator-external",
                            "api.service-reload",
                            "api.service-reload-external",
                        ),
                    },
                    "database": {
                        "replicas": 2,
                        "timeoutClient": 300,
                        "storageSize": "8Gi",
                        "storageClassName": "foo-class",
                        "proxy": {
                            "replicas": 1,
                            "resources": testutils.
                            generate_db_proxy_resources(),
                        },
                        "backup": {
                            "schedule": "0 * * * *"
                        },
                        "resources": testutils.generate_db_resources(),
                    },
                    "messageQueue": {
                        "replicas": 1,
                        "storageSize": "2Gi",
                        "storageClassName": "bar-class",
                        "resources": testutils.generate_amqp_resources(),
                    },
                    "memcached": {
                        "replicas": 2,
                        "memory": "512",
                        "connections": "2000",
                        "resources": testutils.generate_memcached_resources(),
                    },
                    "targetRelease": "yoga",
                    "neutronConfig": {},
                    "neutronSecrets": [
                        {
                            "secretName": CONFIG_SECRET_NAME,
                            "items": [{
                                "key": CONFIG_SECRET_KEY,
                                "path": "/DEFAULT/mytestsecret",
                            }],
                        },
                    ],
                    "neutronML2Config": {},
                    "jobResources": testutils.generate_resources_dict(
                        "job.neutron-db-sync-job",
                    ),
                },
            }

        async def asyncSetUp(self):
            await super().asyncSetUp()

            self._keystone_name = self._provide_keystone(NAMESPACE)

            self.metadata_pw = f"foobar2342-{uuid.uuid4()}"
            self.client_mock.put_object(
                "", "v1", "secrets",
                NAMESPACE, "magic-metadata-secret",
                {
                    "apiVersion": "v1",
                    "kind": "Secret",
                    "metadata": {
                        "namespace": NAMESPACE,
                        "name": "magic-metadata-secret",
                        "labels": {
                            context.LABEL_COMPONENT:
                                op_common.NOVA_METADATA_SECRET_COMPONENT,
                            context.LABEL_PARENT_PLURAL: "novadeployments",
                            context.LABEL_PARENT_GROUP: "yaook.cloud",
                            context.LABEL_PARENT_NAME: "nova",
                        },
                    },
                    "data": sm.api_utils.encode_secret_data({
                        "password": self.metadata_pw,
                    }),
                },
            )
            self.client_mock.put_object(
                    "", "v1", "secrets",
                    NAMESPACE, CONFIG_SECRET_NAME,
                    {
                        "apiVersion": "v1",
                        "kind": "Secret",
                        "metadata": {
                            "namespace": NAMESPACE,
                            "name": CONFIG_SECRET_NAME,
                        },
                        "data": sm.api_utils.encode_secret_data({
                            CONFIG_SECRET_KEY: CONFIG_SECRET_VALUE,
                        }),
                    },
                )

        def _make_all_dependencies_complete_immediately(self):
            super()._make_all_dependencies_complete_immediately()
            self._make_all_ovsdb_ready_immediately()

        def _make_all_ovsdb_ready_immediately(self):
            def create_ovsdb_service(ovsdb, **kwargs):
                self.logger.info(
                    "made ovsdb %s ready immediately",
                    ovsdb["metadata"])
                namespace = ovsdb["metadata"]["namespace"]
                name = ovsdb["metadata"]["name"]
                self.client_mock.put_object(
                    "", "v1", "services",
                    namespace, name,
                    {
                        "apiVersion": "v1",
                        "kind": "Service",
                        "metadata": {
                            "namespace": namespace,
                            "name": name,
                            "labels": {
                                context.LABEL_COMPONENT:
                                op_common.OVSDB_DATABASE_SERVICE_COMPONENT,
                                context.LABEL_PARENT_GROUP:
                                "infra.yaook.cloud",
                                context.LABEL_PARENT_NAME: name,
                                context.LABEL_PARENT_PLURAL: "ovsdbservices",
                                testutils.LABEL_IGNORE_DURING_TESTS: "true",
                            },
                        },
                        "spec": {
                            "clusterIP": "10.0.0.1",
                            "selector": {},
                            "ports": {},
                        },
                    },
                )
                return ovsdb

            def create_ovn_relay_service(ovsdb, **kwargs):
                self.logger.info(
                    "made ovn-relay %s ready immediately",
                    ovsdb["metadata"])
                namespace = ovsdb["metadata"]["namespace"]
                name = ovsdb["metadata"]["name"]
                self.client_mock.put_object(
                    "", "v1", "services",
                    namespace, f"{name}-relay",
                    {
                        "apiVersion": "v1",
                        "kind": "Service",
                        "metadata": {
                            "namespace": namespace,
                            "name": f"{name}-relay",
                            "labels": {
                                context.LABEL_COMPONENT:
                                op_common.OVN_ACCESS_SERVICE_COMPONENT,
                                context.LABEL_PARENT_GROUP:
                                "infra.yaook.cloud",
                                context.LABEL_PARENT_NAME: name,
                                context.LABEL_PARENT_PLURAL: "ovsdbservices",
                                testutils.LABEL_IGNORE_DURING_TESTS: "true",
                            },
                        },
                        "spec": {
                            "clusterIP": "10.0.0.2",
                            "selector": {},
                            "ports": {},
                        },
                    },
                )
                return ovsdb

            def patch_ovsdbservice(ovsdb, **kwargs):
                self.logger.info("made ovsdb %s ready immediately",
                                 ovsdb["metadata"])
                ovsdb.setdefault("metadata", {})["generation"] = 1
                ovsdb.setdefault("status", {})["observedGeneration"] = 1
                ovsdb.setdefault("status", {})["updatedGeneration"] = 1
                ovsdb["status"]["phase"] = "Updated"
                return ovsdb

            self.client_mock.add_hook(
                "infra.yaook.cloud", "v1", "ovsdbservices", "PATCH",
                create_ovsdb_service,
            )
            self.client_mock.add_hook(
                "infra.yaook.cloud", "v1", "ovsdbservices", "PATCH",
                create_ovn_relay_service,
            )
            self.client_mock.add_hook(
                "infra.yaook.cloud", "v1", "ovsdbservices", "PATCH",
                patch_ovsdbservice,
            )

        async def test_keystone_user_matches_keystone_reference(self):
            await self.cr.sm.ensure(self.ctx)

            users = interfaces.keystoneuser_interface(self.api_client)
            user, = await users.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "api_keystone_user"},
            )

            self.assertEqual(
                user["spec"]["keystoneRef"]["name"],
                self._keystone_name
            )
            self.assertEqual(
                user["spec"]["keystoneRef"]["kind"],
                "KeystoneDeployment"
            )

        async def test_keystone_endpoint_matches_keystone_reference(self):
            await self.cr.sm.ensure(self.ctx)

            endpoints = interfaces.keystoneendpoint_interface(self.api_client)
            endpoint, = await endpoints.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
            )

            self.assertEqual(
                endpoint["spec"]["keystoneRef"]["name"],
                self._keystone_name
            )
            self.assertEqual(
                endpoint["spec"]["keystoneRef"]["kind"],
                "KeystoneDeployment"
            )

        async def test_keystone_endpoint_is_created(self):
            deployment_yaml = self._get_neutron_deployment_yaml()
            self._configure_cr(neutron.Neutron, deployment_yaml)

            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            endpoints_int = interfaces.keystoneendpoint_interface(
                self.api_client)
            endpoints = await endpoints_int.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
            )

            self.assertEqual(len(endpoints), 1)

        async def test_keystone_endpoint_is_not_created(self):
            deployment_yaml = self._get_neutron_deployment_yaml()
            deployment_yaml["spec"]["api"]["publishEndpoint"] = False
            self._configure_cr(neutron.Neutron, deployment_yaml)

            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            endpoints_int = interfaces.keystoneendpoint_interface(
                self.api_client)
            endpoints = await endpoints_int.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
            )

            self.assertEqual(len(endpoints), 0)

        async def test_keystone_endpoint_matches_region(self):
            await self.cr.sm.ensure(self.ctx)

            endpoints = interfaces.keystoneendpoint_interface(self.api_client)
            endpoint, = await endpoints.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "keystone_endpoint"},
            )

            self.assertEqual(
                endpoint["spec"]["region"]["name"],
                "regionname"
            )
            self.assertEqual(
                endpoint["spec"]["region"]["parent"],
                "parentregionname"
            )

        async def test_certificate_contains_service_name(self):
            self._make_all_dependencies_complete_immediately()

            await self.cr.sm.ensure(self.ctx)

            certificates = interfaces.certificates_interface(self.api_client)
            services = interfaces.service_interface(self.api_client)

            service = await services.read(NAMESPACE, "neutron-api")

            certificate, = await certificates.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "certificate",
                },
            )

            self.assertIn(
                f"{service.metadata.name}.{NAMESPACE}.svc",
                certificate["spec"]["dnsNames"],
            )

        async def test_certificate_contains_ingress_fqdn(self):
            self._make_all_dependencies_complete_immediately()

            await self.cr.sm.ensure(self.ctx)

            certificates = interfaces.certificates_interface(self.api_client)

            certificate, = await certificates.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "certificate",
                },
            )

            self.assertIn(
                "neutron-ingress",
                certificate["spec"]["dnsNames"],
            )

        async def test_certificate_contains_issuer_name(self):
            self._make_all_dependencies_complete_immediately()

            await self.cr.sm.ensure(self.ctx)

            certificates = interfaces.certificates_interface(self.api_client)

            certificate, = await certificates.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "certificate",
                },
            )

            self.assertEqual(
                "issuername",
                certificate["spec"]["issuerRef"]["name"],
            )

        async def test_jobs_use_config_secrets(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            secrets = interfaces.secret_interface(self.api_client)
            config_secret, = await secrets.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "config"},
            )

            jobs = interfaces.job_interface(self.api_client)
            for job in await jobs.list_(NAMESPACE):
                if job._metadata.generate_name == "neutron-policy-validator-":
                    continue
                self.assertEqual(
                    job.spec.template.spec.volumes[0].secret.secret_name,
                    config_secret.metadata.name,
                    str(job.metadata),
                )
                self.assertEqual(
                    job.spec.template.spec.volumes[1].secret.secret_name,
                    config_secret.metadata.name,
                    str(job.metadata),
                )

        async def test_creates_config_matches_keystone_user(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            secrets = interfaces.secret_interface(self.api_client)
            config, = await secrets.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "config"},
            )

            users = interfaces.keystoneuser_interface(self.api_client)
            user, = await users.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "api_keystone_user"},
            )

            user_credentials_secret, = await secrets.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT:
                        op_common.KEYSTONE_USER_CREDENTIALS_COMPONENT,
                    context.LABEL_PARENT_NAME: user["metadata"]["name"],
                    context.LABEL_PARENT_PLURAL: "keystoneusers",
                },
            )

            user_credentials = sm.api_utils.decode_secret_data(
                user_credentials_secret.data
            )
            cfg = testutils._parse_config(
                config.data["neutron.conf"], decode=True
            )

            self.assertEqual(
                cfg.get("keystone_authtoken", "username"),
                user_credentials["OS_USERNAME"]
            )
            self.assertEqual(
                cfg.get("keystone_authtoken", "password"),
                user_credentials["OS_PASSWORD"]
            )

        async def test_creates_database_and_user(self):
            self._make_all_databases_ready_immediately()

            await self.cr.sm.ensure(self.ctx)

            dbs = interfaces.mysqlservice_interface(self.api_client)
            dbusers = interfaces.mysqluser_interface(self.api_client)
            secrets = interfaces.secret_interface(self.api_client)

            db, = await dbs.list_(NAMESPACE)
            api_user, = await dbusers.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "db_api_user",
                }
            )
            api_user_password, = await secrets.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "db_api_user_password",
                }
            )

            self.assertEqual(
                api_user["spec"]["serviceRef"]["name"],
                db["metadata"]["name"])
            self.assertEqual(
                api_user["spec"]["passwordSecretKeyRef"]["name"],
                api_user_password.metadata.name,
            )
            self.assertEqual(db["spec"]["replicas"], 2)
            self.assertEqual(db["spec"]["proxy"]["timeoutClient"], 300)
            self.assertEqual(db["spec"]["proxy"]["replicas"], 1)
            self.assertEqual(
                db["spec"]["storageClassName"],
                "foo-class")
            self.assertEqual(db["spec"]["storageSize"], "8Gi")

        async def test_database_frontendIssuer_name(self):
            self._make_all_databases_ready_immediately()
            await self.cr.sm.ensure(self.ctx)

            mysqlservices = interfaces.mysqlservice_interface(self.api_client)

            db, = await mysqlservices.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "db",
                },
            )

            self.assertEqual(
                "issuername",
                db["spec"]["frontendIssuerRef"]["name"],
            )

        async def test_creates_config_with_database_uri(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            secrets = interfaces.secret_interface(self.api_client)
            services = interfaces.service_interface(self.api_client)
            mysqlusers = interfaces.mysqluser_interface(self.api_client)
            mysqlservices = interfaces.mysqlservice_interface(self.api_client)
            config, = await secrets.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "config",
                },
            )
            db_user_password_secret, = await secrets.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "db_api_user_password",
                },
            )
            db_service, = await services.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "public_service",
                    context.LABEL_PARENT_PLURAL: "mysqlservices",
                },
            )
            db_user_password = await sm.extract_password(
                self.ctx,
                db_user_password_secret.metadata.name,
            )
            db_user, = await mysqlusers.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "db_api_user",
                },
            )
            db, = await mysqlservices.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "db",
                },
            )
            db_name = db["spec"]["database"]
            cinder_conf = config.data["neutron.conf"]
            cfg = testutils._parse_config(cinder_conf, decode=True)

            self.assertEqual(
                cfg.get("database", "connection"),
                f"mysql+pymysql://{db_user['spec']['user']}:"
                f"{db_user_password}@{db_service.metadata.name}."
                f"{db_service.metadata.namespace}:3306/{db_name}?charset=utf8"
                "&ssl_ca=/etc/ssl/certs/ca-bundle.crt",
            )

        async def test_database_uri_refers_to_mounted_ca_bundle(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            secrets = interfaces.secret_interface(self.api_client)
            configmaps = interfaces.config_map_interface(self.api_client)
            deployments = interfaces.deployment_interface(self.api_client)

            config, = await secrets.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "config",
                },
            )

            ca_certs, = await configmaps.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "ca_certs",
                    context.LABEL_PARENT_PLURAL: "neutrondeployments",
                },
            )

            api, = await deployments.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "api_deployment",
                },
            )

            cert_mountpoint = testutils.find_volume_mountpoint(
                api.spec.template.spec,
                testutils.find_configmap_volume(
                    api.spec.template.spec,
                    ca_certs.metadata.name,
                ),
                "neutron-api",
            )

            neutron_conf = config.data["neutron.conf"]
            cfg = testutils._parse_config(neutron_conf, decode=True)

            self.assertIn(
                f"ssl_ca={cert_mountpoint}/ca-bundle.crt",
                cfg.get("database", "connection"),
            )

        async def test_amqp_server_frontendIssuer_name(self):
            self._make_all_databases_ready_immediately()
            self._make_all_mqs_succeed_immediately()
            await self.cr.sm.ensure(self.ctx)

            amqpserver = interfaces.amqpserver_interface(self.api_client)

            amqp, = await amqpserver.list_(
                NAMESPACE,
            )

            self.assertEqual(
                "issuername",
                amqp["spec"]["frontendIssuerRef"]["name"],
            )

        async def test_creates_message_queue_and_user(self):
            self._make_all_mqs_succeed_immediately()

            await self.cr.sm.ensure(self.ctx)

            mqs = interfaces.amqpserver_interface(self.api_client)
            mqusers = interfaces.amqpuser_interface(self.api_client)
            secrets = interfaces.secret_interface(self.api_client)

            mq, = await mqs.list_(NAMESPACE)
            api_user, = await mqusers.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "mq_api_user",
                }
            )
            api_user_password, = await secrets.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "mq_api_user_password",
                }
            )

            self.assertEqual(
                api_user["spec"]["serverRef"]["name"],
                mq["metadata"]["name"])
            self.assertEqual(
                api_user["spec"]["passwordSecretKeyRef"]["name"],
                api_user_password.metadata.name,
            )
            self.assertEqual(mq["spec"]["replicas"], 1)
            self.assertEqual(mq["spec"]["storageSize"], "2Gi")
            self.assertEqual(mq["spec"]["storageClassName"], "bar-class")

        async def test_creates_config_with_transport_url(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            secrets = interfaces.secret_interface(self.api_client)
            services = interfaces.service_interface(self.api_client)
            config, = await secrets.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "config",
                },
            )
            mq_user_password_secret, = await secrets.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "mq_api_user_password",
                },
            )
            mq_service, = await services.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "public_service",
                    context.LABEL_PARENT_PLURAL: "amqpservers",
                },
            )
            mq_user_password = await sm.extract_password(
                self.ctx,
                mq_user_password_secret.metadata.name,
            )
            neutron_conf = config.data["neutron.conf"]
            cfg = testutils._parse_config(neutron_conf, decode=True)

            self.assertEqual(
                cfg.get("DEFAULT", "transport_url"),
                f"rabbit://api:{mq_user_password}@"
                f"{mq_service.metadata.name}."
                f"{mq_service.metadata.namespace}:5671/"
            )
            self.assertTrue(
                cfg.get("oslo_messaging_rabbit", "ssl"),
            )
            self.assertEqual(
                cfg.get("oslo_messaging_rabbit", "ssl_ca_file"),
                "/etc/ssl/certs/ca-bundle.crt",
            )

        async def test_creates_memcached(self):
            await self.cr.sm.ensure(self.ctx)

            memcacheds = interfaces.memcachedservice_interface(self.api_client)
            memcached, = await memcacheds.list_(NAMESPACE)

            self.assertEqual(
                memcached["spec"]["memory"],
                "512"
            )
            self.assertEqual(
                memcached["spec"]["connections"],
                "2000"
            )

        async def test_creates_api_deployment_with_replica_spec(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            deployments = interfaces.deployment_interface(self.api_client)
            secrets = interfaces.secret_interface(self.api_client)

            api_deployment, = await deployments.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "api_deployment"},
            )
            config_secret, = await secrets.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "config"},
            )

            self.assertEqual(api_deployment.spec.replicas, 2)
            self.assertEqual(
                api_deployment.spec.template.spec.volumes[0].projected.
                sources[0].secret.name,
                config_secret.metadata.name,
            )
            self.assertEqual(
                api_deployment.spec.template.spec.volumes[1].secret.
                secret_name,
                config_secret.metadata.name,
            )

        async def test_creates_service(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            services = interfaces.service_interface(self.api_client)
            service = await services.read(NAMESPACE, "neutron-api")

            self.assertIsNotNone(service)

        async def test_creates_endpoint_with_ingress(self):
            await self.cr.sm.ensure(self.ctx)

            endpoints = interfaces.keystoneendpoint_interface(self.api_client)
            endpoint = await endpoints.read(NAMESPACE, "neutron-api-endpoint")

            self.assertEqual(
                endpoint["spec"]["endpoints"]["public"],
                "https://neutron-ingress:8080",
            )

        async def test_creates_ingress(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            ingresses = interfaces.ingress_interface(self.api_client)
            ingress = await ingresses.read(NAMESPACE, "neutron")

            self.assertEqual(
                ingress.spec.rules[0].host,
                "neutron-ingress",
            )

        async def test_disable_ingress_creation(self):
            deployment_yaml = self._get_neutron_deployment_yaml()
            deployment_yaml["spec"]["api"]["ingress"]["createIngress"] = False
            self._configure_cr(neutron.Neutron, deployment_yaml)
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            ingress_int = interfaces.ingress_interface(self.api_client)
            ingresses = await ingress_int.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "api_ingress"},
            )
            self.assertEqual(len(ingresses), 0)

        async def test_enable_ingress_creation(self):
            deployment_yaml = self._get_neutron_deployment_yaml()
            deployment_yaml["spec"]["api"]["ingress"]["createIngress"] = True
            self._configure_cr(neutron.Neutron, deployment_yaml)
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            ingress_int = interfaces.ingress_interface(self.api_client)
            ingresses = await ingress_int.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "api_ingress"},
            )
            self.assertEqual(len(ingresses), 1)

        async def test_ingress_force_ssl_annotation(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            ingresses = interfaces.ingress_interface(self.api_client)
            ingress = await ingresses.read(NAMESPACE, "neutron")

            self.assertEqual(
                ingress
                .metadata
                .annotations["nginx.ingress.kubernetes.io/force-ssl-redirect"],
                "true",
            )

        async def test_ingress_matches_service(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            ingresses = interfaces.ingress_interface(self.api_client)
            ingress = await ingresses.read(NAMESPACE, "neutron")

            services = interfaces.service_interface(self.api_client)
            service = await services.read(NAMESPACE, "neutron-api")

            self.assertEqual(
                ingress.spec.rules[0].http.paths[0].backend.service.name,
                service.metadata.name,
            )
            self.assertEqual(
                ingress.spec.rules[0].http.paths[0].backend.service.port.
                number,
                ([x.port for x in service.spec.ports if x.name == "external"]
                    [0]),
            )

        async def test_applies_scheduling_key_to_jobs(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            job_interface = interfaces.job_interface(self.api_client)
            jobs = await job_interface.list_(NAMESPACE)

            for job in jobs:
                self.assertEqual(
                    job.spec.template.spec.affinity.node_affinity.
                    required_during_scheduling_ignored_during_execution.
                    to_dict(),
                    {
                        "node_selector_terms": [
                            {
                                "match_expressions": [
                                    {
                                        "key":
                                            scheduling_keys.SchedulingKey.
                                            OPERATOR_NEUTRON.value,
                                        "operator": "Exists",
                                        "values": None,
                                    },
                                ],
                                "match_fields": None,
                            },
                            {
                                "match_expressions": [
                                    {
                                        "key":
                                            scheduling_keys.SchedulingKey.
                                            OPERATOR_ANY.value,
                                        "operator": "Exists",
                                        "values": None,
                                    },
                                ],
                                "match_fields": None,
                            },
                            {
                                "match_expressions": [
                                    {
                                        "key":
                                            scheduling_keys.SchedulingKey.
                                            NEUTRON_ANY_SERVICE.value,
                                        "operator": "Exists",
                                        "values": None,
                                    },
                                ],
                                "match_fields": None,
                            },
                        ],
                    },
                )

                self.assertCountEqual(
                    job.spec.template.spec.to_dict()["tolerations"],
                    [
                        {
                            "key": scheduling_keys.SchedulingKey.
                            OPERATOR_NEUTRON.value,
                            "operator": "Exists",
                            "effect": None,
                            "toleration_seconds": None,
                            "value": None,
                        },
                        {
                            "key": scheduling_keys.SchedulingKey.
                            OPERATOR_ANY.value,
                            "operator": "Exists",
                            "effect": None,
                            "toleration_seconds": None,
                            "value": None,
                        },
                        {
                            "key": scheduling_keys.SchedulingKey.
                            NEUTRON_ANY_SERVICE.value,
                            "operator": "Exists",
                            "effect": None,
                            "toleration_seconds": None,
                            "value": None,
                        },
                    ],
                )

        @ddt.data({}, NEUTRON_POLICY)
        async def test_creates_config_with_policy_file(self, policy):
            neutron_deployment_yaml = \
                self._get_neutron_deployment_yaml()
            neutron_deployment_yaml["spec"].update(policy)
            self._configure_cr(neutron.Neutron, neutron_deployment_yaml)

            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            expected_policy_file = os.path.join(CONFIG_PATH, POLICY_FILE_NAME)

            secrets = interfaces.secret_interface(self.api_client)
            config_secret, = await secrets.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "config"},
            )
            neutron_conf_content = testutils._parse_config(
                config_secret.data[CONFIG_FILE_NAME],
                decode=True
            )

            observed_policy_file = \
                neutron_conf_content.get("oslo_policy", "policy_file")

            self.assertEqual(observed_policy_file, expected_policy_file)

        @ddt.data({}, NEUTRON_POLICY)
        async def test_creates_policy_configmap(self, policy):
            neutron_deployment_yaml = \
                self._get_neutron_deployment_yaml()
            neutron_deployment_yaml["spec"].update(policy)
            self._configure_cr(neutron.Neutron, neutron_deployment_yaml)

            self._make_all_jobs_succeed_immediately()
            await self.cr.sm.ensure(self.ctx)

            config_maps = interfaces.config_map_interface(self.api_client)
            neutron_policy, = await config_maps.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "policy"},
            )

            self.assertEqual(neutron_policy.kind, "ConfigMap")
            self.assertTrue(
                neutron_policy.metadata.name.startswith("neutron-policy"))

            self.assertEqual(neutron_policy.data, {})
            self.assertEqual(
                neutron_policy.metadata.annotations[
                    'state.yaook.cloud/policies'
                ],
                str(policy.get("policy", {}))
            )

        async def test_injects_secret(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)

            secrets = interfaces.secret_interface(self.api_client)
            secret, = await secrets.list_(
                NAMESPACE,
                label_selector={
                    context.LABEL_COMPONENT: "config"},
            )
            decoded = sm.api_utils.decode_secret_data(secret.data)
            lines = decoded["neutron.conf"].splitlines()

            self.assertIn(f"mytestsecret = {CONFIG_SECRET_VALUE}", lines)

        async def test_creates_containers_with_resources(self):
            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)
            deployments = interfaces.deployment_interface(self.api_client)
            jobs = interfaces.job_interface(self.api_client)

            api_deployment, = await deployments.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "api_deployment"}
            )
            db_sync_job, = await jobs.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "db_sync"}
            )

            self.assertEqual(
                testutils.container_resources(api_deployment, 0),
                testutils.unique_resources("api.neutron-api")
            )
            self.assertEqual(
                testutils.container_resources(api_deployment, 1),
                testutils.unique_resources("api.ssl-terminator")
            )
            self.assertEqual(
                testutils.container_resources(api_deployment, 2),
                testutils.unique_resources("api.ssl-terminator-external")
            )
            self.assertEqual(
                testutils.container_resources(api_deployment, 3),
                testutils.unique_resources("api.service-reload")
            )
            self.assertEqual(
                testutils.container_resources(api_deployment, 4),
                testutils.unique_resources("api.service-reload-external")
            )
            self.assertEqual(
                testutils.container_resources(db_sync_job, 0),
                testutils.unique_resources("job.neutron-db-sync-job")
            )

        async def test_messagequeue_container_resources(self):
            # Assumes that the CRD includes the crd.#messagequeue definition.
            crd_spec = self.ctx.parent_spec
            expected_resources = crd_spec["messageQueue"]["resources"]

            self._make_all_dependencies_complete_immediately()
            await self.cr.sm.ensure(self.ctx)
            msgqueue_interface = interfaces.amqpserver_interface(
                self.api_client)
            msgqueue_server, = await msgqueue_interface.list_(
                self.cr_namespace,
            )
            self.assertEqual(
                msgqueue_server["spec"]["resources"],
                expected_resources)


@ddt.ddt
class TestNeutronOVSDeployment(
        TestNeutronDeploymentCases.TestNeutronBaseDeployment):

    async def asyncSetUp(self):
        await super().asyncSetUp()
        self.labels_l2 = {
            scheduling_keys.SchedulingKey.COMPUTE_HYPERVISOR.value:
                str(uuid.uuid4()),
        }
        self.labels_l3 = {
            scheduling_keys.SchedulingKey.NETWORK_NEUTRON_L3_AGENT.value:
                str(uuid.uuid4()),
            context.LABEL_L2_REQUIRE_MIGRATION: "False",
        }
        self.labels_dhcp = {
            scheduling_keys.SchedulingKey.NETWORK_NEUTRON_DHCP_AGENT.value:
                str(uuid.uuid4()),
            context.LABEL_L2_REQUIRE_MIGRATION: "False",
        }
        self.labels_bgp = {
            scheduling_keys.SchedulingKey.NETWORK_NEUTRON_BGP_DRAGENT.value:
                str(uuid.uuid4()),
            context.LABEL_L2_REQUIRE_MIGRATION: "False",
        }
        self.labels_bgp_require_migration = {
            scheduling_keys.SchedulingKey.NETWORK_NEUTRON_BGP_DRAGENT.value:
                str(uuid.uuid4()),
        }

        self.default_node_setup = {
            "node1": {},
            "node2": self.labels_l2,
            "node3": self.labels_l3,
            "node4": self.labels_dhcp,
            "node5": self.labels_bgp,
        }
        self.l2_nodes = ["node2", "node3", "node4", "node5"]
        self.l3_nodes = ["node3"]
        self.dhcp_nodes = ["node4"]
        self.bgp_nodes = ["node5"]

        self._configure_cr(
            neutron.Neutron,
            self._get_neutron_deployment_yaml(),
        )

    def _get_neutron_deployment_yaml(self):
        deployment = super()._get_neutron_deployment_yaml()
        deployment["spec"]["setup"] = {
            "ovs": {
                "l2": {
                    "configTemplates": [
                        {
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                            "neutronConfig": {},
                        },
                        {
                            "nodeSelectors": [
                                {"matchLabels": self.labels_l3},
                            ],
                            "neutronOpenvSwitchAgentConfig": {
                                "ovs": {
                                    "bridge_mappings": ["physnet1:br-ex"],
                                },
                            },
                            "bridgeConfig": [
                                {
                                    "bridgeName": "br-ex",
                                    "uplinkDevice": "eth1",
                                },
                            ],
                            "overlayNetworkConfig": {
                                "ovs_local_ip_subnet": "10.11.13.0/24",
                            },
                        },
                    ],
                    "resources": testutils.generate_resources_dict(
                        "setup.ovs.l2.neutron-ovs-bridge-setup",
                        "setup.ovs.l2.neutron-openvswitch-agent",
                        "setup.ovs.l2.ovs-vswitchd",
                        "setup.ovs.l2.ovsdb-server",
                    ),
                },
                "l3": {
                    "startupLimitMinutes": 1,
                    "configTemplates": [
                        {
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                            "neutronConfig": {},
                            "neutronL3AgentConfig": {
                                "DEFAULT": {
                                    "use_stderr": True,
                                },
                            },
                        },
                    ],
                    "evictor": {
                        "allowFallback": True,
                        "pollInterval": 5,
                        "maxParallelMigrations": 10,
                        "respectAvailabilityZones": False,
                        "verifySeconds": 0,
                    },
                    "resources": testutils.generate_resources_dict(
                        "setup.ovs.l3.neutron-l3-agent",
                        "setup.ovs.l3.neutron-metadata-agent",
                        "setup.ovs.l3.l3-evict-job",
                    ),
                },
                "dhcp": {
                    "configTemplates": [
                        {
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                            "neutronConfig": {},
                            "neutronDHCPAgentConfig": {
                                "DEFAULT": {
                                    "use_stderr": True,
                                },
                            },
                        },
                    ],
                    "evictor": {
                        "pollInterval": 5,
                        "maxParallelMigrations": 5,
                    },
                    "resources": testutils.generate_resources_dict(
                        "setup.ovs.dhcp.neutron-dhcp-agent",
                        "setup.ovs.dhcp.neutron-metadata-agent",
                        "setup.ovs.dhcp.dhcp-evict-job",
                    ),
                },
                "bgp": {
                    "bgp-key": {
                        "configTemplates": [
                            {
                                "nodeSelectors": [
                                    {"matchLabels": {}},  # all nodes
                                ],
                                "neutronConfig": {},
                                "neutronBGPDragentConfig": {
                                    "DEFAULT": {
                                        "use_stderr": True,
                                    },
                                },
                                "bgpInterfaceMapping": {
                                    "bridgeName": "br-ex"
                                },
                            },
                        ],
                        "resources": testutils.generate_resources_dict(
                            "setup.ovs.bgp.bgp-key.neutron-bgp-interface-setup",  # noqa E501
                            "setup.ovs.bgp.bgp-key.neutron-bgp-dragent",
                            "setup.ovs.bgp.bgp-key.bgp-evict-job",
                        ),
                    }
                },
            }
        }
        return deployment

    async def test_creates_certificate_and_halts(self):
        await self.cr.sm.ensure(self.ctx)
        self.api_client.client_side_validation = False
        cert_interface = interfaces.certificates_interface(self.api_client)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(
            cert["metadata"]["labels"][context.LABEL_COMPONENT],
            "certificate"
        )

        await self.cr.sm.ensure(self.ctx)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 1)
        cert, = all_certs

        self.assertEqual(
            cert["metadata"]["labels"][context.LABEL_COMPONENT],
            "certificate"
        )

    async def test_creates_api_db_sync_job_and_halts(self):
        self._make_all_mqs_succeed_immediately()
        self._make_all_databases_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        self._make_all_ovsdb_ready_immediately()
        self._make_all_issuers_ready_immediately()
        self._make_all_memcached_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 2)
        _, db_sync, = all_jobs

        self.assertEqual(
            db_sync.metadata.labels[context.LABEL_COMPONENT], "db_sync"
        )

        await self.cr.sm.ensure(self.ctx)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 2)
        _, db_sync, = all_jobs

        self.assertEqual(
            db_sync.metadata.labels[context.LABEL_COMPONENT], "db_sync"
        )

        deployments = interfaces.deployment_interface(self.api_client)
        self.assertEqual(len(await deployments.list_(NAMESPACE)), 0)

    async def test__get_target_nodes_returns_multiple_nodes(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes({
            "node2": self.labels_l3,
            "node3": self.labels_l3,
        })
        await self.cr.sm.ensure(self.ctx)

        result = await self.cr.l3_agents.wrapped_state._get_target_nodes(
            self.ctx
        )

        node_list = self.l3_nodes.copy()
        node_list.append('node2')
        self.assertCountEqual([i.metadata.name for i in result], node_list)

    async def test_creates_l2_agents(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        l2_agents = interfaces.neutron_l2_agent_interface(self.api_client)

        l2agents = await l2_agents.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "l2_agents"},
        )
        l2_agents_map = {
            agent["metadata"]["name"]: agent
            for agent in l2agents
        }
        self.assertCountEqual(
            l2_agents_map.keys(),
            self.l2_nodes,
        )

        for agent_name, agent_body in l2_agents_map.items():
            self.assertEqual(
                agent_body["spec"]["keystoneRef"]["kind"],
                "KeystoneDeployment",
            )

    async def test_configures_l2_agent_individually(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        l2_interface = interfaces.neutron_l2_agent_interface(self.api_client)
        l2_agents = await l2_interface.list_(
            NAMESPACE,
        )
        agent_configs = {
            agent['metadata']['name']: (
                agent['spec']['neutronConfig'],
                agent['spec']['neutronOpenvSwitchAgentConfig'],
                agent['spec']['bridgeConfig'],
                agent['spec']['overlayNetworkConfig'],
            )
            for agent in l2_agents
        }

        self.assertEqual(agent_configs[self.l3_nodes[0]][0], [{}])
        self.assertEqual(
            agent_configs[self.l3_nodes[0]][1][0].get("ovs").
            get("bridge_mappings"),
            ["physnet1:br-ex"],
        )
        self.assertEqual(
            agent_configs[self.l3_nodes[0]][2][0],
            {'bridgeName': 'br-ex', 'uplinkDevice': 'eth1'},
        )
        self.assertEqual(
            agent_configs[self.l3_nodes[0]][3],
            {'ovs_local_ip_subnet': '10.11.13.0/24'},
        )

        self.assertEqual(agent_configs[self.dhcp_nodes[0]][0], [{}])
        self.assertEqual(agent_configs[self.dhcp_nodes[0]][1], [])
        self.assertEqual(agent_configs[self.dhcp_nodes[0]][2], [])

    async def test_creates_l3_agents(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        l3_agents = interfaces.neutron_l3_agent_interface(self.api_client)

        l3agents = await l3_agents.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "l3_agents"},
        )
        l3_agents_map = {
            agent["metadata"]["name"]: agent
            for agent in l3agents
        }
        self.assertCountEqual(
            l3_agents_map.keys(),
            self.l3_nodes,
        )

        for agent_name, agent_body in l3_agents_map.items():
            self.assertEqual(
                agent_body["spec"]["keystoneRef"]["kind"],
                "KeystoneDeployment",
            )

    async def test_l3_agents_gets_startupLimitMinutes(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        l3_agents = interfaces.neutron_l3_agent_interface(self.api_client)

        l3agents = await l3_agents.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "l3_agents"},
        )
        l3_agents_map = {
            agent["metadata"]["name"]: agent
            for agent in l3agents
        }
        for agent_name, agent_body in l3_agents_map.items():
            self.assertEqual(
                agent_body["spec"]["startupLimitMinutes"],
                self.ctx.parent_spec["setup"]["ovs"]
                ["l3"]["startupLimitMinutes"],
            )

    async def test_creates_dhcp_agents(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        dhcp_agents = interfaces.neutron_dhcp_agent_interface(self.api_client)

        dhgcpagents = await dhcp_agents.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "dhcp_agents"},
        )
        dhcp_agents_map = {
            agent["metadata"]["name"]: agent
            for agent in dhgcpagents
        }
        self.assertCountEqual(
            dhcp_agents_map.keys(),
            self.dhcp_nodes,
        )

    async def test_creates_bgp_dragents(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        bgp_dragents = \
            interfaces.neutron_bgp_dragent_interface(self.api_client)

        bgpdragents = await bgp_dragents.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "bgp_dragents"},
        )
        bgp_dragents_map = {
            agent["spec"]["hostname"]: agent
            for agent in bgpdragents
        }
        self.assertCountEqual(
            bgp_dragents_map.keys(),
            self.bgp_nodes,
        )

        for agent_name, agent_body in bgp_dragents_map.items():
            self.assertEqual(
                agent_body["spec"]["keystoneRef"]["kind"],
                "KeystoneDeployment",
            )

    async def test_creates_multiple_bgp_dragents_per_node(self):
        bgp_dragents_config = {
            "bgp": {
                "bgp1": {
                    "configTemplates": [
                        {
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                            "neutronConfig": {},
                            "neutronBGPDragentConfig": {
                                "DEFAULT": {
                                    "use_stderr": True,
                                },
                            },
                            "bgpInterfaceMapping": {
                                "bridgeName": "br-ex"
                            },
                        },
                    ],
                },
                "bgp2": {
                    "configTemplates": [
                        {
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                            "neutronConfig": {},
                            "neutronBGPDragentConfig": {
                                "DEFAULT": {
                                    "use_stderr": True,
                                },
                            },
                            "bgpInterfaceMapping": {
                                "bridgeName": "br-ex"
                            },
                        },
                    ],
                }
            },
        }
        neutron_deployment_yaml = self._get_neutron_deployment_yaml()
        neutron_deployment_yaml["spec"]["setup"]["ovs"].update(
            bgp_dragents_config)
        self._configure_cr(neutron.Neutron, neutron_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        bgp_dragents = \
            interfaces.neutron_bgp_dragent_interface(self.api_client)

        bgpdragents = await bgp_dragents.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "bgp_dragents"},
        )
        bgp_dragents_map = {
            agent["metadata"]["name"]: agent
            for agent in bgpdragents
        }
        accessors = await \
            neutron.neutron_resources._bgp_spec_configkeys(self.ctx,
                                                           setupkey='ovs')
        instances = [accessor+'.'+self.bgp_nodes[0] for accessor in accessors]

        self.assertEqual(
            2,
            len(instances),
        )
        self.assertCountEqual(
            bgp_dragents_map.keys(),
            instances,
        )

        for agent_name, agent_body in bgp_dragents_map.items():
            self.assertEqual(
                agent_body["spec"]["keystoneRef"]["kind"],
                "KeystoneDeployment",
            )

    async def test_does_not_create_bgp_dragents_if_maintenance_required(self):
        bgp_dragents_config = {
            "bgp": {
                "bgp1": {
                    "configTemplates": [
                        {
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                            "neutronConfig": {},
                            "neutronBGPDragentConfig": {
                                "DEFAULT": {
                                    "use_stderr": True,
                                },
                            },
                            "bgpInterfaceMapping": {
                                "bridgeName": "br-ex"
                            },
                        },
                    ],
                },
                "bgp2": {
                    "configTemplates": [
                        {
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                            "neutronConfig": {},
                            "neutronBGPDragentConfig": {
                                "DEFAULT": {
                                    "use_stderr": True,
                                },
                            },
                            "bgpInterfaceMapping": {
                                "bridgeName": "br-ex"
                            },
                        },
                    ],
                }
            },
        }
        neutron_deployment_yaml = self._get_neutron_deployment_yaml()
        neutron_deployment_yaml["spec"]["setup"]["ovs"].update(
            bgp_dragents_config
        )
        self._configure_cr(neutron.Neutron, neutron_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes({
            "node1": {},
            "node2": self.labels_l2,
            "node3": self.labels_l3,
            "node4": self.labels_bgp_require_migration,
            "node5": self.labels_bgp,
        })
        await self.cr.sm.ensure(self.ctx)

        bgp_dragents = \
            interfaces.neutron_bgp_dragent_interface(self.api_client)

        bgpdragents = await bgp_dragents.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "bgp_dragents"},
        )
        bgp_dragents_names = [
            agent["metadata"]["name"]
            for agent in bgpdragents
        ]
        self.assertCountEqual(
            [
                "bgp1.node5",
                "bgp2.node5",
            ],
            bgp_dragents_names,
        )

    async def test_creates_agents_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)

        bgp_if = interfaces.neutron_bgp_dragent_interface(self.api_client)
        dhcp_if = interfaces.neutron_dhcp_agent_interface(self.api_client)
        l2_if = interfaces.neutron_l2_agent_interface(self.api_client)
        l3_if = interfaces.neutron_l3_agent_interface(self.api_client)

        bgp, = await bgp_if.list_(NAMESPACE)
        dhcp, = await dhcp_if.list_(NAMESPACE)
        l2_list = await l2_if.list_(NAMESPACE)
        l3, = await l3_if.list_(NAMESPACE)

        crd_spec = self.ctx.parent_spec
        self.assertEqual(
            bgp["spec"]["resources"],
            crd_spec["setup"]["ovs"]["bgp"]["bgp-key"]["resources"],
        )
        self.assertEqual(
            dhcp["spec"]["resources"],
            crd_spec["setup"]["ovs"]["dhcp"]["resources"],
        )
        for l2 in l2_list:
            self.assertEqual(
                l2["spec"]["resources"],
                crd_spec["setup"]["ovs"]["l2"]["resources"],
            )
        self.assertEqual(
            l3["spec"]["resources"],
            crd_spec["setup"]["ovs"]["l3"]["resources"],
        )

    async def test_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_service"
            })
        deployment, = await deployments.list_(NAMESPACE)

        pod_labels = deployment.spec.template.metadata.labels
        service_labels = service.spec.selector

        self.assertTrue(
            sm.matches_labels(pod_labels, service_labels),
            f"pods: {pprint.pformat(pod_labels)}\n"
            f"service: {pprint.pformat(service_labels)}\n",
        )

    # OVS has one less certificate mounted then ovn
    @ddt.data({}, NEUTRON_POLICY)
    async def test_creates_api_deployment_with_projected_volume(
            self,
            policy):
        neutron_deployment_yaml = \
            self._get_neutron_deployment_yaml()
        neutron_deployment_yaml["spec"].update(policy)
        self._configure_cr(neutron.Neutron, neutron_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        policy_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "policy"},
        )

        self.assertEqual(len(api_deployment.spec.template.spec.volumes), 7)
        observed_projected_volume = \
            api_deployment.spec.template.spec.volumes[0]
        observed_projected_volume_mount = \
            api_deployment.spec.template.spec.containers[0]\
            .volume_mounts[0]
        self.assertEqual(
            observed_projected_volume_mount.name,
            observed_projected_volume.name,
        )
        self.assertEqual(
            observed_projected_volume_mount.mount_path,
            CONFIG_PATH,
        )

        observed_projected_volume_sources = \
            observed_projected_volume.projected.sources
        self.assertEqual(len(observed_projected_volume_sources), 2)

        conf_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.secret
        )
        self.assertEqual(
            conf_vol_source.secret.name,
            config_secret.metadata.name,
        )
        self.assertEqual(len(conf_vol_source.secret.items), 1)
        self.assertEqual(
            conf_vol_source.secret.items[0].key,
            CONFIG_FILE_NAME,
        )
        self.assertEqual(
            conf_vol_source.secret.items[0].path,
            CONFIG_FILE_NAME,
        )

        policy_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.config_map
        )
        self.assertEqual(
            policy_vol_source.config_map.name,
            policy_config_map.metadata.name,
        )
        self.assertEqual(len(policy_vol_source.config_map.items), 1)
        self.assertEqual(
            policy_vol_source.config_map.items[0].key,
            POLICY_FILE_NAME,
        )
        self.assertEqual(
            policy_vol_source.config_map.items[0].path,
            POLICY_FILE_NAME,
        )


NODE_NAME = "node2"
CONFIG_KEY = "testbgp"
LOCAL_AS = 1111
PEER_AS = 1234
PEER_IP = "2.2.2.2"


@ddt.ddt
class TestNeutronOVNDeployment(
        TestNeutronDeploymentCases.TestNeutronBaseDeployment):
    run_service_reload_test = False  # otherwise we cry for northd

    async def asyncSetUp(self):
        await super().asyncSetUp()

        self.labels_gtw = {
            scheduling_keys.SchedulingKey.NETWORK_NEUTRON_NETWORK_NODE.value:
                str(uuid.uuid4()),
            context.LABEL_L2_REQUIRE_MIGRATION: 'False',
            context.LABEL_NAMESPACE: NAMESPACE
        }
        self.labels_cmp = {
            scheduling_keys.SchedulingKey.COMPUTE_HYPERVISOR.value:
                str(uuid.uuid4()),
            context.LABEL_NAMESPACE: NAMESPACE,
        }
        self.default_node_setup = {
            "node1": {},
            "node2": self.labels_gtw,
            "node3": self.labels_cmp,
        }
        self.ovn_nodes = ["node2", "node3"]
        self.gtw_nodes = ["node2"]
        self.cmp_nodes = ["node3"]
        self.bgp_nodes = ["node2", "node3"]

        self._configure_cr(
            neutron.Neutron,
            self._get_neutron_deployment_yaml(),
        )

    def _get_neutron_deployment_yaml(self):
        deployment = super()._get_neutron_deployment_yaml()

        deployment["spec"]["serviceMonitor"] = {
            "additionalLabels": {
                "mykey": "mylabel",
            },
        }

        deployment["spec"]["setup"] = {
            "ovn": {
                "controller": {
                    "configTemplates": [
                        {
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                            "neutronConfig": {},
                        },
                        {
                            "nodeSelectors": [
                                {"matchLabels": self.labels_gtw},
                            ],
                            "bridgeConfig": [
                                {
                                    "bridgeName": "br-ex",
                                    "uplinkDevice": "eth1",
                                    "openstackPhysicalNetwork": "physnet1",
                                    },
                            ],
                        },
                    ],
                    "scrapeIntervalMs": 50,
                    "monitoringDsUpdateStrategy": {
                        "type": "RollingUpdate",
                        "rollingUpdate": {
                            "maxUnavailable": "100%",
                            "maxSurge": 5,
                        },
                    },
                    "resources": testutils.generate_resources_dict(
                        "setup.ovn.controller.ovsdb-server",
                        "setup.ovn.controller.ovs-vswitchd",
                        "setup.ovn.controller.ovs-vswitchd-monitoring",
                        "setup.ovn.controller.ssl-terminator",
                        "setup.ovn.controller.service-reload",
                    ),
                },
                "northboundOVSDB": {
                    "replicas": 3,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "backup": {
                        "schedule": "0 * * * *",
                    },
                    "inactivityProbeMs": 42000,
                    "scrapeIntervalMs": 60,
                    "storageSize": "6Gi",
                    "storageClassName": "baz-class",
                    "resources": testutils.generate_resources_dict(
                        "setup.ovn.northboundOVSDB.ovsdb",
                        "setup.ovn.northboundOVSDB.setup-ovsdb",
                    ),
                },
                "southboundOVSDB": {
                    "replicas": 3,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "backup": {
                        "schedule": "0 * * * *",
                    },
                    "inactivityProbeMs": 43000,
                    "scrapeIntervalMs": 120,
                    "ovnRelay": {
                        "resources": testutils.generate_resources_dict(
                            "setup.ovn.southboundOVSDB.ovnRelay.ovn-relay",
                            "setup.ovn.southboundOVSDB.ovnRelay."
                            "ssl-terminator",
                            "setup.ovn.southboundOVSDB.ovnRelay."
                            "service-reload",
                        ),
                    },
                    "storageSize": "6Gi",
                    "storageClassName": "baz-class",
                    "resources": testutils.generate_resources_dict(
                        "setup.ovn.southboundOVSDB.ovsdb",
                        "setup.ovn.southboundOVSDB.setup-ovsdb",
                    ),
                },
                "northd": {
                    "replicas": 4,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "resources": testutils.generate_resources_dict(
                        "setup.ovn.northd",
                    ),
                },
                "bgp": {
                    "testbgp": {
                        "configTemplates": [
                            {
                                "config": {
                                    "addressScopes": ['ID-foo', 'ID-bar'],
                                    "bridgeName": "br-test",
                                    "debug": False,
                                    "driver": "ovn_stretched_l2_bgp_driver",
                                    "bridgeName": "br-test",
                                    "localAS": LOCAL_AS,
                                    "peers": {
                                        "switchA": {
                                            "IP": PEER_IP,
                                            "AS": PEER_AS,
                                        },
                                    },
                                    "syncInterval": 64,
                                },
                                "nodeSelectors": [
                                    {"matchLabels": {}},  # all nodes
                                ],
                            }
                        ]
                    }
                },
            }
        }

        return deployment

    async def test_creates_certificates_and_halts(self):
        await self.cr.sm.ensure(self.ctx)
        self.api_client.client_side_validation = False
        cert_interface = interfaces.certificates_interface(self.api_client)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 3)
        cert, ovn_central_ca_cert, ovn_monitoring_cert, = all_certs

        self.assertEqual(
            cert["metadata"]["labels"][context.LABEL_COMPONENT],
            "certificate"
        )
        self.assertEqual(ovn_central_ca_cert["metadata"]["labels"]
                         [context.LABEL_COMPONENT],
                         "ovn_central_ca_certificate")

        await self.cr.sm.ensure(self.ctx)

        all_certs = await cert_interface.list_(NAMESPACE)
        self.assertEqual(len(all_certs), 3)
        cert, ovn_central_ca_cert, ovn_monitoring_cert, = all_certs

        self.assertEqual(
            cert["metadata"]["labels"][context.LABEL_COMPONENT],
            "certificate"
        )
        self.assertEqual(ovn_central_ca_cert["metadata"]["labels"]
                         [context.LABEL_COMPONENT],
                         "ovn_central_ca_certificate")
        self.assertEqual(
            ovn_monitoring_cert["metadata"]["labels"][context.LABEL_COMPONENT],
            "ovn_monitoring_certificate",
        )

    async def test_service_matches_deployment_pods(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        service, = await services.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "api_service"
            })
        northd, deployment, = await deployments.list_(NAMESPACE)

        pod_labels = deployment.spec.template.metadata.labels
        service_labels = service.spec.selector

        self.assertTrue(
            sm.matches_labels(pod_labels, service_labels),
            f"pods: {pprint.pformat(pod_labels)}\n"
            f"service: {pprint.pformat(service_labels)}\n",
        )

    async def test_creates_ovn_central_ca_certificate(self):
        self._make_all_deployments_ready_immediately()

        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "ovn_central_ca_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "ovn_central_ca_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            "selfsigned-issuer",
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )

    async def test_creates_ovn_central_ca_issuer(self):
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        issuers = sm.issuer_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        issuer, = await issuers.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "ovn_central_ca",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "ovn_central_ca_certificate_secret",
            }
        )

        self.assertEqual(
            issuer["spec"]["ca"]["secretName"],
            cert_secret.metadata.name,
        )

    async def test_creates_api_db_sync_job_and_halts(self):
        self._make_all_mqs_succeed_immediately()
        self._make_all_databases_ready_immediately()
        self._make_all_keystoneusers_complete_immediately()
        self._make_all_certificates_succeed_immediately()
        self._make_all_ovsdb_ready_immediately()
        self._make_all_issuers_ready_immediately()
        self._make_all_memcached_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        job_interface = interfaces.job_interface(self.api_client)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 2)
        _, db_sync, = all_jobs

        self.assertEqual(
            db_sync.metadata.labels[context.LABEL_COMPONENT], "db_sync"
        )

        await self.cr.sm.ensure(self.ctx)

        all_jobs = await job_interface.list_(NAMESPACE)
        self.assertEqual(len(all_jobs), 2)
        _, db_sync, = all_jobs

        self.assertEqual(
            db_sync.metadata.labels[context.LABEL_COMPONENT], "db_sync"
        )

        deployments = interfaces.deployment_interface(self.api_client)
        # only northd deployment should exists
        deployment_list = await deployments.list_(NAMESPACE)
        self.assertEqual(len(deployment_list), 1)
        self.assertEqual(deployment_list[0].metadata.name, 'ovn-northd')

    async def test_creates_northd_certificate_with_own_ca(self):
        self._make_all_issuers_ready_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        issuers = sm.issuer_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        issuer, = await issuers.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "ovn_central_ca",
            }
        )
        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "northd_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "northd_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            issuer["metadata"]["name"],
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )
        self.assertFalse(cert["spec"].get("isCA", False))

    async def test_creates_ml2_certificate_with_own_ca(self):
        self._make_all_issuers_ready_immediately()
        self._make_all_certificates_succeed_immediately()
        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        issuers = sm.issuer_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        issuer, = await issuers.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "ovn_central_ca",
            }
        )
        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "ml2_plugin_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "ml2_plugin_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            issuer["metadata"]["name"],
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )
        self.assertFalse(cert["spec"].get("isCA", False))

    async def test_creates_ovsdb_service_with_resources(self):
        crd_spec = self.ctx.parent_spec
        ovn_spec = crd_spec["setup"]["ovn"]
        expected_nb_resources = ovn_spec["northboundOVSDB"]["resources"]
        expected_sb_resources = ovn_spec["southboundOVSDB"]["resources"]
        expected_relay_resources = \
            ovn_spec["southboundOVSDB"]["ovnRelay"]["resources"]
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        ovsdb_interface = interfaces.ovsdbservice_interface(self.api_client)

        nb, = await ovsdb_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovsdb_nb"
                }
        )
        sb, = await ovsdb_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovsdb_sb"
                }
        )

        self.assertEqual(
            nb["spec"]["resources"],
            expected_nb_resources
        )
        self.assertEqual(
            sb["spec"]["resources"],
            expected_sb_resources
        )
        self.assertEqual(
            sb["spec"]["ovnRelay"]["resources"],
            expected_relay_resources
        )

    async def test_creates_northd_containers_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(self.api_client)

        northd, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "northd"}
        )

        self.assertEqual(
            testutils.container_resources(northd, 0),
            testutils.unique_resources("setup.ovn.northd")
        )

    async def test_creates_northd_deployment_with_replica_spec(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        deployments = interfaces.deployment_interface(self.api_client)

        northd, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "northd"},
        )

        self.assertEqual(northd.spec.replicas, 4)

    async def test_holds_back_northd_deployment_with_outdated_agents(self):

        URL = 'registry.gitlab.com/yaook/images/ovn/ovn'

        async def _assert_northd_ovn_image(tag):
            deployments = interfaces.deployment_interface(self.api_client)
            northd_list = await deployments.list_(
                NAMESPACE,
                label_selector={context.LABEL_COMPONENT: "northd"},
            )

            self.assertEqual(len(northd_list), 1)

            image, = [container.image for container in
                      northd_list[0].spec.template.spec.containers
                      if container.name == 'northd']

            self.assertEqual(image, f'{URL}:{tag}')

        async def _patch_ovn_agent_version(version):
            agents = interfaces.neutron_ovn_agent_interface(self.api_client)
            for agent in await agents.list_(NAMESPACE):
                agent.setdefault("status", {})["ovnVersion"] = version
                await agents.patch(NAMESPACE,
                                   agent["metadata"]["name"],
                                   agent)

        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        self._parse_version_patcher.stop()

        ovn_url_mock = unittest.mock.AsyncMock(
            name='get_versioned_url',
            return_value=f'{URL}:v0.00.1-0.0.1')
        self.cr.ovn_image.get_versioned_url = ovn_url_mock

        # northd should deploy successfully before the ovn agents are created
        await self.cr.sm.ensure(self.ctx)
        await _assert_northd_ovn_image('v0.00.1-0.0.1')

        # northd should not update because the ovn agents are missing the
        # ovn version status
        ovn_url_mock.return_value = f'{URL}:v0.00.2-0.0.1'
        await self.cr.sm.ensure(self.ctx)
        await _assert_northd_ovn_image('v0.00.1-0.0.1')

        # northd should still not update because the ovn version of the agents
        # does not match the version in the new ovn image.
        await _patch_ovn_agent_version('0.00.1')
        await self.cr.sm.ensure(self.ctx)
        await _assert_northd_ovn_image('v0.00.1-0.0.1')

        # northd should now reconcile because the ovn agents are up-to-date
        await _patch_ovn_agent_version('0.00.2')
        await self.cr.sm.ensure(self.ctx)
        await _assert_northd_ovn_image('v0.00.2-0.0.1')

    # OVN has one more certificate mounted (for the ml2 plugin)
    @ddt.data({}, NEUTRON_POLICY)
    async def test_creates_api_deployment_with_projected_volume(
            self,
            policy):
        neutron_deployment_yaml = \
            self._get_neutron_deployment_yaml()
        neutron_deployment_yaml["spec"].update(policy)
        self._configure_cr(neutron.Neutron, neutron_deployment_yaml)
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        secrets = interfaces.secret_interface(self.api_client)
        config_maps = interfaces.config_map_interface(self.api_client)
        deployments = interfaces.deployment_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"},
        )
        config_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "config"},
        )
        policy_config_map, = await config_maps.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "policy"},
        )

        self.assertEqual(len(api_deployment.spec.template.spec.volumes), 8)
        observed_projected_volume = \
            api_deployment.spec.template.spec.volumes[0]
        observed_projected_volume_mount = \
            api_deployment.spec.template.spec.containers[0]\
            .volume_mounts[0]
        self.assertEqual(
            observed_projected_volume_mount.name,
            observed_projected_volume.name,
        )
        self.assertEqual(
            observed_projected_volume_mount.mount_path,
            CONFIG_PATH,
        )

        observed_projected_volume_sources = \
            observed_projected_volume.projected.sources
        self.assertEqual(len(observed_projected_volume_sources), 2)

        conf_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.secret
        )
        self.assertEqual(
            conf_vol_source.secret.name,
            config_secret.metadata.name,
        )
        self.assertEqual(len(conf_vol_source.secret.items), 1)
        self.assertEqual(
            conf_vol_source.secret.items[0].key,
            CONFIG_FILE_NAME,
        )
        self.assertEqual(
            conf_vol_source.secret.items[0].path,
            CONFIG_FILE_NAME,
        )

        policy_vol_source = next(
            vol_source
            for vol_source in observed_projected_volume_sources
            if vol_source.config_map
        )
        self.assertEqual(
            policy_vol_source.config_map.name,
            policy_config_map.metadata.name,
        )
        self.assertEqual(len(policy_vol_source.config_map.items), 1)
        self.assertEqual(
            policy_vol_source.config_map.items[0].key,
            POLICY_FILE_NAME,
        )
        self.assertEqual(
            policy_vol_source.config_map.items[0].path,
            POLICY_FILE_NAME,
        )

    async def test_creates_ovn_agents(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes(self.default_node_setup)
        await self.cr.sm.ensure(self.ctx)
        crd_spec = self.ctx.parent_spec
        ovn_spec = crd_spec["setup"]["ovn"]
        ovn_agents = interfaces.neutron_ovn_agent_interface(self.api_client)

        ovnagents = await ovn_agents.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_agents"},
        )
        ovn_agents_map = {
            agent["metadata"]["name"]: agent
            for agent in ovnagents
        }
        self.assertCountEqual(
            ovn_agents_map.keys(),
            self.ovn_nodes,
        )

        # not all resource limits set in ovn.controller.resources are
        # passed on to the ovn agents, some are used for the
        # monitoring daemonset
        self.assertDictEqual(
            ovnagents[0]["spec"]["resources"],
            {container: ovn_spec["controller"]["resources"][container]
             for container in ("ovsdb-server", "ovs-vswitchd")}
        )

        # test if the imageRef is set properly
        self.assertEqual(
            ovnagents[0]["spec"]["imageRef"]["ovn"],
            "registry.yaook.cloud/yaook/ovn:0.0.1"
        )
        self.assertEqual(
            ovnagents[0]["spec"]["imageRef"]["ovs"],
            'registry.yaook.cloud/yaook/openvswitch:0.0.1'

        )
        self.assertEqual(
            ovnagents[0]["spec"]["imageRef"]["neutron-ovn-agent"],
            'registry.yaook.cloud/yaook/neutron-ovn-agent-yoga:0.0.1'
        )

    async def test_creates_ovn_bgp_agents(self):
        self._make_all_dependencies_complete_immediately()
        self._mock_labelled_nodes({"node2": self.labels_gtw})
        self._mock_labelled_nodes({"node3": self.labels_gtw})
        await self.cr.sm.ensure(self.ctx)

        ovn_bgp_agents_int = \
            interfaces.neutron_ovn_bgp_agent_interface(self.api_client)

        ovn_bgp_agents = await ovn_bgp_agents_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_bgp_agents"},
        )
        ovn_bgp_agents_map = {
            agent["spec"]["hostname"]: agent
            for agent in ovn_bgp_agents
        }
        self.assertCountEqual(
            ovn_bgp_agents_map.keys(),
            self.bgp_nodes,
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["addressScopes"],
            ['ID-foo', 'ID-bar'],
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["bridgeName"],
            "br-test",
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["localAS"],
            LOCAL_AS,
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["peers"],
            {"switchA": {"AS": PEER_AS, "IP": PEER_IP}},
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["syncInterval"],
            64,
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["lockName"],
            "bgp-testbgp",
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["bgpNodeAnnotationSuffix"],
            CONFIG_KEY,
        )

    async def test_creates_multiple_ovn_bgp_agents_per_node(self):
        LOCAL_AS2 = 1142
        PEER_AS2 = 1143
        PEER_IP2 = "1.1.4.2"
        ovn_bgp_agents_spec = {
            "bgp": {
                "testbgp1": {
                    "configTemplates": [
                        {
                            "config": {
                                "bridgeName": "br-test",
                                "debug": False,
                                "driver": "ovn_stretched_l2_bgp_driver",
                                "localAS": LOCAL_AS,
                                "peers": {
                                    "switchA": {
                                        "AS": PEER_AS,
                                        "IP": PEER_IP,
                                    },
                                },
                                "syncInterval": 64,
                            },
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                        }
                    ]
                },
                "testbgp2": {
                    "configTemplates": [
                        {
                            "config": {
                                "bridgeName": "br-test2",
                                "debug": False,
                                "driver": "ovn_stretched_l2_bgp_driver",
                                "localAS": LOCAL_AS2,
                                "peers": {
                                    "switchA": {
                                        "AS": PEER_AS2,
                                        "IP": PEER_IP2,
                                    },
                                },
                                "syncInterval": 62,
                            },
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                        }
                    ]
                }
            }
        }
        self._mock_labelled_nodes({"node2": self.labels_gtw})

        neutron_deployment_yaml = self._get_neutron_deployment_yaml()
        neutron_deployment_yaml["spec"]["setup"]["ovn"].update(
            ovn_bgp_agents_spec)
        self._configure_cr(neutron.Neutron, neutron_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ovn_bgp_agents_int = \
            interfaces.neutron_ovn_bgp_agent_interface(self.api_client)

        ovn_bgp_agents = await ovn_bgp_agents_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_bgp_agents"},
        )
        ovn_bgp_agents_map = {
            agent["metadata"]["name"]: agent
            for agent in ovn_bgp_agents
        }
        accessors = await \
            neutron.neutron_resources._bgp_spec_configkeys(self.ctx,
                                                           setupkey='ovn')
        instances = [accessor+'.'+self.bgp_nodes[0] for accessor in accessors]

        self.assertEqual(
            2,
            len(instances),
        )
        self.assertCountEqual(
            ovn_bgp_agents_map.keys(),
            instances,
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["bridgeName"],
            "br-test",
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["localAS"],
            LOCAL_AS,
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["peers"],
            {"switchA": {"AS": PEER_AS, "IP": PEER_IP}},
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["syncInterval"],
            64,
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["lockName"],
            "bgp-testbgp1",
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["bgpNodeAnnotationSuffix"],
            "testbgp1",
        )
        self.assertEqual(
            ovn_bgp_agents[1]["spec"]["bridgeName"],
            "br-test2",
        )
        self.assertEqual(
            ovn_bgp_agents[1]["spec"]["localAS"],
            LOCAL_AS2,
        )
        self.assertEqual(
            ovn_bgp_agents[1]["spec"]["peers"],
            {"switchA": {"AS": PEER_AS2, "IP": PEER_IP2}},
        )
        self.assertEqual(
            ovn_bgp_agents[1]["spec"]["syncInterval"],
            62,
        )
        self.assertEqual(
            ovn_bgp_agents[1]["spec"]["lockName"],
            "bgp-testbgp2",
        )
        self.assertEqual(
            ovn_bgp_agents[1]["spec"]["bgpNodeAnnotationSuffix"],
            "testbgp2",
        )

    async def test_creates_multiple_ovn_bgp_agents_per_node_and_update_simultaneously_with_default_ydb(self):  # noqa: E501
        LOCAL_AS = 1111
        LOCAL_AS2 = 1142
        PEER_AS2 = 1143
        PEER_IP2 = "1.1.4.2"
        ovn_bgp_agents_spec = {
            "bgp": {
                "testbgp1": {
                    "configTemplates": [
                        {
                            "config": {
                                "bridgeName": "br-test",
                                "debug": False,
                                "driver": "ovn_stretched_l2_bgp_driver",
                                "localAS": LOCAL_AS,
                                "peers": {
                                    "switchA": {
                                        "AS": PEER_AS,
                                        "IP": PEER_IP,
                                    },
                                },
                                "syncInterval": 64,
                            },
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                        }
                    ]
                },
                "testbgp2": {
                    "configTemplates": [
                        {
                            "config": {
                                "bridgeName": "br-test2",
                                "debug": False,
                                "driver": "ovn_stretched_l2_bgp_driver",
                                "localAS": LOCAL_AS2,
                                "peers": {
                                    "switchA": {
                                        "AS": PEER_AS2,
                                        "IP": PEER_IP2,
                                    },
                                },
                                "syncInterval": 62,
                            },
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                        }
                    ]
                }
            }
        }
        self._mock_labelled_nodes({"node2": self.labels_gtw})
        self._mock_labelled_nodes({"node3": self.labels_gtw})

        neutron_deployment_yaml = self._get_neutron_deployment_yaml()
        neutron_deployment_yaml["spec"]["setup"]["ovn"].update(
            ovn_bgp_agents_spec)
        self._configure_cr(neutron.Neutron, neutron_deployment_yaml)
        self._make_all_dependencies_complete_immediately()

        # Initial ensure to create BGP agents on the node
        await self.cr.sm.ensure(self.ctx)

        ovn_bgp_agents_int = \
            interfaces.neutron_ovn_bgp_agent_interface(self.api_client)

        ovn_bgp_agents = await ovn_bgp_agents_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_bgp_agents"},
        )
        ovn_bgp_agents_map = {
            agent["metadata"]["name"]: agent
            for agent in ovn_bgp_agents
        }
        for bgp_agent in ovn_bgp_agents:
            self.logger.info("made ovn_bgp_agent %s ready immediately",
                             bgp_agent["metadata"])
            bgp_agent.setdefault("metadata", {})["generation"] = 1
            bgp_agent.setdefault("status", {})["observedGeneration"] = 1
            bgp_agent.setdefault("status", {})["updatedGeneration"] = 1
            bgp_agent["status"]["phase"] = "Updated"
            await ovn_bgp_agents_int.patch(NAMESPACE,
                                           bgp_agent["metadata"]["name"],
                                           bgp_agent)

        accessors = await \
            neutron.neutron_resources._bgp_spec_configkeys(self.ctx,
                                                           setupkey='ovn')
        instances = [accessor+'.'+node for accessor in accessors
                     for node in self.bgp_nodes]

        self.assertEqual(
            4,
            len(instances),
        )
        self.assertCountEqual(
            ovn_bgp_agents_map.keys(),
            instances,
        )
        self.assertEqual(
            ovn_bgp_agents[0]["spec"]["bridgeName"],
            "br-test",
        )

        ovn_bgp_agents_spec["bgp"]["testbgp1"]["configTemplates"][0]["config"]["bridgeName"] = "br-test-new"  # noqa: E501
        ovn_bgp_agents_spec["bgp"]["testbgp2"]["configTemplates"][0]["config"]["bridgeName"] = "br-test2-new"  # noqa: E501

        neutron_deployment_yaml = self._get_neutron_deployment_yaml()
        neutron_deployment_yaml["spec"]["setup"]["ovn"].update(
            ovn_bgp_agents_spec)
        self._configure_cr(neutron.Neutron, neutron_deployment_yaml)

        # This should delete(so they can be recreated) both the BGP agents at
        # the same time, as the default yaook disruption budget for BGP agents
        # is based on the configkeys, for each configkey maxUnavailable is 1
        await self.cr.sm.ensure(self.ctx)

        ovn_bgp_agents_int = \
            interfaces.neutron_ovn_bgp_agent_interface(self.api_client)

        ovn_bgp_agents = await ovn_bgp_agents_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_bgp_agents"},
        )
        ovn_bgp_agents_map = {
            agent["metadata"]["name"]: agent
            for agent in ovn_bgp_agents
        }
        instances = [accessor+'.'+"node3" for accessor in accessors]
        self.assertEqual(
            2,
            len(ovn_bgp_agents),
        )

        # Calling it for the 3rd time so that we can see the newly created BGP
        # agents with updated values
        await self.cr.sm.ensure(self.ctx)
        ovn_bgp_agents_int = \
            interfaces.neutron_ovn_bgp_agent_interface(self.api_client)

        ovn_bgp_agents = await ovn_bgp_agents_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_bgp_agents"},
        )
        self.assertEqual(
            4,
            len(ovn_bgp_agents),
        )
        self.assertCountEqual(
            ovn_bgp_agents_map.keys(),
            instances,
        )
        # Verify new bridge names
        for bgp_agent in ovn_bgp_agents:
            if bgp_agent["metadata"]["name"] == "testbgp1.node2":
                self.assertEqual(
                    bgp_agent["spec"]["bridgeName"],
                    "br-test-new",
                )
            if bgp_agent["metadata"]["name"] == "testbgp2.node2":
                self.assertEqual(
                    bgp_agent["spec"]["bridgeName"],
                    "br-test2-new",
                )

    async def test_does_not_create_ovn_bgp_agents_if_maintenance_required(
            self):
        ovn_bgp_agents_spec = {
            "bgp": {
                "testbgp1": {
                    "configTemplates": [
                        {
                            "config": {
                                "bridgeName": "br-test",
                                "debug": False,
                                "driver": "ovn_stretched_l2_bgp_driver",
                                "localAS": LOCAL_AS,
                                "peers": {
                                    "switchA": {
                                        "AS": PEER_AS,
                                        "IP": PEER_IP,
                                    },
                                },
                                "syncInterval": 64,
                            },
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                        }
                    ]
                },
                "testbgp2": {
                    "configTemplates": [
                        {
                            "config": {
                                "bridgeName": "br-test",
                                "debug": False,
                                "driver": "ovn_stretched_l2_bgp_driver",
                                "localAS": LOCAL_AS,
                                "peers": {
                                    "switchA": {
                                        "AS": PEER_AS,
                                        "IP": PEER_IP,
                                    },
                                },
                                "syncInterval": 64,
                            },
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                        }
                    ]
                }
            }
        }
        self._mock_labelled_nodes({"node2": self.labels_gtw})

        neutron_deployment_yaml = self._get_neutron_deployment_yaml()
        neutron_deployment_yaml["spec"]["setup"]["ovn"].update(
            ovn_bgp_agents_spec)
        self._configure_cr(neutron.Neutron, neutron_deployment_yaml)

        self._make_all_dependencies_complete_immediately()
        self.labels_bgp_require_migration = {
            scheduling_keys.SchedulingKey.NETWORK_NEUTRON_BGP_DRAGENT.value:
                str(uuid.uuid4()),
            context.LABEL_L2_REQUIRE_MIGRATION: "True",
        }
        self._mock_labelled_nodes({
            "node1": {},
            "node2": self.labels_bgp_require_migration,
            "node3": self.labels_gtw,
        })
        await self.cr.sm.ensure(self.ctx)
        ovn_bgp_agents_int = \
            interfaces.neutron_ovn_bgp_agent_interface(self.api_client)

        ovn_bgp_agents = await ovn_bgp_agents_int.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "ovn_bgp_agents"},
        )
        ovn_bgp_agents_map = {
            agent["metadata"]["name"]: agent
            for agent in ovn_bgp_agents
        }

        self.assertCountEqual(
            [
                "testbgp1.node3",
                "testbgp2.node3",
            ],
            ovn_bgp_agents_map,
        )

    async def test_creates_nb(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ovsdbs = interfaces.ovsdbservice_interface(self.api_client)
        nb, = await ovsdbs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovsdb_nb"
                }
        )

        self.assertEqual(
            nb["spec"]["dbSchema"],
            "northbound"
        )
        self.assertEqual(
            nb["spec"]["replicas"],
            3
        )

    async def test_creates_sb(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ovsdbs = interfaces.ovsdbservice_interface(self.api_client)
        sb, = await ovsdbs.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovsdb_sb"
                }
        )

        self.assertEqual(
            sb["spec"]["dbSchema"],
            "southbound"
        )
        self.assertEqual(
            sb["spec"]["replicas"],
            3
        )
        self.assertTrue('ovnRelay' in sb["spec"])

    async def test_creates_ovsdb_service_with_inactivity_probe(self):
        crd_spec = self.ctx.parent_spec
        ovn_spec = crd_spec["setup"]["ovn"]
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        ovsdb_interface = interfaces.ovsdbservice_interface(self.api_client)

        nb, = await ovsdb_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovsdb_nb"
                }
        )
        sb, = await ovsdb_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovsdb_sb"
                }
        )

        self.assertEqual(
            nb["spec"]["inactivityProbeMs"],
            ovn_spec["northboundOVSDB"]["inactivityProbeMs"]
        )
        self.assertEqual(
            sb["spec"]["inactivityProbeMs"],
            ovn_spec["southboundOVSDB"]["inactivityProbeMs"]
        )

    async def test_creates_ovsdb_service_with_scrape_intervals(self):
        crd_spec = self.ctx.parent_spec
        ovn_spec = crd_spec["setup"]["ovn"]
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        ovsdb_interface = interfaces.ovsdbservice_interface(self.api_client)

        nb, = await ovsdb_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovsdb_nb"
                }
        )
        sb, = await ovsdb_interface.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovsdb_sb"
                }
        )

        self.assertEqual(
            nb["spec"]["scrapeIntervalMs"],
            ovn_spec["northboundOVSDB"]["scrapeIntervalMs"]
        )
        self.assertEqual(
            sb["spec"]["scrapeIntervalMs"],
            ovn_spec["southboundOVSDB"]["scrapeIntervalMs"]
        )

    async def test_creates_ovs_vswitchd_certificate(self):
        self._make_all_deployments_ready_immediately()

        await self.cr.sm.ensure(self.ctx)
        certificates = sm.certificates_interface(self.api_client)
        secrets = sm.secret_interface(self.api_client)

        cert, = await certificates.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT: "ovn_monitoring_certificate",
            }
        )
        cert_secret, = await secrets.list_(
            NAMESPACE,
            label_selector={
                sm.context.LABEL_COMPONENT:
                    "ovn_monitoring_certificate_secret",
            }
        )

        self.assertEqual(
            cert["spec"]["issuerRef"]["name"],
            "issuername",
        )
        self.assertEqual(
            cert["spec"]["secretName"],
            cert_secret.metadata.name,
        )

    async def test_ovn_monitoring_daemonset_exposes_ports_exporter(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        daemonsets = interfaces.daemon_set_interface(self.api_client)

        ovn_monitoring_ds, = await daemonsets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovn_monitoring_daemonset"}
        )

        container_specs = ovn_monitoring_ds.spec.template.spec.containers

        ovs_monitoring, = [x for x in container_specs
                           if x.name == "ovs-vswitchd-monitoring"]

        ssl_terminator, = [x for x in container_specs
                           if x.name == "ssl-terminator"]

        port_map_ovs_vswitchd_monitoring = {
            port.name: (port.container_port, port.protocol)
            for port in ovs_monitoring.ports
        }

        port_map_ssl_terminator = {
            port.name: (port.container_port, port.protocol)
            for port in ssl_terminator.ports
        }

        self.assertDictEqual(
            {
                "metrics": (9999, "TCP"),
            },
            port_map_ovs_vswitchd_monitoring,
        )

        self.assertDictEqual(
            {
                "metrics": (8008, "TCP"),
                "ovs-metrics": (8007, "TCP"),
            },
            port_map_ssl_terminator,
        )

    async def test_creates_monitoring_daemonset_containers_with_resources(self): # noqa E501
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        daemonsets = interfaces.daemon_set_interface(self.api_client)

        ds, = await daemonsets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovn_monitoring_daemonset"}
        )

        self.assertEqual(
            testutils.container_resources(ds, 0),
            testutils.unique_resources(
                "setup.ovn.controller.ovs-vswitchd-monitoring"),
        )
        self.assertEqual(
            testutils.container_resources(ds, 1),
            testutils.unique_resources("setup.ovn.controller.ssl-terminator"),
        )
        self.assertEqual(
            testutils.container_resources(ds, 2),
            testutils.unique_resources("setup.ovn.controller.service-reload"),
        )

    async def test_monitoring_pod_monitor_labelled_with_additional_label(self): # noqa E501
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        podmonitors = sm.podmonitor_interface(self.api_client)
        pms, = await podmonitors.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovn_monitoring_pod_monitor"}
        )

        self.assertEqual("mylabel", pms["metadata"]["labels"]["mykey"])
        self.assertEqual("prometheus",
                         pms["spec"]["podMetricsEndpoints"][0]["port"])
        self.assertEqual("ovs-vswitchd-metrics",
                         pms["spec"]["podMetricsEndpoints"][1]["port"])

    async def test_creates_monitoring_daemonset_with_the_configured_scrape_interval(self): # noqa E501
        crd_spec = self.ctx.parent_spec
        ovn_spec = crd_spec["setup"]["ovn"]
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        daemonsets = interfaces.daemon_set_interface(self.api_client)

        ds, = await daemonsets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovn_monitoring_daemonset"}
        )

        self.assertEqual(
            ds.spec.template.spec.containers[0].name,
            "ovs-vswitchd-monitoring"
        )

        for env_spec in ds.spec.template.spec.containers[0].env:
            if env_spec.name == "SCRAPE_INTERVAL":
                self.assertEqual(
                    env_spec.value,
                    str(ovn_spec["controller"]["scrapeIntervalMs"])
                )
                break
        else:
            self.fail("The SCRAPE_INVERVAL is not specified in the template "
                      "for the ovs-vswitchd-monitoring container")

    async def test_creates_monitoring_daemonset_with_the_configured_update_strategy(self): # noqa E501
        crd_spec = self.ctx.parent_spec
        ovn_spec = crd_spec["setup"]["ovn"]
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        daemonsets = interfaces.daemon_set_interface(self.api_client)

        ds, = await daemonsets.list_(
            NAMESPACE,
            label_selector={
                context.LABEL_COMPONENT: "ovn_monitoring_daemonset"}
        )

        self.assertDictEqual(
            self.api_client.sanitize_for_serialization(
                ds.spec.update_strategy),
            ovn_spec["controller"]["monitoringDsUpdateStrategy"],
        )


class TestNeutronOVNDeploymentWithInternalIngress(
        TestNeutronDeploymentCases.TestNeutronBaseDeployment):
    run_service_reload_test = False  # otherwise we cry for northd

    async def asyncSetUp(self):
        await super().asyncSetUp()

        self.labels_gtw = {
            scheduling_keys.SchedulingKey.NETWORK_NEUTRON_NETWORK_NODE.value:
                str(uuid.uuid4()),
        }
        self.labels_cmp = {
            scheduling_keys.SchedulingKey.COMPUTE_HYPERVISOR.value:
                str(uuid.uuid4()),
        }
        self.default_node_setup = {
            "node1": {},
            "node2": self.labels_gtw,
            "node3": self.labels_cmp,
        }
        self.ovn_nodes = ["node2", "node3"]
        self.gtw_nodes = ["node2"]
        self.cmp_nodes = ["node3"]

        self._configure_cr(
            neutron.Neutron,
            self._get_neutron_deployment_yaml(),
        )

    def _get_neutron_deployment_yaml(self):
        deployment = super()._get_neutron_deployment_yaml()
        deployment["spec"]["api"]["internal"] = {
            "ingress": {
                "ingressClassName": "nginx",
                "fqdn": "internal-neutron-ingress",
                "port": 8081,
            }
        }
        deployment["spec"]["api"]["resources"] = \
            testutils.generate_resources_dict(
                "api.neutron-api",
                "api.ssl-terminator",
                "api.ssl-terminator-external",
                "api.ssl-terminator-internal",
                "api.service-reload",
                "api.service-reload-external",
                "api.service-reload-internal",
            )
        deployment["spec"]["setup"] = {
            "ovn": {
                "controller": {
                    "configTemplates": [
                        {
                            "nodeSelectors": [
                                {"matchLabels": {}},  # all nodes
                            ],
                            "neutronConfig": {},
                        },
                        {
                            "nodeSelectors": [
                                {"matchLabels": self.labels_gtw},
                            ],
                            "bridgeConfig": [
                                {
                                    "bridgeName": "br-ex",
                                    "uplinkDevice": "eth1",
                                    "openstackPhysicalNetwork": "physnet1",
                                    },
                            ],
                        },
                    ],
                },
                "northboundOVSDB": {
                    "replicas": 3,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "backup": {
                        "schedule": "0 * * * *",
                    },
                    "storageSize": "6Gi",
                    "storageClassName": "baz-class",
                    "resources": testutils.generate_resources_dict(
                        "setup.ovn.northboundOVSDB.ovsdb",
                        "setup.ovn.northboundOVSDB.setup-ovsdb",
                    ),
                },
                "southboundOVSDB": {
                    "replicas": 3,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "backup": {
                        "schedule": "0 * * * *",
                    },
                    "storageSize": "6Gi",
                    "storageClassName": "baz-class",
                    "resources": testutils.generate_resources_dict(
                        "setup.ovn.southboundOVSDB.ovsdb",
                        "setup.ovn.southboundOVSDB.setup-ovsdb",
                    ),
                },
                "northd": {
                    "replicas": 4,
                    "scheduleRuleWhenUnsatisfiable": "ScheduleAnyway",
                    "resources": testutils.generate_resources_dict(
                        "setup.ovn.northd",
                    ),
                },
            }
        }

        return deployment

    async def test_creates_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "neutron-api")

        self.assertIsNotNone(service)

    async def test_creates_endpoint_with_ingress(self):
        await self.cr.sm.ensure(self.ctx)

        endpoints = interfaces.keystoneendpoint_interface(self.api_client)
        endpoint = await endpoints.read(NAMESPACE, "neutron-api-endpoint")

        self.assertEqual(
            endpoint["spec"]["endpoints"]["internal"],
            "https://internal-neutron-ingress:8081",
        )

    async def test_creates_ingress(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "neutron-internal")

        self.assertEqual(
            ingress.spec.rules[0].host,
            "internal-neutron-ingress",
        )

    async def test_ingress_matches_service(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)

        ingresses = interfaces.ingress_interface(self.api_client)
        ingress = await ingresses.read(NAMESPACE, "neutron")
        internal_ingress = await ingresses.read(NAMESPACE, "neutron-internal")

        services = interfaces.service_interface(self.api_client)
        service = await services.read(NAMESPACE, "neutron-api")

        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            ingress.spec.rules[0].http.paths[0].backend.service.port.number,
            ([x.port for x in service.spec.ports if x.name == "external"][0]),
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].backend.service.name,
            service.metadata.name,
        )
        self.assertEqual(
            internal_ingress.spec.rules[0].http.paths[0].
            backend.service.port.number,
            ([x.port for x in service.spec.ports
                if x.name == "internal-ingress"][0]),
        )

    async def test_creates_containers_with_resources(self):
        self._make_all_dependencies_complete_immediately()
        await self.cr.sm.ensure(self.ctx)
        deployments = interfaces.deployment_interface(self.api_client)
        jobs = interfaces.job_interface(self.api_client)

        api_deployment, = await deployments.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "api_deployment"}
        )
        db_sync_job, = await jobs.list_(
            NAMESPACE,
            label_selector={context.LABEL_COMPONENT: "db_sync"}
        )

        self.assertEqual(
            testutils.container_resources(api_deployment, 0),
            testutils.unique_resources("api.neutron-api")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 1),
            testutils.unique_resources("api.ssl-terminator")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 2),
            testutils.unique_resources("api.ssl-terminator-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 3),
            testutils.unique_resources("api.ssl-terminator-internal")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 4),
            testutils.unique_resources("api.service-reload")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 5),
            testutils.unique_resources("api.service-reload-external")
        )
        self.assertEqual(
            testutils.container_resources(api_deployment, 6),
            testutils.unique_resources("api.service-reload-internal")
        )
        self.assertEqual(
            testutils.container_resources(db_sync_job, 0),
            testutils.unique_resources("job.neutron-db-sync-job")
        )
